object registerstaffmember: Tregisterstaffmember
  Left = 426
  Top = 78
  Caption = 'Register Staff Member'
  ClientHeight = 408
  ClientWidth = 637
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  WindowState = wsMinimized
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object bvl2: TBevel
    Left = 0
    Top = 350
    Width = 637
    Height = 1
    Align = alTop
    Shape = bsTopLine
    ExplicitWidth = 629
  end
  object btnNext: TcxButton
    Left = 331
    Top = 360
    Width = 80
    Height = 32
    Caption = '&Next'
    Glyph.Data = {
      36090000424D3609000000000000360000002800000018000000180000000100
      2000000000000009000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007C5F20B818130623000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000A98531FFA37E2CFF503E16780000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000A98535FDA68030FEA88130FF8A6C28D2110D0517000000000000
      0000000000000000000000000000000000000000000000000000000000004636
      155DA47F32EBA17C31E6A17C31E6A17C31E6A17C31E6A17C31E6A17C31E6A17C
      31E69B792FE4AA8434FEAB8535FEAB8535FDAC8535FFAB8535FF493A16680000
      000000000000000000000000000000000000000000000000000000000000785D
      26A3B08A37FFB08A37FFB08A37FFB08A37FFB08A37FFB08A37FFB08A37FFB08A
      37FFB08A37FFB08A37FFAF8937FDB08A37FFAF8937FDB08A37FFB08A37FF896B
      2CC50A08030D000000000000000000000000000000000000000000000000785E
      28A0B48C3CFFB48C3CFFB48C3CFFB38B3CFDB48C3CFFB48C3CFFB38B3CFDB48C
      3CFFB48C3CFFB48C3CFFB38B3CFDB48C3CFFB38B3CFDB48C3CFFB48C3CFFB48C
      3BFFB38C3BFF4233165800000000000000000000000000000000000000007B61
      2BA0B89140FFB89140FFB89140FFB7903FFDB89140FFB89140FFB7903FFDB891
      40FFB89140FFB89140FFB7903FFDB89140FFB7903FFDB89140FFB89140FFB790
      3FFDB89140FFB7913FFF84682EB6040301050000000000000000000000007D63
      2DA0BC9344FFBB9243FDBB9243FDBB9244FEBB9243FDBB9243FDBB9244FEBB92
      43FDBB9243FDBB9243FDBB9244FEBB9243FDBB9244FEBB9243FDBB9243FDBB92
      44FEBB9243FDBB9243FDBC9344FFB68E40FA352A144100000000000000008065
      2FA0BF9848FFBF9848FFBF9848FFBE9747FDBF9848FFBF9848FFBE9747FDBF98
      48FFBF9848FFBF9848FFBE9747FDBF9848FFBE9747FDBF9848FFBF9848FFBE97
      47FDBF9848FFBF9746FFBC9342FFCEAB61FF5043275B00000000000000008367
      32A0C59A4AFFC39949FDC39949FDC4994AFEC39949FDC39949FDC4994AFEC399
      49FDC39949FDC39949FDC4994AFEC39949FDC4994AFEC39949FDC39949FDC499
      4AFEC29748FEC79E4FFFBA9E61DA1A160D1D0000000000000000000000008669
      34A0CA9F4FFFCA9F4FFFCA9F4FFFC89E4EFDCA9F4FFFCA9F4FFFC89E4EFDCA9F
      4FFFCA9F4FFFCA9F4FFFC89E4EFDCA9F4FFFC89E4EFDCA9F4FFFC99E4EFFC79B
      4AFFD3AF65FF74633D8400000000000000000000000000000000000000008D72
      3DA2CCA04FFFCCA050FFCCA050FFCCA050FFCCA050FFCCA050FFCCA050FFCCA0
      50FFCCA050FFCDA252FFCCA252FDCEA353FFCCA252FDCCA150FFCEA455FFC9AA
      6AE72721142B0000000000000000000000000000000000000000000000004539
      2148B79C62C7B29760C3B29760C3B29760C3B29760C3B29760C3B29760C3B298
      60C3AC935CC0CFA557FDD0A456FECFA455FDD0A252FFD9B469FF877248980000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D5A95AF9D3A757FED6AA5CFFD7B773F3382E1C3D000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DEB05EFBDFB96EFF9B8453AC0202010200000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DAB975E44C402652000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000}
    LookAndFeel.Kind = lfUltraFlat
    TabOrder = 0
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Segoe UI'
    Font.Style = [fsBold]
    ParentFont = False
    OnClick = btnNextClick
  end
  object btnCancel: TcxButton
    Left = 433
    Top = 360
    Width = 80
    Height = 32
    Cancel = True
    Caption = 'Cancel'
    Glyph.Data = {
      36050000424D3605000000000000360400002800000010000000100000000100
      08000000000000010000120B0000120B0000000100000000000000000000FFFF
      FF00FF00FF000066FF00217AFF00BAD6FF000063FD000061FC000062FC00005E
      FA00005EF9000A64F9001D70FA001D71FA003785FD007EAFFD008BB8FE0091BC
      FF00A0C5FE00005CF900005BF8000059F7000058F6000057F500095DF60071A5
      FC000055F4000053F3000052F2000051F1000357F4000757F200095BF400004D
      EF00004CEE00003FCD00003FCB00003FC9000046EA000042E800003FD600003F
      D400003FD300003FD100124AD8001B4FCF001E52CF000040E600003CE400003A
      E2000037E0000035DF000239E1000034DE000031DD000026A9000026A6005F78
      DA0000199000697EDC007385DE00001387000A1F9C003E4BC500444FC8001C24
      B0002630B6002F36C1000000B0000000AE000000AD000000AA000000A6000000
      A20000009D0000009C0000009800000097000000920000008D00000087000000
      820000007D00000077000000730000006F0000006C0000006B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000020202025702
      02020202025602020202020202553D5502020202553A55020202020254380325
      54020254250338540202025337143314245353241D3614375302523E04343532
      1323231B353534043E5202512D1E3131300A1A313131202E51020202502B172F
      2F272F2F2F172B5002020202024F2A1526262626152A4F0202020202024E2913
      2222222107294E02020202024D28091C1C1F1F1C1B08284C0202024B2C071616
      181919181615062C4A024942110C0A0B0F39390F0B0A0D11414902483F120E10
      3B48483B100E123F480202024740053C470202473C0540470202020202454345
      0202020245434602020202020202440202020202024402020202}
    LookAndFeel.Kind = lfUltraFlat
    ModalResult = 2
    TabOrder = 1
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object btnBack: TcxButton
    Left = 245
    Top = 360
    Width = 80
    Height = 32
    Caption = '&Back'
    Enabled = False
    Glyph.Data = {
      36090000424D3609000000000000360000002800000018000000180000000100
      2000000000000009000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000161106207D6020BC000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004C3B1572A37E2CFFA98431FF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000F0B0414886A
      27CDA88130FFA78130FFAC8736FF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000046371562AC8535FFAC85
      35FFAB8535FDAB8535FDAA8434FE9A782EE4A17C31E6A17C31E6A17C31E6A17C
      31E6A17C31E6A17C31E6A17C31E6A47F32EB4A3A176500000000000000000000
      00000000000000000000000000000806020A85682BBFB08A37FFAF8937FEB08A
      37FFB08A37FFB08A37FFAF8937FDB08A37FFB08A37FFB08A37FFB08A37FFB08A
      37FFB08A37FFB08A37FFB08A37FFB08A37FF7B5F27A900000000000000000000
      000000000000000000003E311552B18A3AFBB48C3BFFB48C3CFFB38B3CFDB48C
      3CFFB48C3CFFB48C3CFFB38B3CFDB48C3CFFB38B3CFDB48C3CFFB48C3CFFB38B
      3CFDB48C3CFFB48C3CFFB38B3CFDB48C3CFF7C6029A600000000000000000000
      00000202010382652CB0B7913FFFB79040FEB89140FFB89140FFB7903FFDB891
      40FFB89140FFB89140FFB7903FFDB89140FFB7903FFDB89140FFB89140FFB790
      3FFDB89140FFB89140FFB7903FFDB89140FF7E632CA600000000000000003127
      133BB38C3FF6BB9344FFBB9243FDBB9244FEBB9243FDBB9243FDBB9244FEBB92
      43FDBB9243FDBB9243FDBB9244FEBB9243FDBB9244FEBB9243FDBB9243FDBB92
      44FEBB9243FDBB9243FDBB9244FEBC9344FF81652EA600000000000000004B3F
      2455CFAD62FFBC9342FFBF9746FFBE9747FDBF9848FFBF9848FFBE9747FDBF98
      48FFBF9848FFBF9848FFBE9747FDBF9848FFBE9747FDBF9848FFBF9848FFBE97
      47FDBF9848FFBF9848FFBE9747FDBF9848FF836830A600000000000000000000
      000016120B18B79C60D5C79E4FFFC29748FEC39949FDC39949FDC4994AFEC399
      49FDC39949FDC39949FDC4994AFEC39949FDC4994AFEC39949FDC39949FDC499
      4AFEC39949FDC39949FDC4994AFEC59A4AFF876A33A600000000000000000000
      000000000000000000006F5F3A7DD5B067FFC79A49FFC99E4DFFC89E4EFDCA9F
      4FFFCA9F4FFFCA9F4FFFC89E4EFDCA9F4FFFC89E4EFDCA9F4FFFCA9F4FFFC89E
      4EFDCA9F4FFFCA9F4FFFC89E4EFDCA9F4FFF896D35A600000000000000000000
      0000000000000000000000000000241E1227C6A969E4CFA557FFCAA04FFDCEA3
      53FFCEA353FFCEA353FFCCA152FECCA050FFCCA050FFCCA050FFCCA050FFCCA0
      50FFCCA050FFCCA050FFCCA050FFCCA04FFF92753FA900000000000000000000
      00000000000000000000000000000000000000000000836F4592DBB56BFFD0A2
      52FFCFA455FDD0A456FED0A556FEAA915BC0B29860C3B29760C3B29760C3B297
      60C3B29760C3B29760C3B29760C3B79C61C7493D244E00000000000000000000
      0000000000000000000000000000000000000000000000000000332A1937D6B5
      72F0D6AC5DFFD4A857FFD6AB5EFC000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000988151A8E0BA6FFFDEB060FE000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000473B244CDCB976E9000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000}
    LookAndFeel.Kind = lfUltraFlat
    TabOrder = 2
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    OnClick = btnBackClick
  end
  object btnHelp: TcxButton
    Left = 581
    Top = 360
    Width = 39
    Height = 32
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000120B0000120B00000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFCFCFCF808080AFAFAFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF10101000
      0000000000CFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFF101010000000000000CFCFCFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFCF80
      8080AFAFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFF808080000000808080FFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF40404000
      0000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFF5050500000000000008F8F8FFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFAFAF00
      0000000000000000707070FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F9F9F000000000000000000707070FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F9F9F808080CFCFCFFFFFFFFF
      FFFF9F9F9F000000000000000000CFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      CFCFCF000000000000505050FFFFFFFFFFFFFFFFFF6060600000000000008080
      80FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFCF000000000000000000CFCFCFFF
      FFFFFFFFFF505050000000000000808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFF404040000000000000101010707070404040000000000000000000DFDF
      DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDF30303000000000000000
      00000000000000000000009F9F9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFF9F9F9F505050404040404040707070CFCFCFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    LookAndFeel.Kind = lfUltraFlat
    TabOrder = 3
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object pcAddNewStaff: TcxPageControl
    Left = 0
    Top = 0
    Width = 637
    Height = 350
    ActivePage = tabPersonalInformation
    Align = alTop
    HideTabs = True
    TabOrder = 4
    ExplicitWidth = 629
    ClientRectBottom = 346
    ClientRectLeft = 4
    ClientRectRight = 633
    ClientRectTop = 4
    object tabWelcome: TcxTabSheet
      Caption = 'Welcome'
      ImageIndex = 0
      object img1: TImage
        Left = 0
        Top = 0
        Width = 145
        Height = 342
        Align = alLeft
        ExplicitHeight = 343
      end
      object label1lbl1: TLabel
        Left = 162
        Top = 42
        Width = 230
        Height = 25
        Caption = 'Register New Staff Member'
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -19
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object label1lbl2: TLabel
        Left = 206
        Top = 103
        Width = 358
        Height = 34
        Caption = 
          'This Wizard shall guide you through the process of adding a new ' +
          'member of staff to the PSM database'
        Font.Charset = ANSI_CHARSET
        Font.Color = clGreen
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object label1lbl3: TLabel
        Left = 331
        Top = 326
        Width = 153
        Height = 13
        Caption = 'Please select &Next to continue'
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
    object tabPersonalInformation: TcxTabSheet
      Caption = 'Personal Information'
      ImageIndex = 1
      ExplicitLeft = 2
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object label1Surname: TLabel
        Left = 77
        Top = 100
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Surname'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object label1OtherNames: TLabel
        Left = 49
        Top = 128
        Width = 73
        Height = 13
        Alignment = taRightJustify
        Caption = 'Other Name(s)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object label1DOB: TLabel
        Left = 56
        Top = 182
        Width = 66
        Height = 13
        Alignment = taRightJustify
        Caption = 'Date of Birth'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object label1Sex: TLabel
        Left = 246
        Top = 155
        Width = 59
        Height = 13
        Alignment = taRightJustify
        Caption = 'Gender/Sex'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object label1Citizenship: TLabel
        Left = 65
        Top = 210
        Width = 57
        Height = 13
        Alignment = taRightJustify
        Caption = 'Citizenship'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object label1DateJoined: TLabel
        Left = 65
        Top = 324
        Width = 57
        Height = 13
        Alignment = taRightJustify
        Caption = 'Date Joined'
        Transparent = True
      end
      object bvl3: TBevel
        Left = 47
        Top = 259
        Width = 570
        Height = 3
        Shape = bsBottomLine
      end
      object label1lbl4: TLabel
        Left = 3
        Top = 4
        Width = 177
        Height = 25
        Caption = 'Personal Information'
        Font.Charset = ANSI_CHARSET
        Font.Color = clMaroon
        Font.Height = -19
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object label1lbl5: TLabel
        Left = 29
        Top = 35
        Width = 435
        Height = 17
        Caption = 
          'Please provide the bio-data and other general information requir' +
          'ed below'
        Font.Charset = ANSI_CHARSET
        Font.Color = clGreen
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object label1lbl7: TLabel
        Left = 233
        Top = 211
        Width = 69
        Height = 13
        Alignment = taRightJustify
        Caption = 'Home District'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object bvl4: TBevel
        Left = 408
        Top = 67
        Width = 5
        Height = 190
        Shape = bsLeftLine
      end
      object Label1: TLabel
        Left = 44
        Top = 73
        Width = 78
        Height = 13
        Alignment = taRightJustify
        Caption = 'Title/Salutation'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label5: TLabel
        Left = 88
        Top = 155
        Width = 34
        Height = 13
        Alignment = taRightJustify
        Caption = 'Initials'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object lIsMemberOf: TLabel
        Left = 292
        Top = 269
        Width = 66
        Height = 13
        Caption = 'Is Member of '
        Transparent = True
      end
      object lDesignation: TLabel
        Left = 33
        Top = 269
        Width = 89
        Height = 13
        Alignment = taRightJustify
        Caption = 'Designation\Post'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label21: TLabel
        Left = 470
        Top = 269
        Width = 11
        Height = 13
        Caption = 'or'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
      end
      object lMaritalStatus: TLabel
        Left = 234
        Top = 182
        Width = 71
        Height = 13
        Alignment = taRightJustify
        Caption = 'Marital Status'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object lEmploymentType: TLabel
        Left = 398
        Top = 296
        Width = 89
        Height = 13
        Alignment = taRightJustify
        Caption = 'Employment Type'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object lContractEnd: TLabel
        Left = 437
        Top = 324
        Width = 75
        Height = 13
        Alignment = taRightJustify
        Caption = 'Contract Expiry'
        Transparent = True
        Visible = False
      end
      object lDepartment: TLabel
        Left = 61
        Top = 296
        Width = 61
        Height = 13
        Alignment = taRightJustify
        Caption = 'Department'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object lContractStart: TLabel
        Left = 245
        Top = 324
        Width = 69
        Height = 13
        Alignment = taRightJustify
        Caption = 'Contract Start'
        Transparent = True
        Visible = False
      end
      object lblReligion: TLabel
        Left = 85
        Top = 238
        Width = 37
        Height = 13
        Alignment = taRightJustify
        Caption = 'Religion'
        Transparent = True
      end
      object tEditSurname: TcxTextEdit
        Left = 127
        Top = 97
        ParentFont = False
        Properties.MaxLength = 25
        Style.BorderStyle = ebsUltraFlat
        Style.Color = 16769759
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 1
        Width = 273
      end
      object tEditOtherNames: TcxTextEdit
        Left = 127
        Top = 125
        ParentFont = False
        Properties.MaxLength = 35
        Style.BorderStyle = ebsUltraFlat
        Style.Color = 16769759
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 2
        Width = 273
      end
      object dEditDOB: TcxDateEdit
        Left = 127
        Top = 179
        EditValue = 0d
        ParentFont = False
        Properties.SaveTime = False
        Properties.ShowTime = False
        Style.Color = 16769759
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 5
        Width = 100
      end
      object comboSex: TcxComboBox
        Left = 309
        Top = 152
        ParentFont = False
        Properties.DropDownListStyle = lsEditFixedList
        Properties.Items.Strings = (
          'Female'
          'Male')
        Style.Color = 16769759
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 4
        Width = 91
      end
      object comboCitizenship: TcxComboBox
        Left = 127
        Top = 207
        ParentFont = False
        Properties.MaxLength = 30
        Style.Color = 15793919
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 7
        Width = 100
      end
      object dEditDateJoined: TcxDateEdit
        Left = 127
        Top = 320
        Style.Color = 15793919
        TabOrder = 16
        Width = 100
      end
      object pcGraphics: TcxPageControl
        Left = 416
        Top = 72
        Width = 205
        Height = 183
        ActivePage = tabStaffSignature
        TabOrder = 10
        TabPosition = tpBottom
        ClientRectBottom = 159
        ClientRectLeft = 4
        ClientRectRight = 201
        ClientRectTop = 4
        object tabEntryID: TcxTabSheet
          Caption = 'Staff ID Photo'
          ImageIndex = 0
          object lblImportEntryID: TLabel
            Left = 148
            Top = 51
            Width = 40
            Height = 13
            Cursor = crHandPoint
            Alignment = taCenter
            AutoSize = False
            Caption = 'Import'
            Font.Charset = ANSI_CHARSET
            Font.Color = clGreen
            Font.Height = -11
            Font.Name = 'Segoe UI'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object lblClearEntryID: TLabel
            Left = 155
            Top = 81
            Width = 26
            Height = 13
            Cursor = crHandPoint
            Alignment = taCenter
            Caption = 'Clear'
            Font.Charset = ANSI_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Segoe UI'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object lbl6: TLabel
            Left = 148
            Top = 125
            Width = 41
            Height = 15
            Alignment = taCenter
            Caption = '140 x 150'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Arial Narrow'
            Font.Style = [fsBold]
            ParentFont = False
            Transparent = True
          end
          object imgStaffID: TcxImage
            Left = 0
            Top = 0
            TabStop = False
            Align = alLeft
            Properties.Caption = 'Staff ID (140x150) px'
            Properties.GraphicClassName = 'TJPEGImage'
            Properties.PopupMenuLayout.MenuItems = [pmiPaste, pmiLoad, pmiCustom]
            Properties.PopupMenuLayout.CustomMenuItemCaption = 'Why 140 x 150?'
            Properties.Stretch = True
            Style.HotTrack = False
            TabOrder = 0
            Height = 155
            Width = 140
          end
        end
        object tabStaffSignature: TcxTabSheet
          Caption = 'Staff Signature'
          ImageIndex = 1
          object Label2: TLabel
            Left = 11
            Top = 129
            Width = 40
            Height = 13
            Cursor = crHandPoint
            Alignment = taCenter
            AutoSize = False
            Caption = 'Import'
            Font.Charset = ANSI_CHARSET
            Font.Color = clGreen
            Font.Height = -11
            Font.Name = 'Segoe UI'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object Label3: TLabel
            Left = 71
            Top = 129
            Width = 26
            Height = 13
            Cursor = crHandPoint
            Alignment = taCenter
            Caption = 'Clear'
            Font.Charset = ANSI_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Segoe UI'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object Label4: TLabel
            Left = 148
            Top = 128
            Width = 36
            Height = 15
            Alignment = taCenter
            Caption = '180 x 55'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Arial Narrow'
            Font.Style = [fsBold]
            ParentFont = False
            Transparent = True
          end
          object imgStaffSignature: TcxImage
            Left = 7
            Top = 47
            TabStop = False
            Properties.Caption = 'Staff Signature (180x55) px'
            Properties.GraphicClassName = 'TJPEGImage'
            Properties.PopupMenuLayout.MenuItems = [pmiPaste, pmiLoad, pmiCustom]
            Properties.PopupMenuLayout.CustomMenuItemCaption = 'Why 195 x 120?'
            Style.HotTrack = False
            TabOrder = 0
            Height = 55
            Width = 180
          end
        end
      end
      object tEditHomeDistrict: TcxTextEdit
        Left = 305
        Top = 207
        ParentFont = False
        Properties.MaxLength = 50
        Style.BorderStyle = ebsUltraFlat
        Style.Color = 15793919
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 8
        Width = 95
      end
      object comboTitle: TcxComboBox
        Left = 127
        Top = 70
        ParentFont = False
        Properties.Items.Strings = (
          'Dr'
          'Mr'
          'Mrs'
          'Ms'
          'Prof'
          'Ps'
          'Rev')
        Properties.MaxLength = 20
        Style.Color = 15793919
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 0
        Width = 120
      end
      object tEditInitials: TcxTextEdit
        Left = 127
        Top = 152
        ParentFont = False
        Properties.MaxLength = 5
        Style.BorderStyle = ebsUltraFlat
        Style.Color = 15793919
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 3
        Width = 70
      end
      object rbTeachingStaff: TcxRadioButton
        Left = 367
        Top = 268
        Width = 97
        Height = 17
        Caption = 'Teaching Staff'
        TabOrder = 12
        TabStop = True
        Transparent = True
      end
      object rbNonTeachingStaff: TcxRadioButton
        Left = 493
        Top = 268
        Width = 123
        Height = 17
        Caption = 'Non-Teaching Staff?'
        TabOrder = 13
        TabStop = True
        Transparent = True
      end
      object tEditDesignation: TcxTextEdit
        Left = 127
        Top = 266
        ParentFont = False
        Properties.MaxLength = 50
        Style.BorderStyle = ebsUltraFlat
        Style.Color = 16769759
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 11
        Width = 144
      end
      object comboMaritalStatus: TcxComboBox
        Left = 309
        Top = 179
        ParentFont = False
        Properties.Items.Strings = (
          'Single'
          'Married'
          'Widowed'
          'Separated')
        Properties.MaxLength = 16
        Style.Color = 15793919
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 6
        Width = 91
      end
      object comboEmploymentType: TcxComboBox
        Left = 493
        Top = 293
        ParentFont = False
        Properties.Items.Strings = (
          'Permanent'
          'Contract'
          'Casual')
        Properties.MaxLength = 30
        Style.Color = 16769759
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 15
        Width = 125
      end
      object dEditContractEnd: TcxDateEdit
        Left = 517
        Top = 320
        Style.Color = 15793919
        TabOrder = 18
        Visible = False
        Width = 100
      end
      object lookupDepartment: TcxLookupComboBox
        Left = 127
        Top = 293
        Properties.KeyFieldNames = 'SD_ID'
        Properties.ListColumns = <
          item
            Caption = 'Departments'
            FieldName = 'SD_NAME'
          end>
        Properties.ListOptions.ShowHeader = False
        Style.Color = 15793919
        TabOrder = 14
        Width = 235
      end
      object dEditContractStart: TcxDateEdit
        Left = 319
        Top = 320
        Style.Color = 15793919
        TabOrder = 17
        Visible = False
        Width = 100
      end
      object btnNewDepartment: TButton
        Left = 362
        Top = 293
        Width = 22
        Height = 21
        Caption = '...'
        TabOrder = 19
        TabStop = False
        OnClick = btnNewDepartmentClick
      end
      object comboReligion: TcxComboBox
        Left = 127
        Top = 234
        ParentFont = False
        Properties.MaxLength = 0
        Properties.Sorted = True
        Style.Color = 15793919
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 9
        Width = 144
      end
    end
    object tabBackgroundInformation: TcxTabSheet
      Caption = 'Home && Background'
      ImageIndex = 2
      object Label7: TLabel
        Left = 8
        Top = 8
        Width = 226
        Height = 25
        Caption = 'Contact/Home Information'
        Font.Charset = ANSI_CHARSET
        Font.Color = clMaroon
        Font.Height = -19
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label8: TLabel
        Left = 29
        Top = 35
        Width = 410
        Height = 17
        Caption = 
          'Please provide the contact and next-of-kin information required ' +
          'below'
        Font.Charset = ANSI_CHARSET
        Font.Color = clGreen
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object lContactTel: TLabel
        Left = 62
        Top = 75
        Width = 60
        Height = 13
        Alignment = taRightJustify
        Caption = 'Contact Tel.'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label10: TLabel
        Left = 47
        Top = 103
        Width = 75
        Height = 13
        Alignment = taRightJustify
        Caption = 'Alternative Tel.'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object lNoK: TLabel
        Left = 66
        Top = 257
        Width = 56
        Height = 13
        Alignment = taRightJustify
        Caption = 'Next of Kin'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label12: TLabel
        Left = 46
        Top = 285
        Width = 76
        Height = 13
        Alignment = taRightJustify
        Caption = 'Next of Kin Tel.'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label13: TLabel
        Left = 45
        Top = 131
        Width = 76
        Height = 13
        Alignment = taRightJustify
        Caption = 'E-Mail Address'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label14: TLabel
        Left = 43
        Top = 166
        Width = 78
        Height = 26
        Alignment = taRightJustify
        Caption = 'Postal/Physical Address'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Label15: TLabel
        Left = 31
        Top = 313
        Width = 91
        Height = 13
        Alignment = taRightJustify
        Caption = 'Next of Kin E-Mail'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object tEditTelephone: TcxTextEdit
        Left = 127
        Top = 72
        ParentFont = False
        Properties.MaxLength = 25
        Style.BorderStyle = ebsUltraFlat
        Style.Color = 15793919
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 0
        Width = 178
      end
      object tEditAlternativeTelephone: TcxTextEdit
        Left = 127
        Top = 100
        ParentFont = False
        Properties.MaxLength = 25
        Style.BorderStyle = ebsUltraFlat
        Style.Color = 15793919
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 1
        Width = 178
      end
      object tEditNoK: TcxTextEdit
        Left = 127
        Top = 254
        ParentFont = False
        Properties.MaxLength = 50
        Style.BorderStyle = ebsUltraFlat
        Style.Color = 15793919
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 4
        Width = 253
      end
      object tEditNoKTelephone: TcxTextEdit
        Left = 127
        Top = 282
        ParentFont = False
        Properties.MaxLength = 25
        Style.BorderStyle = ebsUltraFlat
        Style.Color = 15793919
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 5
        Width = 178
      end
      object tEditEMail: TcxTextEdit
        Left = 126
        Top = 128
        ParentFont = False
        Properties.MaxLength = 80
        Style.BorderStyle = ebsUltraFlat
        Style.Color = 15793919
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 2
        Width = 253
      end
      object memoAddress: TcxMemo
        Left = 127
        Top = 156
        Properties.ScrollBars = ssVertical
        Style.Color = 15793919
        TabOrder = 3
        Height = 89
        Width = 489
      end
      object tEditNoKEMail: TcxTextEdit
        Left = 127
        Top = 310
        ParentFont = False
        Properties.MaxLength = 80
        Style.BorderStyle = ebsUltraFlat
        Style.Color = 15793919
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 6
        Width = 253
      end
    end
    object tabProfessionalInformation: TcxTabSheet
      Caption = 'tabProfessionalInformation'
      ImageIndex = 6
      object Label16: TLabel
        Left = 8
        Top = 8
        Width = 207
        Height = 25
        Caption = 'Professional Information'
        Font.Charset = ANSI_CHARSET
        Font.Color = clMaroon
        Font.Height = -19
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label17: TLabel
        Left = 29
        Top = 35
        Width = 227
        Height = 17
        Caption = 'Academic && Professional Qualifications'
        Font.Charset = ANSI_CHARSET
        Font.Color = clGreen
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label18: TLabel
        Left = 43
        Top = 62
        Width = 93
        Height = 13
        Caption = 'Institution/School'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label19: TLabel
        Left = 293
        Top = 62
        Width = 26
        Height = 13
        Caption = 'From'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label22: TLabel
        Left = 346
        Top = 62
        Width = 12
        Height = 13
        Caption = 'To'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label23: TLabel
        Left = 400
        Top = 62
        Width = 114
        Height = 13
        Caption = 'Qualification Attained'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label24: TLabel
        Left = 43
        Top = 218
        Width = 152
        Height = 13
        Caption = 'Institution/Firm/Organisation'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label25: TLabel
        Left = 293
        Top = 218
        Width = 26
        Height = 13
        Caption = 'From'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label26: TLabel
        Left = 346
        Top = 218
        Width = 12
        Height = 13
        Caption = 'To'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label27: TLabel
        Left = 400
        Top = 218
        Width = 23
        Height = 13
        Caption = 'Role'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label28: TLabel
        Left = 29
        Top = 195
        Width = 95
        Height = 17
        Caption = 'Prior Experience'
        Font.Charset = ANSI_CHARSET
        Font.Color = clGreen
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object gQualifications: TcxGrid
        Left = 40
        Top = 100
        Width = 577
        Height = 84
        TabOrder = 10
        TabStop = False
        object gtvQualifications: TcxGridTableView
          PopupMenu = pmQualifications
          NavigatorButtons.ConfirmDelete = False
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsData.Deleting = False
          OptionsView.NoDataToDisplayInfoText = 'No Qualification Added Yet'
          OptionsView.CellAutoHeight = True
          OptionsView.ColumnAutoWidth = True
          OptionsView.GroupByBox = False
          object gcSQInstitute: TcxGridColumn
            Caption = 'Institution/School'
            Options.Filtering = False
            Options.Moving = False
            Width = 243
          end
          object gcSQFrom: TcxGridColumn
            Caption = 'From'
            PropertiesClassName = 'TcxSpinEditProperties'
            Options.Filtering = False
            Options.Moving = False
            Width = 55
          end
          object gcSQTo: TcxGridColumn
            Caption = 'To'
            PropertiesClassName = 'TcxSpinEditProperties'
            Options.Filtering = False
            Options.Moving = False
            Width = 54
          end
          object gcSQQualification: TcxGridColumn
            Caption = 'Qualification'
            Options.Filtering = False
            Options.Moving = False
            Width = 172
          end
          object gcQCVerified: TcxGridColumn
            Caption = 'Verified?'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.ValueChecked = 'Y'
            Properties.ValueUnchecked = 'N'
            Options.Filtering = False
            Options.Moving = False
            Width = 51
          end
        end
        object glQualifications: TcxGridLevel
          GridView = gtvQualifications
        end
      end
      object tEditInstitute: TcxTextEdit
        Left = 40
        Top = 77
        ParentFont = False
        Properties.MaxLength = 100
        Style.BorderStyle = ebsUltraFlat
        Style.Color = 16769759
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 0
        Width = 245
      end
      object tEditInstituteQualification: TcxTextEdit
        Left = 397
        Top = 77
        BeepOnEnter = False
        ParentFont = False
        Properties.MaxLength = 100
        Style.BorderStyle = ebsUltraFlat
        Style.Color = clWindow
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 3
        Width = 176
      end
      object gExperience: TcxGrid
        Left = 40
        Top = 256
        Width = 577
        Height = 82
        TabOrder = 11
        TabStop = False
        object gtvExperience: TcxGridTableView
          PopupMenu = pmExperience
          NavigatorButtons.ConfirmDelete = False
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsData.Deleting = False
          OptionsView.NoDataToDisplayInfoText = 'No Prior Experience Added Yet'
          OptionsView.CellAutoHeight = True
          OptionsView.ColumnAutoWidth = True
          OptionsView.GroupByBox = False
          object gcSPEOrganisation: TcxGridColumn
            Caption = 'Institution/Firm/Organisation'
            Options.Filtering = False
            Options.Moving = False
            Width = 244
          end
          object gcSPEFrom: TcxGridColumn
            Caption = 'From'
            PropertiesClassName = 'TcxSpinEditProperties'
            Options.Filtering = False
            Options.Moving = False
            Width = 55
          end
          object gcSPETo: TcxGridColumn
            Caption = 'To'
            PropertiesClassName = 'TcxSpinEditProperties'
            Options.Filtering = False
            Options.Moving = False
            Width = 55
          end
          object gcSPERole: TcxGridColumn
            Caption = 'Role'
            Options.Filtering = False
            Options.Moving = False
            Width = 170
          end
          object gcSPEVerified: TcxGridColumn
            Caption = 'Verified?'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.ValueChecked = 'Y'
            Properties.ValueUnchecked = 'N'
            Options.Filtering = False
            Options.Moving = False
            Width = 51
          end
        end
        object glExperience: TcxGridLevel
          GridView = gtvExperience
        end
      end
      object sEditInstituteFrom: TcxSpinEdit
        Left = 290
        Top = 77
        TabOrder = 1
        Value = 2000
        Width = 50
      end
      object sEditInstituteTo: TcxSpinEdit
        Left = 343
        Top = 77
        TabOrder = 2
        Value = 2000
        Width = 50
      end
      object btnAddQualification: TcxButton
        Left = 577
        Top = 76
        Width = 40
        Height = 21
        Caption = 'Add'
        TabOrder = 4
      end
      object tEditOrganisation: TcxTextEdit
        Left = 40
        Top = 233
        ParentFont = False
        Properties.MaxLength = 100
        Style.BorderStyle = ebsUltraFlat
        Style.Color = 16769759
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 5
        Width = 245
      end
      object tEditOrganisationRole: TcxTextEdit
        Left = 397
        Top = 233
        BeepOnEnter = False
        ParentFont = False
        Properties.MaxLength = 80
        Style.BorderStyle = ebsUltraFlat
        Style.Color = 16769759
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 8
        Width = 176
      end
      object sEditOrganisationFrom: TcxSpinEdit
        Left = 290
        Top = 233
        TabOrder = 6
        Value = 2000
        Width = 50
      end
      object sEditOrganisationTo: TcxSpinEdit
        Left = 343
        Top = 233
        TabOrder = 7
        Value = 2000
        Width = 50
      end
      object btnAddExperience: TcxButton
        Left = 577
        Top = 232
        Width = 40
        Height = 21
        Caption = 'Add'
        TabOrder = 9
      end
    end
    object tabSubjectAllocation: TcxTabSheet
      Caption = 'Classes && Subjects'
      ImageIndex = 3
      object l1: TLabel
        Left = 8
        Top = 8
        Width = 220
        Height = 25
        Caption = 'Subject && Class Allocation'
        Font.Charset = ANSI_CHARSET
        Font.Color = clMaroon
        Font.Height = -19
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object l2: TLabel
        Left = 29
        Top = 35
        Width = 549
        Height = 34
        Caption = 
          'Please specify the subjects and classes taught by the new member' +
          ' of staff. This information is crucial for accurate automatic ti' +
          'me-tabling results'
        Font.Charset = ANSI_CHARSET
        Font.Color = clGreen
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object l3: TLabel
        Left = 43
        Top = 79
        Width = 71
        Height = 13
        Caption = 'Select Subject'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object l4: TLabel
        Left = 43
        Top = 123
        Width = 115
        Height = 13
        Caption = 'Select Class(es) Taught'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object bvl1: TBevel
        Left = 273
        Top = 92
        Width = 3
        Height = 249
        Shape = bsRightLine
      end
      object btnAddClassSubject: TcxButton
        Left = 229
        Top = 317
        Width = 40
        Height = 21
        Caption = 'Add'
        TabOrder = 0
      end
      object gStaffSubjects: TcxGrid
        Left = 280
        Top = 94
        Width = 336
        Height = 244
        TabOrder = 1
        object gtvStaffSubjects: TcxGridTableView
          PopupMenu = pmSubjects
          NavigatorButtons.ConfirmDelete = False
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.NoDataToDisplayInfoText = 'No Subjects/Classes Assigned Yet'
          OptionsView.CellAutoHeight = True
          OptionsView.ColumnAutoWidth = True
          OptionsView.GroupByBox = False
          object gcSubjectID: TcxGridColumn
            Caption = 'Subject ID'
            DataBinding.ValueType = 'Integer'
            Visible = False
            Options.Editing = False
            Options.Filtering = False
            Options.Focusing = False
            Options.Grouping = False
            Options.Moving = False
          end
          object gcClassID: TcxGridColumn
            Caption = 'Class ID'
            DataBinding.ValueType = 'Integer'
            Visible = False
            Options.Editing = False
            Options.Filtering = False
            Options.Focusing = False
            Options.Grouping = False
            Options.Moving = False
          end
          object gcAssignedSubject: TcxGridColumn
            Caption = 'Subject'
            Options.Editing = False
            Options.Filtering = False
            Options.Focusing = False
            Options.CellMerging = True
            Options.Grouping = False
            Options.Moving = False
            Width = 180
          end
          object gcAssignedClass: TcxGridColumn
            Caption = 'Classes Taught'
            Options.Editing = False
            Options.Filtering = False
            Options.Focusing = False
            Options.Grouping = False
            Options.Moving = False
            Width = 154
          end
          object gcClassLevel: TcxGridColumn
            Caption = 'Level'
            Visible = False
            Options.Editing = False
            Options.Filtering = False
            Options.FilteringFilteredItemsList = False
            Options.FilteringMRUItemsList = False
          end
        end
        object glStaffSubjects: TcxGridLevel
          GridView = gtvStaffSubjects
        end
      end
      object gClassesTaught: TcxGrid
        Left = 40
        Top = 139
        Width = 229
        Height = 172
        TabOrder = 2
        object gtvClassesTaught: TcxGridDBTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsSelection.MultiSelect = True
          OptionsView.NoDataToDisplayInfoText = 'No Classes for Selected Subject'
          OptionsView.CellAutoHeight = True
          OptionsView.ColumnAutoWidth = True
          OptionsView.GroupByBox = False
          OptionsView.Header = False
          object gcClassesTaughtDBTableView1CLASS_STREAM: TcxGridDBColumn
            Caption = 'Class'
            DataBinding.FieldName = 'CLASS_STREAM'
            Options.Editing = False
            Options.Focusing = False
          end
        end
        object glClassesTaught: TcxGridLevel
          GridView = gtvClassesTaught
        end
      end
      object lookupSubject: TcxLookupComboBox
        Left = 40
        Top = 94
        Properties.DropDownAutoSize = True
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'UNQ_ID'
        Properties.ListColumns = <
          item
            Caption = 'Subject'
            FieldName = 'SUBJECT_PAPER'
          end
          item
            Caption = 'Code'
            Width = 80
            FieldName = 'SSP_CODE'
          end
          item
            Caption = 'Level'
            Width = 35
            FieldName = 'SS_LEVEL'
          end>
        Style.Color = 16769759
        TabOrder = 3
        Width = 229
      end
    end
    object tabResponsibilities: TcxTabSheet
      Caption = 'Responsibilities'
      ImageIndex = 4
      object Label33: TLabel
        Left = 8
        Top = 8
        Width = 222
        Height = 25
        Caption = 'Additional Responsibilities'
        Font.Charset = ANSI_CHARSET
        Font.Color = clMaroon
        Font.Height = -19
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label34: TLabel
        Left = 29
        Top = 35
        Width = 519
        Height = 17
        Caption = 
          'Please specify any additional responsibilities and/or duties ass' +
          'igned to this staff member'
        Font.Charset = ANSI_CHARSET
        Font.Color = clGreen
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Label35: TLabel
        Left = 43
        Top = 63
        Width = 72
        Height = 13
        Caption = 'Responsibility'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label36: TLabel
        Left = 291
        Top = 63
        Width = 73
        Height = 13
        Caption = 'Date Assumed'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label38: TLabel
        Left = 400
        Top = 63
        Width = 43
        Height = 13
        Caption = 'Remarks'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label37: TLabel
        Left = 42
        Top = 281
        Width = 571
        Height = 39
        Caption = 
          'Such responsibilities may include positions such as Registrar, L' +
          'ibrarian, Tutor e.t.c. '#13#10'For Class/House Teachers, Department He' +
          'ads and Club/Association Patrons, however, please use the respec' +
          'tive view in SchoolMaster Administration to define and update th' +
          'ese roles.'
        Font.Charset = ANSI_CHARSET
        Font.Color = clGray
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object dEditDateAssumed: TcxDateEdit
        Left = 288
        Top = 78
        TabOrder = 1
        Width = 107
      end
      object gResponsibilities: TcxGrid
        Left = 40
        Top = 101
        Width = 577
        Height = 171
        TabOrder = 4
        TabStop = False
        object gtvResponsibilities: TcxGridTableView
          PopupMenu = pmResponsibilities
          NavigatorButtons.ConfirmDelete = False
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsView.NoDataToDisplayInfoText = 'No Additional Responsibilities Added Yet'
          OptionsView.CellAutoHeight = True
          OptionsView.ColumnAutoWidth = True
          OptionsView.GroupByBox = False
          object gcResponsibility: TcxGridColumn
            Caption = 'Responsibility'
            Options.Filtering = False
            Options.Moving = False
            Width = 249
          end
          object gcDateAssumed: TcxGridColumn
            Caption = 'Date Assumed'
            DataBinding.ValueType = 'DateTime'
            PropertiesClassName = 'TcxDateEditProperties'
            Properties.SaveTime = False
            Properties.ShowTime = False
            Options.Filtering = False
            Options.Moving = False
            Width = 106
          end
          object gcResponsibilityRemarks: TcxGridColumn
            Caption = 'Remarks'
            Options.Filtering = False
            Options.Moving = False
            Width = 220
          end
        end
        object glResponsibilities: TcxGridLevel
          GridView = gtvResponsibilities
        end
      end
      object tEditResponsibility: TcxTextEdit
        Left = 40
        Top = 78
        ParentFont = False
        Properties.MaxLength = 100
        Style.BorderStyle = ebsUltraFlat
        Style.Color = 16769759
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 0
        Width = 244
      end
      object tEditResponsibilityRemarks: TcxTextEdit
        Left = 397
        Top = 78
        BeepOnEnter = False
        ParentFont = False
        Properties.MaxLength = 80
        Style.BorderStyle = ebsUltraFlat
        Style.Color = clWindow
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 2
        Width = 176
      end
      object btnAddResponsibility: TcxButton
        Left = 577
        Top = 77
        Width = 40
        Height = 21
        Caption = 'Add'
        TabOrder = 3
      end
    end
    object tabSMAccountFinish: TcxTabSheet
      Caption = 'Finish'
      ImageIndex = 5
      object Label39: TLabel
        Left = 8
        Top = 8
        Width = 230
        Height = 25
        Caption = 'Complete Staff Registration'
        Font.Charset = ANSI_CHARSET
        Font.Color = clMaroon
        Font.Height = -19
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label40: TLabel
        Left = 29
        Top = 35
        Width = 523
        Height = 34
        Caption = 
          'Please specify whether the new staff member shall be a SchoolMas' +
          'ter User and finish the registration'
        Font.Charset = ANSI_CHARSET
        Font.Color = clGreen
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object lUsername: TLabel
        Left = 332
        Top = 87
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Username'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
        Visible = False
      end
      object Label42: TLabel
        Left = 57
        Top = 162
        Width = 184
        Height = 13
        Caption = 'Important Additional Staff Information'
        Transparent = True
      end
      object Label43: TLabel
        Left = 63
        Top = 108
        Width = 543
        Height = 39
        Caption = 
          'Users should use the default SM password to log in for the first' +
          ' time upon which they shall be prompted to change it.'#13#10'Rights &&' +
          ' Privileges are assigned by the PSM Super User'
        Font.Charset = ANSI_CHARSET
        Font.Color = clGray
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object lbl21: TLabel
        Left = 178
        Top = 331
        Width = 271
        Height = 13
        Caption = 'Please select &Finish to complete the staff registration'
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label6: TLabel
        Left = 63
        Top = 267
        Width = 495
        Height = 26
        Caption = 
          'If you are using the PSM Finance Module (Payroll), the Finance p' +
          'ersonnel shall add and manage additional staff details such as T' +
          'IN, NSSF No. e.t.c.'
        Font.Charset = ANSI_CHARSET
        Font.Color = clGray
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object chkSMUser: TcxCheckBox
        Left = 57
        Top = 85
        Caption = 'Is This Member of Staff A PSM User?'
        Properties.Alignment = taRightJustify
        Properties.ValueChecked = 'Y'
        Properties.ValueUnchecked = 'N'
        TabOrder = 0
        Transparent = True
        Width = 257
      end
      object tEditUsername: TcxTextEdit
        Left = 388
        Top = 83
        ParentFont = False
        Properties.MaxLength = 50
        Style.BorderStyle = ebsUltraFlat
        Style.Color = 16769759
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 1
        Visible = False
        Width = 222
      end
      object memoAdditional: TcxMemo
        Left = 58
        Top = 180
        Properties.ScrollBars = ssVertical
        Style.Color = clWindow
        TabOrder = 2
        Height = 83
        Width = 552
      end
    end
  end
  object dlgImportGraphic: TOpenPictureDialog
    Filter = 'JPEG Image File (*.jpg)|*.jpg|JPEG Image File (*.jpeg)|*.jpeg'
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Title = 'Import Graphic File'
    Left = 452
    Top = 16
  end
  object pmResponsibilities: TPopupMenu
    Left = 21
    Top = 354
    object mniRemoveSelectedResponsibility: TMenuItem
      Caption = 'Remove Selected Responsibility'
    end
  end
  object pmQualifications: TPopupMenu
    Left = 58
    Top = 354
    object mniRemoveSelectedQualification1: TMenuItem
      Caption = 'Remove Selected Qualification'
    end
  end
  object pmExperience: TPopupMenu
    Left = 97
    Top = 354
    object mniRemoveSelectedRole1: TMenuItem
      Caption = 'Remove Selected Role'
    end
  end
  object pmSubjects: TPopupMenu
    Left = 135
    Top = 354
    object RemoveAssignedSubjects1: TMenuItem
      Caption = 'Remove Selected Subject/Class'
    end
  end
end
