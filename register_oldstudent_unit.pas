unit register_oldstudent_unit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinFoggy,
  dxSkiniMaginary, dxSkinscxPCPainter, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, Menus, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxTextEdit, cxCheckBox, cxDropDownEdit,
  cxSpinEdit, StdCtrls, cxRadioGroup, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxClasses, cxGridCustomView, cxGrid, cxButtons, cxMemo, ExtDlgs, ExtCtrls,
  cxMaskEdit, cxCalendar, cxPC, jpeg,FMTBcd, SqlExpr, DBClient, DB,Provider,dxGDIPlusClasses;

type
  Toldstudentregistration = class(TForm)
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    cxTabSheet3: TcxTabSheet;
    label1lbl4: TLabel;
    label1lbl5: TLabel;
    label1Surname: TLabel;
    tEditSurname: TcxTextEdit;
    label1OtherNames: TLabel;
    tEditOtherNames: TcxTextEdit;
    label1DOB: TLabel;
    dEditDOB: TcxDateEdit;
    label1Sex: TLabel;
    comboSex: TcxComboBox;
    bvl1: TBevel;
    label1Nationality: TLabel;
    comboNationality: TcxComboBox;
    label1HomeDistrict: TLabel;
    tEditHomeDistrict: TcxTextEdit;
    label1Religion: TLabel;
    comboReligion: TcxComboBox;
    label1l5: TLabel;
    tEditEMail: TcxTextEdit;
    bvl2: TBevel;
    dlgOpenPicImportID: TOpenPictureDialog;
    label1lbl10: TLabel;
    memoHomeAddress: TcxMemo;
    label1lbl9: TLabel;
    label1l14: TLabel;
    comboParentType: TcxComboBox;
    label1l15: TLabel;
    tEditParentName: TcxTextEdit;
    label1lbl23: TLabel;
    tEditParentOccupation: TcxTextEdit;
    label1l16: TLabel;
    tEditParentPhone: TcxTextEdit;
    label1l17: TLabel;
    tEditParentMail: TcxTextEdit;
    label1l11: TLabel;
    tEditParentRemarks: TcxTextEdit;
    btnAddNoK: TcxButton;
    gParentsInformation: TcxGrid;
    gtvParentsInformation: TcxGridTableView;
    gcParent: TcxGridColumn;
    gcParentName: TcxGridColumn;
    gcOccupation: TcxGridColumn;
    gcParentTel: TcxGridColumn;
    gcParentEmail: TcxGridColumn;
    gcPSMO: TcxGridColumn;
    gcParentRemarks: TcxGridColumn;
    glParentsInformation: TcxGridLevel;
    bvl3: TBevel;
    label1l1: TLabel;
    label1DateJoined: TLabel;
    dEditDateJoined: TcxDateEdit;
    label1ClassJoined: TLabel;
    lookupClassJoined: TcxLookupComboBox;
    label1DateLeft: TLabel;
    dEditDateLeft: TcxDateEdit;
    label1Class: TLabel;
    lookupCurrentClass: TcxLookupComboBox;
    label1l3: TLabel;
    comboReasonLeft: TcxComboBox;
    label1lbl19: TLabel;
    tEditAdmissionNumber: TcxTextEdit;
    chkIsBoarder: TcxCheckBox;
    label1Dormitory: TLabel;
    lookupDormitory: TcxLookupComboBox;
    btnBack: TcxButton;
    btnNext: TcxButton;
    btnCancel: TcxButton;
    btnHelp: TcxButton;
    label1lbl8: TLabel;
    label1l6: TLabel;
    label1l7: TLabel;
    tEditFormerSchool: TcxTextEdit;
    label1l8: TLabel;
    comboFromTerm: TcxComboBox;
    sEditFromYear: TcxSpinEdit;
    label1l9: TLabel;
    comboToTerm: TcxComboBox;
    sEditToYear: TcxSpinEdit;
    label1l19: TLabel;
    tEditFormerIndexNo: TcxTextEdit;
    btnAddFormerSchool: TcxButton;
    gFormerSchools: TcxGrid;
    gtvFormerSchools: TcxGridTableView;
    gcFormerSchool: TcxGridColumn;
    gcFromTerm: TcxGridColumn;
    gcFromYear: TcxGridColumn;
    gcToTerm: TcxGridColumn;
    gcToYear: TcxGridColumn;
    gcIndexNo: TcxGridColumn;
    gcQCVerified: TcxGridColumn;
    glFormerSchools: TcxGridLevel;
    label1l2: TLabel;
    rbSummaryResults: TcxRadioButton;
    rbDetailedResults: TcxRadioButton;
    label1lbl22: TLabel;
    comboResultsLevel: TcxComboBox;
    label1Subject: TLabel;
    lookupSubject: TcxLookupComboBox;
    sEditResultSummary: TcxSpinEdit;
    comboGradeSummary: TcxComboBox;
    label1ResultDetailed: TLabel;
    comboResultDetailed: TcxComboBox;
    btnAddResult: TcxButton;
    pmParents: TPopupMenu;
    mniRemoveSelectedResponsibility: TMenuItem;
    pmResults: TPopupMenu;
    mniRemoveResult1: TMenuItem;
    gFormerResults: TcxGrid;
    gtvFormerResultsDetailed: TcxGridTableView;
    gcDetailedLevel: TcxGridColumn;
    gcDetailedSubject: TcxGridColumn;
    gcDetailedResult: TcxGridColumn;
    gcDetailedResultNo: TcxGridColumn;
    gtvFormerResultsSummary: TcxGridTableView;
    gcSummaryLevel: TcxGridColumn;
    gcSummaryResult: TcxGridColumn;
    gcSummaryGrade: TcxGridColumn;
    glFormerResultsDetailed: TcxGridLevel;
    glFormerResultsSummary: TcxGridLevel;
    label1lbl17: TLabel;
    chkDisabled: TcxCheckBox;
    label1l4: TLabel;
    memoAdditional: TcxMemo;
    img1: TImage;
    label1lbl1: TLabel;
    label1lbl2: TLabel;
    label1lbl3: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure btnNextClick(Sender: TObject);
    procedure btnBackClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnAddNoKClick(Sender: TObject);
    procedure lookupCurrentClassMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lookupClassJoinedMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure chkIsBoarderClick(Sender: TObject);
    procedure btnAddFormerSchoolClick(Sender: TObject);
    procedure btnAddResultClick(Sender: TObject);
    function RandomString: string;
  private
     //student_level: String;
     importID: TJpegImage;
     IDAssigned: Boolean;
  public
    currentPage: Integer;
  protected
    sduseregoldstd,sdDJold,sdstreamregoldstd,sdcombinationregoldstd,sddistrictregoldstd :Tsqldataset;
    dspuseregoldstd,dspDJold,dspdistrictregoldstd,dspcombinationregoldstd,dspstreamregoldstd :TDataSetProvider;
    cduseregoldstd,cdDJold,cdstreamregoldstd,cdcombinationregoldstd,cddistrictregoldstd:Tclientdataset;
    dsuseregoldstd,dDJold,dsdistrictregoldstd,dscombinationregoldstd,dsstreamregoldstd: TDataSource;
  end;

var
  oldstudentregistration: Toldstudentregistration;

implementation
uses data_unit, global_unit;

{$R *.dfm}

procedure Toldstudentregistration.btnAddFormerSchoolClick(Sender: TObject);
var
i:Integer;
begin
if Length(Trim(tEditFormerSchool.Text)) = 0 then
  begin
    tEditFormerSchool.SetFocus;
    Exit;
  end;

  if Length(Trim(comboFromTerm.Text)) = 0 then
  begin
    comboFromTerm.SetFocus;
    Exit;
  end;

  if Length(Trim(comboToTerm.Text)) = 0 then
  begin
    comboToTerm.SetFocus;
    Exit;
  end;

  i:=gtvFormerSchools.DataController.RecordCount;
  gtvFormerSchools.DataController.RecordCount:=i + 1;
  gtvFormerSchools.DataController.Values[i,0]:=tEditFormerSchool.Text;
  gtvFormerSchools.DataController.Values[i,1]:=Trim(comboFromTerm.Text);
  gtvFormerSchools.DataController.Values[i,2]:=Trim(sEditFromYear.Text);
  gtvFormerSchools.DataController.Values[i,3]:=Trim(comboToTerm.Text);
  gtvFormerSchools.DataController.Values[i,4]:=Trim(tEditFormerIndexNo.Text);
  gtvFormerSchools.DataController.Values[i,5]:='N';

  tEditFormerSchool.Clear;
  comboFromTerm.Clear;
  sEditFromYear.Clear;
  comboToTerm.Clear;
  tEditFormerIndexNo.Clear;
end;

procedure Toldstudentregistration.btnAddNoKClick(Sender: TObject);
var
i:Integer;
begin
if Length(Trim(comboParentType.Text)) = 0 then
  begin
    comboParentType.SetFocus;
    Exit;
  end;

  if Length(Trim(tEditParentName.Text)) = 0 then
  begin
    tEditParentName.SetFocus;
    Exit;
  end;

  i:=gtvParentsInformation.DataController.RecordCount;
  gtvParentsInformation.DataController.RecordCount:=i + 1;
  gtvParentsInformation.DataController.Values[i,0]:=comboParentType.Text;
  gtvParentsInformation.DataController.Values[i,1]:=Trim(tEditParentName.Text);
  gtvParentsInformation.DataController.Values[i,2]:=Trim(tEditParentOccupation.Text);
  gtvParentsInformation.DataController.Values[i,3]:=Trim(tEditParentPhone.Text);
  gtvParentsInformation.DataController.Values[i,4]:=Trim(tEditParentMail.Text);
  gtvParentsInformation.DataController.Values[i,5]:='N';
  gtvParentsInformation.DataController.Values[i,6]:=Trim(tEditParentRemarks.Text);

  tEditParentName.Clear;
  tEditParentOccupation.Clear;
  tEditParentPhone.Clear;
  tEditParentMail.Clear;
  comboParentType.SetFocus;
end;

procedure Toldstudentregistration.btnAddResultClick(Sender: TObject);
var
  i, result_no: Integer;
  add_result: Boolean;
begin
if Length(Trim(comboResultsLevel.Text)) = 0 then
  begin
    comboResultsLevel.SetFocus;
    Exit;
  end;

  if rbDetailedResults.Checked then
  begin
    if Length(Trim(lookupSubject.Text)) = 0 then
    begin
      MessageDlg('Please enter the subject', mtInformation, [mbOK], 0);
      lookupSubject.SetFocus;
      Exit;
    end;

    if Length(Trim(comboResultDetailed.Text)) = 0 then
    begin
      MessageDlg('Please enter the result obtained', mtInformation, [mbOK], 0);
      comboResultDetailed.SetFocus;
      Exit;
    end;

    //check if subject has already been added
    add_result:=True;

    for i := 0 to gtvFormerResultsDetailed.DataController.RecordCount - 1 do   // Iterate
    begin
      if (gtvFormerResultsDetailed.DataController.Values[i, 0] = Trim(comboResultsLevel.Text)) and (gtvFormerResultsDetailed.DataController.Values[i, 1] = lookupSubject.Text) then
      begin
        add_result:=False;
        Break;
      end;
    end;

    if not add_result then
    begin
      MessageDlg('This subject result ('+comboResultsLevel.Text+' '+lookupSubject.Text+') has already been added for this new student', mtInformation, [mbOK], 0);
      Exit;
    end;

    //get the result no.
    result_no:=0;

    case comboResultsLevel.ItemIndex of
      0..1: begin
           if comboResultDetailed.ItemIndex  < 9 then  result_no:=comboResultDetailed.ItemIndex + 1
           else result_no:=0;
         end;
      2: begin
           if comboResultDetailed.Properties.Items.Count < 9  then
           begin
             case comboResultDetailed.ItemIndex of
               0: result_no:=6;
               1: result_no:=5;
               2: result_no:=4;
               3: result_no:=3;
               4: result_no:=2;
               5: result_no:=1;
               6: result_no:=0;
               7: result_no:=0;
             end;
           end
           else
           begin
             if comboResultDetailed.ItemIndex  < 6 then result_no:= 1
             else result_no:=0;
           end;
         end;
    end;

    i:=gtvFormerResultsDetailed.DataController.RecordCount;
    gtvFormerResultsDetailed.DataController.RecordCount:=i + 1;
    gtvFormerResultsDetailed.DataController.Values[i,0]:=Trim(comboResultsLevel.Text);
    gtvFormerResultsDetailed.DataController.Values[i,1]:=lookupSubject.Text;
    gtvFormerResultsDetailed.DataController.Values[i,2]:=comboResultDetailed.Text;
    gtvFormerResultsDetailed.DataController.Values[i,3]:=result_no;

    lookupSubject.Clear;
    comboResultDetailed.Clear;
    lookupSubject.SetFocus;
  end
  else
  begin
    if sEditResultSummary.Value <= 0 then
    begin
      MessageDlg('Please enter the result scored', mtInformation, [mbOK], 0);
      sEditResultSummary.SetFocus;
      Exit;
    end;

    if Length(Trim(comboGradeSummary.Text)) = 0 then
    begin
      MessageDlg('Please enter the grade obtained', mtInformation, [mbOK], 0);
      comboGradeSummary.SetFocus;
      Exit;
    end;

    //check if result has already been added
    add_result:=True;

    for i := 0 to gtvFormerResultsSummary.DataController.RecordCount - 1 do   // Iterate
    begin
      if (gtvFormerResultsSummary.DataController.Values[i, 0] = Trim(comboResultsLevel.Text)) then
      begin
        add_result:=False;
        Break;
      end;
    end;

    if not add_result then
    begin
      MessageDlg('This summary result ('+comboResultsLevel.Text+') has already been added for this new student', mtInformation, [mbOK], 0);
      Exit;
    end;

    i:=gtvFormerResultsSummary.DataController.RecordCount;
    gtvFormerResultsSummary.DataController.RecordCount:=i + 1;
    gtvFormerResultsSummary.DataController.Values[i,0]:=Trim(comboResultsLevel.Text);
    gtvFormerResultsSummary.DataController.Values[i,1]:=sEditResultSummary.Value;
    gtvFormerResultsSummary.DataController.Values[i,2]:=comboGradeSummary.Text;

    sEditResultSummary.Clear;
    comboGradeSummary.Clear;
    comboResultsLevel.SetFocus;
  end;
end;

procedure Toldstudentregistration.btnBackClick(Sender: TObject);
begin
currentPage:=currentPage-1;
   btnNext.Enabled:=True;
   btnNext.Caption:='&Next';
   cxPageControl1.ActivePageIndex:=currentPage;
   if currentPage < 1 then
   begin
      btnBack.Enabled:=False;
      oldstudentregistration.Caption:='Register Old Student - Welcome!';
   end
   else
   begin
      oldstudentregistration.Caption:='Registering Old Student - Step '+IntToStr(currentPage)+' of 2';
   end;
end;

procedure Toldstudentregistration.btnCancelClick(Sender: TObject);
begin
if MessageDlg('This shall close the wizard without adding the student. Are you sure you want to do this?', mtConfirmation, [mbNo, mbYes],0) = mrYes then
   begin
      oldstudentregistration.ModalResult:=mrCancel;
   end;
end;

procedure Toldstudentregistration.btnNextClick(Sender: TObject);
var
  i, newSG_ID, req_length: Integer;
  importIDStream: TMemoryStream;
begin
if currentPage = 0 then
  begin
    //welcome page
  end;

  if currentPage = 1 then
  begin
    if Length(Trim(tEditSurname.Text)) = 0 then
    begin
      MessageDlg('Please enter the old student''s surname before proceeding!', mtError, [mbOk],0);
      label1Surname.Font.Color:=clRed;
      tEditSurname.SetFocus;
      Exit;
    end
    else
    begin
      label1Surname.Font.Color:=clBlack;
    end;

    if Length(Trim(tEditOtherNames.Text)) = 0 then
    begin
      MessageDlg('Please enter the old student''s other names before proceeding!', mtError, [mbOk],0);
      label1OtherNames.Font.Color:=clRed;
      tEditOtherNames.SetFocus;
      Exit;
    end
    else
    begin
      label1OtherNames.Font.Color:=clBlack;
    end;

    {if Length(Trim(dEditDOB.Text)) = 0 then
    begin
      MessageDlg('Please enter the old student''s date of birth before proceeding!', mtError, [mbOk],0);
      lDOB.Font.Color:=clRed;
      dEditDOB.SetFocus;
      Exit;
    end
    else
    begin
      lDOB.Font.Color:=clBlack;
    end;  }

    if Length(Trim(comboSex.Text)) = 0 then
    begin
      MessageDlg('Please specify the old student''s gender before proceeding!', mtError, [mbOk],0);
      label1Sex.Font.Color:=clRed;
      comboSex.SetFocus;
      Exit;
    end
    else
    begin
      label1Sex.Font.Color:=clBlack;
    end;

    {if Length(Trim(comboNationality.Text)) = 0 then
    begin
      MessageDlg('Please enter the old student''s nationality before proceeding!', mtError, [mbOk],0);
      lNationality.Font.Color:=clRed;
      comboNationality.SetFocus;
      Exit;
    end
    else
    begin
      lNationality.Font.Color:=clBlack;
    end;

    if Length(Trim(tEditHomeDistrict.Text)) = 0 then
    begin
      MessageDlg('Please enter the old student''s home district (or equivalent) before proceeding!', mtError, [mbOk],0);
      lHomeDistrict.Font.Color:=clRed;
      tEditHomeDistrict.SetFocus;
      Exit;
    end
    else
    begin
      lHomeDistrict.Font.Color:=clBlack;
    end; }

    if gtvParentsInformation.DataController.RecordCount > 0 then
    begin
      for i:=0 to gtvParentsInformation.DataController.RecordCount - 1 do
      begin
        if gtvParentsInformation.DataController.Values[i,5] = 'Y' then
        begin
          if (Length(Trim(gtvParentsInformation.DataController.DisplayTexts[i,1])) = 0) or (Length(Trim(gtvParentsInformation.DataController.DisplayTexts[i,3])) = 0) or (Length(Trim(gtvParentsInformation.DataController.DisplayTexts[i,4])) = 0) then
          begin
            if gtvParentsInformation.DataController.DisplayTexts[i,1] <> 'Other' then
            begin
              if MessageDlg('You have subscribed '+tEditOthernames.Text+'''s '+gtvParentsInformation.DataController.DisplayTexts[0,0]+' ['+gtvParentsInformation.DataController.DisplayTexts[0,1]+'] to the PSMO Online service, but you have not entered all of his/her contact details. This may adversely impair the services offered. Do you want to continue?', mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
              begin
                Exit;
              end;
            end
            else
            begin
              if MessageDlg('You have subscribed one of '+tEditOthernames.Text+'''s Next-of-Kin ['+gtvParentsInformation.DataController.DisplayTexts[0,1]+'] to the myChild service, but you have not entered all of his/her contact details. This may adversely impair the services offered. Do you want to continue?', mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
              begin
                Exit;
              end;
            end;
          end;
        end;
      end;
    end
    else
    begin
      {if MessageDlg('It is very important to register each student/pupil''s parents and/or next-of-kin! Do you want to continue without doing this?', mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
      begin
        Exit;
      end; }
    end;

    {if Length(Trim(dEditDateJoined.Text)) = 0 then
    begin
      MessageDlg('Please specify the date the old student joined the school before proceeding!', mtError, [mbOk],0);
      lDateJoined.Font.Color:=clRed;
      dEditDateJoined.SetFocus;
      Exit;
    end
    else
    begin
      lDateJoined.Font.Color:=clBlack;
    end;}

    if Length(Trim(lookupClassJoined.Text)) = 0 then
    begin
      MessageDlg('Please specify the student''s class at the time of joining before proceeding!', mtError, [mbOk],0);
      label1ClassJoined.Font.Color:=clRed;
      lookupClassJoined.SetFocus;
      Exit;
    end
    else
    begin
      label1ClassJoined.Font.Color:=clBlack;
    end;

    {if Length(Trim(dEditDateLeft.Text)) = 0 then
    begin
      MessageDlg('Please specify the date the old student left the school before proceeding!', mtError, [mbOk],0);
      lDateLeft.Font.Color:=clRed;
      dEditDateLeft.SetFocus;
      Exit;
    end
    else
    begin
      lDateLeft.Font.Color:=clBlack;
    end;}

    if Length(Trim(lookupCurrentClass.Text)) = 0 then
    begin
      MessageDlg('Please specify the student''s class at the time of leaving before proceeding!', mtError, [mbOk],0);
      label1Class.Font.Color:=clRed;
      lookupCurrentClass.SetFocus;
      Exit;
    end
    else
    begin
      label1Class.Font.Color:=clBlack;
    end;

    {if (dEditDateJoined.Date <= dEditDOB.Date) or (dEditDateJoined.Date > Date) or (dEditDateJoined.Date > dEditDateLeft.Date) then
    begin
      MessageDlg('Either the student''s date of birth or the date joined is invalid! Please check them before proceeding', mtError, [mbOk],0);
      lDateJoined.Font.Color:=clRed;
      lDOB.Font.Color:=clRed;
      dEditDateJoined.SetFocus;
      Exit;
    end
    else
    begin
      lDateJoined.Font.Color:=clBlack;
      lDOB.Font.Color:=clBlack;
    end; }

    {if (chkIsBoarder.Checked) and (Length(Trim(lookUpDormitory.Text)) = 0) then
    begin
      MessageDlg('Please specify the student''s dormitory or hostel before proceeding!', mtError, [mbOk],0);
      lDormitory.Font.Color:=clRed;
      lookUpDormitory.SetFocus;
      Exit;
    end
    else
    begin
      lDormitory.Font.Color:=clBlack;
    end; }

    btnNext.Caption:='&Finish';

  end;

  if currentPage = 2 then
  begin

    Randomize;

    sdDJold.Active:=False;
    sdDJold.CommandText:='start transaction';
    sdDJold.ExecSQL;

    //*check uniqueness of the admission no.

    sdDJold.Active:=False;
    sdDJold.CommandText:='insert into students_general (SG_SURNAME,SG_OTHERNAME,SG_DOB,SG_SEX,SG_CLASS,SG_IS_BOARDER,SG_DORMITORY,SG_RELIGION,SG_DISTRICT,SG_NATIONALITY,SG_HOME_ADDRESS,SG_JOINED_DATE,'+
                               'SG_CLASS_JOINED,SG_COMBINATION,SG_PIN_CODE,SG_EMAIL,SG_ADMISSION_NUMBER,SG_OTHER_INFO,SG_FORMER_RESULTS_TYPE,SG_CREATED_TIME,SG_CREATED_BY,MODIFIED_TIME,SG_MODIFIED_BY, '+
                               'SG_LEFT,SG_LEFT_DATE,SG_CLASS_LEFT,SG_LEFT_REASON,SG_DISABLED,SG_IS_CURRENT) '+
                               'values (:SG_SURNAME,:SG_OTHERNAME,:SG_DOB,:SG_SEX,:SG_CLASS,:SG_IS_BOARDER,:SG_DORMITORY,:SG_RELIGION,:SG_DISTRICT,:SG_NATIONALITY,:SG_HOME_ADDRESS,:SG_JOINED_DATE,'+
                               ':SG_CLASS_JOINED,:SG_COMBINATION,:SG_PIN_CODE,:SG_EMAIL,:SG_ADMISSION_NUMBER,:SG_OTHER_INFO,:SG_FORMER_RESULTS_TYPE,now(),:SG_CREATED_BY,now(),:SG_MODIFIED_BY, '+
                               ':SG_LEFT,:SG_LEFT_DATE,:SG_CLASS_LEFT,:SG_LEFT_REASON,:SG_DISABLED,:SG_IS_CURRENT) ';

    sdDJold.Params[0].AsString:=Trim(tEditSurname.Text);
    sdDJold.Params[1].AsString:=Trim(tEditOtherNames.Text);
    if Length(dEditDOB.Text) > 0 then
    begin
      sdDJold.Params[2].AsDate:=dEditDOB.Date;
    end
    else
    begin
       sdDJold.Params[2].Clear;
       sdDJold.Params[2].Bound:=True;
       sdDJold.Params[2].DataType:=ftDate;
    end;
    sdDJold.Params[3].AsString:=(comboSex.Text)[1];
    sdDJold.Params[4].AsString:=lookupCurrentClass.Text;
    sdDJold.Params[5].AsString:=chkIsBoarder.EditValue;
    if chkIsBoarder.Checked then sdDJold.Params[6].AsString:=lookUpDormitory.Text
    else sdDJold.Params[6].AsString:='';
    sdDJold.Params[7].AsString:=Trim(comboReligion.Text);
    sdDJold.Params[8].AsString:=Trim(tEditHomeDistrict.Text);
    sdDJold.Params[9].AsString:=Trim(comboNationality.Text);
    sdDJold.Params[10].AsString:=Trim(memoHomeAddress.Text);
    if Length(dEditDateJoined.Text) > 0 then
    begin
      sdDJold.Params[11].AsDate:=dEditDateJoined.Date;
    end
    else
    begin
       sdDJold.Params[11].Clear;
       sdDJold.Params[11].Bound:=True;
       sdDJold.Params[11].DataType:=ftDate;
    end;
    sdDJold.Params[12].AsString:=Trim(lookupClassJoined.Text);
    sdDJold.Params[13].AsString:='';
    sdDJold.Params[14].AsInteger:=Random(8999)+1000;
    sdDJold.Params[15].AsString:=Trim(tEditEMail.Text);
    sdDJold.Params[16].AsString:=Trim(tEditAdmissionNumber.Text);
    sdDJold.Params[17].AsString:=Trim(memoAdditional.Text);

    if rbDetailedResults.Checked then
    begin
      if gtvFormerResultsDetailed.DataController.RecordCount > 0 then sdDJold.Params[18].AsString:='D'
      else sdDJold.Params[18].AsString:='N';
    end
    else if rbSummaryResults.Checked then
    begin
      if gtvFormerResultsSummary.DataController.RecordCount > 0 then sdDJold.Params[18].AsString:='S'
      else sdDJold.Params[18].AsString:='N';
    end
    else
    begin
      sdDJold.Params[18].AsString:='N';
    end;

    sdDJold.Params[19].AsInteger:=global_unit.current_user_id;
    sdDJold.Params[20].AsInteger:=global_unit.current_user_id;
    sdDJold.Params[21].AsString:='Y';
    if Length(dEditDateLeft.Text) > 0 then
    begin
      sdDJold.Params[22].AsDate:=dEditDateLeft.Date;
    end
    else
    begin
       sdDJold.Params[22].Clear;
       sdDJold.Params[22].Bound:=True;
       sdDJold.Params[22].DataType:=ftDate;
    end;
    sdDJold.Params[23].AsString:=lookupCurrentClass.Text;
    sdDJold.Params[24].AsString:=comboReasonLeft.Text;
    sdDJold.Params[25].AsString:=chkDisabled.EditValue;
    sdDJold.Params[26].AsString:='N';

    try
      sdDJold.ExecSQL;

      sdDJold.Active:=False;
      sdDJold.CommandText:='select MAX(SG_ID) from students_general';
      sdDJold.Open;

      newSG_ID:=sdDJold.Fields[0].AsInteger;

      //Parents and NOK
      if gtvParentsInformation.DataController.RecordCount > 0 then
      begin
        for i:=0 to gtvParentsInformation.DataController.RecordCount - 1 do
        begin
          sdDJold.Active:=False;
          sdDJold.CommandText:='insert into students_parents (SP_SG_ID,SP_TYPE,SP_NAME,SP_TEL,SP_EMAIL,SP_OCCUPATION,SP_REMARKS,SP_PSMO_X_CODE,SP_PSMO_SUBSCRIBE,SP_CREATED_TIME,SP_CREATED_BY)'+
                                     'values (:SP_SG_ID,:SP_TYPE,:SP_NAME,:SP_TEL,:SP_EMAIL,:SP_OCCUPATION,:SP_REMARKS,:SP_PSMO_X_CODE,:SP_PSMO_SUBSCRIBE,now(),:SP_CREATED_BY)';

          sdDJold.Params[0].AsInteger:=newSG_ID;
          sdDJold.Params[1].AsString:=Trim(gtvParentsInformation.DataController.DisplayTexts[i,0]);
          sdDJold.Params[2].AsString:=Trim(gtvParentsInformation.DataController.DisplayTexts[i,1]);
          sdDJold.Params[3].AsString:=Trim(gtvParentsInformation.DataController.DisplayTexts[i,3]);
          sdDJold.Params[4].AsString:=Trim(gtvParentsInformation.DataController.DisplayTexts[i,4]);
          sdDJold.Params[5].AsString:=Trim(gtvParentsInformation.DataController.DisplayTexts[i,2]);
          sdDJold.Params[6].AsString:=Trim(gtvParentsInformation.DataController.DisplayTexts[i,6]);
          sdDJold.Params[7].AsString:=Copy(RandomString, 1, 5);
          sdDJold.Params[8].AsString:=gtvParentsInformation.DataController.Values[i,5];
          sdDJold.Params[9].AsInteger:=global_unit.current_user_id;
          sdDJold.ExecSQL;
        end;
      end;

      //Previous Schools
      if gtvFormerSchools.DataController.RecordCount > 0 then
      begin
        for i:=0 to gtvFormerSchools.DataController.RecordCount - 1 do
        begin
          sdDJold.Active:=False;
          sdDJold.CommandText:='insert into students_previous_schools (SPS_SG_ID, SPS_SCHOOL, SPS_ATTENDED_FROM_TERM, SPS_ATTENDED_FROM_YEAR, SPS_ATTENDED_TO_TERM, SPS_ATTENDED_TO_YEAR, SPS_INDEX_NO, SPS_VERIFIED, SPS_POSTED_BY) '+
                                     'values (:SPS_SG_ID, :SPS_SCHOOL, :SPS_ATTENDED_FROM_TERM, :SPS_ATTENDED_FROM_YEAR, :SPS_ATTENDED_TO_TERM, :SPS_ATTENDED_TO_YEAR, :SPS_INDEX_NO, :SPS_VERIFIED, :SPS_POSTED_BY)';

          sdDJold.Params[0].AsInteger:=newSG_ID;
          sdDJold.Params[1].AsString:=Trim(gtvFormerSchools.DataController.DisplayTexts[i,0]);

          if Length(Trim(gtvFormerSchools.DataController.DisplayTexts[i,1])) > 0 then sdDJold.Params[2].AsString:=(Trim(gtvFormerSchools.DataController.DisplayTexts[i,1]))[2]
          else
          begin
            sdDJold.Params[2].Clear;
            sdDJold.Params[2].Bound:=True;
            sdDJold.Params[2].DataType:=ftInteger;
          end;

          if Length(gtvFormerSchools.DataController.DisplayTexts[i,2]) > 0 then sdDJold.Params[3].AsInteger:=gtvFormerSchools.DataController.Values[i,2]
          else
          begin
            sdDJold.Params[3].Clear;
            sdDJold.Params[3].Bound:=True;
            sdDJold.Params[3].DataType:=ftInteger;
          end;

          if Length(Trim(gtvFormerSchools.DataController.DisplayTexts[i,3])) > 0 then sdDJold.Params[4].AsString:=(Trim(gtvFormerSchools.DataController.DisplayTexts[i,3]))[2]
          else
          begin
            sdDJold.Params[4].Clear;
            sdDJold.Params[4].Bound:=True;
            sdDJold.Params[4].DataType:=ftInteger;
          end;

          if Length(gtvFormerSchools.DataController.DisplayTexts[i,4]) > 0 then sdDJold.Params[5].AsInteger:=gtvFormerSchools.DataController.Values[i,4]
          else
          begin
            sdDJold.Params[5].Clear;
            sdDJold.Params[5].Bound:=True;
            sdDJold.Params[5].DataType:=ftInteger;
          end;

          sdDJold.Params[6].AsString:=Trim(gtvFormerSchools.DataController.DisplayTexts[i,5]);
          sdDJold.Params[7].AsString:=Trim(gtvFormerSchools.DataController.Values[i,6]);
          sdDJold.Params[8].AsInteger:=global_unit.current_user_id;

          try
            sdDJold.ExecSQL;
          except
            on e: Exception do
            begin
              MessageDlg('Error encountered during the posting of the new student''s former schools: '+e.Message, mtError, [mbOK], 0);
            end;
          end;

          {sdDJold.Active:=False;
          sdDJold.CommandText:='insert into students_previous_schools (SPS_SG_ID, SPS_SCHOOL, SPS_ATTENDED_FROM_TERM, SPS_ATTENDED_FROM_YEAR, SPS_ATTENDED_TO_TERM, SPS_ATTENDED_TO_YEAR, SPS_INDEX_NO, SPS_VERIFIED, SPS_POSTED_BY) '+
                                     'values (:SPS_SG_ID, :SPS_SCHOOL, :SPS_ATTENDED_FROM_TERM, :SPS_ATTENDED_FROM_YEAR, :SPS_ATTENDED_TO_TERM, :SPS_ATTENDED_TO_YEAR, :SPS_INDEX_NO, :SPS_VERIFIED, :SPS_POSTED_BY)';

          sdDJold.Params[0].AsInteger:=newSG_ID;
          sdDJold.Params[1].AsString:=Trim(gtvFormerSchools.DataController.DisplayTexts[i,0]);
          sdDJold.Params[2].AsString:=(Trim(gtvFormerSchools.DataController.DisplayTexts[i,1]))[2];
          sdDJold.Params[3].AsInteger:=gtvFormerSchools.DataController.Values[i,2];
          sdDJold.Params[4].AsString:=(Trim(gtvFormerSchools.DataController.DisplayTexts[i,3]))[2];
          sdDJold.Params[5].AsInteger:=gtvFormerSchools.DataController.Values[i,4];
          sdDJold.Params[6].AsString:=Trim(gtvFormerSchools.DataController.DisplayTexts[i,5]);
          sdDJold.Params[7].AsString:=gtvFormerSchools.DataController.Values[i,6];
          sdDJold.Params[8].AsInteger:=global_unit.current_user_id;
          sdDJold.ExecSQL; }
        end;
      end;


      //Previous Academic Performance
      if gtvFormerResultsDetailed.DataController.RecordCount > 0 then
      begin
        for i:=0 to gtvFormerResultsDetailed.DataController.RecordCount - 1 do
        begin
          sdDJold.Active:=False;
          sdDJold.CommandText:='insert into students_previous_education_results_details (SPERD_SG_ID, SPERD_WHICH_RESULT, SPERD_SUBJECT, SPERD_GRADE_NO, SPERD_GRADE, SPERD_POSTED_BY) '+
                                    'values (:SPERD_SG_ID, :SPERD_WHICH_RESULT, :SPERD_SUBJECT, :SPERD_GRADE_NO, :SPERD_GRADE, :SPERD_POSTED_BY) ';

          sdDJold.Params[0].AsInteger:=newSG_ID;
          sdDJold.Params[1].AsString:=Trim(gtvFormerResultsDetailed.DataController.Values[i,0]);
          sdDJold.Params[2].AsString:=Trim(gtvFormerResultsDetailed.DataController.Values[i,1]);
          sdDJold.Params[3].AsInteger:=gtvFormerResultsDetailed.DataController.Values[i,3];
          sdDJold.Params[4].AsString:=Trim(gtvFormerResultsDetailed.DataController.Values[i,2]);
          sdDJold.Params[5].AsInteger:=global_unit.current_user_id;
          sdDJold.ExecSQL;
        end;
      end;

      if gtvFormerResultsSummary.DataController.RecordCount > 0 then
      begin
        for i:=0 to gtvFormerResultsSummary.DataController.RecordCount - 1 do
        begin
          sdDJold.Active:=False;
          sdDJold.CommandText:='insert into students_previous_education_results_summary (SPERS_SG_ID, SPERS_WHICH_RESULT, SPERS_RESULT, SPERS_GRADE, SPERS_POSTED_BY) '+
                                    'values (:SPERS_SG_ID, :SPERS_WHICH_RESULT, :SPERS_RESULT, :SPERS_GRADE, :SPERS_POSTED_BY) ';

          sdDJold.Params[0].AsInteger:=newSG_ID;
          sdDJold.Params[1].AsString:=Trim(gtvFormerResultsSummary.DataController.Values[i,0]);
          sdDJold.Params[2].AsInteger:=gtvFormerResultsSummary.DataController.Values[i,1];
          sdDJold.Params[3].AsString:=Trim(gtvFormerResultsSummary.DataController.Values[i,2]);
          sdDJold.Params[4].AsInteger:=global_unit.current_user_id;
          sdDJold.ExecSQL;
        end;
      end;


      //Initial Progress History
      sdDJold.Active:=False;
      sdDJold.CommandText:='insert into students_progress_history (SPH_YEAR, SPH_TERM, SPH_SG_ID, SPH_CLASS_OLD, SPH_CLASS_NEW, SPH_INFORMATION, SPH_POSTED_TIME, SPH_POSTED_BY) '+
                                 'values (:SPH_YEAR, :SPH_TERM, :SPH_SG_ID, :SPH_CLASS_OLD, :SPH_CLASS_NEW, :SPH_INFORMATION, now(), :SPH_POSTED_BY) ';

      sdDJold.Params[0].AsInteger:=global_unit.current_year;
      sdDJold.Params[1].AsInteger:=global_unit.current_term;
      sdDJold.Params[2].AsInteger:=newSG_ID;
      sdDJold.Params[3].AsString:=Trim(lookupClassJoined.Text);
      sdDJold.Params[4].AsString:=Trim(lookupCurrentClass.Text);
      sdDJold.Params[5].AsString:='PSM Registration (Old Student)';
      sdDJold.Params[6].AsInteger:=global_unit.current_user_id;
      sdDJold.ExecSQL;

      //Finalize All except photos

      sdDJold.Active:=False;
      sdDJold.CommandText:='commit';
      sdDJold.ExecSQL;

      //If Photo...
      if IDAssigned = True then
      begin
        dmPSM.sdPostIDS.Active:=False;
        dmPSM.cdsPostIDS.Active:=False;
        dmPSM.sdPostIDS.Params[0].AsInteger:=newSG_ID;
        dmPSM.cdsPostIDS.Open;

        importID:=TJpegImage.Create;
        importIDStream:=TMemoryStream.Create;
        //importID.Assign(imgEntryID.Picture);
        importID.SaveToStream(importIDStream);

        try
          //psmData.sdPostStudentID.Active:=True;
          //psmData.cdsPostID.Locate('SG_ID', newSG_ID,[]);
          dmPSM.cdsPostIDS.Edit;
          dmPSM.bfPostIDSG_PHOTO_1.LoadFromStream(importIDStream);
          dmPSM.cdsPostIDS.Post;

          dmPSM.cdsPostIDS.ApplyUpdates(-1);

        finally
          importIDStream.Clear;
          importIDStream.Free;
          importID.Free;
        end;
      end;

      sdcombinationregoldstd.Active:=False;
      sdcombinationregoldstd.Params[0].AsString:='STAFF';
      sdcombinationregoldstd.Params[1].AsInteger:=global_unit.current_user_id;
      sdcombinationregoldstd.Params[2].AsString:='STUDENTS';
      sdcombinationregoldstd.Params[3].AsString:='Registered Old Student: '+tEditSurname.Text+' '+tEditOtherNames.Text+' ('+lookupCurrentClass.Text+')';
      sdcombinationregoldstd.Params[4].AsString:=global_unit.user_ip;
      sdcombinationregoldstd.Params[5].AsString:=global_unit.user_host;
      try
        sdcombinationregoldstd.ExecSQL;
      except
      end;

      oldstudentregistration.ModalResult:=mrOk;

    except
      on e: Exception do
      begin
        sdDJold.Active:=False;
        sdDJold.CommandText:='rollback';
        sdDJold.ExecSQL;

        MessageDlg('Sorry, an error has been encountered and the student registration has not been effected'+#13#10#13#10+'Please note the following error and report it to the PSM support team: '+e.Message, mtError,[mbOk],0);
        Exit;
      end;
    end;

  end;

  Inc(currentPage);
  oldstudentregistration.Caption:='Registering Old Student - Step '+IntToStr(currentPage)+' of 2';
  btnBack.Enabled:=True;
  cxpagecontrol1.ActivePageIndex:=currentPage;
end;

procedure Toldstudentregistration.chkIsBoarderClick(Sender: TObject);
begin
  label1Dormitory.Enabled:=chkIsBoarder.Checked;
  lookUpDormitory.Enabled:=chkIsBoarder.Checked;
end;

procedure Toldstudentregistration.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
oldstudentregistration:=nil;
action:=caFree;
end;

procedure Toldstudentregistration.FormCreate(Sender: TObject);
begin
     {creating the sql dataset}
         sduseregoldstd :=TSQLDataSet.Create(Self);
         sdstreamregoldstd :=TSQLDataSet.Create(Self);
         sdDJold:=TSQLDataSet.Create(Self);


      {Assigning sqlconnection to the sql datasets}

        sdDJold.SQLConnection:=dmPSM.cPSM;
        sduseregoldstd.SQLConnection:=dmPSM.cPSM;
        sdstreamregoldstd.SQLConnection :=dmPSM.cPSM;

      //**********************************************************


      {Creating the dataset providers}
    dspuseregoldstd:=TDataSetProvider.Create(self);
    dspstreamregoldstd :=TDataSetProvider.Create(Self);
    dspDJold:=TDataSetProvider.Create(Self);

    {Assigning DataSet Providers Names}
    dspuseregoldstd.Name:='useDataSetProviderold';
    dspstreamregoldstd.Name:='streamDataSetProviderold';
    dspDJold.Name:='DJDataSetProviderold';
//**********************************************************

        {Assigning datasetproviders datasets}
          dspuseregoldstd.DataSet:=sduseregoldstd;
          dspstreamregoldstd.DataSet:=sdstreamregoldstd;
          dspDJold.DataSet:=sdDJold;

          {Creating ClientDataSets}
          cduseregoldstd :=TClientDataSet.Create(self);
          cdstreamregoldstd :=TClientDataSet.Create(Self);
          cdDJold:=TClientDataSet.Create(Self);

      {Assigning ClientDatasets DataSetProviders}
      cduseregoldstd.ProviderName:='useDataSetProviderold';
      cdstreamregoldstd.ProviderName:='streamDataSetProviderold';
      cdDJold.ProviderName:='DJDataSetProviderold';

//**********************************************************

  {Creating DataSources}

   dsuseregoldstd:=TDataSource.Create(Self);
   dsstreamregoldstd :=TDataSource.Create(Self);
    dDJold:=TDataSource.Create(Self);


   {Assiging Datasets to the dataSources}

    dsuseregoldstd.DataSet:=cduseregoldstd;
    dsstreamregoldstd.DataSet :=cdstreamregoldstd;
     dDJold.DataSet:=cdDJold;
end;

procedure Toldstudentregistration.lookupClassJoinedMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
lookupClassJoined.Clear;
    cdstreamregoldstd.Active:=False;
    sdstreamregoldstd.Active:=false;
    sdstreamregoldstd.CommandText:='select concat(sc_class,'' '',sc_stream) as Stream from school_classes where sc_class =''S. 5''';
    sdstreamregoldstd.open;
    // := TComponent.Create(Self);
    try
   lookupClassJoined.Properties.ListSource:=dsstreamregoldstd;
   lookupClassJoined.Properties.ListFieldNames:='Stream';
   lookupClassJoined.Properties.KeyFieldNames:='Stream';
    finally
      //.Free;
    end;
    sdstreamregoldstd.Active:=true;
   cdstreamregoldstd.Active:=True;
end;

procedure Toldstudentregistration.lookupCurrentClassMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
lookupClassJoined.Clear;
    cdstreamregoldstd.Active:=False;
    sdstreamregoldstd.Active:=false;
    sdstreamregoldstd.CommandText:='select concat(sc_class,'' '',sc_stream) as Stream from school_classes where sc_class =''S. 5''';
    sdstreamregoldstd.open;
    // := TComponent.Create(Self);
    try
   lookupClassJoined.Properties.ListSource:=dsstreamregoldstd;
   lookupClassJoined.Properties.ListFieldNames:='Stream';
   lookupClassJoined.Properties.KeyFieldNames:='Stream';
    finally
      //.Free;
    end;
    sdstreamregoldstd.Active:=true;
   cdstreamregoldstd.Active:=True;
end;

function Toldstudentregistration.RandomString: string;
var
  char_pool, output_code: string;
  i: Integer;
begin
  Randomize;

  char_pool:='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  output_code:='';

  for i := 0 to 100 do
  begin
    output_code:=output_code+char_pool[Random(61) + 1];
  end;

  Result:=output_code;

end;

end.
