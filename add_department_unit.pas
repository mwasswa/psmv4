unit add_department_unit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, dxSkinsCore, dxSkiniMaginary, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, cxGraphics, Menus, cxLookAndFeelPainters, ExtCtrls,
  cxButtons, cxMemo, cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxLookAndFeels, dxSkinFoggy, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinGlassOceans, dxSkinLilian,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, cxCheckBox;

type
  TformAddDepartment = class(TForm)
    lbl4: TLabel;
    lbl5: TLabel;
    lDepartment: TLabel;
    tEditDepartment: TcxTextEdit;
    lHoD: TLabel;
    lookupHoD: TcxLookupComboBox;
    memoDepartmentDetails: TcxMemo;
    Label1: TLabel;
    btnPost: TcxButton;
    btnCancel: TcxButton;
    btnHelp: TcxButton;
    Bevel2: TBevel;
    chkClearance: TcxCheckBox;
    procedure btnPostClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  formAddDepartment: TformAddDepartment;

implementation

uses data_unit, global_unit;

{$R *.dfm}

procedure TformAddDepartment.btnPostClick(Sender: TObject);
begin
  if Length(Trim(tEditDepartment.Text)) = 0 then
  begin
    MessageDlg('Please enter the department name', mtError, [mbOk],0);
    lDepartment.Font.Color:=clRed;
    tEditDepartment.SetFocus;
    Exit;
  end
  else
  begin
    lDepartment.Font.Color:=clBlack;
  end;

  dmPSM.sdDJ1.Active:=False;
  dmPSM.sdDJ1.CommandText:='insert into school_departments (SD_NAME, SD_DETAILS, SD_HEAD, SD_ADDED_TIME, SD_ADDED_BY, SD_CLEARANCE) values (:SD_NAME, :SD_DETAILS, :SD_HEAD, now(), :SD_ADDED_BY, :SD_CLEARANCE) ';
  dmPSM.sdDJ1.Params[0].AsString:=Trim(tEditDepartment.Text);
  dmPSM.sdDJ1.Params[1].AsString:=Trim(memoDepartmentDetails.Text);
  if Length(lookupHoD.Text) > 0 then dmPSM.sdDJ1.Params[2].AsInteger:=lookupHoD.EditValue
  else dmPSM.sdDJ1.Params[2].AsInteger:=0;
  dmPSM.sdDJ1.Params[3].AsInteger:=global_unit.current_user_id;
  dmPSM.sdDJ1.Params[4].AsString:=chkClearance.EditValue;
  dmPSM.sdDJ1.ExecSQL;

  dmPSM.sdUsage.Active:=False;
  dmPSM.sdUsage.Params[0].AsString:='STAFF';
  dmPSM.sdUsage.Params[1].AsInteger:=global_unit.current_user_id;
  dmPSM.sdUsage.Params[2].AsString:='ADMIN';
  dmPSM.sdUsage.Params[3].AsString:='Registered Department: '+Trim(tEditDepartment.Text);
  dmPSM.sdUsage.Params[4].AsString:=global_unit.user_ip;
  dmPSM.sdUsage.Params[5].AsString:=global_unit.user_host;
  try
    dmPSM.sdUsage.ExecSQL;
  except
  end;

  formAddDepartment.ModalResult:=mrOk;

end;

procedure TformAddDepartment.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  formAddDepartment:=nil;
  Action:=caFree;
end;

end.
