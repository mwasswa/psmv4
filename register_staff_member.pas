unit register_staff_member;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinFoggy,
  dxSkiniMaginary, dxSkinscxPCPainter, cxControls, cxContainer, cxEdit,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxSpinEdit,
  cxCheckBox, cxCalendar, DB, cxDBData, ExtDlgs, cxGridDBTableView, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxClasses, cxGridCustomView, cxGrid,
  cxMemo, StdCtrls, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox,
  cxRadioGroup, cxImage, jpeg,dxSkinSpringTime, SqlTimSt,cxDropDownEdit, cxMaskEdit, cxTextEdit, DateUtils,ExtCtrls, cxPC,
  cxButtons;

type
  Tregisterstaffmember = class(TForm)
    bvl2: TBevel;
    btnNext: TcxButton;
    btnCancel: TcxButton;
    btnBack: TcxButton;
    btnHelp: TcxButton;
    pcAddNewStaff: TcxPageControl;
    tabWelcome: TcxTabSheet;
    img1: TImage;
    label1lbl1: TLabel;
    label1lbl2: TLabel;
    label1lbl3: TLabel;
    tabPersonalInformation: TcxTabSheet;
    label1Surname: TLabel;
    label1OtherNames: TLabel;
    label1DOB: TLabel;
    label1Sex: TLabel;
    label1Citizenship: TLabel;
    label1DateJoined: TLabel;
    bvl3: TBevel;
    label1lbl4: TLabel;
    label1lbl5: TLabel;
    label1lbl7: TLabel;
    bvl4: TBevel;
    Label1: TLabel;
    Label5: TLabel;
    lIsMemberOf: TLabel;
    lDesignation: TLabel;
    Label21: TLabel;
    lMaritalStatus: TLabel;
    lEmploymentType: TLabel;
    lContractEnd: TLabel;
    lDepartment: TLabel;
    lContractStart: TLabel;
    lblReligion: TLabel;
    tEditSurname: TcxTextEdit;
    tEditOtherNames: TcxTextEdit;
    dEditDOB: TcxDateEdit;
    comboSex: TcxComboBox;
    comboCitizenship: TcxComboBox;
    dEditDateJoined: TcxDateEdit;
    pcGraphics: TcxPageControl;
    tabEntryID: TcxTabSheet;
    lblImportEntryID: TLabel;
    lblClearEntryID: TLabel;
    lbl6: TLabel;
    imgStaffID: TcxImage;
    tabStaffSignature: TcxTabSheet;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    imgStaffSignature: TcxImage;
    tEditHomeDistrict: TcxTextEdit;
    comboTitle: TcxComboBox;
    tEditInitials: TcxTextEdit;
    rbTeachingStaff: TcxRadioButton;
    rbNonTeachingStaff: TcxRadioButton;
    tEditDesignation: TcxTextEdit;
    comboMaritalStatus: TcxComboBox;
    comboEmploymentType: TcxComboBox;
    dEditContractEnd: TcxDateEdit;
    lookupDepartment: TcxLookupComboBox;
    dEditContractStart: TcxDateEdit;
    btnNewDepartment: TButton;
    comboReligion: TcxComboBox;
    tabBackgroundInformation: TcxTabSheet;
    Label7: TLabel;
    Label8: TLabel;
    lContactTel: TLabel;
    Label10: TLabel;
    lNoK: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    tEditTelephone: TcxTextEdit;
    tEditAlternativeTelephone: TcxTextEdit;
    tEditNoK: TcxTextEdit;
    tEditNoKTelephone: TcxTextEdit;
    tEditEMail: TcxTextEdit;
    memoAddress: TcxMemo;
    tEditNoKEMail: TcxTextEdit;
    tabProfessionalInformation: TcxTabSheet;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    gQualifications: TcxGrid;
    gtvQualifications: TcxGridTableView;
    gcSQInstitute: TcxGridColumn;
    gcSQFrom: TcxGridColumn;
    gcSQTo: TcxGridColumn;
    gcSQQualification: TcxGridColumn;
    gcQCVerified: TcxGridColumn;
    glQualifications: TcxGridLevel;
    tEditInstitute: TcxTextEdit;
    tEditInstituteQualification: TcxTextEdit;
    gExperience: TcxGrid;
    gtvExperience: TcxGridTableView;
    gcSPEOrganisation: TcxGridColumn;
    gcSPEFrom: TcxGridColumn;
    gcSPETo: TcxGridColumn;
    gcSPERole: TcxGridColumn;
    gcSPEVerified: TcxGridColumn;
    glExperience: TcxGridLevel;
    sEditInstituteFrom: TcxSpinEdit;
    sEditInstituteTo: TcxSpinEdit;
    btnAddQualification: TcxButton;
    tEditOrganisation: TcxTextEdit;
    tEditOrganisationRole: TcxTextEdit;
    sEditOrganisationFrom: TcxSpinEdit;
    sEditOrganisationTo: TcxSpinEdit;
    btnAddExperience: TcxButton;
    tabSubjectAllocation: TcxTabSheet;
    l1: TLabel;
    l2: TLabel;
    l3: TLabel;
    l4: TLabel;
    bvl1: TBevel;
    btnAddClassSubject: TcxButton;
    gStaffSubjects: TcxGrid;
    gtvStaffSubjects: TcxGridTableView;
    gcSubjectID: TcxGridColumn;
    gcClassID: TcxGridColumn;
    gcAssignedSubject: TcxGridColumn;
    gcAssignedClass: TcxGridColumn;
    gcClassLevel: TcxGridColumn;
    glStaffSubjects: TcxGridLevel;
    gClassesTaught: TcxGrid;
    gtvClassesTaught: TcxGridDBTableView;
    gcClassesTaughtDBTableView1CLASS_STREAM: TcxGridDBColumn;
    glClassesTaught: TcxGridLevel;
    lookupSubject: TcxLookupComboBox;
    tabResponsibilities: TcxTabSheet;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    Label38: TLabel;
    Label37: TLabel;
    dEditDateAssumed: TcxDateEdit;
    gResponsibilities: TcxGrid;
    gtvResponsibilities: TcxGridTableView;
    gcResponsibility: TcxGridColumn;
    gcDateAssumed: TcxGridColumn;
    gcResponsibilityRemarks: TcxGridColumn;
    glResponsibilities: TcxGridLevel;
    tEditResponsibility: TcxTextEdit;
    tEditResponsibilityRemarks: TcxTextEdit;
    btnAddResponsibility: TcxButton;
    tabSMAccountFinish: TcxTabSheet;
    Label39: TLabel;
    Label40: TLabel;
    lUsername: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    lbl21: TLabel;
    Label6: TLabel;
    chkSMUser: TcxCheckBox;
    tEditUsername: TcxTextEdit;
    memoAdditional: TcxMemo;
    dlgImportGraphic: TOpenPictureDialog;
    pmResponsibilities: TPopupMenu;
    mniRemoveSelectedResponsibility: TMenuItem;
    pmQualifications: TPopupMenu;
    mniRemoveSelectedQualification1: TMenuItem;
    pmExperience: TPopupMenu;
    mniRemoveSelectedRole1: TMenuItem;
    pmSubjects: TPopupMenu;
    RemoveAssignedSubjects1: TMenuItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnNextClick(Sender: TObject);
    procedure btnBackClick(Sender: TObject);
    procedure btnNewDepartmentClick(Sender: TObject);
  private
    importID, importSignature: TJpegImage;
     IDAssigned, signatureAssigned: Boolean;
  public
    currentPage:Integer;
  end;

var
  registerstaffmember: Tregisterstaffmember;

implementation

uses data_unit, global_unit,add_department_unit;

{$R *.dfm}

procedure Tregisterstaffmember.btnBackClick(Sender: TObject);
begin
currentPage:=currentPage-1;
  if currentPage = 4 then
  begin
    if rbNonTeachingStaff.Checked then currentPage:=currentPage - 1;
  end;

  btnNext.Enabled:=True;
  btnNext.Caption:='&Next';
  pcAddNewStaff.ActivePageIndex:=currentPage;
  if currentPage < 1 then
  begin
    btnBack.Enabled:=False;
    registerstaffmember.Caption:='Register New Staff Member - Welcome!';
  end
  else
  begin
    registerstaffmember.Caption:='Registering New Member of Staff - Step '+IntToStr(currentPage)+' of 6';
  end;
end;

procedure Tregisterstaffmember.btnNewDepartmentClick(Sender: TObject);
begin
//**security
  if not (global_unit.is_super_user or global_unit.can_manage_departments) then
  begin
    dmPSM.sdUsage.Active:=False;
    dmPSM.sdUsage.Params[0].AsString:='STAFF';
    dmPSM.sdUsage.Params[1].AsInteger:=global_unit.current_user_id;
    dmPSM.sdUsage.Params[2].AsString:='SECURITY';
    dmPSM.sdUsage.Params[3].AsString:='Access Attempt: Add Department';
    dmPSM.sdUsage.Params[4].AsString:=global_unit.user_ip;
    dmPSM.sdUsage.Params[5].AsString:=global_unit.user_host;
    try
      dmPSM.sdUsage.ExecSQL;
    except
    end;

    Application.MessageBox(PWideChar('Access denied!'+#13#10#13#10+'Please contact the PSM Administrator for assistance'), 'Restricted Access', MB_OK+MB_ICONSTOP);
    Exit;
  end;


  formAddDepartment:=TformAddDepartment.Create(Self);

  dmPSM.ClientDataSet1Staff.Active:=False;
  dmPSM.sdGetStaff.Active:=False;
  dmPSM.sdGetStaff.CommandText:='select T_ID, concat(ifnull(T_TITLE,''''), '' '', T_SURNAME, '' '', T_OTHERNAMES) T_FULLNAME from staff_general where T_ID > 0 order by concat(ifnull(T_TITLE,''''), '' '', T_SURNAME, '' '', T_OTHERNAMES)';
  dmPSM.ClientDataSet1Staff.Open;

  formAddDepartment.ShowModal;
  if formAddDepartment.ModalResult = mrOk then
  begin
    dmPSM.clientDataSet1GetDepartments.Refresh;
  end;
end;

procedure Tregisterstaffmember.btnNextClick(Sender: TObject);
var
   i, newT_ID: Integer;
   importIDStream, importSignatureStream: TMemoryStream;
begin
    if currentPage = 0 then
  begin

  end;

  if currentPage = 1 then
  begin
    if Length(Trim(tEditSurname.Text)) = 0 then
    begin
      MessageDlg('Please enter the new staff member''s surname', mtError, [mbOk],0);
      label1Surname.Font.Color:=clRed;
      tEditSurname.SetFocus;
      Exit;
    end
    else
    begin
      label1Surname.Font.Color:=clBlack;
    end;

    if Length(Trim(tEditOtherNames.Text)) = 0 then
    begin
      MessageDlg('Please enter the new staff member''s other names', mtError, [mbOk],0);
      label1OtherNames.Font.Color:=clRed;
      tEditOtherNames.SetFocus;
      Exit;
    end
    else
    begin
      label1OtherNames.Font.Color:=clBlack;
    end;

    if Length(Trim(dEditDOB.Text)) = 0 then
    begin
      MessageDlg('Please enter the new staff member''s date of birth', mtError, [mbOk],0);
      label1DOB.Font.Color:=clRed;
      dEditDOB.SetFocus;
      Exit;
    end
    else
    begin
      label1DOB.Font.Color:=clBlack;
    end;

    if Length(Trim(comboSex.Text)) = 0 then
    begin
      MessageDlg('Please specify the new staff member''s gender', mtError, [mbOk],0);
      label1Sex.Font.Color:=clRed;
      comboSex.SetFocus;
      Exit;
    end
    else
    begin
      label1Sex.Font.Color:=clBlack;
    end;

    if Length(Trim(comboMaritalStatus.Text)) = 0 then
    begin
      MessageDlg('Please specify the new staff member''s marital status', mtError, [mbOk],0);
      lMaritalStatus.Font.Color:=clRed;
      comboMaritalStatus.SetFocus;
      Exit;
    end
    else
    begin
      lMaritalStatus.Font.Color:=clBlack;
    end;

    if Length(Trim(comboCitizenship.Text)) = 0 then
    begin
      MessageDlg('Please enter the new staff member''s nationality', mtError, [mbOk],0);
      label1Citizenship.Font.Color:=clRed;
      comboCitizenship.SetFocus;
      Exit;
    end
    else
    begin
      label1Citizenship.Font.Color:=clBlack;
    end;

    if Length(Trim(tEditDesignation.Text)) = 0 then
    begin
      MessageDlg('Please enter the new staff member''s designation (job title/institute post)', mtError, [mbOk],0);
      lDesignation.Font.Color:=clRed;
      tEditDesignation.SetFocus;
      Exit;
    end
    else
    begin
      lDesignation.Font.Color:=clBlack;
    end;

    if not ((rbTeachingStaff.Checked) or (rbNonTeachingStaff.Checked)) then
    begin
      MessageDlg('Please specify whether '+tEditOtherNames.Text+' is a member of the teaching staff or non-teaching staff', mtError, [mbOk],0);
      lIsMemberOf.Font.Color:=clRed;
      Exit;
    end
    else
    begin
      lIsMemberOf.Font.Color:=clBlack;
    end;

    if Length(Trim(lookupDepartment.Text)) = 0 then
    begin
      MessageDlg('Please specify the new staff member''s department', mtError, [mbOk],0);
      lDepartment.Font.Color:=clRed;
      lookupDepartment.SetFocus;
      Exit;
    end
    else
    begin
      lDepartment.Font.Color:=clBlack;
    end;

    if Length(Trim(comboEmploymentType.Text)) = 0 then
    begin
      MessageDlg('Please specify the new staff member''s employment type', mtError, [mbOk],0);
      lEmploymentType.Font.Color:=clRed;
      comboEmploymentType.SetFocus;
      Exit;
    end
    else
    begin
      lEmploymentType.Font.Color:=clBlack;
    end;

    if Length(Trim(dEditDateJoined.Text)) = 0 then
    begin
      MessageDlg('Please specify the date the new staff member joined the school', mtError, [mbOk],0);
      label1DateJoined.Font.Color:=clRed;
      dEditDateJoined.SetFocus;
      Exit;
    end
    else
    begin
      label1DateJoined.Font.Color:=clBlack;
    end;

    if dEditDateJoined.Date <= dEditDOB.Date then
    begin
      MessageDlg('Either the staff member''s date of birth or the date joined is invalid! Please check them before proceeding', mtError, [mbOk],0);
      label1DateJoined.Font.Color:=clRed;
      label1DOB.Font.Color:=clRed;
      dEditDateJoined.SetFocus;
      Exit;
    end
    else
    begin
      label1DateJoined.Font.Color:=clBlack;
      label1DOB.Font.Color:=clBlack;
    end;

    if comboEmploymentType.ItemIndex = 1 then
    begin
      if Length(Trim(dEditContractStart.Text)) = 0 then
      begin
        MessageDlg('Please specify the date the new staff member''s contract started', mtError, [mbOk],0);
        lContractStart.Font.Color:=clRed;
        dEditContractStart.SetFocus;
        Exit;
      end
      else
      begin
        lContractStart.Font.Color:=clBlack;
      end;

      if Length(Trim(dEditContractEnd.Text)) = 0 then
      begin
        MessageDlg('Please specify the date the new staff member''s contract shall end', mtError, [mbOk],0);
        lContractEnd.Font.Color:=clRed;
        dEditContractEnd.SetFocus;
        Exit;
      end
      else
      begin
        lContractEnd.Font.Color:=clBlack;
      end;

      if dEditContractEnd.Date <= dEditContractStart.Date then
      begin
        MessageDlg('Please check the staff member''s contract dates (start and end)', mtError, [mbOk],0);
        lContractStart.Font.Color:=clRed;
        lContractEnd.Font.Color:=clRed;
        dEditContractStart.SetFocus;
        Exit;
      end
      else
      begin
        lContractStart.Font.Color:=clBlack;
        lContractEnd.Font.Color:=clBlack;
      end;
    end;

  end;

  if currentPage = 2 then
  begin
    {if Length(Trim(tEditTelephone.Text)) = 0 then
    begin
      MessageDlg('Please specify the new staff member''s contact number', mtError, [mbOk],0);
      lContactTel.Font.Color:=clRed;
      tEditTelephone.SetFocus;
      Exit;
    end
    else
    begin
      lContactTel.Font.Color:=clBlack;
    end;

    if Length(Trim(tEditNoK.Text)) = 0 then
    begin
      MessageDlg('Please specify the new staff member''s next of kin', mtError, [mbOk],0);
      lNoK.Font.Color:=clRed;
      tEditNoK.SetFocus;
      Exit;
    end
    else
    begin
      lNoK.Font.Color:=clBlack;
    end; }

  end;

  if currentPage = 3 then
  begin

    if rbNonTeachingStaff.Checked then Inc(currentPage);
  end;

  if currentPage = 5 then
  begin

    btnNext.Caption:='&Finish';
  end;

  if currentPage = 6 then
  begin
    if chkSMUser.Checked then
    begin
      if Length(Trim(tEditUsername.Text)) = 0 then
      begin
        MessageDlg('Please specify the new staff member''s PSM username', mtError, [mbOk],0);
        lUsername.Font.Color:=clRed;
        tEditUsername.SetFocus;
        Exit;
      end
      else
      begin
        lUsername.Font.Color:=clBlack;
      end;


      dmPSM.sdDJ1.Active:=False;
      dmPSM.sdDJ1.CommandText:='select count(*) from staff_general where T_USERNAME = :T_USERNAME ';
      dmPSM.sdDJ1.Params[0].AsString:=Trim(tEditUsername.Text);
      dmPSM.sdDJ1.Open;

      if dmPSM.sdDJ1.Fields[0].AsInteger > 0 then
      begin
        MessageDlg('This PSM username is already in use. Please assign another one', mtError, [mbOk],0);
        lUsername.Font.Color:=clRed;
        tEditUsername.SetFocus;
        dmPSM.sdDJ1.Active:=False;
        Exit;
      end
      else
      begin
        lUsername.Font.Color:=clBlack;
      end;

      dmPSM.sdDJ1.Active:=False;
    end;

    dmPSM.sdDJ1.Active:=False;
    dmPSM.sdDJ1.CommandText:='start transaction';
    dmPSM.sdDJ1.ExecSQL;

    dmPSM.sdDJ1.Active:=False;
    dmPSM.sdDJ1.CommandText:='insert into staff_general (T_SURNAME,T_OTHERNAMES,T_DOB,T_GENDER,T_TITLE,T_INITIALS,T_MARITAL_STATUS,T_RELIGION,T_CITIZENSHIP,T_DISTRICT,T_TEL_MOBILE,T_TEL_ALTERNATIVE,T_EMAIL,'+
                               'T_NEXT_OF_KIN,T_NEXT_OF_KIN_TEL,T_NEXT_OF_KIN_EMAIL,T_HOME_ADDRESS,T_JOINED_DATE,T_OTHER_INFO,T_DESIGNATION,T_EMP_STATUS,'+
                               'T_IS_TEACHER,T_IS_USER,T_USER_PROFILE,T_USERNAME,T_PASSWORD,T_CREATED_TIME,T_CREATED_BY,MODIFIED_TIME,T_MODIFIED_BY, T_SD_ID) '+
                               'values (:T_SURNAME,:T_OTHERNAMES,:T_DOB,:T_GENDER,:T_TITLE,:T_INITIALS,:T_MARITAL_STATUS,:T_RELIGION,:T_CITIZENSHIP,:T_DISTRICT,:T_TEL_MOBILE,:T_TEL_ALTERNATIVE,:T_EMAIL,'+
                               ':T_NEXT_OF_KIN,:T_NEXT_OF_KIN_TEL,:T_NEXT_OF_KIN_EMAIL,:T_HOME_ADDRESS,:T_JOINED_DATE,:T_OTHER_INFO,:T_DESIGNATION,:T_EMP_STATUS,'+
                               ':T_IS_TEACHER,:T_IS_USER,:T_USER_PROFILE,:T_USERNAME,aes_encrypt(:T_PASSWORD, :T_PASSWORD_KEY),now(),:T_CREATED_BY,now(),:T_MODIFIED_BY, :T_SD_ID) ';

    dmPSM.sdDJ1.Params[0].AsString:=Trim(tEditSurname.Text);
    dmPSM.sdDJ1.Params[1].AsString:=Trim(tEditOtherNames.Text);
    dmPSM.sdDJ1.Params[2].AsDate:=dEditDOB.Date;
    dmPSM.sdDJ1.Params[3].AsString:=comboSex.Text[1];
    dmPSM.sdDJ1.Params[4].AsString:=Trim(comboTitle.Text);
    dmPSM.sdDJ1.Params[5].AsString:=Trim(tEditInitials.Text);
    dmPSM.sdDJ1.Params[6].AsString:=Trim(comboMaritalStatus.Text);
    dmPSM.sdDJ1.Params[7].AsString:=Trim(comboReligion.Text);
    dmPSM.sdDJ1.Params[8].AsString:=Trim(comboCitizenship.Text);
    dmPSM.sdDJ1.Params[9].AsString:=Trim(tEditHomeDistrict.Text);
    dmPSM.sdDJ1.Params[10].AsString:=Trim(tEditTelephone.Text);
    dmPSM.sdDJ1.Params[11].AsString:=Trim(tEditAlternativeTelephone.Text);
    dmPSM.sdDJ1.Params[12].AsString:=Trim(tEditEMail.Text);
    dmPSM.sdDJ1.Params[13].AsString:=Trim(tEditNoK.Text);
    dmPSM.sdDJ1.Params[14].AsString:=Trim(tEditNoKTelephone.Text);
    dmPSM.sdDJ1.Params[15].AsString:=Trim(tEditNoKEMail.Text);
    dmPSM.sdDJ1.Params[16].AsString:=Trim(memoAddress.Text);
    dmPSM.sdDJ1.Params[17].AsDate:=dEditDateJoined.Date;
    dmPSM.sdDJ1.Params[18].AsString:=Trim(memoAdditional.Text);
    dmPSM.sdDJ1.Params[19].AsString:=Trim(tEditDesignation.Text);
    dmPSM.sdDJ1.Params[20].AsString:=Trim(comboEmploymentType.Text);
    if rbTeachingStaff.Checked then dmPSM.sdDJ1.Params[21].AsString:='Y'
    else dmPSM.sdDJ1.Params[21].AsString:='N';
    dmPSM.sdDJ1.Params[22].AsString:=chkSMUser.EditValue;
    dmPSM.sdDJ1.Params[23].AsString:='NOT ASSIGNED';
    dmPSM.sdDJ1.Params[24].AsString:=Trim(tEditUsername.Text);
    dmPSM.sdDJ1.Params[25].AsString:='abai';
    dmPSM.sdDJ1.Params[26].AsString:='abai';
    dmPSM.sdDJ1.Params[27].AsInteger:=global_unit.current_user_id;
    dmPSM.sdDJ1.Params[28].AsInteger:=global_unit.current_user_id;
    dmPSM.sdDJ1.Params[29].AsInteger:=lookupDepartment.EditValue;

    try
      dmPSM.sdDJ1.ExecSQL;

      dmPSM.sdDJ1.Active:=False;
      dmPSM.sdDJ1.CommandText:='select MAX(T_ID) from staff_general';
      dmPSM.sdDJ1.Open;

      newT_ID:=dmPSM.sdDJ1.Fields[0].AsInteger;

      dmPSM.sdDJ1.Active:=False;
      dmPSM.sdDJ1.CommandText:='insert into staff_history (TH_YEAR, TH_TERM, TH_T_ID, TH_INFORMATION, TH_POSTED_TIME, TH_POSTED_BY) '+
                                 'values (:TH_YEAR, :TH_TERM, :TH_T_ID, :TH_INFORMATION, now(), :TH_POSTED_BY)';

      dmPSM.sdDJ1.Params[0].AsInteger:=global_unit.current_year;
      dmPSM.sdDJ1.Params[1].AsInteger:=global_unit.current_term;
      dmPSM.sdDJ1.Params[2].AsInteger:=newT_ID;
      dmPSM.sdDJ1.Params[3].AsString:='PSM Initial Registration';
      dmPSM.sdDJ1.Params[4].AsInteger:=global_unit.current_user_id;
      dmPSM.sdDJ1.ExecSQL;

      if comboEmploymentType.ItemIndex = 1 then
      begin
        dmPSM.sdDJ1.Active:=False;
        dmPSM.sdDJ1.CommandText:='insert into staff_contract_history (TCH_T_ID, TCH_CONTRACT_START, TCH_CONTRACT_END, TCH_POSTED_TIME, TCH_POSTED_BY) '+
                                   'values (:TCH_T_ID, :TCH_CONTRACT_START, :TCH_CONTRACT_END, now(), :TCH_POSTED_BY)';

        dmPSM.sdDJ1.Params[0].AsInteger:=newT_ID;
        dmPSM.sdDJ1.Params[1].AsDate:=dEditContractStart.Date;
        dmPSM.sdDJ1.Params[2].AsDate:=dEditContractEnd.Date;
        dmPSM.sdDJ1.Params[3].AsInteger:=global_unit.current_user_id;
        dmPSM.sdDJ1.ExecSQL;
      end;

      //Qualifications
      if gtvQualifications.DataController.RecordCount > 0 then
      begin
        for i:=0 to gtvQualifications.DataController.RecordCount - 1 do
        begin
          dmPSM.sdDJ1.Active:=False;
          dmPSM.sdDJ1.CommandText:='insert into staff_qualifications (TQ_T_ID, TQ_INSTITUTE, TQ_DATE_ATTENDED_FROM, TQ_DATE_ATTENDED_TO, TQ_QUALIFICATION, TQ_VERIFIED, TQ_POSTED_TIME, TQ_POSTED_BY)'+
                                     'values (:TQ_T_ID, :TQ_INSTITUTE, :TQ_DATE_ATTENDED_FROM, :TQ_DATE_ATTENDED_TO, :TQ_QUALIFICATION, :TQ_VERIFIED, now(), :TQ_POSTED_BY)';

          dmPSM.sdDJ1.Params[0].AsInteger:=newT_ID;
          dmPSM.sdDJ1.Params[1].AsString:=Trim(gtvQualifications.DataController.DisplayTexts[i,0]);
          dmPSM.sdDJ1.Params[2].AsString:=Trim(gtvQualifications.DataController.DisplayTexts[i,1]);
          dmPSM.sdDJ1.Params[3].AsString:=Trim(gtvQualifications.DataController.DisplayTexts[i,2]);
          dmPSM.sdDJ1.Params[4].AsString:=Trim(gtvQualifications.DataController.Values[i,3]);
          dmPSM.sdDJ1.Params[5].AsString:=Trim(gtvQualifications.DataController.Values[i,4]);
          dmPSM.sdDJ1.Params[6].AsInteger:=global_unit.current_user_id;
          dmPSM.sdDJ1.ExecSQL;
        end;
      end;

      //Experience
      if gtvExperience.DataController.RecordCount > 0 then
      begin
        for i:=0 to gtvExperience.DataController.RecordCount - 1 do
        begin
          dmPSM.sdDJ1.Active:=False;
          dmPSM.sdDJ1.CommandText:='insert into staff_prior_experience (TPE_T_ID, TPE_ORGANISATION, TPE_DATE_FROM, TPE_DATE_TO, TPE_ROLE, TPE_VERIFIED, TPE_POSTED_TIME, TPE_POSTED_BY)'+
                                     'values (:TPE_T_ID, :TPE_ORGANISATION, :TPE_DATE_FROM, :TPE_DATE_TO, :TPE_ROLE, :TPE_VERIFIED, now(), :TPE_POSTED_BY)';

          dmPSM.sdDJ1.Params[0].AsInteger:=newT_ID;
          dmPSM.sdDJ1.Params[1].AsString:=Trim(gtvExperience.DataController.DisplayTexts[i,0]);
          dmPSM.sdDJ1.Params[2].AsString:=Trim(gtvExperience.DataController.DisplayTexts[i,1]);
          dmPSM.sdDJ1.Params[3].AsString:=Trim(gtvExperience.DataController.DisplayTexts[i,2]);
          dmPSM.sdDJ1.Params[4].AsString:=Trim(gtvExperience.DataController.DisplayTexts[i,3]);
          dmPSM.sdDJ1.Params[5].AsString:=Trim(gtvExperience.DataController.Values[i,4]);
          dmPSM.sdDJ1.Params[6].AsInteger:=global_unit.current_user_id;
          dmPSM.sdDJ1.ExecSQL;
        end;
      end;

      //Subjects/Classes Taught
      if (gtvStaffSubjects.DataController.RecordCount > 0) and (rbTeachingStaff.Checked) then
      begin
        for i:=0 to gtvStaffSubjects.DataController.RecordCount - 1 do
        begin
          dmPSM.sdDJ1.Active:=False;
          if (gtvStaffSubjects.DataController.Values[i,4] = 'K') or (gtvStaffSubjects.DataController.Values[i,4] = 'P') or (gtvStaffSubjects.DataController.Values[i,4] = 'CP') then
          begin
            dmPSM.sdDJ1.CommandText:='insert into staff_classes_subjects (SCS_T_ID, SCS_SC_ID, SCS_SS_ID, SCS_POSTED_TIME, SCS_POSTED_BY)'+
                                       'values (:SCS_T_ID, :SCS_SC_ID, :SCS_SS_ID, now(), :SCS_POSTED_BY)';
          end
          else
          begin
            dmPSM.sdDJ1.CommandText:='insert into staff_classes_subjects (SCS_T_ID, SCS_SC_ID, SCS_SSP_ID, SCS_POSTED_TIME, SCS_POSTED_BY)'+
                                       'values (:SCS_T_ID, :SCS_SC_ID, :SCS_SSP_ID, now(), :SCS_POSTED_BY)';
          end;

          dmPSM.sdDJ1.Params[0].AsInteger:=newT_ID;
          dmPSM.sdDJ1.Params[1].AsInteger:=gtvStaffSubjects.DataController.Values[i,1];
          dmPSM.sdDJ1.Params[2].AsInteger:=gtvStaffSubjects.DataController.Values[i,0];
          dmPSM.sdDJ1.Params[3].AsInteger:=global_unit.current_user_id;
          dmPSM.sdDJ1.ExecSQL;
        end;
      end;

      //Additional Responsibilities
      if gtvResponsibilities.DataController.RecordCount > 0 then
      begin
        for i:=0 to gtvResponsibilities.DataController.RecordCount - 1 do
        begin
          dmPSM.sdDJ1.Active:=False;
          dmPSM.sdDJ1.CommandText:='insert into staff_responsibilities (TR_T_ID, TR_RESPONSIBILITY, TR_REMARKS, TR_DATE_ASSUMED, TR_POSTED_TIME, TR_POSTED_BY)'+
                                     'values (:TR_T_ID, :TR_RESPONSIBILITY, :TR_REMARKS, :TR_DATE_ASSUMED, now(), :TR_POSTED_BY)';

          dmPSM.sdDJ1.Params[0].AsInteger:=newT_ID;
          dmPSM.sdDJ1.Params[1].AsString:=Trim(gtvResponsibilities.DataController.DisplayTexts[i,0]);
          dmPSM.sdDJ1.Params[2].AsString:=Trim(gtvResponsibilities.DataController.DisplayTexts[i,2]);
          dmPSM.sdDJ1.Params[3].AsSQLTimeStamp:=DateTimeToSQLTimeStamp(gtvResponsibilities.DataController.Values[i,1]);
          dmPSM.sdDJ1.Params[4].AsInteger:=global_unit.current_user_id;
          dmPSM.sdDJ1.ExecSQL;
        end;
      end;

      dmPSM.sdDJ1.Active:=False;
      dmPSM.sdDJ1.CommandText:='commit';
      dmPSM.sdDJ1.ExecSQL;

      if IDAssigned = True then
      begin
        dmPSM.sdPostIDT.Active:=False;
        dmPSM.cdsPostIDT.Active:=False;
        dmPSM.sdPostIDT.Params[0].AsInteger:=newT_ID;
        dmPSM.cdsPostIDT.Open;

        importID:=TJpegImage.Create;
        importIDStream:=TMemoryStream.Create;
        importID.Assign(imgStaffID.Picture);
        importID.SaveToStream(importIDStream);

        dmPSM.cdsPostIDT.Edit;
        dmPSM.bfPostIDTT_PHOTO.LoadFromStream(importIDStream);
        dmPSM.cdsPostIDT.Post;

        try
          dmPSM.cdsPostIDT.ApplyUpdates(-1);
        except
        end;

        importIDStream.Clear;
        importIDStream.Free;
        importID.Free;
      end;

      if signatureAssigned = True then
      begin
        dmPSM.sdPostIDT.Active:=False;
        dmPSM.cdsPostIDT.Active:=False;
        dmPSM.sdPostIDT.Params[0].AsInteger:=newT_ID;
        dmPSM.cdsPostIDT.Open;

        importSignature:=TJpegImage.Create;
        importSignatureStream:=TMemoryStream.Create;
        importSignature.Assign(imgStaffSignature.Picture);
        importSignature.SaveToStream(importSignatureStream);

        dmPSM.cdsPostIDT.Edit;
        dmPSM.bfPostIDTT_SIGNATURE.LoadFromStream(importSignatureStream);
        dmPSM.cdsPostIDT.Post;

        try
          dmPSM.cdsPostIDT.ApplyUpdates(-1);
        except
        end;

        importSignatureStream.Clear;
        importSignatureStream.Free;
        importSignature.Free;
      end;

      dmPSM.sdUsage.Active:=False;
      dmPSM.sdUsage.Params[0].AsString:='STAFF';
      dmPSM.sdUsage.Params[1].AsInteger:=global_unit.current_user_id;
      dmPSM.sdUsage.Params[2].AsString:='STAFF';
      dmPSM.sdUsage.Params[3].AsString:='Registered Staff Member: '+comboTitle.Text+' '+tEditSurname.Text+' '+tEditOtherNames.Text;
      dmPSM.sdUsage.Params[4].AsString:=global_unit.user_ip;
      dmPSM.sdUsage.Params[5].AsString:=global_unit.user_host;
      try
        dmPSM.sdUsage.ExecSQL;
      except
      end;

       //**additional housekeeping
      if comboCitizenship.ItemIndex = -1 then
      try
        dmPSM.sdDJ1.Active:=False;
        dmPSM.sdDJ1.CommandText:='insert into psm_nationalities (NATIONALITY) values (:NATIONALITY) ';
        dmPSM.sdDJ1.Params[0].AsString:=Trim(comboCitizenship.Text);
        dmPSM.sdDJ1.ExecSQL;
      finally
      end;

      //Pos(tEditHomeDistrict.Text, tEditHomeDistrict.Properties.LookupItems.Text);
      //chill the districts

      if (comboReligion.ItemIndex = -1) and (Length(Trim(comboReligion.Text)) > 0) then
      try
        dmPSM.sdDJ1.Active:=False;
        dmPSM.sdDJ1.CommandText:='insert into psm_religions (RELIGION) values (:RELIGION) ';
        dmPSM.sdDJ1.Params[0].AsString:=Trim(comboReligion.Text);
        dmPSM.sdDJ1.ExecSQL;
      finally
      end;

      registerstaffmember.ModalResult:=mrOk;

    except
      on e: Exception do
      begin
        dmPSM.sdDJ1.Active:=False;
        dmPSM.sdDJ1.CommandText:='rollback';
        dmPSM.sdDJ1.ExecSQL;

        MessageDlg('An error has been encountered: '+e.Message+#13#10#13#10+'The staff member has not been registered', mtError,[mbOk],0);
        Exit;
      end;
    end;

  end;

  Inc(currentPage);
  registerstaffmember.Caption:='Registering New Member of Staff - Step '+IntToStr(currentPage)+' of 6';
  btnBack.Enabled:=True;
  pcAddNewStaff.ActivePageIndex:=currentPage;
end;

procedure Tregisterstaffmember.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
registerstaffmember:=nil;
Action:=caFree;
end;

end.
