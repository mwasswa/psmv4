unit register_new_student;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinCaramel, dxSkinFoggy, dxSkiniMaginary, cxLabel, cxTextEdit,
  cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  DB, cxDBData, Menus, ExtCtrls, cxButtons, cxCheckBox, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, cxMemo,
  cxButtonEdit, cxDropDownEdit, cxMaskEdit, cxCalendar, ExtDlgs, cxImage, cxPC;

type
  Tregisternewstudent = class(TForm)
    label12studentothernames: TLabel;
    studentdob: TcxDateEdit;
    studentemailaddress: TcxButtonEdit;
    label13studentnationality: TLabel;
    label14studentreligion: TLabel;
    label15studentemailaddress: TLabel;
    label16studentdob: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    cxMemo1: TcxMemo;
    label19familyhomeaddress: TLabel;
    label11gender: TLabel;
    label110homedistrict: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    parentemail: TcxTextEdit;
    parentremarks: TcxTextEdit;
    label113parenttype: TLabel;
    label114parentname: TLabel;
    label1occupation: TLabel;
    label1l16parenttelephone: TLabel;
    label1l17parentemail: TLabel;
    label1l11parentremarks: TLabel;
    gtvParentsInformation: TcxGridDBTableView;
    glParentsInformation: TcxGridLevel;
    gParentsInformation: TcxGrid;
    label1l12admissiondetails: TLabel;
    dateofadmission: TcxDateEdit;
    label115datejoined: TLabel;
    label1ClassJoined: TLabel;
    lookupClassJoined: TcxLookupComboBox;
    lookupCurrentClass: TcxLookupComboBox;
    label1Class: TLabel;
    label1lbl19admissionnumber: TLabel;
    bEditAdmissionNumber: TcxButtonEdit;
    chkIsBoarder: TcxCheckBox;
    label1Dormitory1: TLabel;
    lookupDormitory: TcxLookupComboBox;
    label1l3: TLabel;
    btnBack: TcxButton;
    btnNext: TcxButton;
    btnCancel: TcxButton;
    btnHelp: TcxButton;
    bvl1: TBevel;
    surname: TcxTextEdit;
    bvl2: TBevel;
    othernames: TcxTextEdit;
    gender: TcxComboBox;
    nationality: TcxComboBox;
    homedistrict: TcxTextEdit;
    religion: TcxComboBox;
    parentname: TcxTextEdit;
    parentoccupation: TcxTextEdit;
    parenttelephone: TcxTextEdit;
    parenttype: TcxComboBox;
    cxPageControl1: TcxPageControl;
    cxImage1: TcxImage;
    dlgOpenPic1: TOpenPictureDialog;
    Label1: TLabel;
    Label2: TLabel;
    Loginbtnaddparentsrecords: TButton;
    gtvParentsInformationColumn1: TcxGridDBColumn;
    gtvParentsInformationColumn2: TcxGridDBColumn;
    gtvParentsInformationColumn3: TcxGridDBColumn;
    gtvParentsInformationColumn4: TcxGridDBColumn;
    gtvParentsInformationColumn5: TcxGridDBColumn;
    label1Surname: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnNextClick(Sender: TObject);
    procedure LoginbtnaddparentsrecordsClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnBackClick(Sender: TObject);
  private

  public
    target_term: Integer;
    student_curriculum, student_level: String;
    //importID: TJpegImage;
    IDAssigned: Boolean;

    hold_subjects: Array of Integer;

   // procedure ListSubjects;
    //procedure ListCombinations;
    //procedure ListOptions;

    //function RandomString: string;
    currentPage: Integer;
  end;

var
  registernewstudent: Tregisternewstudent;

implementation

uses data_unit, global_unit,speech_unit;

{$R *.dfm}

procedure Tregisternewstudent.btnBackClick(Sender: TObject);
begin
   currentPage:=currentPage-1;
   btnNext.Enabled:=True;
   btnNext.Caption:='&Next';
   //pcAddNewStudent.ActivePageIndex:=currentPage;
   if currentPage < 1 then
   begin
      btnBack.Enabled:=False;
      registernewstudent.Caption:='Register New Student - Welcome!';
   end
   else
   begin
      registernewstudent.Caption:='Registering New Student - Step '+IntToStr(currentPage)+' of 3';
   end;
end;

procedure Tregisternewstudent.btnCancelClick(Sender: TObject);
begin
if MessageDlg('This shall close the wizard without adding the student. Are you sure you want to do this?', mtConfirmation, [mbNo, mbYes],0) = mrYes then
   begin
      registernewstudent.ModalResult:=mrCancel;
   end;
end;

procedure Tregisternewstudent.btnNextClick(Sender: TObject);

var
  i, newSG_ID, req_length: Integer;
  importIDStream: TMemoryStream;
  student_admission_no, req_index: string;
  is_dup, subject_error: Boolean;
begin
    if Length(Trim(Surname.Text)) = 0 then
    begin
      MessageDlg('Please enter the new student''s surname before proceeding!', mtError, [mbOk],0);
      label1Surname.Font.Color:=clRed;
      Surname.SetFocus;
      Exit;
    end
    else
    begin
      label1Surname.Font.Color:=clBlack;
    end;

    if Length(Trim(othernames.Text)) = 0 then
    begin
      MessageDlg('Please enter the new student''s other names before proceeding!', mtError, [mbOk],0);
      label12studentothernames.Font.Color:=clRed;
      othernames.SetFocus;
      Exit;
    end
    else
    begin
      label12studentothernames.Font.Color:=clBlack;
    end;

    if Length(Trim(studentdob.Text)) = 0 then
    begin
      MessageDlg('Please specify the new student''s Date of Birth before proceeding!', mtError, [mbOk],0);
      label16studentdob.Font.Color:=clRed;
      studentdob.SetFocus;
      Exit;
    end
    else
    begin
      label16studentdob.Font.Color:=clBlack;
    end;

    if Length(Trim(gender.Text)) = 0 then
    begin
      MessageDlg('Please specify the new student''s gender before proceeding!', mtError, [mbOk],0);
      label11gender.Font.Color:=clRed;
      gender.SetFocus;
      Exit;
    end
    else
    begin
      label11gender.Font.Color:=clBlack;
    end;

    if Length(Trim(nationality.Text)) = 0 then
    begin
      MessageDlg('Please specify the new student''s nationality before proceeding!', mtError, [mbOk],0);
      label13studentnationality.Font.Color:=clRed;
      nationality.SetFocus;
      Exit;
    end
    else
    begin
      label13studentnationality.Font.Color:=clBlack;
    end;

    if Length(Trim(homedistrict.Text)) = 0 then
    begin
      MessageDlg('Please specify the new student''s home district before proceeding!', mtError, [mbOk],0);
      label110homedistrict.Font.Color:=clRed;
      homedistrict.SetFocus;
      Exit;
    end
    else
    begin
      label110homedistrict.Font.Color:=clBlack;
    end;

    if gtvParentsInformation.DataController.RecordCount > 0 then
    begin
      for i:=0 to gtvParentsInformation.DataController.RecordCount - 1 do
      begin
        if gtvParentsInformation.DataController.Values[i,5] = 'Y' then
        begin
          if (Length(Trim(gtvParentsInformation.DataController.DisplayTexts[i,1])) = 0) or (Length(Trim(gtvParentsInformation.DataController.DisplayTexts[i,3])) = 0) or (Length(Trim(gtvParentsInformation.DataController.DisplayTexts[i,4])) = 0) then
          begin
            if gtvParentsInformation.DataController.DisplayTexts[i,1] <> 'Other' then
            begin
              if MessageDlg('You have subscribed '+othernames.Text+'''s '+gtvParentsInformation.DataController.DisplayTexts[0,0]+' ['+gtvParentsInformation.DataController.DisplayTexts[0,1]+'] to the myChild service, but you have not entered all of his/her contact details. This may adversely impair the services offered. Do you want to continue?', mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
              begin
                Exit;
              end;
            end
            else
            begin
              if MessageDlg('You have subscribed one of '+Othernames.Text+'''s Next-of-Kin ['+gtvParentsInformation.DataController.DisplayTexts[0,1]+'] to the myChild service, but you have not entered all of his/her contact details. This may adversely impair the services offered. Do you want to continue?', mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
              begin
                Exit;
              end;
            end;
          end;
        end;
      end;
    end
    else
    begin
      if MessageDlg('It is very important to register each student/pupil''s parents and/or next-of-kin! Do you want to continue without doing this?', mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
      begin
        Exit;
      end;
    end;
    if Length(Trim(lookupClassJoined.Text)) = 0 then
    begin
      MessageDlg('Please specify the student''s class at the time of joining before proceeding!', mtError, [mbOk],0);
      label115datejoined.Font.Color:=clRed;
      lookupClassJoined.SetFocus;
      Exit;
    end
    else
    begin
      label1ClassJoined.Font.Color:=clBlack;
    end;

    if Length(Trim(lookupCurrentClass.Text)) = 0 then
    begin
      MessageDlg('Please specify the student''s class before proceeding!', mtError, [mbOk],0);
      label1ClassJoined.Font.Color:=clRed;
      lookupCurrentClass.SetFocus;
      Exit;
    end
    else
    begin
      label1ClassJoined.Font.Color:=clBlack;
    end;

  end;
  {if currentPage = 0 then
  begin
    //welcome page
  end
  else if currentPage = 1 then
  begin
    if Length(Trim(Surname.Text)) = 0 then
    begin
      MessageDlg('Please enter the new student''s surname before proceeding!', mtError, [mbOk],0);
      label1Surname.Font.Color:=clRed;
      Surname.SetFocus;
      Exit;
    end
    else
    begin
      label1Surname.Font.Color:=clBlack;
    end;

    if Length(Trim(othernames.Text)) = 0 then
    begin
      MessageDlg('Please enter the new student''s other names before proceeding!', mtError, [mbOk],0);
      label12studentothernames.Font.Color:=clRed;
      othernames.SetFocus;
      Exit;
    end
    else
    begin
      label12studentothernames.Font.Color:=clBlack;
    end;

    if Length(Trim(gender.Text)) = 0 then
    begin
      MessageDlg('Please specify the new student''s gender before proceeding!', mtError, [mbOk],0);
      label11gender.Font.Color:=clRed;
      gender.SetFocus;
      Exit;
    end
    else
    begin
      label11gender.Font.Color:=clBlack;
    end;

    if gtvParentsInformation.DataController.RecordCount > 0 then
    begin
      for i:=0 to gtvParentsInformation.DataController.RecordCount - 1 do
      begin
        if gtvParentsInformation.DataController.Values[i,5] = 'Y' then
        begin
          if (Length(Trim(gtvParentsInformation.DataController.DisplayTexts[i,1])) = 0) or (Length(Trim(gtvParentsInformation.DataController.DisplayTexts[i,3])) = 0) or (Length(Trim(gtvParentsInformation.DataController.DisplayTexts[i,4])) = 0) then
          begin
            if gtvParentsInformation.DataController.DisplayTexts[i,1] <> 'Other' then
            begin
              if MessageDlg('You have subscribed '+othernames.Text+'''s '+gtvParentsInformation.DataController.DisplayTexts[0,0]+' ['+gtvParentsInformation.DataController.DisplayTexts[0,1]+'] to the myChild service, but you have not entered all of his/her contact details. This may adversely impair the services offered. Do you want to continue?', mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
              begin
                Exit;
              end;
            end
            else
            begin
              if MessageDlg('You have subscribed one of '+Othernames.Text+'''s Next-of-Kin ['+gtvParentsInformation.DataController.DisplayTexts[0,1]+'] to the myChild service, but you have not entered all of his/her contact details. This may adversely impair the services offered. Do you want to continue?', mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
              begin
                Exit;
              end;
            end;
          end;
        end;
      end;
    end
    else
    begin
      if MessageDlg('It is very important to register each student/pupil''s parents and/or next-of-kin! Do you want to continue without doing this?', mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
      begin
        Exit;
      end;
    end;
    if Length(Trim(lookupClassJoined.Text)) = 0 then
    begin
      MessageDlg('Please specify the student''s class at the time of joining before proceeding!', mtError, [mbOk],0);
      label115datejoined.Font.Color:=clRed;
      lookupClassJoined.SetFocus;
      Exit;
    end
    else
    begin
      label1ClassJoined.Font.Color:=clBlack;
    end;

    if Length(Trim(lookupCurrentClass.Text)) = 0 then
    begin
      MessageDlg('Please specify the student''s class before proceeding!', mtError, [mbOk],0);
      label1ClassJoined.Font.Color:=clRed;
      lookupCurrentClass.SetFocus;
      Exit;
    end
    else
    begin
      label1ClassJoined.Font.Color:=clBlack;
    end;

  end
  else
  begin
    MessageDlg('I dont know the current page',mtInformation,[mbOK],0);
  end;  }

procedure Tregisternewstudent.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
registernewstudent:=nil;
Action:=caFree;
end;

procedure Tregisternewstudent.LoginbtnaddparentsrecordsClick(Sender: TObject);
var
 i:Integer;
begin
  if Length(Trim(parenttype.Text)) = 0 then
  begin
    parenttype.SetFocus;
    Exit;
  end;

  if Length(Trim(parentname.Text)) = 0 then
  begin
    parentname.SetFocus;
    Exit;
  end;

  i:=gtvParentsInformation.DataController.RecordCount;
  gtvParentsInformation.DataController.RecordCount:=i + 1;
  gtvParentsInformation.DataController.Values[i,0]:=parenttype.Text;
  gtvParentsInformation.DataController.Values[i,1]:=Trim(parentname.Text);
  gtvParentsInformation.DataController.Values[i,2]:=Trim(parentoccupation.Text);
  gtvParentsInformation.DataController.Values[i,3]:=Trim(parenttelephone.Text);
  gtvParentsInformation.DataController.Values[i,4]:=Trim(parentemail.Text);
  gtvParentsInformation.DataController.Values[i,5]:='N';
  gtvParentsInformation.DataController.Values[i,6]:=Trim(parentremarks.Text);

  parentname.Clear;
  parentoccupation.Clear;
  parenttelephone.Clear;
  parentemail.Clear;
  parenttype.SetFocus;
end;

end.
