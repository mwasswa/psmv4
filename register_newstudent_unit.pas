unit register_newstudent_unit;

interface

uses
  {Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinFoggy,
  dxSkiniMaginary, dxSkinscxPCPainter, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, Menus, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxTextEdit, cxCheckBox, cxDropDownEdit,
  cxSpinEdit, jpeg, ExtCtrls, cxCheckListBox, ExtDlgs, StdCtrls, cxRadioGroup,
  cxImage, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxClasses, cxGridCustomView, cxGrid,
  cxButtons, cxMemo, cxButtonEdit, cxMaskEdit, cxCalendar, cxPC; }

  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, cxButtons, ExtCtrls, cxPC, cxControls, cxContainer,
  cxEdit, cxLabel, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar,
  cxImage, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxMemo,
  cxGroupBox, cxSpinEdit, ExtDlgs, JPEG, cxCheckListBox, cxRadioGroup,
  cxCheckBox, Menus, cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinGlassOceans, dxSkiniMaginary,
  dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin,
  dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinSilver, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxGraphics, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, DB, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridDBTableView, cxGrid, dxSkinDarkSide, dxSkinPumpkin,
  dxSkinSpringTime, LbCipher, LbClass, cxLookAndFeels, dxSkinDarkRoom,
  dxSkinFoggy, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinSeven, dxSkinSharp, cxButtonEdit, StrUtils,
  Math, FMTBcd, SqlExpr, DBClient,Provider,dxGDIPlusClasses;

type
  Tnewstudentregistration = class(TForm)
    registernewstudenthome: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    cxTabSheet3: TcxTabSheet;
    label1Surname: TLabel;
    Label11: TLabel;
    Label8: TLabel;
    surname: TcxTextEdit;
    label12studentothernames: TLabel;
    othernames: TcxTextEdit;
    label16studentdob: TLabel;
    studentdob: TcxDateEdit;
    label11gender: TLabel;
    gender: TcxComboBox;
    label13studentnationality: TLabel;
    nationality: TcxComboBox;
    label110homedistrict: TLabel;
    homedistrict: TcxTextEdit;
    label14studentreligion: TLabel;
    religion: TcxComboBox;
    label15studentemailaddress: TLabel;
    studentemailaddress: TcxButtonEdit;
    bvl2: TBevel;
    label19familyhomeaddress: TLabel;
    familyhomeaddress: TcxMemo;
    Label12: TLabel;
    label113parenttype: TLabel;
    parenttype: TcxComboBox;
    parentname: TcxTextEdit;
    parentoccupation: TcxTextEdit;
    parenttelephone: TcxTextEdit;
    parentemail: TcxTextEdit;
    parentremarks: TcxTextEdit;
    Loginbtnaddparentsrecords: TButton;
    label114parentname: TLabel;
    label1occupation: TLabel;
    label1l16parenttelephone: TLabel;
    label1l17parentemail: TLabel;
    label1l11parentremarks: TLabel;
    btnBack: TcxButton;
    btnNext: TcxButton;
    btnCancel: TcxButton;
    btnHelp: TcxButton;
    gParentsInformation: TcxGrid;
    gtvParentsInformation: TcxGridTableView;
    gcParent: TcxGridColumn;
    gcParentName: TcxGridColumn;
    gcOccupation: TcxGridColumn;
    gcParentTel: TcxGridColumn;
    gcParentEmail: TcxGridColumn;
    gcPSMO: TcxGridColumn;
    gcParentRemarks: TcxGridColumn;
    glParentsInformation: TcxGridLevel;
    label1l3: TLabel;
    bvl1: TBevel;
    label1l1: TLabel;
    label1DateJoined: TLabel;
    dateofadmission: TcxDateEdit;
    label1ClassJoined: TLabel;
    lookupClassJoined: TcxLookupComboBox;
    label1Class: TLabel;
    lookupCurrentClass: TcxLookupComboBox;
    label1lbl19admissionnumber: TLabel;
    bEditAdmissionNumber: TcxButtonEdit;
    chkIsBoarder: TcxCheckBox;
    label1Dormitory1: TLabel;
    lookupDormitory: TcxLookupComboBox;
    bvl3: TBevel;
    cxPageControl2: TcxPageControl;
    cxImage1: TcxImage;
    label1lbl8: TLabel;
    label1l7: TLabel;
    tEditFormerSchool: TcxTextEdit;
    label1l8: TLabel;
    comboFromTerm: TcxComboBox;
    sEditFromYear: TcxSpinEdit;
    label1l9: TLabel;
    comboToTerm: TcxComboBox;
    label1l19: TLabel;
    tEditFormerIndexNo: TcxTextEdit;
    btnAddFormerSchool: TcxButton;
    gFormerSchools: TcxGrid;
    gtvFormerSchools: TcxGridTableView;
    gcFormerSchool: TcxGridColumn;
    gcFromTerm: TcxGridColumn;
    gcFromYear: TcxGridColumn;
    gcToTerm: TcxGridColumn;
    gcToYear: TcxGridColumn;
    gcIndexNo: TcxGridColumn;
    gcQCVerified: TcxGridColumn;
    glFormerSchools: TcxGridLevel;
    label1l2: TLabel;
    rbSummaryResults: TcxRadioButton;
    rbDetailedResults: TcxRadioButton;
    label1lbl22: TLabel;
    comboResultsLevel: TcxComboBox;
    label1Subject: TLabel;
    lookupSubject: TcxLookupComboBox;
    label1ResultSummary: TLabel;
    sEditResultSummary: TcxSpinEdit;
    comboGradeSummary: TcxComboBox;
    comboResultDetailed: TcxComboBox;
    btnAddResult: TcxButton;
    gFormerResults: TcxGrid;
    gtvFormerResultsDetailed: TcxGridTableView;
    gcDetailedLevel: TcxGridColumn;
    gcDetailedSubject: TcxGridColumn;
    gcDetailedResult: TcxGridColumn;
    gcDetailedResultNo: TcxGridColumn;
    gtvFormerResultsSummary: TcxGridTableView;
    gcSummaryLevel: TcxGridColumn;
    gcSummaryResult: TcxGridColumn;
    gcSummaryGrade: TcxGridColumn;
    glFormerResultsDetailed: TcxGridLevel;
    glFormerResultsSummary: TcxGridLevel;
    label1l20: TLabel;
    dlgOpenPicImportID: TOpenPictureDialog;
    pmResults: TPopupMenu;
    mniRemoveResult1: TMenuItem;
    pmParents: TPopupMenu;
    mniRemoveSelectedResponsibility: TMenuItem;
    label1lbl12: TLabel;
    label1SpecifySubjects: TLabel;
    label1lbl18: TLabel;
    label1lbl13: TLabel;
    bvl4: TBevel;
    rbSubjectSubjects: TcxRadioButton;
    rbSubjectCombinations: TcxRadioButton;
    rbSubjectOptions: TcxRadioButton;
    label1Combination: TLabel;
    tEditCombination: TcxTextEdit;
    bvl5: TBevel;
    chkLstElements: TcxCheckListBox;
    label1lbl17: TLabel;
    label1lbl20: TLabel;
    chkDisabled: TcxCheckBox;
    label1Disabled: TLabel;
    label1l4: TLabel;
    memoAdditional: TcxMemo;
    cxTabSheet4: TcxTabSheet;
    img1: TImage;
    label1lbl1: TLabel;
    label1lbl2: TLabel;
    label1lbl3: TLabel;
    bvl6: TBevel;
    label1specifysubjects1: TLabel;
    procedure btnNextClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnBackClick(Sender: TObject);
    procedure LoginbtnaddparentsrecordsClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure lookupClassJoinedMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lookupCurrentClassMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure chkIsBoarderClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnAddFormerSchoolClick(Sender: TObject);
    procedure sEditResultSummaryPropertiesChange(Sender: TObject);
    procedure btnAddResultClick(Sender: TObject);
    procedure rbSubjectSubjectsClick(Sender: TObject);
    procedure rbSubjectCombinationsClick(Sender: TObject);
    procedure rbSubjectOptionsClick(Sender: TObject);
    procedure comboResultsLevelPropertiesEditValueChanged(Sender: TObject);
  private
    target_term: Integer;
    student_curriculum, student_level: String;
    importID: TJpegImage;
    IDAssigned: Boolean;

    hold_subjects: Array of Integer;

    procedure ListSubjects;
    procedure ListCombinations;
    procedure ListOptions;

    function RandomString: string;
  public
    currentPage: Integer;
  protected
    sduseregnewstd,sdDJ,sdstreamregnewstd,sdcombinationregnewstd,sddistrictregnewstd :Tsqldataset;
    dspuseregnewstd,dspDJ,dspdistrictregnewstd,dspcombinationregnewstd,dspstreamregnewstd :TDataSetProvider;
    cduseregnewstd,cdDJ,cdstreamregnewstd,cdcombinationregnewstd,cddistrictregnewstd:Tclientdataset;
    dsuseregnewstd,dDJ,dsdistrictregnewstd,dscombinationregnewstd,dsstreamregnewstd: TDataSource;
  end;

var
  newstudentregistration: Tnewstudentregistration;

implementation

uses global_unit, data_unit,speech_unit;

{$R *.dfm}

procedure Tnewstudentregistration.btnAddFormerSchoolClick(Sender: TObject);
var
i:Integer;
begin
  if Length(Trim(tEditFormerSchool.Text)) = 0 then
  begin
    tEditFormerSchool.SetFocus;
    Exit;
  end;

  if Length(Trim(comboFromTerm.Text)) = 0 then
  begin
    comboFromTerm.SetFocus;
    Exit;
  end;

  if Length(Trim(comboToTerm.Text)) = 0 then
  begin
    comboToTerm.SetFocus;
    Exit;
  end;

  i:=gtvFormerSchools.DataController.RecordCount;
  gtvFormerSchools.DataController.RecordCount:=i + 1;
  gtvFormerSchools.DataController.Values[i,0]:=tEditFormerSchool.Text;
  gtvFormerSchools.DataController.Values[i,1]:=Trim(comboFromTerm.Text);
  gtvFormerSchools.DataController.Values[i,2]:=Trim(sEditFromYear.Text);
  gtvFormerSchools.DataController.Values[i,3]:=Trim(comboToTerm.Text);
  gtvFormerSchools.DataController.Values[i,4]:=Trim(tEditFormerIndexNo.Text);
  gtvFormerSchools.DataController.Values[i,5]:='N';

  tEditFormerSchool.Clear;
  comboFromTerm.Clear;
  sEditFromYear.Clear;
  comboToTerm.Clear;
  tEditFormerIndexNo.Clear;
end;

procedure Tnewstudentregistration.btnAddResultClick(Sender: TObject);
var
  i, result_no: Integer;
  add_result: Boolean;
begin
if Length(Trim(comboResultsLevel.Text)) = 0 then
  begin
    comboResultsLevel.SetFocus;
    Exit;
  end;

  if rbDetailedResults.Checked then
  begin
    if Length(Trim(lookupSubject.Text)) = 0 then
    begin
      MessageDlg('Please enter the subject', mtInformation, [mbOK], 0);
      lookupSubject.SetFocus;
      Exit;
    end;

    if Length(Trim(comboResultDetailed.Text)) = 0 then
    begin
      MessageDlg('Please enter the result obtained', mtInformation, [mbOK], 0);
      comboResultDetailed.SetFocus;
      Exit;
    end;

    //check if subject has already been added
    add_result:=True;

    for i := 0 to gtvFormerResultsDetailed.DataController.RecordCount - 1 do   // Iterate
    begin
      if (gtvFormerResultsDetailed.DataController.Values[i, 0] = Trim(comboResultsLevel.Text)) and (gtvFormerResultsDetailed.DataController.Values[i, 1] = lookupSubject.Text) then
      begin
        add_result:=False;
        Break;
      end;
    end;

    if not add_result then
    begin
      MessageDlg('This subject result ('+comboResultsLevel.Text+' '+lookupSubject.Text+') has already been added for this new student', mtInformation, [mbOK], 0);
      Exit;
    end;

    //get the result no.
    result_no:=0;

    case comboResultsLevel.ItemIndex of
      0..1: begin
           if comboResultDetailed.ItemIndex  < 9 then  result_no:=comboResultDetailed.ItemIndex + 1
           else result_no:=0;
         end;
      2: begin
           if comboResultDetailed.Properties.Items.Count < 9  then
           begin
             case comboResultDetailed.ItemIndex of
               0: result_no:=6;
               1: result_no:=5;
               2: result_no:=4;
               3: result_no:=3;
               4: result_no:=2;
               5: result_no:=1;
               6: result_no:=0;
               7: result_no:=0;
             end;
           end
           else
           begin
             if comboResultDetailed.ItemIndex  < 6 then result_no:= 1
             else result_no:=0;
           end;
         end;
    end;

    i:=gtvFormerResultsDetailed.DataController.RecordCount;
    gtvFormerResultsDetailed.DataController.RecordCount:=i + 1;
    gtvFormerResultsDetailed.DataController.Values[i,0]:=Trim(comboResultsLevel.Text);
    gtvFormerResultsDetailed.DataController.Values[i,1]:=lookupSubject.Text;
    gtvFormerResultsDetailed.DataController.Values[i,2]:=comboResultDetailed.Text;
    gtvFormerResultsDetailed.DataController.Values[i,3]:=result_no;

    lookupSubject.Clear;
    comboResultDetailed.Clear;
    lookupSubject.SetFocus;
  end
  else
  begin
    if sEditResultSummary.Value <= 0 then
    begin
      MessageDlg('Please enter the result scored', mtInformation, [mbOK], 0);
      sEditResultSummary.SetFocus;
      Exit;
    end;

    if Length(Trim(comboGradeSummary.Text)) = 0 then
    begin
      MessageDlg('Please enter the grade obtained', mtInformation, [mbOK], 0);
      comboGradeSummary.SetFocus;
      Exit;
    end;

    //check if result has already been added
    add_result:=True;

    for i := 0 to gtvFormerResultsSummary.DataController.RecordCount - 1 do   // Iterate
    begin
      if (gtvFormerResultsSummary.DataController.Values[i, 0] = Trim(comboResultsLevel.Text)) then
      begin
        add_result:=False;
        Break;
      end;
    end;

    if not add_result then
    begin
      MessageDlg('This summary result ('+comboResultsLevel.Text+') has already been added for this new student', mtInformation, [mbOK], 0);
      Exit;
    end;

    i:=gtvFormerResultsSummary.DataController.RecordCount;
    gtvFormerResultsSummary.DataController.RecordCount:=i + 1;
    gtvFormerResultsSummary.DataController.Values[i,0]:=Trim(comboResultsLevel.Text);
    gtvFormerResultsSummary.DataController.Values[i,1]:=sEditResultSummary.Value;
    gtvFormerResultsSummary.DataController.Values[i,2]:=comboGradeSummary.Text;

    sEditResultSummary.Clear;
    comboGradeSummary.Clear;
    comboResultsLevel.SetFocus;
  end;
end;

procedure Tnewstudentregistration.btnBackClick(Sender: TObject);
begin
currentPage:=currentPage-1;
   btnNext.Enabled:=True;
   btnNext.Caption:='&Next';
   registernewstudenthome.ActivePageIndex:=currentPage;
   if currentPage < 1 then
   begin
      btnBack.Enabled:=False;
      newstudentregistration.Caption:='Register New Student - Welcome!';
   end
   else
   begin
      newstudentregistration.Caption:='Registering New Student - Step '+IntToStr(currentPage)+' of 3';
   end;
end;

procedure Tnewstudentregistration.btnCancelClick(Sender: TObject);
begin
if MessageDlg('This shall close the wizard without adding the student. Are you sure you want to do this?', mtConfirmation, [mbNo, mbYes],0) = mrYes then
   begin
      newstudentregistration.ModalResult:=mrCancel;
   end;
end;

procedure Tnewstudentregistration.btnNextClick(Sender: TObject);
var
  i, newSG_ID, req_length: Integer;
  importIDStream: TMemoryStream;
  student_admission_no, req_index: string;
  is_dup, subject_error: Boolean;
begin
  if currentPage = 0 then
  begin
    //welcome page
  end;

  if currentPage = 1 then
  begin
    if Length(Trim(surname.Text)) = 0 then
    begin
      MessageDlg('Please enter the new student''s surname before proceeding!', mtError, [mbOk],0);
      label1Surname.Font.Color:=clRed;
      surname.SetFocus;
      Exit;
    end
    else
    begin
      label1Surname.Font.Color:=clBlack;
    end;

    if Length(Trim(othernames.Text)) = 0 then
    begin
      MessageDlg('Please enter the new student''s other names before proceeding!', mtError, [mbOk],0);
      label12studentothernames.Font.Color:=clRed;
      othernames.SetFocus;
      Exit;
    end
    else
    begin
      label12studentothernames.Font.Color:=clBlack;
    end;

    {if Length(Trim(dEditDOB.Text)) = 0 then
    begin
      MessageDlg('Please enter the new student''s date of birth before proceeding!', mtError, [mbOk],0);
      lDOB.Font.Color:=clRed;
      dEditDOB.SetFocus;
      Exit;
    end
    else
    begin
      lDOB.Font.Color:=clBlack;
    end;}

    if Length(Trim(gender.Text)) = 0 then
    begin
      MessageDlg('Please specify the new student''s gender before proceeding!', mtError, [mbOk],0);
      label11gender.Font.Color:=clRed;
      gender.SetFocus;
      Exit;
    end
    else
    begin
      label11gender.Font.Color:=clBlack;
    end;

    {if Length(Trim(comboNationality.Text)) = 0 then
    begin
      MessageDlg('Please enter the new student''s nationality before proceeding!', mtError, [mbOk],0);
      lNationality.Font.Color:=clRed;
      comboNationality.SetFocus;
      Exit;
    end
    else
    begin
      lNationality.Font.Color:=clBlack;
    end;

    if Length(Trim(tEditHomeDistrict.Text)) = 0 then
    begin
      MessageDlg('Please enter the new student''s home district (or equivalent) before proceeding!', mtError, [mbOk],0);
      lHomeDistrict.Font.Color:=clRed;
      tEditHomeDistrict.SetFocus;
      Exit;
    end
    else
    begin
      lHomeDistrict.Font.Color:=clBlack;
    end; }

    if gtvParentsInformation.DataController.RecordCount > 0 then
    begin
      for i:=0 to gtvParentsInformation.DataController.RecordCount - 1 do
      begin
        if gtvParentsInformation.DataController.Values[i,5] = 'Y' then
        begin
          if (Length(Trim(gtvParentsInformation.DataController.DisplayTexts[i,1])) = 0) or (Length(Trim(gtvParentsInformation.DataController.DisplayTexts[i,3])) = 0) or (Length(Trim(gtvParentsInformation.DataController.DisplayTexts[i,4])) = 0) then
          begin
            if gtvParentsInformation.DataController.DisplayTexts[i,1] <> 'Other' then
            begin
              if MessageDlg('You have subscribed '+othernames.Text+'''s '+gtvParentsInformation.DataController.DisplayTexts[0,0]+' ['+gtvParentsInformation.DataController.DisplayTexts[0,1]+'] to the myChild service, but you have not entered all of his/her contact details. This may adversely impair the services offered. Do you want to continue?', mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
              begin
                Exit;
              end;
            end
            else
            begin
              if MessageDlg('You have subscribed one of '+othernames.Text+'''s Next-of-Kin ['+gtvParentsInformation.DataController.DisplayTexts[0,1]+'] to the myChild service, but you have not entered all of his/her contact details. This may adversely impair the services offered. Do you want to continue?', mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
              begin
                Exit;
              end;
            end;
          end;
        end;
      end;
    end
    else
    begin
      if MessageDlg('It is very important to register each student/pupil''s parents and/or next-of-kin! Do you want to continue without doing this?', mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
      begin
        Exit;
      end;
    end;

    {if Length(Trim(dEditDateJoined.Text)) = 0 then
    begin
      MessageDlg('Please specify the date the new student joined the school before proceeding!', mtError, [mbOk],0);
      lDateJoined.Font.Color:=clRed;
      dEditDateJoined.SetFocus;
      Exit;
    end
    else
    begin
      lDateJoined.Font.Color:=clBlack;
    end;}

    if Length(Trim(lookupClassJoined.Text)) = 0 then
    begin
      MessageDlg('Please specify the student''s class at the time of joining before proceeding!', mtError, [mbOk],0);
      label1ClassJoined.Font.Color:=clRed;
      lookupClassJoined.SetFocus;
      Exit;
    end
    else
    begin
      label1ClassJoined.Font.Color:=clBlack;
    end;

    if Length(Trim(lookupCurrentClass.Text)) = 0 then
    begin
      MessageDlg('Please specify the student''s class before proceeding!', mtError, [mbOk],0);
      label1ClassJoined.Font.Color:=clRed;
      lookupCurrentClass.SetFocus;
      Exit;
    end
    else
    begin
      label1ClassJoined.Font.Color:=clBlack;
    end;

    {if (dEditDateJoined.Date <= dEditDOB.Date) or (dEditDateJoined.Date > Date) then
    begin
      MessageDlg('Either the student''s date of birth or the date joined is invalid! Please check them before proceeding', mtError, [mbOk],0);
      lDateJoined.Font.Color:=clRed;
      lDOB.Font.Color:=clRed;
      dEditDateJoined.SetFocus;
      Exit;
    end
    else
    begin
      lDateJoined.Font.Color:=clBlack;
      lDOB.Font.Color:=clBlack;
    end;}

    {if (chkIsBoarder.Checked) and (Length(Trim(lookUpDormitory.Text)) = 0) then
    begin
      MessageDlg('Please specify the student''s dormitory or hostel before proceeding!', mtError, [mbOk],0);
      lDormitory.Font.Color:=clRed;
      lookUpDormitory.SetFocus;
      Exit;
    end
    else
    begin
      lDormitory.Font.Color:=clBlack;
    end;}

  end;

  if currentPage = 2 then
  begin
    label1specifysubjects1.Caption:='Please specify all the subjects\papers '+othernames.Text+' is taking';

    btnNext.Caption:='&Finish';
  end;

  if currentPage = 3 then
  begin
    student_admission_no:=Trim(bEditAdmissionNumber.Text);

    sduseregnewstd.Active:=False;
    sduseregnewstd.CommandText:='select PSM_STUDENT_ADMISSION_NO_LENGTH from psm_school_settings';
    sduseregnewstd.Open;

    req_length:= sduseregnewstd.Fields[0].AsInteger;

    if Pos('$i', student_admission_no) > 0 then
    begin
      sduseregnewstd.Active:=False;
      sduseregnewstd.CommandText:='select SEQ_ADMISSION_NO from psm_sequences where SEQ_YEAR = :SEQ_YEAR and SEQ_TERM = :SEQ_TERM ';
      sduseregnewstd.Params[0].AsInteger:=global_unit.current_year;
      sduseregnewstd.Params[1].AsInteger:=target_term;
      sduseregnewstd.Open;

      req_index:=sduseregnewstd.Fields[0].AsString;

      while Length(req_index) < req_length do
      begin
        req_index:='0'+req_index;
      end;
    end;
    sduseregnewstd.Active:=False;

    student_admission_no:=AnsiReplaceText(student_admission_no, '$i', req_index);

    Randomize;

    sduseregnewstd.Active:=False;
    sduseregnewstd.CommandText:='start transaction';
    sduseregnewstd.ExecSQL;

    //*check uniqueness of the admission no.

    sduseregnewstd.Active:=False;
    sduseregnewstd.CommandText:='insert into students_general (SG_SURNAME,SG_OTHERNAME,SG_DOB,SG_SEX,SG_CLASS,SG_IS_BOARDER,SG_DORMITORY,SG_RELIGION,SG_DISTRICT,SG_NATIONALITY,SG_HOME_ADDRESS,SG_JOINED_DATE,'+
                               'SG_CLASS_JOINED,SG_COMBINATION,SG_PIN_CODE,SG_EMAIL,SG_ADMISSION_NUMBER,SG_OTHER_INFO,SG_FORMER_RESULTS_TYPE,SG_CREATED_TIME,SG_CREATED_BY,MODIFIED_TIME,SG_MODIFIED_BY,SG_DISABLED) '+
                               'values (:SG_SURNAME,:SG_OTHERNAME,:SG_DOB,:SG_SEX,:SG_CLASS,:SG_IS_BOARDER,:SG_DORMITORY,:SG_RELIGION,:SG_DISTRICT,:SG_NATIONALITY,:SG_HOME_ADDRESS,:SG_JOINED_DATE,'+
                               ':SG_CLASS_JOINED,:SG_COMBINATION,:SG_PIN_CODE,:SG_EMAIL,:SG_ADMISSION_NUMBER,:SG_OTHER_INFO,:SG_FORMER_RESULTS_TYPE,now(),:SG_CREATED_BY,now(),:SG_MODIFIED_BY,:SG_DISABLED) ';

    sduseregnewstd.Params[0].AsString:=Trim(surname.Text);
    sduseregnewstd.Params[1].AsString:=Trim(othernames.Text);
    if Length(studentdob.Text) > 0 then
    begin
      sduseregnewstd.Params[2].AsDate:=studentdob.Date;
    end
    else
    begin
       sduseregnewstd.Params[2].Clear;
       sduseregnewstd.Params[2].Bound:=True;
       sduseregnewstd.Params[2].DataType:=ftDate;
    end;
    sduseregnewstd.Params[3].AsString:=(gender.Text)[1];
    sduseregnewstd.Params[4].AsString:=Trim(lookupCurrentClass.Text);
    sduseregnewstd.Params[5].AsString:=chkIsBoarder.EditValue;
    if chkIsBoarder.Checked then sduseregnewstd.Params[6].AsString:=lookUpDormitory.Text
    else sduseregnewstd.Params[6].AsString:='';
    sduseregnewstd.Params[7].AsString:=Trim(religion.Text);
    sduseregnewstd.Params[8].AsString:=Trim(familyhomeaddress.Text);
    sduseregnewstd.Params[9].AsString:=Trim(nationality.Text);
    sduseregnewstd.Params[10].AsString:=Trim(familyhomeaddress.Text);
    if Length(dateofadmission.Text) > 0 then
    begin
      sduseregnewstd.Params[11].AsDate:=dateofadmission.Date;
    end
    else
    begin
       sduseregnewstd.Params[11].Clear;
       sduseregnewstd.Params[11].Bound:=True;
       sduseregnewstd.Params[11].DataType:=ftDate;
    end;
    sduseregnewstd.Params[12].AsString:=Trim(lookupClassJoined.Text);
    if student_level = 'A' then  sduseregnewstd.Params[13].AsString:=Trim(tEditCombination.Text)
    else sduseregnewstd.Params[13].AsString:='';
    sduseregnewstd.Params[14].AsInteger:=Random(8999)+1000;
    sduseregnewstd.Params[15].AsString:=Trim(studentemailaddress.Text);
    sduseregnewstd.Params[16].AsString:=student_admission_no;
    sduseregnewstd.Params[17].AsString:=Trim(memoAdditional.Text);

    if rbDetailedResults.Checked then
    begin
      if gtvFormerResultsDetailed.DataController.RecordCount > 0 then sduseregnewstd.Params[18].AsString:='D'
      else sduseregnewstd.Params[18].AsString:='N';
    end
    else if rbSummaryResults.Checked then
    begin
      if gtvFormerResultsSummary.DataController.RecordCount > 0 then sduseregnewstd.Params[18].AsString:='S'
      else sduseregnewstd.Params[18].AsString:='N';
    end
    else
    begin
      sduseregnewstd.Params[18].AsString:='N';
    end;

    sduseregnewstd.Params[19].AsInteger:=global_unit.current_user_id;
    sduseregnewstd.Params[20].AsInteger:=global_unit.current_user_id;
    sduseregnewstd.Params[21].AsString:=chkDisabled.EditValue;

    try
      sduseregnewstd.ExecSQL;

      sduseregnewstd.Active:=False;
      sduseregnewstd.CommandText:='select MAX(SG_ID) from students_general';
      sduseregnewstd.Open;

      newSG_ID:=sduseregnewstd.Fields[0].AsInteger;

      //Parents and NOK
      if gtvParentsInformation.DataController.RecordCount > 0 then
      begin
        for i:=0 to gtvParentsInformation.DataController.RecordCount - 1 do
        begin
          sduseregnewstd.Active:=False;
          sduseregnewstd.CommandText:='insert into students_parents (SP_SG_ID,SP_TYPE,SP_NAME,SP_TEL,SP_EMAIL,SP_OCCUPATION,SP_REMARKS,SP_PSMO_X_CODE,SP_PSMO_SUBSCRIBE,SP_CREATED_TIME,SP_CREATED_BY)'+
                                     'values (:SP_SG_ID,:SP_TYPE,:SP_NAME,:SP_TEL,:SP_EMAIL,:SP_OCCUPATION,:SP_REMARKS,:SP_PSMO_X_CODE,:SP_PSMO_SUBSCRIBE,now(),:SP_CREATED_BY)';

          sduseregnewstd.Params[0].AsInteger:=newSG_ID;
          sduseregnewstd.Params[1].AsString:=Trim(gtvParentsInformation.DataController.DisplayTexts[i,0]);
          sduseregnewstd.Params[2].AsString:=Trim(gtvParentsInformation.DataController.DisplayTexts[i,1]);
          sduseregnewstd.Params[3].AsString:=Trim(gtvParentsInformation.DataController.DisplayTexts[i,3]);
          sduseregnewstd.Params[4].AsString:=Trim(gtvParentsInformation.DataController.DisplayTexts[i,4]);
          sduseregnewstd.Params[5].AsString:=Trim(gtvParentsInformation.DataController.DisplayTexts[i,2]);
          sduseregnewstd.Params[6].AsString:=Trim(gtvParentsInformation.DataController.DisplayTexts[i,6]);
          sduseregnewstd.Params[7].AsString:=Copy(RandomString, 1, 5);
          sduseregnewstd.Params[8].AsString:=gtvParentsInformation.DataController.Values[i,5];
          sduseregnewstd.Params[9].AsInteger:=global_unit.current_user_id;
          try
            sduseregnewstd.ExecSQL;
          except
            on e: Exception do
            begin
              MessageDlg('Error encountered during the posting of the new student''s parents and next-of-kin: '+e.Message, mtError, [mbOK], 0);
            end;
          end;
        end;
      end;

      //Previous Schools
      if gtvFormerSchools.DataController.RecordCount > 0 then
      begin
        for i:=0 to gtvFormerSchools.DataController.RecordCount - 1 do
        begin
          sduseregnewstd.Active:=False;
          sduseregnewstd.CommandText:='insert into students_previous_schools (SPS_SG_ID, SPS_SCHOOL, SPS_ATTENDED_FROM_TERM, SPS_ATTENDED_FROM_YEAR, SPS_ATTENDED_TO_TERM, SPS_ATTENDED_TO_YEAR, SPS_INDEX_NO, SPS_VERIFIED, SPS_POSTED_BY) '+
                                     'values (:SPS_SG_ID, :SPS_SCHOOL, :SPS_ATTENDED_FROM_TERM, :SPS_ATTENDED_FROM_YEAR, :SPS_ATTENDED_TO_TERM, :SPS_ATTENDED_TO_YEAR, :SPS_INDEX_NO, :SPS_VERIFIED, :SPS_POSTED_BY)';

          sduseregnewstd.Params[0].AsInteger:=newSG_ID;
          sduseregnewstd.Params[1].AsString:=Trim(gtvFormerSchools.DataController.DisplayTexts[i,0]);

          if Length(Trim(gtvFormerSchools.DataController.DisplayTexts[i,1])) > 0 then sduseregnewstd.Params[2].AsString:=(Trim(gtvFormerSchools.DataController.DisplayTexts[i,1]))[2]
          else
          begin
            sduseregnewstd.Params[2].Clear;
            sduseregnewstd.Params[2].Bound:=True;
            sduseregnewstd.Params[2].DataType:=ftInteger;
          end;

          if Length(gtvFormerSchools.DataController.DisplayTexts[i,2]) > 0 then sduseregnewstd.Params[3].AsInteger:=gtvFormerSchools.DataController.Values[i,2]
          else
          begin
            sduseregnewstd.Params[3].Clear;
            sduseregnewstd.Params[3].Bound:=True;
            sduseregnewstd.Params[3].DataType:=ftInteger;
          end;

          if Length(Trim(gtvFormerSchools.DataController.DisplayTexts[i,3])) > 0 then sduseregnewstd.Params[4].AsString:=(Trim(gtvFormerSchools.DataController.DisplayTexts[i,3]))[2]
          else
          begin
            sduseregnewstd.Params[4].Clear;
            sduseregnewstd.Params[4].Bound:=True;
            sduseregnewstd.Params[4].DataType:=ftInteger;
          end;

          if Length(gtvFormerSchools.DataController.DisplayTexts[i,4]) > 0 then sduseregnewstd.Params[5].AsInteger:=gtvFormerSchools.DataController.Values[i,4]
          else
          begin
            sduseregnewstd.Params[5].Clear;
            sduseregnewstd.Params[5].Bound:=True;
            sduseregnewstd.Params[5].DataType:=ftInteger;
          end;

          sduseregnewstd.Params[6].AsString:=Trim(gtvFormerSchools.DataController.DisplayTexts[i,5]);
          sduseregnewstd.Params[7].AsString:=Trim(gtvFormerSchools.DataController.Values[i,6]);
          sduseregnewstd.Params[8].AsInteger:=global_unit.current_user_id;

          try
            sduseregnewstd.ExecSQL;
          except
            on e: Exception do
            begin
              MessageDlg('Error encountered during the posting of the new student''s former schools: '+e.Message, mtError, [mbOK], 0);
            end;
          end;
        end;
      end;

      //Previous Academic Performance
      if gtvFormerResultsDetailed.DataController.RecordCount > 0 then
      begin
        for i:=0 to gtvFormerResultsDetailed.DataController.RecordCount - 1 do
        begin
          sduseregnewstd.Active:=False;
          sduseregnewstd.CommandText:='insert into students_previous_education_results_details (SPERD_SG_ID, SPERD_WHICH_RESULT, SPERD_SUBJECT, SPERD_GRADE_NO, SPERD_GRADE, SPERD_POSTED_BY) '+
                                     'values (:SPERD_SG_ID, :SPERD_WHICH_RESULT, :SPERD_SUBJECT, :SPERD_GRADE_NO, :SPERD_GRADE, :SPERD_POSTED_BY) ';

          sduseregnewstd.Params[0].AsInteger:=newSG_ID;
          sduseregnewstd.Params[1].AsString:=Trim(gtvFormerResultsDetailed.DataController.DisplayTexts[i,0]);
          sduseregnewstd.Params[2].AsString:=Trim(gtvFormerResultsDetailed.DataController.DisplayTexts[i,1]);
          sduseregnewstd.Params[3].AsInteger:=gtvFormerResultsDetailed.DataController.Values[i,3];
          sduseregnewstd.Params[4].AsString:=Trim(gtvFormerResultsDetailed.DataController.DisplayTexts[i,2]);
          sduseregnewstd.Params[5].AsInteger:=global_unit.current_user_id;
          sduseregnewstd.ExecSQL;
        end;
      end;

      if gtvFormerResultsSummary.DataController.RecordCount > 0 then
      begin
        for i:=0 to gtvFormerResultsSummary.DataController.RecordCount - 1 do
        begin
          sduseregnewstd.Active:=False;
          sduseregnewstd.CommandText:='insert into students_previous_education_results_summary (SPERS_SG_ID, SPERS_WHICH_RESULT, SPERS_RESULT, SPERS_GRADE, SPERS_POSTED_BY) '+
                                     'values (:SPERS_SG_ID, :SPERS_WHICH_RESULT, :SPERS_RESULT, :SPERS_GRADE, :SPERS_POSTED_BY) ';

          sduseregnewstd.Params[0].AsInteger:=newSG_ID;
          sduseregnewstd.Params[1].AsString:=Trim(gtvFormerResultsSummary.DataController.Values[i,0]);
          sduseregnewstd.Params[2].AsInteger:=gtvFormerResultsSummary.DataController.Values[i,1];
          sduseregnewstd.Params[3].AsString:=Trim(gtvFormerResultsSummary.DataController.Values[i,2]);
          sduseregnewstd.Params[4].AsInteger:=global_unit.current_user_id;
          sduseregnewstd.ExecSQL;
        end;
      end;


      //Initial Progress History
      sduseregnewstd.Active:=False;
      sduseregnewstd.CommandText:='insert into students_progress_history (SPH_YEAR, SPH_TERM, SPH_SG_ID, SPH_CLASS_OLD, SPH_CLASS_NEW, SPH_INFORMATION, SPH_POSTED_TIME, SPH_POSTED_BY) '+
                                'values (:SPH_YEAR, :SPH_TERM, :SPH_SG_ID, :SPH_CLASS_OLD, :SPH_CLASS_NEW, :SPH_INFORMATION, now(), :SPH_POSTED_BY) ';

      sduseregnewstd.Params[0].AsInteger:=global_unit.current_year;   //*change to class term
      sduseregnewstd.Params[1].AsInteger:=target_term;
      sduseregnewstd.Params[2].AsInteger:=newSG_ID;
      sduseregnewstd.Params[3].AsString:=Trim(lookupClassJoined.Text);
      sduseregnewstd.Params[4].AsString:=Trim(lookupCurrentClass.Text);
      sduseregnewstd.Params[5].AsString:='PSM Registration';
      sduseregnewstd.Params[6].AsInteger:=global_unit.current_user_id;
      sduseregnewstd.ExecSQL;

      sduseregnewstd.Active:=False;
      sduseregnewstd.CommandText:='commit';
      sduseregnewstd.ExecSQL;

      //Assigned Subjects
      subject_error:=False;
      try

        if rbSubjectSubjects.Checked then
  			begin
          //if kindergarten or primary!
          if (student_level = 'K') or (student_level = 'P') or (student_level = 'CP') then //probably cambridge primary as well
          begin
            sduseregnewstd.Active:=False;
            sduseregnewstd.CommandText:='insert into students_marks (M_SG_ID, M_CLASS, M_STREAM, M_YEAR, M_TERM, M_SS_ID, M_CREATED_TIME, M_CREATED_BY, MODIFIED_TIME, M_MODIFIED_BY) '+
                                       'values (:M_SG_ID, :M_CLASS, :M_STREAM, :M_YEAR, :M_TERM, :M_SS_ID, now(), :M_CREATED_BY, now(), :M_MODIFIED_BY ) ';
          end
          else
          begin
            sduseregnewstd.Active:=False;
            sduseregnewstd.CommandText:='insert into students_marks (M_SG_ID, M_CLASS, M_STREAM, M_YEAR, M_TERM, M_SSP_ID, M_CREATED_TIME, M_CREATED_BY, MODIFIED_TIME, M_MODIFIED_BY) '+
                                       'values (:M_SG_ID, :M_CLASS, :M_STREAM, :M_YEAR, :M_TERM, :M_SSP_ID, now(), :M_CREATED_BY, now(), :M_MODIFIED_BY ) ';
          end;

          for i:= 0 to chkLstElements.Items.Count - 1 do    // Iterate
          begin
            if chkLstElements.Items[i].Checked then
            begin
              // for
              sduseregnewstd.Params[0].AsInteger:=newSG_ID;
              sduseregnewstd.Params[1].AsString:=cduseregnewstd.FieldByName('SC_CLASS').AsString;
              sduseregnewstd.Params[2].AsString:=cduseregnewstd.FieldByName('SC_STREAM').AsString;
              sduseregnewstd.Params[3].AsInteger:=global_unit.current_year;
              sduseregnewstd.Params[4].AsInteger:=target_term;
              sduseregnewstd.Params[5].AsInteger:=hold_subjects[i];
              sduseregnewstd.Params[6].AsInteger:=global_unit.current_user_id;
              sduseregnewstd.Params[7].AsInteger:=global_unit.current_user_id;

              try
                sduseregnewstd.ExecSQL;

                dmPSM.sdUsage.Active:=False;
                dmPSM.sdUsage.Params[0].AsString:='STAFF';
                dmPSM.sdUsage.Params[1].AsInteger:=global_unit.current_user_id;
                dmPSM.sdUsage.Params[2].AsString:='ADMIN';
                dmPSM.sdUsage.Params[3].AsString:='Assigned '+chkLstElements.Items[i].Text+' to '+Trim(surname.Text+' '+othernames.Text)+' ['+lookupCurrentClass.Text+'] - Initial Reg.';
                dmPSM.sdUsage.Params[4].AsString:=global_unit.user_ip;
                dmPSM.sdUsage.Params[5].AsString:=global_unit.user_host;
                try
                  dmPSM.sdUsage.ExecSQL;
                except
                end;

              except
                //**update M_OPTION = 'N' taking care of K & P levels as well
                subject_error:=True;
              end;
            end;
          end;

        end
        else if rbSubjectCombinations.Checked then
        begin
          for i:= 0 to chkLstElements.Items.Count - 1 do    // Iterate
          begin
            if chkLstElements.Items[i].Checked then
            begin

              sdDJ.Active:=False;
              sdDJ.CommandText:='select P.SSP_SS_ID, P.SSP_ID, P.SSP_PAPER, S.SS_SUBJECT from school_subjects_papers P inner join school_subjects S on P.SSP_SS_ID = S.SS_ID '+
                                         'where P.SSP_SS_ID in (select SSCS_SS_ID from school_subjects_combinations_subjects where SSCS_SSC_ID = :SSCS_SSC_ID) order by P.SSP_SS_ID ';

              sdDJ.Params[0].AsInteger:=hold_subjects[i];
              sdDJ.Open;

              while not sdDJ.Eof do
              begin
                sduseregnewstd.Active:=False;
                sduseregnewstd.CommandText:='insert into students_marks (M_SG_ID, M_CLASS, M_STREAM, M_YEAR, M_TERM, M_SSP_ID, M_CREATED_TIME, M_CREATED_BY, MODIFIED_TIME, M_MODIFIED_BY) '+
                                           'values (:M_SG_ID, :M_CLASS, :M_STREAM, :M_YEAR, :M_TERM, :M_SSP_ID, now(), :M_CREATED_BY, now(), :M_MODIFIED_BY ) ';

                sduseregnewstd.Params[0].AsInteger:=newSG_ID;
                sduseregnewstd.Params[1].AsString:=cduseregnewstd.FieldByName('SC_CLASS').AsString;
                sduseregnewstd.Params[2].AsString:=cduseregnewstd.FieldByName('SC_STREAM').AsString;
                sduseregnewstd.Params[3].AsInteger:=global_unit.current_year;
                sduseregnewstd.Params[4].AsInteger:=target_term;
                sduseregnewstd.Params[5].AssignField(sdDJ.FieldByName('SSP_ID'));
                sduseregnewstd.Params[6].AsInteger:=global_unit.current_user_id;
                sduseregnewstd.Params[7].AsInteger:=global_unit.current_user_id;

                try
                  sduseregnewstd.ExecSQL;

                  dmPSM.sdUsage.Active:=False;
                  dmPSM.sdUsage.Params[0].AsString:='STAFF';
                  dmPSM.sdUsage.Params[1].AsInteger:=global_unit.current_user_id;
                  dmPSM.sdUsage.Params[2].AsString:='ADMIN';
                  dmPSM.sdUsage.Params[3].AsString:='Assigned '+sdDJ.FieldByName('SS_SUBJECT').AsString+' P'+sdDJ.FieldByName('SSP_PAPER').AsString+' to '+Trim(surname.Text+' '+othernames.Text)+' ['+lookupCurrentClass.Text+'] Initial Reg. <- '+chkLstElements.Items[i].Text;
                  dmPSM.sdUsage.Params[4].AsString:=global_unit.user_ip;
                  dmPSM.sdUsage.Params[5].AsString:=global_unit.user_host;
                  try
                    dmPSM.sdUsage.ExecSQL;
                  except
                  end;

                except
                  subject_error:=True;
                end;

                sdDJ.Next;

              end;
            end;
          end;
        end
        else if rbSubjectOptions.Checked then
        begin

          for i:= 0 to chkLstElements.Items.Count - 1 do    // Iterate
          begin
            if chkLstElements.Items[i].Checked then
            begin
              //insert option, then marks base data
              is_dup:=False;

              sdDJ.Active:=False;
              sdDJ.CommandText:='select distinct SSOD_SS_ID from school_subjects_options_details where SSOD_SSO_ID = :SSOD_SSO_ID ';
              sdDJ.Params[0].AsInteger:=hold_subjects[i];
              sdDJ.Open;

              sduseregnewstd.Active:=False;
              sduseregnewstd.CommandText:='insert into students_marks_subject_options (SMSO_SG_ID, SMSO_CLASS, SMSO_STREAM, SMSO_YEAR, SMSO_TERM, SMSO_SS_ID, SMSO_SSO_ID, SMSO_CREATED_TIME, SMSO_CREATED_BY, MODIFIED_TIME, SMSO_MODIFIED_BY) '+
                                         'values (:SMSO_SG_ID, :SMSO_CLASS, :SMSO_STREAM, :SMSO_YEAR, :SMSO_TERM, :SMSO_SS_ID, :SMSO_SSO_ID, now(), :SMSO_CREATED_BY, now(), :SMSO_MODIFIED_BY) ';

              sduseregnewstd.Params[0].AsInteger:=newSG_ID;
              sduseregnewstd.Params[1].AssignField(cduseregnewstd.FieldByName('SC_CLASS'));
              sduseregnewstd.Params[2].AssignField(cduseregnewstd.FieldByName('SC_STREAM'));
              sduseregnewstd.Params[3].AsInteger:=global_unit.current_year;    //****get actual year/term from classes table!!!
              sduseregnewstd.Params[4].AssignField(cduseregnewstd.FieldByName('SC_TERM'));
              sduseregnewstd.Params[5].AssignField(sdDJ.Fields[0]);
              sduseregnewstd.Params[6].AsInteger:=hold_subjects[i];
              sduseregnewstd.Params[7].AsInteger:=global_unit.current_user_id;
              sduseregnewstd.Params[8].AsInteger:=global_unit.current_user_id;

              try
                sduseregnewstd.ExecSQL;

                dmPSM.sdUsage.Active:=False;
                dmPSM.sdUsage.Params[0].AsString:='STAFF';
                dmPSM.sdUsage.Params[1].AsInteger:=global_unit.current_user_id;
                dmPSM.sdUsage.Params[2].AsString:='ADMIN';
                dmPSM.sdUsage.Params[3].AsString:='Assigned '+chkLstElements.Items[i].Text+' to '+Trim(surname.Text+' '+othernames.Text)+' ['+lookupCurrentClass.Text+'] - Initial Reg.';
                dmPSM.sdUsage.Params[4].AsString:=global_unit.user_ip;
                dmPSM.sdUsage.Params[5].AsString:=global_unit.user_host;
                try
                  dmPSM.sdUsage.ExecSQL;
                except
                end;
              except
                on e: Exception do
                begin
                  if Pos('Duplicate', e.Message) > 0 then
                  begin
                    is_dup:=True;

                    sduseregnewstd.Active:=False;
                    sduseregnewstd.CommandText:='update students_marks_subject_options set MODIFIED_TIME = now(), SMSO_MODIFIED_BY = :SMSO_MODIFIED_BY where SMSO_SG_ID = :SMSO_SG_ID and SMSO_CLASS = :SMSO_CLASS and '+
                                               'SMSO_STREAM = :SMSO_STREAM and SMSO_YEAR = :SMSO_YEAR and SMSO_TERM = :SMSO_TERM and SMSO_SSO_ID = :SMSO_SSO_ID ';

                    sduseregnewstd.Params[0].AsInteger:=global_unit.current_user_id;
                    sduseregnewstd.Params[1].AsInteger:=newSG_ID;
                    sduseregnewstd.Params[2].AssignField(cduseregnewstd.FieldByName('SC_CLASS'));
                    sduseregnewstd.Params[3].AssignField(cduseregnewstd.FieldByName('SC_STREAM'));
                    sduseregnewstd.Params[4].AsInteger:=global_unit.current_year;    //****get actual year/term from classes table!!!
                    sduseregnewstd.Params[5].AssignField(cduseregnewstd.FieldByName('SC_TERM'));
                    sduseregnewstd.Params[6].AsInteger:=hold_subjects[i];

                    try
                      sduseregnewstd.ExecSQL;
                    except

                    end;
                  end
                  else
                  begin
                    subject_error:=True;
                  end;
                end;
              end;

              if not is_dup then
              begin
                //select all option subject papers and post
                sdDJ.Active:=False;
                sdDJ.CommandText:='select distinct SSOD_SSP_ID from v_school_subjects_options where SSO_ID = :SSO_ID and SSOD_SSP_ID in (select distinct SCS_SSP_ID from school_classes_subjects where SCS_SC_ID = :SCS_SC_ID )  ';
                sdDJ.Params[0].AsInteger:=hold_subjects[i];
                sdDJ.Params[1].AsInteger:=lookupCurrentClass.EditValue;
                sdDJ.Open;

                while not sdDJ.Eof do
                begin
                  sduseregnewstd.Active:=False;
                  sduseregnewstd.CommandText:='insert into students_marks (M_SG_ID, M_CLASS, M_STREAM, M_YEAR, M_TERM, M_SSP_ID, M_CREATED_TIME, M_CREATED_BY, MODIFIED_TIME, M_MODIFIED_BY, M_OPTION) '+
                                             'values (:M_SG_ID, :M_CLASS, :M_STREAM, :M_YEAR, :M_TERM, :M_SSP_ID, now(), :M_CREATED_BY, now(), :M_MODIFIED_BY, :M_OPTION ) ';

                  sduseregnewstd.Params[0].AsInteger:=newSG_ID;
                  sduseregnewstd.Params[1].AssignField(cduseregnewstd.FieldByName('SC_CLASS'));
                  sduseregnewstd.Params[2].AssignField(cduseregnewstd.FieldByName('SC_STREAM'));
                  sduseregnewstd.Params[3].AsInteger:=global_unit.current_year;   //**get proper year & term!!!
                  sduseregnewstd.Params[4].AssignField(cduseregnewstd.FieldByName('SC_TERM'));
                  sduseregnewstd.Params[5].AssignField(sdDJ.FieldByName('SSOD_SSP_ID'));
                  sduseregnewstd.Params[6].AsInteger:=global_unit.current_user_id;
                  sduseregnewstd.Params[7].AsInteger:=global_unit.current_user_id;
                  sduseregnewstd.Params[8].AsString:='O';
                  try
                    sduseregnewstd.ExecSQL;
                  except
                    subject_error:=True;
                  end;

                  sdDJ.Next;
                end;
              end;


            end;

          end;

      	end;

        if subject_error then
        begin
          MessageDlg('An error occurred during the assignment of subjects. The student will however still be registered but please check the subject assignment in the Academics Module', mtWarning, [mbOK], 0);
        end;

      except
        on e: Exception do
        begin
          MessageDlg('An error occurred during the assignment of subjects. The student will however still be registered but please check the subject assignment in the Academics Module', mtWarning, [mbOK], 0);
        end;
      end;


      //*check if there are any termly requirements and post for the student
      try
        sdDJ.Active:=False;
        sdDJ.CommandText:='select * from students_clearance_settings where SCS_YEAR = :SCS_YEAR and SCS_TERM = :SCS_TERM and SCS_CLASS = :SCS_CLASS ';

        sdDJ.Params[0].AsInteger:=global_unit.current_year;
        sdDJ.Params[1].AsInteger:=target_term;
        sdDJ.Params[2].AsString:=lookupCurrentClass.Text;
        //psmData.sdDJ2.Params[3].AsString:='C';
        sdDJ.Open;

        while not sdDJ.Eof do
        begin
          sduseregnewstd.Active:=False;
          sduseregnewstd.CommandText:='insert into students_clearance_history (SCH_TYPE,SCH_YEAR,SCH_TERM,SCH_SCOPE,SCH_SCS_ID,SCH_SG_ID,SCH_ITEM,SCH_ENTERED_TIME,SCH_ENTERED_BY)'+
                                     'values (:SCH_TYPE,:SCH_YEAR,:SCH_TERM,:SCH_SCOPE,:SCH_SCS_ID,:SCH_SG_ID,:SCH_ITEM,now(),:SCH_ENTERED_BY)';

          sduseregnewstd.Params[0].AssignField(sdDJ.FieldByName('SCS_TYPE'));  //.AsString:=Trim(gtvRequirements.DataController.Values[i,0]);
          sduseregnewstd.Params[1].AsInteger:=global_unit.current_year;
          sduseregnewstd.Params[2].AsInteger:=target_term;
          sduseregnewstd.Params[3].AsString:='C';
          sduseregnewstd.Params[4].AssignField(sdDJ.FieldByName('SCS_ID'));  //.AsInteger:=req_scs_id;
          sduseregnewstd.Params[5].AsInteger:=newSG_ID;
          sduseregnewstd.Params[6].AssignField(sdDJ.FieldByName('SCS_ITEM'));  //.AsString:=Trim(gtvRequirements.DataController.Values[i,3]);
          sduseregnewstd.Params[7].AsInteger:=global_unit.current_user_id;
          sduseregnewstd.ExecSQL;

          sdDJ.Next;
        end;
      except
        on e: Exception do
        begin
          MessageDlg('An error occurred during the posting of school requirements. The student will however still be registered but please check the clearance requirements in the Students Module', mtWarning, [mbOK], 0);
        end;
      end;


      //*check for school fees requirements and post them as well

      sdDJ.Active:=False;
      sdDJ.CommandText:='SELECT * FROM students_finance_class_dues_payable where SFCDP_CLASS = :SFCDP_CLASS and SFCDP_STREAM = :SFCDP_STREAM and SFCDP_YEAR = :SFCDP_YEAR and SFCDP_TERM = :SFCDP_TERM ';

      //sdDJ.Params[0].AssignField(cdDJ.FieldByName('SFCDP_CLASS'));
      //sdDJ.Params[1].AssignField(cdDJ.FieldByName('SFCDP_STREAM'));
      sdDJ.Params[2].AsInteger:=global_unit.current_year;
      sdDJ.Params[3].AsInteger:=target_term;
      sdDJ.Open;

      while not sdDJ.Eof do
      begin
        sduseregnewstd.Active:=False;
        sduseregnewstd.CommandText:='insert into students_finance_dues_payable (SFDP_SG_ID,SFDP_TYPE,SFDP_CLASS,SFDP_STREAM,SFDP_YEAR,SFDP_TERM,SFDP_PURPOSE,SFDP_AMOUNT,SFDP_REMARKS,SFDP_POSTED_TIME,SFDP_POSTED_BY,SFDP_SFCDP_ID) '+
                                   'values (:SFDP_SG_ID,:SFDP_TYPE,:SFDP_CLASS,:SFDP_STREAM,:SFDP_YEAR,:SFDP_TERM,:SFDP_PURPOSE,:SFDP_AMOUNT,:SFDP_REMARKS,now(),:SFDP_POSTED_BY,:SFDP_SFCDP_ID)';

        sduseregnewstd.Params[0].AsInteger:=newSG_ID;
        sduseregnewstd.Params[1].AsString:='C';
        sduseregnewstd.Params[2].AssignField(cduseregnewstd.FieldByName('SC_CLASS'));
        sduseregnewstd.Params[3].AssignField(cduseregnewstd.FieldByName('SC_STREAM'));
        sduseregnewstd.Params[4].AsInteger:=global_unit.current_year;
        sduseregnewstd.Params[5].AsInteger:=target_term;
        sduseregnewstd.Params[6].AssignField(sdDJ.FieldByName('SFCDP_PURPOSE'));
        sduseregnewstd.Params[7].AssignField(sdDJ.FieldByName('SFCDP_AMOUNT'));
        sduseregnewstd.Params[8].AssignField(sdDJ.FieldByName('SFCDP_REMARKS'));
        sduseregnewstd.Params[9].AsInteger:=global_unit.current_user_id;
        sduseregnewstd.Params[10].AssignField(sdDJ.FieldByName('SFCDP_ID'));

        try
          sduseregnewstd.ExecSQL;
        except
          on e: Exception do
          begin
            MessageDlg('An error occurred during the posting of financial obligations. The student will however still be registered but please check the dues payable in the Finance Module', mtWarning, [mbOK], 0);
          end;
        end;

        sdDJ.Next;
      end;

      //Finalize All except photos

      sduseregnewstd.Active:=False;
      sduseregnewstd.CommandText:='update psm_sequences set SEQ_ADMISSION_NO = SEQ_ADMISSION_NO + 1 where SEQ_YEAR = :SEQ_YEAR and SEQ_TERM = :SEQ_TERM ';
      sduseregnewstd.Params[0].AsInteger:=global_unit.current_year;
      sduseregnewstd.Params[1].AsInteger:=target_term;
      sduseregnewstd.ExecSQL;


      //If Photo...
      if IDAssigned = True then
      begin
        dmPSM.sdPostIDS.Active:=False;
        dmPSM.cdsPostIDS.Active:=False;
        dmPSM.sdPostIDS.Params[0].AsInteger:=newSG_ID;
        dmPSM.cdsPostIDS.Open;

        importID:=TJpegImage.Create;
        importIDStream:=TMemoryStream.Create;
        //importID.Assign(imgEntryID.Picture);
        importID.SaveToStream(importIDStream);

        try
          //psmData.sdPostStudentID.Active:=True;
          //psmData.cdsPostID.Locate('SG_ID', newSG_ID,[]);
          dmPSM.cdsPostIDS.Edit;
          dmPSM.bfPostIDSG_PHOTO_1.LoadFromStream(importIDStream);
          dmPSM.cdsPostIDS.Post;

          dmPSM.cdsPostIDS.ApplyUpdates(-1);

        finally
          importIDStream.Clear;
          importIDStream.Free;
          importID.Free;
        end;
      end;

      dmPSM.sdUsage.Active:=False;
      dmPSM.sdUsage.Params[0].AsString:='STAFF';
      dmPSM.sdUsage.Params[1].AsInteger:=global_unit.current_user_id;
      dmPSM.sdUsage.Params[2].AsString:='STUDENTS';
      dmPSM.sdUsage.Params[3].AsString:='Registered New Student: '+surname.Text+' '+othernames.Text+' ('+lookupCurrentClass.Text+')';
      dmPSM.sdUsage.Params[4].AsString:=global_unit.user_ip;
      dmPSM.sdUsage.Params[5].AsString:=global_unit.user_host;
      try
        dmPSM.sdUsage.ExecSQL;
      except
      end;

      //**additional housekeeping
      if (nationality.ItemIndex = -1) and (Length(Trim(nationality.Text)) > 0) then
      try
        sduseregnewstd.Active:=False;
        sduseregnewstd.CommandText:='insert into psm_nationalities (NATIONALITY) values (:NATIONALITY) ';
        sduseregnewstd.Params[0].AsString:=Trim(nationality.Text);
        sduseregnewstd.ExecSQL;
      finally
      end;

      //Pos(tEditHomeDistrict.Text, tEditHomeDistrict.Properties.LookupItems.Text);
      //chill the districts

      if (religion.ItemIndex = -1) and (Length(Trim(religion.Text)) > 0) then
      try
        sduseregnewstd.Active:=False;
        sduseregnewstd.CommandText:='insert into psm_religions (RELIGION) values (:RELIGION) ';
        sduseregnewstd.Params[0].AsString:=Trim(religion.Text);
        sduseregnewstd.ExecSQL;
      finally
      end;

      newstudentregistration.ModalResult:=mrOk;

    except
      on e: Exception do
      begin
        sduseregnewstd.Active:=False;
        sduseregnewstd.CommandText:='rollback';
        sduseregnewstd.ExecSQL;

        MessageDlg('Sorry, an error has been encountered. Some of the information may not have been posted'+#13#10#13#10+'Please note the following error and report it to the PSM support team: '+e.Message, mtError,[mbOk],0);
        Exit;
      end;
    end;

  end;

  Inc(currentPage);
  newstudentregistration.Caption:='Registering New Student - Step '+IntToStr(currentPage)+' of 3';
  btnBack.Enabled:=True;
  registernewstudenthome.ActivePageIndex:=currentPage;
 end;

 procedure Tnewstudentregistration.chkIsBoarderClick(Sender: TObject);
begin
label1Dormitory1.Enabled:=chkIsBoarder.Checked;
  lookUpDormitory.Enabled:=chkIsBoarder.Checked;
end;

procedure Tnewstudentregistration.comboResultsLevelPropertiesEditValueChanged(
  Sender: TObject);
  var
  req_level: string;
  subject_results: TStrings;
begin
  //req_class:=Trim(lookupClassJoined.Text);

  if student_curriculum = 'UNEB' then
  begin
    if student_level = 'K' then
    begin
      req_level:='K';
    end
    else if student_level = 'P' then
    begin
      case comboResultsLevel.ItemIndex of
        0: req_level:='K';
        1: req_level:='P';
      end;
    end
    else if student_level = 'O' then
    begin
      case comboResultsLevel.ItemIndex of
        0: req_level:='P';
        1: req_level:='O';
      end;
    end
    else if student_level = 'A' then
    begin
      case comboResultsLevel.ItemIndex of
        0: req_level:='P';
        1: req_level:='O';
        2: req_level:='A';
      end;
    end;

  end
  else if student_curriculum = 'CIE' then
  begin
    if student_level = 'CP' then
    begin
      req_level:='CP';
    end;

    if student_level = 'CS1' then
    begin
      case comboResultsLevel.ItemIndex of
        0: req_level:='CP';
        1: req_level:='CS1';
      end;
    end;

    if student_level = 'CS2' then
    begin
      case comboResultsLevel.ItemIndex of
        0: req_level:='CP';
        1: req_level:='CS1';
        2: req_level:='CS2';
      end;
    end;

    if student_level = 'CA' then
    begin
      case comboResultsLevel.ItemIndex of
        0: req_level:='CP';
        1: req_level:='CS1';
        2: req_level:='CS2';
        3: req_level:='CA';
      end;
    end;

  end;

  dmPSM.ClientDataSet1GetSubjectsUg.Active:=False;
  dmPSM.sdGetSubjectsUg.Active:=False;
  dmPSM.sdGetSubjectsUg.CommandText:='select * from school_subjects_default where SSD_CURRICULUM = :SSD_CURRICULUM and SSD_LEVEL = :SSD_LEVEL order by SSD_SUBJECT ';

  dmPSM.sdGetSubjectsUg.Params[0].AsString:=student_curriculum;
  dmPSM.sdGetSubjectsUg.Params[1].AsString:=req_level;

  dmPSM.ClientDataSet1GetSubjectsUg.Open;

  if ((req_level = 'K') or (req_level = 'CP')) and (registernewstudenthome.ActivePageIndex = 2) then MessageDlg('Please note the ''Summary Results'' option might not be applicable for the specified class level ', mtWarning, [mbOK], 0);

  subject_results:=TStringList.Create;
  try
    if student_curriculum = 'UNEB' then
    begin
      sEditResultSummary.Enabled:=True;

      if req_level = 'K' then
      begin
        //comboResultDetailed.Properties.ReadOnly:=False;
        subject_results.Add('Passed');
        subject_results.Add('Failed');

        sEditResultSummary.Properties.MinValue:=4;
        sEditResultSummary.Properties.MaxValue:=36;
      end
      else if req_level = 'P' then
      begin
        //comboResultDetailed.Properties.ReadOnly:=True;
        subject_results.Add('D1');
        subject_results.Add('D2');
        subject_results.Add('C3');
        subject_results.Add('C4');
        subject_results.Add('C5');
        subject_results.Add('C6');
        subject_results.Add('P7');
        subject_results.Add('P8');
        subject_results.Add('F9');
        subject_results.Add('X');

        sEditResultSummary.Properties.MinValue:=4;
        sEditResultSummary.Properties.MaxValue:=36;
      end
      else if req_level = 'O' then
      begin
        //comboResultDetailed.Properties.ReadOnly:=True;
        subject_results.Add('D1');
        subject_results.Add('D2');
        subject_results.Add('C3');
        subject_results.Add('C4');
        subject_results.Add('C5');
        subject_results.Add('C6');
        subject_results.Add('P7');
        subject_results.Add('P8');
        subject_results.Add('F9');
        subject_results.Add('X');

        sEditResultSummary.Properties.MinValue:=8;
        sEditResultSummary.Properties.MaxValue:=72;
      end
      else if req_level = 'A' then
      begin
        //comboResultDetailed.Properties.ReadOnly:=True;
        subject_results.Add('A');
        subject_results.Add('B');
        subject_results.Add('C');
        subject_results.Add('D');
        subject_results.Add('E');
        subject_results.Add('F');
        subject_results.Add('O');
        subject_results.Add('X');

        sEditResultSummary.Properties.MinValue:=0;
        sEditResultSummary.Properties.MaxValue:=26;
      end;

      comboResultDetailed.Properties.Items.Clear;
      comboResultDetailed.Properties.Items.AddStrings(subject_results);
    end
    else if student_curriculum = 'CIE' then
    begin
      sEditResultSummary.Enabled:=False;

      if req_level = 'CP' then
      begin
        //comboResultDetailed.Properties.ReadOnly:=False;
        subject_results.Add('Passed');
        subject_results.Add('Failed');

      end
      else if req_level = 'CS1' then
      begin
        //comboResultDetailed.Properties.ReadOnly:=True;
        subject_results.Add('A*');
        subject_results.Add('A');
        subject_results.Add('B');
        subject_results.Add('C');
        subject_results.Add('D');
        subject_results.Add('E');
        subject_results.Add('F');
        subject_results.Add('G');

      end
      else if req_level = 'CS2' then
      begin
        //comboResultDetailed.Properties.ReadOnly:=True;
        subject_results.Add('A*');
        subject_results.Add('A');
        subject_results.Add('B');
        subject_results.Add('C');
        subject_results.Add('D');
        subject_results.Add('E');
        subject_results.Add('F');
        subject_results.Add('G');

      end
      else if req_level = 'CA' then
      begin
        //comboResultDetailed.Properties.ReadOnly:=True;
        subject_results.Add('A');
        subject_results.Add('B');
        subject_results.Add('C');
        subject_results.Add('D');
        subject_results.Add('E');

      end;

      comboResultDetailed.Properties.Items.Clear;
      comboResultDetailed.Properties.Items.AddStrings(subject_results);
    end;

  finally
    subject_results.Free;
  end;
end;

procedure Tnewstudentregistration.ListSubjects;
var
  i: Integer;
begin
  chkLstElements.Items.Clear;

  if (student_level = 'K') or (student_level = 'P') or (student_level = 'CP') then //probably cambridge primary as well
  begin
    sduseregnewstd.Active:=False;
    sduseregnewstd.CommandText:='select count(*) from school_subjects S inner join school_classes_subjects SC on S.SS_ID = SC.SCS_SS_ID '+
                               'where SC.SCS_SC_ID = :SCS_SC_ID ';


    sduseregnewstd.Params[0].AsInteger:=lookupCurrentClass.EditValue;
    sduseregnewstd.Open;

    SetLength(hold_subjects, sduseregnewstd.Fields[0].AsInteger);

    sduseregnewstd.Active:=False;
    sduseregnewstd.CommandText:='select S.SS_ID, S.SS_SUBJECT, SC.SCS_COMPULSORY '+
                               'from school_subjects S inner join school_classes_subjects SC on S.SS_ID = SC.SCS_SS_ID '+
                               'where SC.SCS_SC_ID = :SCS_SC_ID order by S.SS_SUBJECT ';

    sduseregnewstd.Params[0].AsInteger:=lookupCurrentClass.EditValue;

    sduseregnewstd.Open;

    i:=0;

    chkLstElements.Items.BeginUpdate;

    while not sduseregnewstd.Eof do
    begin
      chkLstElements.AddItem(sduseregnewstd.FieldByName('SS_SUBJECT').AsString);
      chkLstElements.Items[i].Checked:=sduseregnewstd.FieldByName('SCS_COMPULSORY').AsString = 'Y';
      chkLstElements.Items[i].Enabled:=sduseregnewstd.FieldByName('SCS_COMPULSORY').AsString = 'N';

      hold_subjects[i]:=sduseregnewstd.FieldByName('SS_ID').AsInteger;

      Inc(i);
      sduseregnewstd.Next;
    end;

    chkLstElements.Items.EndUpdate;

  end
  else
  begin

    sduseregnewstd.Active:=False;
    sduseregnewstd.CommandText:='select count(*) from school_subjects S inner join school_subjects_papers P on S.SS_ID = P.SSP_SS_ID inner join school_classes_subjects SC on P.SSP_ID = SC.SCS_SSP_ID '+
                               'where SC.SCS_SC_ID = :SCS_SC_ID ';


    sduseregnewstd.Params[0].AsInteger:=lookupCurrentClass.EditValue;
    sduseregnewstd.Open;

    SetLength(hold_subjects, sduseregnewstd.Fields[0].AsInteger);

    sduseregnewstd.Active:=False;
    sduseregnewstd.CommandText:='select P.SSP_ID, S.SS_SUBJECT, P.SSP_PAPER, concat(S.SS_SUBJECT, '' P'', P.SSP_PAPER) SUBJECT_PAPER, SC.SCS_COMPULSORY '+
                               'from school_subjects S inner join school_subjects_papers P on S.SS_ID = P.SSP_SS_ID inner join school_classes_subjects SC on P.SSP_ID = SC.SCS_SSP_ID '+
                               'where SC.SCS_SC_ID = :SCS_SC_ID order by S.SS_SUBJECT, P.SSP_PAPER ';

    sduseregnewstd.Params[0].AsInteger:=lookupCurrentClass.EditValue;

    sduseregnewstd.Open;

    i:=0;

    chkLstElements.Items.BeginUpdate;

    while not sduseregnewstd.Eof do
    begin
      chkLstElements.AddItem(sduseregnewstd.FieldByName('SUBJECT_PAPER').AsString);
      chkLstElements.Items[i].Checked:=sduseregnewstd.FieldByName('SCS_COMPULSORY').AsString = 'Y';
      chkLstElements.Items[i].Enabled:=sduseregnewstd.FieldByName('SCS_COMPULSORY').AsString = 'N';

      hold_subjects[i]:=sduseregnewstd.FieldByName('SSP_ID').AsInteger;

      Inc(i);
      sduseregnewstd.Next;
    end;

    chkLstElements.Items.EndUpdate;

  end;

end;

procedure Tnewstudentregistration.LoginbtnaddparentsrecordsClick(
  Sender: TObject);
  var
  i: Integer;
begin
 if Length(Trim(parenttype.Text)) = 0 then
  begin
    parenttype.SetFocus;
    Exit;
  end;

  if Length(Trim(parentname.Text)) = 0 then
  begin
    parentname.SetFocus;
    Exit;
  end;

  i:=gtvParentsInformation.DataController.RecordCount;
  gtvParentsInformation.DataController.RecordCount:=i + 1;
  gtvParentsInformation.DataController.Values[i,0]:=parenttype.Text;
  gtvParentsInformation.DataController.Values[i,1]:=Trim(parentname.Text);
  gtvParentsInformation.DataController.Values[i,2]:=Trim(parentoccupation.Text);
  gtvParentsInformation.DataController.Values[i,3]:=Trim(parenttelephone.Text);
  gtvParentsInformation.DataController.Values[i,4]:=Trim(parentemail.Text);
  gtvParentsInformation.DataController.Values[i,5]:='N';
  gtvParentsInformation.DataController.Values[i,6]:=Trim(parentremarks.Text);

  parentname.Clear;
  parentoccupation.Clear;
  parenttelephone.Clear;
  parentemail.Clear;
  parenttype.SetFocus;
end;

procedure Tnewstudentregistration.lookupClassJoinedMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    lookupClassJoined.Clear;
    cdstreamregnewstd.Active:=False;
    sdstreamregnewstd.Active:=false;
    sdstreamregnewstd.CommandText:='select concat(sc_class,'' '',sc_stream) as Stream from school_classes where sc_class =''S. 5''';
    sdstreamregnewstd.open;
    // := TComponent.Create(Self);
    try
   lookupClassJoined.Properties.ListSource:=dsstreamregnewstd;
   lookupClassJoined.Properties.ListFieldNames:='Stream';
   lookupClassJoined.Properties.KeyFieldNames:='Stream';
    finally
      //.Free;
    end;
    sdstreamregnewstd.Active:=true;
   cdstreamregnewstd.Active:=True;
end;

procedure Tnewstudentregistration.lookupCurrentClassMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
lookupCurrentClass.Clear;
    cdstreamregnewstd.Active:=False;
    sdstreamregnewstd.Active:=false;
    sdstreamregnewstd.CommandText:='select concat(sc_class,'' '',sc_stream) as Stream from school_classes where sc_class =''S. 5''';
    sdstreamregnewstd.open;
    // := TComponent.Create(Self);
    try
   lookupCurrentClass.Properties.ListSource:=dsstreamregnewstd;
   lookupCurrentClass.Properties.ListFieldNames:='Stream';
   lookupCurrentClass.Properties.KeyFieldNames:='Stream';
    finally
      //.Free;
    end;
    sdstreamregnewstd.Active:=true;
   cdstreamregnewstd.Active:=True;
end;

procedure Tnewstudentregistration.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
newstudentregistration:=nil;
Action:=caFree;
end;

procedure Tnewstudentregistration.FormCreate(Sender: TObject);
begin


         {creating the sql dataset}
         sduseregnewstd :=TSQLDataSet.Create(Self);
         sdstreamregnewstd :=TSQLDataSet.Create(Self);
         sdDJ:=TSQLDataSet.Create(Self);


      {Assigning sqlconnection to the sql datasets}

        sdDJ.SQLConnection:=dmPSM.cPSM;
        sduseregnewstd.SQLConnection:=dmPSM.cPSM;
        sdstreamregnewstd.SQLConnection :=dmPSM.cPSM;

      //**********************************************************


      {Creating the dataset providers}
    dspuseregnewstd:=TDataSetProvider.Create(self);
    dspstreamregnewstd :=TDataSetProvider.Create(Self);
    dspDJ:=TDataSetProvider.Create(Self);

    {Assigning DataSet Providers Names}
    dspuseregnewstd.Name:='useDataSetProvider';
    dspstreamregnewstd.Name:='streamDataSetProvider';
    dspDJ.Name:='DJDataSetProvider';
//**********************************************************

        {Assigning datasetproviders datasets}
          dspuseregnewstd.DataSet:=sduseregnewstd;
          dspstreamregnewstd.DataSet:=sdstreamregnewstd;
          dspDJ.DataSet:=sdDJ;

          {Creating ClientDataSets}
          cduseregnewstd :=TClientDataSet.Create(self);
          cdstreamregnewstd :=TClientDataSet.Create(Self);
          cdDJ:=TClientDataSet.Create(Self);

      {Assigning ClientDatasets DataSetProviders}
      cduseregnewstd.ProviderName:='useDataSetProvider';
      cdstreamregnewstd.ProviderName:='streamDataSetProvider';
      cdDJ.ProviderName:='DJDataSetProvider';

//**********************************************************

  {Creating DataSources}

   dsuseregnewstd:=TDataSource.Create(Self);
   dsstreamregnewstd :=TDataSource.Create(Self);
    dDJ:=TDataSource.Create(Self);


   {Assiging Datasets to the dataSources}

    dsuseregnewstd.DataSet:=cduseregnewstd;
    dsstreamregnewstd.DataSet :=cdstreamregnewstd;
     dDJ.DataSet:=cdDJ;

end;

procedure Tnewstudentregistration.ListCombinations;
var
  i: Integer;
begin
  chkLstElements.Items.Clear;

  sduseregnewstd.Active:=False;
  sduseregnewstd.CommandText:='select count(*) from school_classes_subject_combinations where SCSC_SC_ID = :SCSC_SC_ID ';

  sduseregnewstd.Params[0].AsInteger:=lookupCurrentClass.EditValue;
  sduseregnewstd.Open;

  SetLength(hold_subjects, sduseregnewstd.Fields[0].AsInteger);

  sduseregnewstd.Active:=False;
  sduseregnewstd.CommandText:='select O.SSC_ID, O.SSC_COMBINATION from school_subjects_combinations O inner join school_classes_subject_combinations C on O.SSC_ID = C.SCSC_SSC_ID '+
                             'where C.SCSC_SC_ID = :SCSC_SC_ID order by O.SSC_COMBINATION ';

  sduseregnewstd.Params[0].AsInteger:=lookupCurrentClass.EditValue;

  sduseregnewstd.Open;

  i:=0;

  chkLstElements.Items.BeginUpdate;

  while not sduseregnewstd.Eof do
  begin
    chkLstElements.AddItem(sduseregnewstd.FieldByName('SSC_COMBINATION').AsString);
    //chkLstElements.Items[i].Checked:=psmData.sdDJ1.FieldByName('SCS_COMPULSORY').AsString = 'Y';
    //chkLstElements.Items[i].Enabled:=psmData.sdDJ1.FieldByName('SCS_COMPULSORY').AsString = 'N';

    hold_subjects[i]:=sduseregnewstd.FieldByName('SSC_ID').AsInteger;

    Inc(i);
    sduseregnewstd.Next;
  end;

  chkLstElements.Items.EndUpdate;

end;

procedure Tnewstudentregistration.ListOptions;
var
  i: Integer;
begin
  chkLstElements.Items.Clear;

  sduseregnewstd.Active:=False;
  sduseregnewstd.CommandText:='select count(*) from school_classes_subject_options where SCSO_SC_ID = :SCSO_SC_ID ';

  sduseregnewstd.Params[0].AsInteger:=lookupCurrentClass.EditValue;
  sduseregnewstd.Open;

  SetLength(hold_subjects, sduseregnewstd.Fields[0].AsInteger);

  sduseregnewstd.Active:=False;
  sduseregnewstd.CommandText:='select O.SSO_ID, O.SSO_OPTION from school_subjects_options O inner join school_classes_subject_options C on O.SSO_ID = C.SCSO_SSO_ID '+
                             'where C.SCSO_SC_ID = :SCSO_SC_ID order by O.SSO_OPTION ';

  sduseregnewstd.Params[0].AsInteger:=lookupCurrentClass.EditValue;

  sduseregnewstd.Open;

  i:=0;

  chkLstElements.Items.BeginUpdate;

  while not sduseregnewstd.Eof do
  begin
    chkLstElements.AddItem(sduseregnewstd.FieldByName('SSO_OPTION').AsString);
    //chkLstElements.Items[i].Checked:=psmData.sdDJ1.FieldByName('SCS_COMPULSORY').AsString = 'Y';
    //chkLstElements.Items[i].Enabled:=psmData.sdDJ1.FieldByName('SCS_COMPULSORY').AsString = 'N';

    hold_subjects[i]:=sduseregnewstd.FieldByName('SSO_ID').AsInteger;

    Inc(i);
    sduseregnewstd.Next;
  end;

  chkLstElements.Items.EndUpdate;

end;

function Tnewstudentregistration.RandomString: string;
var
  char_pool, output_code: string;
  i: Integer;
begin
  Randomize;

  char_pool:='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  output_code:='';

  for i := 0 to 100 do
  begin
    output_code:=output_code+char_pool[Random(61) + 1];
  end;

  Result:=output_code;

end;

procedure Tnewstudentregistration.rbSubjectCombinationsClick(Sender: TObject);
begin
ListCombinations;
end;

procedure Tnewstudentregistration.rbSubjectOptionsClick(Sender: TObject);
begin
ListOptions;
end;

procedure Tnewstudentregistration.rbSubjectSubjectsClick(Sender: TObject);
begin
 ListSubjects;
end;

procedure Tnewstudentregistration.sEditResultSummaryPropertiesChange(
  Sender: TObject);
var
  req_class, req_level: string;
begin
//if Length(comboGradeSummary.Text) > 0 then Exit;

  req_level:='';

  req_class:=Trim(lookupClassJoined.Text);

  if comboResultsLevel.ItemIndex = 0 then
  begin
    if req_class[1] = 'P' then
    begin
      req_level:='P';
    end
    else if req_class[1] = 'S' then
    begin
      if Pos('1', req_class) > 0 then
      begin
        req_level:='P';
      end;

      if Pos('2', req_class) > 0 then
      begin
        req_level:='P';
      end;

      if Pos('3', req_class) > 0 then
      begin
        req_level:='P';
      end;

      if Pos('4', req_class) > 0 then
      begin
        req_level:='P';
      end;

      if Pos('5', req_class) > 0 then
      begin
        req_level:='P';
      end;

      if Pos('6', req_class) > 0 then
      begin
        req_level:='P';
      end;
    end;
  end
  else if comboResultsLevel.ItemIndex = 1 then
  begin
    if req_class[1] = 'P' then
    begin
      req_level:='P';
    end
    else if req_class[1] = 'S' then
    begin
      if Pos('1', req_class) > 0 then
      begin
        req_level:='O';
      end;

      if Pos('2', req_class) > 0 then
      begin
        req_level:='O';
      end;

      if Pos('3', req_class) > 0 then
      begin
        req_level:='O';
      end;

      if Pos('4', req_class) > 0 then
      begin
        req_level:='O';
      end;

      if Pos('5', req_class) > 0 then
      begin
        req_level:='O';
      end;

      if Pos('6', req_class) > 0 then
      begin
        req_level:='O';
      end;
    end;
  end
  else if comboResultsLevel.ItemIndex = 2 then
  begin
    if req_class[1] = 'P' then
    begin
      req_level:='P';
    end
    else if req_class[1] = 'S' then
    begin
      if Pos('5', req_class) > 0 then
      begin
        req_level:='A';
      end;

      if Pos('6', req_class) > 0 then
      begin
        req_level:='A';
      end;
    end;
  end;

  if req_level = 'P' then
  begin
    case sEditResultSummary.Value of
      4..12: comboGradeSummary.Text:='1';
      13..24: comboGradeSummary.Text:='2';
      25..36: comboGradeSummary.Text:='3';
      37..48: comboGradeSummary.Text:='4';
    end;
  end
  else if req_level = 'O' then
  begin
    case sEditResultSummary.Value of
      8..32: comboGradeSummary.Text:='1';
      33..45: comboGradeSummary.Text:='2';
      46..58: comboGradeSummary.Text:='3';
      59..72: comboGradeSummary.Text:='4';
    end;
  end
  else if req_level = 'A' then
  begin
     comboGradeSummary.Text:='NA';
  end;
end;

end.
