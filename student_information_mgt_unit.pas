unit student_information_mgt_unit;

interface

uses
  Windows, Messages, SysUtils, StrUtils,Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinFoggy,
  dxSkiniMaginary, dxSkinsdxBarPainter, dxBar, cxClasses, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinGlassOceans, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  dxSkinscxPCPainter, ComCtrls, Menus, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, DB, cxDBData, cxMemo, cxCheckBox, cxCurrencyEdit, cxLabel,
  cxImage, cxBlobEdit, cxGridBandedTableView, cxGridDBBandedTableView,
  cxGridCardView, cxGridDBCardView, cxPivotGridChartConnection, AppEvnts,
  cxButtonEdit, cxDBLabel, cxDBEdit, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridCustomView, cxGrid, cxDropDownEdit,
  cxCalendar, cxMaskEdit, cxCheckComboBox, StdCtrls, cxButtons, cxTextEdit,
  cxSplitter, cxTreeView, cxRadioGroup, ExtCtrls, cxPC, cxGroupBox,FMTBcd, SqlExpr, DBClient,Provider,dxGDIPlusClasses;

type
  Tstudentinformationmanagement = class(TForm)
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarButton1: TdxBarButton;
    dxBarSubItem1: TdxBarSubItem;
    dxBarSubItem2: TdxBarSubItem;
    dxBarSubItem3: TdxBarSubItem;
    dxBarSubItem4: TdxBarSubItem;
    dxBarSubItem5: TdxBarSubItem;
    dxBarSubItem6: TdxBarSubItem;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    dxBarButton4: TdxBarButton;
    dxBarSubItem7: TdxBarSubItem;
    dxBarButton5: TdxBarButton;
    dxBarSubItem8: TdxBarSubItem;
    dxBarButton6: TdxBarButton;
    s2: TcxSplitter;
    label1l4: TLabel;
    label1l5: TLabel;
    dlgSaveExport: TSaveDialog;
    aeSM: TApplicationEvents;
    pcStudents: TcxPageControl;
    tabViewStudentsAdmissions: TcxTabSheet;
    l157: TLabel;
    gbApplicationListing: TcxGroupBox;
    pcApplicationListing: TcxPageControl;
    tabAcademicYear: TcxTabSheet;
    l144: TLabel;
    tvApplicationListing: TcxTreeView;
    s14: TcxSplitter;
    gbFilterAdmissions: TcxGroupBox;
    rbAllApplications: TcxRadioButton;
    rbAdmittedApplications: TcxRadioButton;
    rbRejectedApplications: TcxRadioButton;
    rbUnprocessedApplications: TcxRadioButton;
    tabSearchApplication: TcxTabSheet;
    l145: TLabel;
    l146: TLabel;
    tEditSearchApplicant: TcxTextEdit;
    btnSearchApplicant: TcxButton;
    gbApplications: TcxGroupBox;
    pnlp25: TPanel;
    l147: TLabel;
    bvl31: TBevel;
    rbApplicationsFull: TcxRadioButton;
    rbApplicationsWizardSheet: TcxRadioButton;
    bEditQuickSearchApplications: TcxButtonEdit;
    gApplications: TcxGrid;
    gtvApplicationsFull: TcxGridDBTableView;
    gcApplicationsFullSAG_APPLICATION_NO: TcxGridDBColumn;
    gcApplicationsFullSAG_TARGET_YEAR: TcxGridDBColumn;
    gcApplicationsFullSAG_TARGET_TERM: TcxGridDBColumn;
    gcApplicationsFullSAG_TARGET_CLASS: TcxGridDBColumn;
    gcApplicationsFullSAG_SURNAME: TcxGridDBColumn;
    gcApplicationsFullSAG_OTHERNAMES: TcxGridDBColumn;
    gcApplicationsFullSAG_REG_NO: TcxGridDBColumn;
    gcApplicationsFullSAG_DOB: TcxGridDBColumn;
    gcApplicationsFullSAG_GENDER: TcxGridDBColumn;
    gcApplicationsFullSAG_CITIZENSHIP: TcxGridDBColumn;
    gcApplicationsFullSAG_HOME_DISTRICT: TcxGridDBColumn;
    gcApplicationsFullSAG_ADDRESS: TcxGridDBColumn;
    gcApplicationsFullSAG_TELEPHONE: TcxGridDBColumn;
    gcApplicationsFullSAG_EMAIL_ADDRESS: TcxGridDBColumn;
    gcApplicationsFullSAG_APPLICATION_FEE_MODE: TcxGridDBColumn;
    gcApplicationsFullSAG_APPLICATION_FEE_DETAILS: TcxGridDBColumn;
    gcApplicationsFullSAG_APPLICATION_FEE_AMOUNT: TcxGridDBColumn;
    gcApplicationsFullSAG_POSTED_TIME: TcxGridDBColumn;
    gcApplicationsFullSAG_REMARKS: TcxGridDBColumn;
    gcApplicationsFullSAG_SUFFICIENT_QUALIFICATIONS: TcxGridDBColumn;
    gcApplicationsFullSAG_INTERVIEW: TcxGridDBColumn;
    gcApplicationsFullSAG_ADMITTED: TcxGridDBColumn;
    gcApplicationsFullSAG_ADMITTED_DATE: TcxGridDBColumn;
    gcApplicationsFullSAG_ADMITTED_METHOD: TcxGridDBColumn;
    gcApplicationsFullSAG_ADMITTED_BY: TcxGridDBColumn;
    gcApplicationsFullSAG_DATE_FORM_RETURNED: TcxGridDBColumn;
    gbtvApplicationsWizard: TcxGridDBBandedTableView;
    glApplicationsFull: TcxGridLevel;
    glApplicationsWizard: TcxGridLevel;
    pcApplicationAdditionalDetails: TcxPageControl;
    tabApplicantParents: TcxTabSheet;
    gApplicantParents: TcxGrid;
    gtvApplicantParents: TcxGridDBTableView;
    gcApplicantParentsSAP_TYPE: TcxGridDBColumn;
    gcApplicantParentsSAP_NAME: TcxGridDBColumn;
    gcApplicantParentsSAP_TEL: TcxGridDBColumn;
    gcApplicantParentsSAP_EMAIL: TcxGridDBColumn;
    gcApplicantParentsSAP_OCCUPATION: TcxGridDBColumn;
    glApplicantParents: TcxGridLevel;
    tabApplicantFormerSchools: TcxTabSheet;
    gApplicantFormerSchools: TcxGrid;
    gtvApplicantFormerSchools: TcxGridDBTableView;
    gcApplicantFormerSchoolsSAPS_SCHOOL: TcxGridDBColumn;
    gcApplicantFormerSchoolsSAPS_ATTENDED_FROM: TcxGridDBColumn;
    gcApplicantFormerSchoolsSAPS_ATTENDED_TO: TcxGridDBColumn;
    gcApplicantFormerSchoolsSAPS_INDEX_NO: TcxGridDBColumn;
    gcApplicantFormerSchoolsSAPS_VERIFIED: TcxGridDBColumn;
    glApplicantFormerSchools: TcxGridLevel;
    gApplicantFormerResults: TcxGrid;
    gtvApplicantFormerResults: TcxGridDBTableView;
    gcApplicantFormerResultsSAERD_WHICH_RESULT: TcxGridDBColumn;
    gcApplicantFormerResultsSAERD_SUBJECT: TcxGridDBColumn;
    gcApplicantFormerResultsSAERD_GRADE: TcxGridDBColumn;
    glApplicantFormerResults: TcxGridLevel;
    s9: TcxSplitter;
    sApplicationAdditionalDetails: TcxSplitter;
    sApplications: TcxSplitter;
    tabViewStudentsCurrent: TcxTabSheet;
    lSchoolStudents: TLabel;
    s1: TcxSplitter;
    pnlStudentsView: TPanel;
    rbStudentsClassicView: TcxRadioButton;
    rbStudentsGalleryView: TcxRadioButton;
    rbStudentsParentsView: TcxRadioButton;
    bEditQuickSearchStudents: TcxButtonEdit;
    pcStudentsView: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    gbSelectedStudentDetails: TcxGroupBox;
    gbHomeFamilyInformation: TcxGroupBox;
    l8: TLabel;
    l9: TLabel;
    l10: TLabel;
    cxdbm1: TcxDBMemo;
    dbTEdit4: TcxDBTextEdit;
    gStudentParents: TcxGrid;
    gtv3: TcxGridDBTableView;
    gc11: TcxGridDBColumn;
    gc12: TcxGridDBColumn;
    gc13: TcxGridDBColumn;
    gc14: TcxGridDBColumn;
    gc15: TcxGridDBColumn;
    gtvStudentParents: TcxGridDBTableView;
    gcSP_TYPE: TcxGridDBColumn;
    gcSP_NAME: TcxGridDBColumn;
    gcSP_OCCUPATION: TcxGridDBColumn;
    gcSP_TEL: TcxGridDBColumn;
    gcSP_EMAIL: TcxGridDBColumn;
    gcSP_SMO_SUBSCRIBE: TcxGridDBColumn;
    glStudentParents: TcxGridLevel;
    s3: TcxSplitter;
    pnl8: TPanel;
    pnl9: TPanel;
    pcStudentSnap: TcxPageControl;
    tabEntryID: TcxTabSheet;
    dbImgEntry: TcxDBImage;
    tabExitID: TcxTabSheet;
    dbImgExitImage: TcxDBImage;
    pnl10: TPanel;
    gbPersonalSchoolInformation: TcxGroupBox;
    l11: TLabel;
    l12: TLabel;
    l13: TLabel;
    l14: TLabel;
    l15: TLabel;
    l16: TLabel;
    l17: TLabel;
    l18: TLabel;
    Bevel1: TBevel;
    l19: TLabel;
    dbTEdit6: TcxDBTextEdit;
    tEditStudentAge: TcxTextEdit;
    dbTEdit7: TcxDBTextEdit;
    dbTEdit8: TcxDBTextEdit;
    dbTEdit9: TcxDBTextEdit;
    dbTEdit10: TcxDBTextEdit;
    dbTEdit11: TcxDBTextEdit;
    bDBEditBEdit1: TcxDBButtonEdit;
    dbTEdit12: TcxDBTextEdit;
    pnl11: TPanel;
    cxDBLabel1: TcxDBLabel;
    pnl3: TPanel;
    l20: TLabel;
    pcStudentTrackRecords: TcxPageControl;
    tabDiscipline: TcxTabSheet;
    pnlp3: TPanel;
    btnNewDisc: TcxButton;
    btnPrintDisc: TcxButton;
    btnRefreshDisc: TcxButton;
    gDisc: TcxGrid;
    gtvDisc: TcxGridDBTableView;
    gcDiscDBTableView1DISC_CLASS: TcxGridDBColumn;
    gcDiscDBTableView1DISC_DATE: TcxGridDBColumn;
    gcDiscDBTableView1DISC_ISSUE: TcxGridDBColumn;
    gcDiscDBTableView1DISC_ACTION: TcxGridDBColumn;
    gcDiscDBTableView1T_FULLNAME: TcxGridDBColumn;
    glDisc: TcxGridLevel;
    tabHealth: TcxTabSheet;
    pnlp4: TPanel;
    btnNewHealth: TcxButton;
    btn1: TcxButton;
    btnRefreshHealth: TcxButton;
    gHealth: TcxGrid;
    gtvgdbtv1: TcxGridDBTableView;
    gc1: TcxGridDBColumn;
    gc2: TcxGridDBColumn;
    gc3: TcxGridDBColumn;
    gc4: TcxGridDBColumn;
    gc5: TcxGridDBColumn;
    gtvHealth: TcxGridDBTableView;
    gcHealthDBTableView1HEALTH_CLASS: TcxGridDBColumn;
    gcHealthDBTableView1HEALTH_DATE: TcxGridDBColumn;
    gcHealthDBTableView1HEALTH_ISSUE: TcxGridDBColumn;
    gcHealthDBTableView1HEALTH_ACTION: TcxGridDBColumn;
    gcHealthDBTableView1HEALTH_COST: TcxGridDBColumn;
    gcHealthDBTableView1T_FULLNAME: TcxGridDBColumn;
    glHealth: TcxGridLevel;
    tabExtraCurricular: TcxTabSheet;
    pnlp5: TPanel;
    btnNewActivity: TcxButton;
    btn2: TcxButton;
    btnRefreshCoCur: TcxButton;
    gCoCur: TcxGrid;
    gtvgdbtv2: TcxGridDBTableView;
    gc6: TcxGridDBColumn;
    gc7: TcxGridDBColumn;
    gc8: TcxGridDBColumn;
    gc9: TcxGridDBColumn;
    gc10: TcxGridDBColumn;
    gtvCoCur: TcxGridDBTableView;
    gcCoCurDBTableView1COCUR_CLASS: TcxGridDBColumn;
    gcCoCurDBTableView1COCUR_DATE: TcxGridDBColumn;
    gcCoCurDBTableView1COCUR_ISSUE: TcxGridDBColumn;
    gcCoCurDBTableView1COCUR_ACTION: TcxGridDBColumn;
    gcCoCurDBTableView1T_FULLNAME: TcxGridDBColumn;
    glCoCur: TcxGridLevel;
    tabLibraryActivity: TcxTabSheet;
    gbStudentFile: TcxGroupBox;
    pnl100: TPanel;
    l24: TLabel;
    pnl23: TPanel;
    dbMemo1: TcxDBMemo;
    btnViewNotes: TcxButton;
    pnl22: TPanel;
    l25: TLabel;
    l26: TLabel;
    l27: TLabel;
    l28: TLabel;
    btnStudentFileInformation: TcxButton;
    cxdbtxtdtTEdit13: TcxDBTextEdit;
    cxdbtxtdtTEdit14: TcxDBTextEdit;
    cxdbtxtdtTEdit15: TcxDBTextEdit;
    cxdbtxtdtTEdit16: TcxDBTextEdit;
    s4: TcxSplitter;
    gbSelectedStudentList: TcxGroupBox;
    gStudentList: TcxGrid;
    gtvStudentList: TcxGridDBTableView;
    gcStudentListSG_ID: TcxGridDBColumn;
    gcStudentListSG_FULLNAME: TcxGridDBColumn;
    gcStudentListSG_IS_BOARDER: TcxGridDBColumn;
    gcStudentListSG_DORMITORY: TcxGridDBColumn;
    gcStudentListSG_CLASS: TcxGridDBColumn;
    gcStudentListSG_SEX: TcxGridDBColumn;
    gcStudentListSG_RELIGION: TcxGridDBColumn;
    gcStudentListSG_NATIONALITY: TcxGridDBColumn;
    gcStudentListSG_JOINED_DATE: TcxGridDBColumn;
    gcStudentListSG_CLASS_JOINED: TcxGridDBColumn;
    gcStudentListSG_LEFT: TcxGridDBColumn;
    gcStudentListSG_LEFT_DATE: TcxGridDBColumn;
    gcStudentListSG_CLASS_LEFT: TcxGridDBColumn;
    gcStudentListSG_FEES_STATUS: TcxGridDBColumn;
    gcStudentListSG_OOS: TcxGridDBColumn;
    gcStudentListSG_EMAIL: TcxGridDBColumn;
    glStudentList: TcxGridLevel;
    gb1: TcxGroupBox;
    gIDView: TcxGrid;
    gcvIDView: TcxGridDBCardView;
    cvrIDViewSG_FULLNAME: TcxGridDBCardViewRow;
    cvrIDViewSG_DOB: TcxGridDBCardViewRow;
    cvrIDViewSG_SEX: TcxGridDBCardViewRow;
    cvrIDViewSG_CLASS: TcxGridDBCardViewRow;
    cvrIDViewSG_IS_BOARDER: TcxGridDBCardViewRow;
    cvrIDViewSG_DORMITORY: TcxGridDBCardViewRow;
    cvrIDViewSG_RELIGION: TcxGridDBCardViewRow;
    cvrIDViewSG_DISTRICT: TcxGridDBCardViewRow;
    cvrIDViewSG_NATIONALITY: TcxGridDBCardViewRow;
    cvrIDViewSG_PHOTO_1: TcxGridDBCardViewRow;
    cvrIDViewSG_LEFT: TcxGridDBCardViewRow;
    cvrIDViewSG_OOS: TcxGridDBCardViewRow;
    glIDView: TcxGridLevel;
    cxTabSheet3: TcxTabSheet;
    gStudentsParents: TcxGrid;
    gbtvStudentsParents: TcxGridDBBandedTableView;
    gbcStudentsParentsSG_PHOTO_1: TcxGridDBBandedColumn;
    gbcStudentsParentsSG_FULLNAME: TcxGridDBBandedColumn;
    gbcStudentsParentsSG_DOB: TcxGridDBBandedColumn;
    gbcStudentsParentsSG_SEX: TcxGridDBBandedColumn;
    gbcStudentsParentsSG_CLASS: TcxGridDBBandedColumn;
    gbcStudentsParentsSG_IS_BOARDER: TcxGridDBBandedColumn;
    gbcStudentsParentsSG_DORMITORY: TcxGridDBBandedColumn;
    gbcStudentsParentsSG_RELIGION: TcxGridDBBandedColumn;
    gbcStudentsParentsSG_DISTRICT: TcxGridDBBandedColumn;
    gbcStudentsParentsSG_NATIONALITY: TcxGridDBBandedColumn;
    gbcStudentsParentsSG_HOME_ADDRESS: TcxGridDBBandedColumn;
    gbcStudentsParentsSG_DISC_KOUNT: TcxGridDBBandedColumn;
    gbcStudentsParentsSG_HEALTH_KOUNT: TcxGridDBBandedColumn;
    gbcStudentsParentsSG_RESP_KOUNT: TcxGridDBBandedColumn;
    gbcStudentsParentsSG_COCURR_KOUNT: TcxGridDBBandedColumn;
    gbcStudentsParentsSG_LIBRARY_KOUNT: TcxGridDBBandedColumn;
    gbcStudentsParentsSG_LIBRARY_STATUS: TcxGridDBBandedColumn;
    gbcStudentsParentsSG_LEFT: TcxGridDBBandedColumn;
    gbcStudentsParentsSG_COMBINATION: TcxGridDBBandedColumn;
    gbcStudentsParentsSG_EMAIL: TcxGridDBBandedColumn;
    gbcStudentsParentsSG_IS_OBOG: TcxGridDBBandedColumn;
    gbcStudentsParentsSG_ADMISSION_NUMBER: TcxGridDBBandedColumn;
    gbcStudentsParentsSG_OOS: TcxGridDBBandedColumn;
    gbcStudentsParentsSG_DISABLED: TcxGridDBBandedColumn;
    gbcStudentsParentsSP_TYPE: TcxGridDBBandedColumn;
    gbcStudentsParentsSP_NAME: TcxGridDBBandedColumn;
    gbcStudentsParentsSP_TEL: TcxGridDBBandedColumn;
    gbcStudentsParentsSP_EMAIL: TcxGridDBBandedColumn;
    gbcStudentsParentsSP_OCCUPATION: TcxGridDBBandedColumn;
    gbcStudentsParentsSP_REMARKS: TcxGridDBBandedColumn;
    gbcStudentsParentsSP_SMO_SUBSCRIBE: TcxGridDBBandedColumn;
    glStudentsParents: TcxGridLevel;
    pmRefreshTreeAcademics: TPopupMenu;
    mniRefreshSelection1: TMenuItem;
    pmStaffStatistics: TPopupMenu;
    SourceData2: TMenuItem;
    AllCells2: TMenuItem;
    SelectedCellsOnly2: TMenuItem;
    BasisforCategories2: TMenuItem;
    Rows2: TMenuItem;
    Columns2: TMenuItem;
    ChangeChartType2: TMenuItem;
    N38: TMenuItem;
    Export8: TMenuItem;
    MSExcel7: TMenuItem;
    HTML7: TMenuItem;
    XML8: TMenuItem;
    PlainText7: TMenuItem;
    MenuItem15: TMenuItem;
    MenuItem16: TMenuItem;
    Refresh10: TMenuItem;
    pmStudentStatistics: TPopupMenu;
    SourceData1: TMenuItem;
    AllCells1: TMenuItem;
    SelectedCellsOnly1: TMenuItem;
    BasisforCategories1: TMenuItem;
    Rows1: TMenuItem;
    Columns1: TMenuItem;
    ChangeChartType1: TMenuItem;
    N36: TMenuItem;
    Export7: TMenuItem;
    MSExcel6: TMenuItem;
    H1: TMenuItem;
    XML7: TMenuItem;
    PlainText6: TMenuItem;
    Print2: TMenuItem;
    N37: TMenuItem;
    Refresh9: TMenuItem;
    gbStudentListing: TcxGroupBox;
    pcStudentListing: TcxPageControl;
    tabSelectClassDorm: TcxTabSheet;
    pnlp2: TPanel;
    bvl1: TBevel;
    rbStudentSelectClass: TcxRadioButton;
    rbStudentSelectDorm: TcxRadioButton;
    pcStudentSelect: TcxPageControl;
    tabStudentSelectClass: TcxTabSheet;
    tvStudentsSelectClass: TcxTreeView;
    tabStudentSelectDorm: TcxTabSheet;
    tvStudentsSelectDorm: TcxTreeView;
    gbStudentListOptions: TcxGroupBox;
    rbActiveStudentsS: TcxRadioButton;
    rbArchiveStudentsS: TcxRadioButton;
    rbBothActiveArchiveStudentsS: TcxRadioButton;
    s11: TcxSplitter;
    tabSearchStudent: TcxTabSheet;
    label1lbl8: TLabel;
    label1l2: TLabel;
    bvl2: TBevel;
    label1StudentJoined: TLabel;
    label1StartDate: TLabel;
    label1EndDate: TLabel;
    rbStudentSearchContaining: TcxRadioButton;
    rbStudentSearchStarting: TcxRadioButton;
    tEditStudentSearch: TcxTextEdit;
    btnSearchStudents: TcxButton;
    chkComboStudentSearch: TcxCheckComboBox;
    chkStudentSearchArchives: TcxCheckBox;
    chkStudentSearchDateJoined: TcxCheckBox;
    dEditStudentSearchStartDate: TcxDateEdit;
    dEditStudentSearchEndDate: TcxDateEdit;
    tabSpecialGroups: TcxTabSheet;
    label1lbl9: TLabel;
    label1lbl10: TLabel;
    gStudentGroups: TcxGrid;
    gtvStudentGroups: TcxGridDBTableView;
    gcStudentGroupsDBTableView1G_NAME: TcxGridDBColumn;
    glStudentGroups: TcxGridLevel;
    tabStudentLists: TcxTabSheet;
    pmFinanceSummaryMaster: TPopupMenu;
    Listing1: TMenuItem;
    Expand1: TMenuItem;
    Collapse1: TMenuItem;
    Grouping1: TMenuItem;
    N23: TMenuItem;
    Export4: TMenuItem;
    Excel1: TMenuItem;
    HTML4: TMenuItem;
    XML4: TMenuItem;
    PlainText3: TMenuItem;
    N28: TMenuItem;
    Refresh8: TMenuItem;
    pmFinanceDetailed: TPopupMenu;
    EditPayment1: TMenuItem;
    DeletePayment1: TMenuItem;
    N24: TMenuItem;
    RePrintReceipt1: TMenuItem;
    N27: TMenuItem;
    Listing2: TMenuItem;
    Expand2: TMenuItem;
    Collapse2: TMenuItem;
    Grouping2: TMenuItem;
    N29: TMenuItem;
    Export5: TMenuItem;
    MSExcel4: TMenuItem;
    HTML5: TMenuItem;
    XML5: TMenuItem;
    PlainText4: TMenuItem;
    N30: TMenuItem;
    Refresh7: TMenuItem;
    pmFinanceDue: TPopupMenu;
    RecordPayment1: TMenuItem;
    N25: TMenuItem;
    EditDuesPayable1: TMenuItem;
    SetAdditionalDuesPayable1: TMenuItem;
    SelectedStudent1: TMenuItem;
    EntireClass1: TMenuItem;
    N26: TMenuItem;
    Listing3: TMenuItem;
    Expand3: TMenuItem;
    Collapse3: TMenuItem;
    Grouping3: TMenuItem;
    N31: TMenuItem;
    Export6: TMenuItem;
    MSExcel5: TMenuItem;
    HTML6: TMenuItem;
    XML6: TMenuItem;
    PlainText5: TMenuItem;
    N32: TMenuItem;
    Refresh6: TMenuItem;
    bpmFinanceAdditional: TdxBarPopupMenu;
    bpmStaffManagement: TdxBarPopupMenu;
    bpmStaffEditStaff: TdxBarPopupMenu;
    bpmStaffAttendanceRecords: TdxBarPopupMenu;
    bpmAdmissionTasks: TdxBarPopupMenu;
    bpmSchoolInformation: TdxBarPopupMenu;
    bpmRegisterStudents: TdxBarPopupMenu;
    bpmUpdateStudent: TdxBarPopupMenu;
    bpmManageStudents: TdxBarPopupMenu;
    bpmAcademicsEnterMarks: TdxBarPopupMenu;
    bpmPrintAllStudents: TdxBarPopupMenu;
    bpmAcademicsView: TdxBarPopupMenu;
    bpmStaffTasks: TdxBarPopupMenu;
    bpmAcademicsUpdateMarks: TdxBarPopupMenu;
    bpmAcademicsSubjectAllocation: TdxBarPopupMenu;
    bpmComputeResults: TdxBarPopupMenu;
    bpmPrintStudentStandardForms: TdxBarPopupMenu;
    bpmPrintSelectedStudent: TdxBarPopupMenu;
    pmAcademicsDetailedMarks: TPopupMenu;
    EditStudentsMarks1: TMenuItem;
    ResultsComputationInclusion1: TMenuItem;
    N2: TMenuItem;
    MarksEntry1: TMenuItem;
    MarksEntryWizard1: TMenuItem;
    ImportMarks1: TMenuItem;
    AcademicTasks1: TMenuItem;
    ComputeResults1: TMenuItem;
    CommitMarks1: TMenuItem;
    EnterComments1: TMenuItem;
    AssignSubjects1: TMenuItem;
    DeAssignSubjects1: TMenuItem;
    AuditMarksEntry1: TMenuItem;
    PrintOptions1: TMenuItem;
    PrintCurrentView1: TMenuItem;
    AcademicReports1: TMenuItem;
    Intermediate1: TMenuItem;
    ermly1: TMenuItem;
    ranscripts1: TMenuItem;
    ResultSheets1: TMenuItem;
    Export1: TMenuItem;
    MSExcel1: TMenuItem;
    HTML1: TMenuItem;
    XML1: TMenuItem;
    PlainText1: TMenuItem;
    ViewOptions1: TMenuItem;
    ActivateFilters1: TMenuItem;
    ShowGroupingBar1: TMenuItem;
    ExpandAll1: TMenuItem;
    CollapseAll1: TMenuItem;
    N4: TMenuItem;
    SwitchView1: TMenuItem;
    SummaryView1: TMenuItem;
    N5: TMenuItem;
    ArchivedResults1: TMenuItem;
    UNEBResults1: TMenuItem;
    rendsStatistics1: TMenuItem;
    N3: TMenuItem;
    Refresh2: TMenuItem;
    pmAcademicsUNEB: TPopupMenu;
    mniExport3: TMenuItem;
    mniMSExcel3: TMenuItem;
    mniHTML3: TMenuItem;
    mniXML3: TMenuItem;
    mniPlainText2: TMenuItem;
    mniN16: TMenuItem;
    mniComputePositions1: TMenuItem;
    mniR: TMenuItem;
    mniRefresh6: TMenuItem;
    pmResultSheets: TPopupMenu;
    mniViewOptions2: TMenuItem;
    mniColumnsAutoSize1: TMenuItem;
    mniActivateFilterBar1: TMenuItem;
    mniActivateGroupingBar1: TMenuItem;
    mniN41: TMenuItem;
    mniExpandAll2: TMenuItem;
    mniCollapseAll2: TMenuItem;
    mniN42: TMenuItem;
    mniSwitchView2: TMenuItem;
    N39: TMenuItem;
    Export9: TMenuItem;
    MSExcel8: TMenuItem;
    HTML8: TMenuItem;
    XML9: TMenuItem;
    PlainText8: TMenuItem;
    Print3: TMenuItem;
    N40: TMenuItem;
    Refresh12: TMenuItem;
    pmMarksSummaries: TPopupMenu;
    ViewOptions2: TMenuItem;
    ColumnsAutoSize1: TMenuItem;
    ActivateFilterBar1: TMenuItem;
    ActivateGroupingBar1: TMenuItem;
    N41: TMenuItem;
    ExpandAll2: TMenuItem;
    CollapseAll2: TMenuItem;
    N42: TMenuItem;
    Export10: TMenuItem;
    MSExcel9: TMenuItem;
    HTML9: TMenuItem;
    XML10: TMenuItem;
    PlainText9: TMenuItem;
    Print5: TMenuItem;
    N43: TMenuItem;
    Refresh13: TMenuItem;
    pmSpecialGroups: TPopupMenu;
    ViewMembers1: TMenuItem;
    ViewGroupDetails1: TMenuItem;
    N1: TMenuItem;
    Refresh1: TMenuItem;
    pmSchoolInformation: TPopupMenu;
    Print1: TMenuItem;
    Export2: TMenuItem;
    MSExcel2: TMenuItem;
    HTML2: TMenuItem;
    XML2: TMenuItem;
    N19: TMenuItem;
    Refresh5: TMenuItem;
    bpmAcademicsAdditionalTasks: TdxBarPopupMenu;
    bpmFinanceView: TdxBarPopupMenu;
    bpmFinanceSetDuesPayable: TdxBarPopupMenu;
    bpmLibraryView: TdxBarPopupMenu;
    bpmLibraryAddItems: TdxBarPopupMenu;
    bpmUpdateLibraryItem: TdxBarPopupMenu;
    bpmLibraryLend: TdxBarPopupMenu;
    bpmLibraryReceive: TdxBarPopupMenu;
    bpmLibraryReserve: TdxBarPopupMenu;
    bpmPrintLibraryDefaulters: TdxBarPopupMenu;
    pmApplicationsFull: TPopupMenu;
    mniAdmitApplicant1: TMenuItem;
    RejectApplication1: TMenuItem;
    mniRevokeAdmission1: TMenuItem;
    EditApplication1: TMenuItem;
    N20: TMenuItem;
    PostInterviewResults1: TMenuItem;
    N21: TMenuItem;
    mniRegisterApplicantAsStudent1: TMenuItem;
    N35: TMenuItem;
    CommunicateNotify1: TMenuItem;
    mniN2: TMenuItem;
    mniExportApplicationData1: TMenuItem;
    mniMSExcelSpreadsheet1: TMenuItem;
    mniHTMLDocument1: TMenuItem;
    mniN5: TMenuItem;
    mniRefresh1: TMenuItem;
    pmStudentsList: TPopupMenu;
    MultipleStudentOptions1: TMenuItem;
    ChangeClass2: TMenuItem;
    ViewStudentProfile1: TMenuItem;
    N7: TMenuItem;
    UpdateStudentDetails1: TMenuItem;
    EditGeneralInformation1: TMenuItem;
    mniDescribeDisabilities1: TMenuItem;
    ChangeClass1: TMenuItem;
    agAsDueToRepeat1: TMenuItem;
    MarkAsLeftSchool1: TMenuItem;
    mniDiscoverUpdateSiblings1: TMenuItem;
    mniN39: TMenuItem;
    mniAssignCombination1: TMenuItem;
    TrackRecords1: TMenuItem;
    mniNewDisciplinaryIncident1: TMenuItem;
    mniNewHealthIncident1: TMenuItem;
    mniNewCoCurricularActivity1: TMenuItem;
    ManageStudent1: TMenuItem;
    N8: TMenuItem;
    OutofSchoolPermission1: TMenuItem;
    MarkAsAbsent2: TMenuItem;
    N9: TMenuItem;
    AssignRoleResponsibility1: TMenuItem;
    ViewAssignedRoles1: TMenuItem;
    N10: TMenuItem;
    ermlyReportingRegistration1: TMenuItem;
    BoTClearance1: TMenuItem;
    EoTClearance1: TMenuItem;
    ClearanceStatus1: TMenuItem;
    StudentNotes1: TMenuItem;
    PostNote1: TMenuItem;
    mniViewNotes1: TMenuItem;
    N11: TMenuItem;
    CommunicateWithParents1: TMenuItem;
    GeneralCommunication2: TMenuItem;
    SendPSMOAccountDetails3: TMenuItem;
    N6: TMenuItem;
    StudentRollCalls1: TMenuItem;
    UploadNewRollcallData1: TMenuItem;
    ViewRollCallRecords1: TMenuItem;
    N13: TMenuItem;
    RegisterNewStudent1: TMenuItem;
    N14: TMenuItem;
    Refresh3: TMenuItem;
    bpmLibraryAdditionalTasks: TdxBarPopupMenu;
    bpmPrintFinanceS: TdxBarPopupMenu;
    bpmPrintFinanceM: TdxBarPopupMenu;
    pmBookCategoryTree: TPopupMenu;
    pmLibraryBooks: TPopupMenu;
    mniLendItemOut1: TMenuItem;
    mniReceiveItemBack1: TMenuItem;
    mniN161: TMenuItem;
    mniRegisterNewLibraryItem1: TMenuItem;
    mniRegisterAdditionalItemCopies1: TMenuItem;
    mniN17: TMenuItem;
    mniEditItemDetails1: TMenuItem;
    mniUpdateImportedItem1: TMenuItem;
    mniDeleteItem1: TMenuItem;
    mniN18: TMenuItem;
    History2: TMenuItem;
    N16: TMenuItem;
    mni1: TMenuItem;
    pmLibraryBookCopies: TPopupMenu;
    mniLendItemCopyOut1: TMenuItem;
    mniReceiveItemCopyBack1: TMenuItem;
    mniN19: TMenuItem;
    mniEditItemCopyDetails1: TMenuItem;
    mniRemoveItemCopy1: TMenuItem;
    mniN20: TMenuItem;
    History1: TMenuItem;
    N15: TMenuItem;
    mniRefresh51: TMenuItem;
    pmLibraryBorrowers: TPopupMenu;
    mniLendItemtoBorrower1: TMenuItem;
    mniReceiveItemfromBorrower1: TMenuItem;
    mniN21: TMenuItem;
    mniPrintDefaulterLetter1: TMenuItem;
    mniPrintBorrowerHistory1: TMenuItem;
    mniN22: TMenuItem;
    mniRefresh61: TMenuItem;
    pmLibraryBorrowerHistory: TPopupMenu;
    mniReceiveItemBack2: TMenuItem;
    mniExtendBorrowingPeriod1: TMenuItem;
    mniMarkasNotReturnableLostStolen1: TMenuItem;
    mniN23: TMenuItem;
    mniRefresh7: TMenuItem;
    pmParents1: TPopupMenu;
    SubscribetoPSMO1: TMenuItem;
    mniRegister1: TMenuItem;
    mniFather1: TMenuItem;
    mniMother1: TMenuItem;
    mniGuardian1: TMenuItem;
    EditDetails1: TMenuItem;
    N18: TMenuItem;
    Communicate3: TMenuItem;
    General1: TMenuItem;
    SendPSMOAccountDetails1: TMenuItem;
    mniN40: TMenuItem;
    mniViewmyChildAccount1: TMenuItem;
    N12: TMenuItem;
    Refresh4: TMenuItem;
    pmParents2: TPopupMenu;
    mniSubscribetoPSMO2: TMenuItem;
    mniEditDetails2: TMenuItem;
    N17: TMenuItem;
    Communicate1: TMenuItem;
    GeneralCommunication1: TMenuItem;
    SendPSMOAccountDetails2: TMenuItem;
    MenuItem3: TMenuItem;
    mniExport2: TMenuItem;
    mniMSExcel2: TMenuItem;
    mniHTML2: TMenuItem;
    mniXML2: TMenuItem;
    mniN15: TMenuItem;
    mniRefresh5: TMenuItem;
    pmStaffList: TPopupMenu;
    UpdateStaffMember1: TMenuItem;
    GeneralInformation1: TMenuItem;
    N45: TMenuItem;
    ProfessionalInformation1: TMenuItem;
    DutiesResponsibilities1: TMenuItem;
    ClassesSubjectsTaught1: TMenuItem;
    N46: TMenuItem;
    MarkAsAbsent1: TMenuItem;
    N47: TMenuItem;
    MarkAsLeft1: TMenuItem;
    N48: TMenuItem;
    Communicate2: TMenuItem;
    SendEMail2: TMenuItem;
    SendSMS2: TMenuItem;
    N49: TMenuItem;
    SendBothEMailSMS2: TMenuItem;
    N50: TMenuItem;
    ExportStaffListMSExcel1: TMenuItem;
    Print4: TMenuItem;
    N53: TMenuItem;
    UploadNewDocument1: TMenuItem;
    N51: TMenuItem;
    ViewOptions4: TMenuItem;
    N52: TMenuItem;
    Refresh11: TMenuItem;
    pmAnnouncements: TPopupMenu;
    NewAnnouncement1: TMenuItem;
    EditNoticeAnnoucement1: TMenuItem;
    N34: TMenuItem;
    ShowCurrentAnnouncements1: TMenuItem;
    ShowActiveAnnouncements1: TMenuItem;
    PrintSelectedNotice1: TMenuItem;
    N33: TMenuItem;
    SendReminders1: TMenuItem;
    N22: TMenuItem;
    Export3: TMenuItem;
    MSExcel3: TMenuItem;
    HTML3: TMenuItem;
    XML3: TMenuItem;
    PlainText2: TMenuItem;
    pgccStudentStatistics: TcxPivotGridChartConnection;
    pgccStaffStatistics: TcxPivotGridChartConnection;
    pgccAnalysisClassPivot: TcxPivotGridChartConnection;
    bpmMenuExport: TdxBarPopupMenu;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ViewStudentHistory(history_option: Integer);
    procedure dxBarButton2Click(Sender: TObject);
    procedure dxBarButton3Click(Sender: TObject);
    procedure rbStudentsClassicViewClick(Sender: TObject);
    procedure ListStudents(student_list_option: Integer);
    procedure FormCreate(Sender: TObject);
    procedure rbStudentsGalleryViewClick(Sender: TObject);
    procedure rbStudentsParentsViewClick(Sender: TObject);
    procedure rbStudentSelectClassClick(Sender: TObject);
    procedure rbStudentSelectDormClick(Sender: TObject);
    procedure rbActiveStudentsSClick(Sender: TObject);
    procedure rbArchiveStudentsSClick(Sender: TObject);
    procedure rbBothActiveArchiveStudentsSClick(Sender: TObject);
    //procedure tvStudentsSelectClassClick(Sender: TObject);
//    procedure tvStudentsSelectClassDblClick(Sender: TObject);
    procedure chkStudentSearchDateJoinedClick(Sender: TObject);
    procedure btnSearchStudentsClick(Sender: TObject);
    procedure gtvStudentGroupsDblClick(Sender: TObject);
    //procedure ViewStudentProfile;
    procedure ListApplications(application_list_option:Integer);
    procedure gtvStudentListCellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure btnStudentFileInformationClick(Sender: TObject);
    procedure btnViewNotesClick(Sender: TObject);
    procedure tvApplicationListingClick(Sender: TObject);
    procedure rbAllApplicationsClick(Sender: TObject);
    procedure rbAdmittedApplicationsClick(Sender: TObject);
    procedure rbRejectedApplicationsClick(Sender: TObject);
    procedure rbUnprocessedApplicationsClick(Sender: TObject);
    procedure rbApplicationsFullClick(Sender: TObject);
    procedure rbApplicationsWizardSheetClick(Sender: TObject);
    procedure gtvApplicationsFullFocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure btnSearchApplicantClick(Sender: TObject);
    procedure tvStudentsSelectDormClick(Sender: TObject);
    procedure tvStudentsSelectClassClick(Sender: TObject);
    procedure tvStudentsSelectClassDblClick(Sender: TObject);
//    procedure cxRadioButton1Click(Sender: TObject);
  private
    student_list_option,application_list_option,application_term_index,student_list_class_index: Integer;
  public
    { Public declarations }
  protected
    sdstudentsparents,sdstudents,sdpreviousresults,sdusereginfostd,sdDJinfo,sdstreamreginfostd,sdcombinationreginfostd,sddistrictreginfostd :Tsqldataset;
    dspstudentsparents,dspstudents,dsppreviousresults,dspusereginfostd,dspDJinfo,dspdistrictreginfostd,dspcombinationreginfostd,dspstreamreginfostd :TDataSetProvider;
    cdstudentsparents,cdstudents,cdpreviousresults,cdusereginfostd,cdDJinfo,cdstreamreginfostd,cdcombinationreginfostd,cddistrictreginfostd:Tclientdataset;
    dstudentsparents,dstudents,dpreviousresults,dsusereginfostd,dDJinfo,dsdistrictreginfostd,dscombinationreginfostd,dsstreamreginfostd: TDataSource;
  end;

var
  studentinformationmanagement: Tstudentinformationmanagement;

implementation

uses register_newstudent_unit,register_oldstudent_unit, data_unit;

{$R *.dfm}

procedure Tstudentinformationmanagement.btnSearchApplicantClick(
  Sender: TObject);
begin
application_list_option:=10;
  ListApplications(application_list_option);
end;

procedure Tstudentinformationmanagement.btnSearchStudentsClick(Sender: TObject);
begin
student_list_option:=31;
  ListStudents(student_list_option);
end;


procedure Tstudentinformationmanagement.gtvApplicationsFullFocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
  cdstreamreginfostd.Active:=False;
  sdstreamreginfostd.Active:=False;

  cdcombinationreginfostd.Active:=False;
  sdcombinationreginfostd.Active:=False;

  cdpreviousresults.Active:=False;
  sdpreviousresults.Active:=False;


  if cdusereginfostd.Active then
  begin
    gtvApplicantParents.OptionsView.NoDataToDisplayInfoText:='No Parents Posted for '+cdusereginfostd.FieldByName('SAG_SURNAME').AsString+' '+cdusereginfostd.FieldByName('SAG_OTHERNAMES').AsString;
    gtvApplicantFormerSchools.OptionsView.NoDataToDisplayInfoText:='No Former Schools Posted for '+cdusereginfostd.FieldByName('SAG_SURNAME').AsString+' '+cdusereginfostd.FieldByName('SAG_OTHERNAMES').AsString;
    gtvApplicantFormerResults.OptionsView.NoDataToDisplayInfoText:='No Previous Results Posted for '+cdusereginfostd.FieldByName('SAG_SURNAME').AsString+' '+cdusereginfostd.FieldByName('SAG_OTHERNAMES').AsString;

    sdstreamreginfostd.Params[0].AssignField(cdusereginfostd.FieldByName('SAG_ID'));
    cdstreamreginfostd.Open;

    sdcombinationreginfostd.Params[0].AssignField(cdusereginfostd.FieldByName('SAG_ID'));
    cdcombinationreginfostd.Open;

    sdpreviousresults.Params[0].AssignField(cdusereginfostd.FieldByName('SAG_ID'));
    cdpreviousresults.Open;

  end
  else
  begin
    gtvApplicantParents.OptionsView.NoDataToDisplayInfoText:='No Applicant Currently Selected';
    gtvApplicantFormerSchools.OptionsView.NoDataToDisplayInfoText:='No Applicant Currently Selected';
    gtvApplicantFormerResults.OptionsView.NoDataToDisplayInfoText:='No Applicant Currently Selected';
  end;
end;

procedure Tstudentinformationmanagement.gtvStudentGroupsDblClick(Sender: TObject);
begin
  if gtvStudentGroups.DataController.RecordCount = 0 then
  begin
    MessageDlg('No student groups/associations have been defined yet', mtWarning, [mbOK], 0);
    Exit;
  end;

  ListStudents(41);
end;

procedure Tstudentinformationmanagement.gtvStudentListCellDblClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
   //ViewStudentProfile;
end;

procedure Tstudentinformationmanagement.ViewStudentHistory(history_option: Integer);
begin
  //0 = Progress Status, 1 = Notes, 2 = Student File Details



end;

procedure Tstudentinformationmanagement.btnStudentFileInformationClick(
  Sender: TObject);
begin
ViewStudentHistory(2);
end;

procedure Tstudentinformationmanagement.btnViewNotesClick(Sender: TObject);
begin
ViewStudentHistory(1);
end;

procedure Tstudentinformationmanagement.chkStudentSearchDateJoinedClick(Sender: TObject);
begin
  if chkStudentSearchDateJoined.Checked then btnSearchStudents.Top:=261
  else btnSearchStudents.Top:=181;

  label1StudentJoined.Visible:=chkStudentSearchDateJoined.Checked;
  label1StartDate.Visible:=chkStudentSearchDateJoined.Checked;
  label1EndDate.Visible:=chkStudentSearchDateJoined.Checked;
  dEditStudentSearchStartDate.Visible:=chkStudentSearchDateJoined.Checked;
  dEditStudentSearchEndDate.Visible:=chkStudentSearchDateJoined.Checked;
  label1StudentJoined.Enabled:=chkStudentSearchDateJoined.Checked;
  label1StartDate.Enabled:=chkStudentSearchDateJoined.Checked;
  label1EndDate.Enabled:=chkStudentSearchDateJoined.Checked;
  dEditStudentSearchStartDate.Enabled:=chkStudentSearchDateJoined.Checked;
  dEditStudentSearchEndDate.Enabled:=chkStudentSearchDateJoined.Checked;
end;

procedure Tstudentinformationmanagement.dxBarButton2Click(Sender: TObject);
begin
newstudentregistration:=Tnewstudentregistration.Create(Application);
//newstudentregistration.ShowModal;
end;

procedure Tstudentinformationmanagement.dxBarButton3Click(Sender: TObject);
begin
oldstudentregistration:=Toldstudentregistration.Create(Application);
oldstudentregistration.ShowModal;
end;

procedure Tstudentinformationmanagement.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
studentinformationmanagement:=nil;
Action:=caFree;
end;

procedure Tstudentinformationmanagement.FormCreate(Sender: TObject);
begin
{creating the sql dataset}
         sdusereginfostd :=TSQLDataSet.Create(Self);
         sdstreamreginfostd :=TSQLDataSet.Create(Self);
         sdDJinfo:=TSQLDataSet.Create(Self);
         sdpreviousresults:=TSQLDataSet.Create(Self);
         sdstudents:=TSQLDataSet.Create(Self);
         sdstudentsparents:=TSQLDataSet.Create(Self);




      {Assigning sqlconnection to the sql datasets}

        sdDJinfo.SQLConnection:=dmPSM.cPSM;
        sdusereginfostd.SQLConnection:=dmPSM.cPSM;
        sdstreamreginfostd.SQLConnection :=dmPSM.cPSM;
        sdpreviousresults.SQLConnection:=dmPSM.cPSM;
        sdstudents.SQLConnection:=dmPSM.cPSM;
        sdstudentsparents.SQLConnection:=dmPSM.cPSM;

      //**********************************************************


      {Creating the dataset providers}
    dspusereginfostd:=TDataSetProvider.Create(self);
    dspstreamreginfostd :=TDataSetProvider.Create(Self);
    dspDJinfo:=TDataSetProvider.Create(Self);
    dsppreviousresults:=TDataSetProvider.Create(Self);
    dspstudents:=TDataSetProvider.Create(Self);
    dspstudentsparents:=TDataSetProvider.Create(Self);

    {Assigning DataSet Providers Names}
    dspusereginfostd.Name:='useDataSetProviderinfo';
    dspstreamreginfostd.Name:='streamDataSetProviderinfo';
    dspDJinfo.Name:='DJDataSetProviderinfo';
    dsppreviousresults.Name:='studentpreviousresults';
    dspstudents.Name:='studentsprovider';
    dspstudentsparents.Name:='studentsparentsprovider';
//**********************************************************

        {Assigning datasetproviders datasets}
          dspusereginfostd.DataSet:=sdusereginfostd;
          dspstreamreginfostd.DataSet:=sdstreamreginfostd;
          dspDJinfo.DataSet:=sdDJinfo;
          dsppreviousresults.DataSet:=sdpreviousresults;
          dspstudents.DataSet:=sdstudents;
          dspstudentsparents.DataSet:=sdstudentsparents;

          {Creating ClientDataSets}
          cdusereginfostd :=TClientDataSet.Create(self);
          cdstreamreginfostd :=TClientDataSet.Create(Self);
          cdDJinfo:=TClientDataSet.Create(Self);
          cdpreviousresults:=TClientDataSet.Create(Self);
          cdstudents:=TClientDataSet.Create(Self);
          cdstudentsparents:=TClientDataSet.Create(Self);

      {Assigning ClientDatasets DataSetProviders}
      cdusereginfostd.ProviderName:='useDataSetProviderinfo';
      cdstreamreginfostd.ProviderName:='streamDataSetProviderinfo';
      cdDJinfo.ProviderName:='DJDataSetProviderinfo';
      cdpreviousresults.ProviderName:='studentpreviousresults';
      cdstudents.ProviderName:='studentsprovider';
      cdstudentsparents.ProviderName:='studentsparentsprovider';

//**********************************************************

  {Creating DataSources}

   dsusereginfostd:=TDataSource.Create(Self);
   dsstreamreginfostd :=TDataSource.Create(Self);
    dDJinfo:=TDataSource.Create(Self);
    dpreviousresults:=TDataSource.Create(Self);
    dstudents:=TDataSource.Create(Self);
    dstudentsparents:=TDataSource.Create(Self);


   {Assiging Datasets to the dataSources}

    dsusereginfostd.DataSet:=cdusereginfostd;
    dsstreamreginfostd.DataSet :=cdstreamreginfostd;
     dDJinfo.DataSet:=cdDJinfo;
     dpreviousresults.DataSet:=cdpreviousresults;
     dstudents.DataSet:=cdstudents;
     dstudentsparents.DataSet:=cdstudentsparents;

     sdstudents.CommandText:='select concat(sg.SG_SURNAME,'',sg.SG_OTHER1NAME) as SG_FULLNAME, sg.*, concat(t.T_SURNAME,'',t.T_OTHERNAMES) as T_FULLNAME from students_general SG inner join staff_general T on SG.SG_FILE_CREATED_BY = T.T_ID order by concat(sg.SG_SURNAME,'',sg.SG_OTHERNAME)';
     sdstudents.open;
end;

procedure Tstudentinformationmanagement.rbActiveStudentsSClick(Sender: TObject);
begin
ListStudents(student_list_option);
end;

procedure Tstudentinformationmanagement.rbAdmittedApplicationsClick(
  Sender: TObject);
begin
ListApplications(application_list_option);
end;

procedure Tstudentinformationmanagement.rbAllApplicationsClick(Sender: TObject);
begin
ListApplications(application_list_option);
end;

procedure Tstudentinformationmanagement.rbApplicationsFullClick(
  Sender: TObject);
begin
glApplicationsWizard.Visible:=False;
  glApplicationsFull.Visible:=True;

  ListApplications(application_list_option);

  pcApplicationAdditionalDetails.Visible:=True;
end;

procedure Tstudentinformationmanagement.rbApplicationsWizardSheetClick(
  Sender: TObject);
begin
glApplicationsFull.Visible:=False;
  glApplicationsWizard.Visible:=True;

  ListApplications(application_list_option);

  pcApplicationAdditionalDetails.Visible:=False;
end;

procedure Tstudentinformationmanagement.rbArchiveStudentsSClick(
  Sender: TObject);
begin
ListStudents(student_list_option);
end;

procedure Tstudentinformationmanagement.rbBothActiveArchiveStudentsSClick(
  Sender: TObject);
begin
ListStudents(student_list_option);
end;

procedure Tstudentinformationmanagement.rbRejectedApplicationsClick(
  Sender: TObject);
begin
ListApplications(application_list_option);
end;

procedure Tstudentinformationmanagement.rbStudentSelectClassClick(
  Sender: TObject);
begin
pcStudentSelect.ActivePage:=tabStudentSelectClass;
end;

procedure Tstudentinformationmanagement.rbStudentSelectDormClick(
  Sender: TObject);
begin
pcStudentSelect.ActivePage:=tabStudentSelectDorm;
end;


procedure Tstudentinformationmanagement.rbUnprocessedApplicationsClick(
  Sender: TObject);
begin
ListApplications(application_list_option);
end;

procedure Tstudentinformationmanagement.ListApplications(application_list_option: Integer);
begin
  cdusereginfostd.Active:=False;
  sdusereginfostd.Active:=False;
  cdDJinfo.Active:=False;
  sdDJinfo.Active:=False;

  if application_list_option = 0 then   //All Applications
  begin
    sdusereginfostd.CommandText:='select SAG.*, concat(SAG.SAG_SURNAME, '' '', SAG.SAG_OTHERNAMES) SAG_FULLNAME from students_admissions_general SAG ';
    if ((rbAdmittedApplications.Checked) or (rbRejectedApplications.Checked) or (rbUnprocessedApplications.Checked)) then
    begin
      sdusereginfostd.CommandText:=sdusereginfostd.CommandText +' where SAG.SAG_ADMITTED = :SAG_ADMITTED ';
    end;
    sdusereginfostd.CommandText:=sdusereginfostd.CommandText +' order by SAG_TARGET_YEAR desc, SAG_TARGET_TERM, SAG_TARGET_CLASS, SAG_SURNAME, SAG_OTHERNAMES ';

    if (rbAdmittedApplications.Checked) then
    begin
      sdusereginfostd.Params[0].AsString:='Y';
    end;

    if (rbRejectedApplications.Checked) then
    begin
      sdusereginfostd.Params[0].AsString:='R';
    end;

    if (rbUnprocessedApplications.Checked) then
    begin
      sdusereginfostd.Params[0].AsString:='N';
    end;

    gcApplicationsFullSAG_TARGET_YEAR.GroupIndex:=0;
    gcApplicationsFullSAG_TARGET_TERM.GroupIndex:=1;

  end;

  if application_list_option = 1 then   //Particular Academic Year
  begin
    sdusereginfostd.CommandText:='select SAG.*, concat(SAG.SAG_SURNAME, '' '', SAG.SAG_OTHERNAMES) SAG_FULLNAME from students_admissions_general SAG where SAG_TARGET_YEAR = :SAG_TARGET_YEAR ';
    if ((rbAdmittedApplications.Checked) or (rbRejectedApplications.Checked) or (rbUnprocessedApplications.Checked)) then
    begin
      sdusereginfostd.CommandText:=sdusereginfostd.CommandText +' and SAG.SAG_ADMITTED = :SAG_ADMITTED ';
    end;
    sdusereginfostd.CommandText:=sdusereginfostd.CommandText +' order by SAG_TARGET_TERM, SAG_SURNAME, SAG_OTHERNAMES ';

    sdusereginfostd.Params[0].AsString:=tvApplicationListing.Selected.Text;

    if (rbAdmittedApplications.Checked) then
    begin
      sdusereginfostd.Params[1].AsString:='Y';
    end;

    if (rbRejectedApplications.Checked) then
    begin
      sdusereginfostd.Params[1].AsString:='R';
    end;

    if (rbUnprocessedApplications.Checked) then
    begin
      sdusereginfostd.Params[1].AsString:='N';
    end;

    gcApplicationsFullSAG_TARGET_YEAR.GroupIndex:=-1;
    gcApplicationsFullSAG_TARGET_TERM.GroupIndex:=0;

  end;

  if application_list_option = 2 then   //Particular Academic Year and Term
  begin
    sdusereginfostd.CommandText:='select SAG.*, concat(SAG.SAG_SURNAME, '' '', SAG.SAG_OTHERNAMES) SAG_FULLNAME from students_admissions_general SAG where SAG_TARGET_YEAR = :SAG_TARGET_YEAR and SAG_TARGET_TERM = :SAG_TARGET_TERM ';
    if ((rbAdmittedApplications.Checked) or (rbRejectedApplications.Checked) or (rbUnprocessedApplications.Checked)) then
    begin
      sdusereginfostd.CommandText:=sdusereginfostd.CommandText +' and SAG.SAG_ADMITTED = :SAG_ADMITTED ';
    end;
    sdusereginfostd.CommandText:=sdusereginfostd.CommandText +' order by SAG_SURNAME, SAG_OTHERNAMES ';

    sdusereginfostd.Params[0].AsString:=tvApplicationListing.Selected.Parent.Text;
    sdusereginfostd.Params[1].AsString:=AnsiReplaceStr(tvApplicationListing.Selected.Text, 'Term ', '');

    if (rbAdmittedApplications.Checked) then
    begin
      sdusereginfostd.Params[2].AsString:='Y';
    end;

    if (rbRejectedApplications.Checked) then
    begin
      sdusereginfostd.Params[2].AsString:='R';
    end;

    if (rbUnprocessedApplications.Checked) then
    begin
      sdusereginfostd.Params[2].AsString:='N';
    end;

    gcApplicationsFullSAG_TARGET_YEAR.GroupIndex:=-1;
    gcApplicationsFullSAG_TARGET_TERM.GroupIndex:=-1;

  end;

  if application_list_option = 10 then   //Search for Student
  begin
    if Length(Trim(tEditSearchApplicant.Text)) = 0 then
    begin
      MessageDlg('Please enter the search item (full or partial applicant name)', mtInformation,[mbOk],0);
      tEditSearchApplicant.SetFocus;
      Exit;
    end;

    sdusereginfostd.CommandText:='select SAG.*, concat(SAG.SAG_SURNAME, '' '', SAG.SAG_OTHERNAMES) SAG_FULLNAME from students_admissions_general SAG where (concat(SAG_SURNAME, '' '', SAG_OTHERNAMES) like :SEARCH_APPLICANT or concat(SAG_OTHERNAMES, '' '', SAG_SURNAME) like :SEARCH_APPLICANT) '+
                                           'or SAG_REG_NO = :SEARCH_APPLICANT or SAG_TELEPHONE like :SEARCH_APPLICANT or SAG_EMAIL_ADDRESS like :SEARCH_APPLICANT order by SAG_TARGET_YEAR desc, SAG_TARGET_TERM, SAG_SURNAME, SAG_OTHERNAMES ';

    sdusereginfostd.ParamByName('SEARCH_APPLICANT').AsString:='%'+AnsiReplaceStr(Trim(tEditSearchApplicant.Text), ' ','%')+'%';

    gcApplicationsFullSAG_TARGET_YEAR.GroupIndex:=0;
    gcApplicationsFullSAG_TARGET_TERM.GroupIndex:=1;

  end;


  if application_list_option = 11 then   //Quick Search for Student
  begin
    if Length(Trim(bEditQuickSearchApplications.Text)) = 0 then
    begin
      MessageDlg('Please enter the quick search item (full or partial applicant name)', mtInformation,[mbOk],0);
      bEditQuickSearchApplications.SetFocus;
      Exit;
    end;

    sdusereginfostd.CommandText:='select SAG.*, concat(SAG.SAG_SURNAME, '' '', SAG.SAG_OTHERNAMES) SAG_FULLNAME from students_admissions_general SAG where (concat(SAG_SURNAME, '' '', SAG_OTHERNAMES) like :SEARCH_APPLICANT or concat(SAG_OTHERNAMES, '' '', SAG_SURNAME) like :SEARCH_APPLICANT) '+
                                           'or SAG_REG_NO = :SEARCH_APPLICANT or SAG_TELEPHONE like :SEARCH_APPLICANT or SAG_EMAIL_ADDRESS like :SEARCH_APPLICANT order by SAG_TARGET_YEAR desc, SAG_TARGET_TERM, SAG_SURNAME, SAG_OTHERNAMES ';

    sdusereginfostd.ParamByName('SEARCH_APPLICANT').AsString:='%'+AnsiReplaceStr(Trim(bEditQuickSearchApplications.Text),' ','%')+'%';

    gcApplicationsFullSAG_TARGET_YEAR.GroupIndex:=0;
    gcApplicationsFullSAG_TARGET_TERM.GroupIndex:=1;

  end;


  if rbApplicationsFull.Checked then
  begin
    cdusereginfostd.Open;

    if (cdusereginfostd.Eof) and (cdusereginfostd.Bof) then
    begin
      case application_list_option of
        0: begin
             if (rbAdmittedApplications.Checked) then
             begin
               MessageDlg('No successful applications found', mtInformation, [mbOK], 0);
             end
             else if (rbRejectedApplications.Checked) then
             begin
               MessageDlg('No rejected applications found', mtInformation, [mbOK], 0);
             end
             else if (rbUnprocessedApplications.Checked) then
             begin
               MessageDlg('No unprocessed applications found', mtInformation, [mbOK], 0);
             end
             else
             begin
               MessageDlg('No applications have been posted', mtInformation, [mbOK], 0);
             end;
           end;

        1: begin
             if (rbAdmittedApplications.Checked) then
             begin
               MessageDlg('No successful applications found for the selected target year', mtInformation, [mbOK], 0);
             end
             else if (rbRejectedApplications.Checked) then
             begin
               MessageDlg('No rejected applications found for the selected target year', mtInformation, [mbOK], 0);
             end
             else if (rbUnprocessedApplications.Checked) then
             begin
               MessageDlg('No unprocessed applications found for the selected target year', mtInformation, [mbOK], 0);
             end
             else
             begin
               MessageDlg('No applications have been posted for the selected target year', mtInformation, [mbOK], 0);
             end;
           end;

        2: begin
             if (rbAdmittedApplications.Checked) then
             begin
               MessageDlg('No successful applications found for the selected target year/term', mtInformation, [mbOK], 0);
             end
             else if (rbRejectedApplications.Checked) then
             begin
               MessageDlg('No rejected applications found for the selected target year/term', mtInformation, [mbOK], 0);
             end
             else if (rbUnprocessedApplications.Checked) then
             begin
               MessageDlg('No unprocessed applications found for the selected target year/term', mtInformation, [mbOK], 0);
             end
             else
             begin
               MessageDlg('No applications have been posted for the selected target year/term', mtInformation, [mbOK], 0);
             end;
           end;

        10..11: MessageDlg('No applicant matching your search criteria was found. Please revise your search terms and try again', mtInformation, [mbOK], 0);
      end;

      cdusereginfostd.Close;
    end;
  end
  else if rbApplicationsWizardSheet.Checked then
  begin
    cdDJinfo.Open;

    if (cdDJinfo.Eof) and (cdDJinfo.Bof) then
    begin
      case application_list_option of
        0: MessageDlg('No admissions wizard run results found', mtInformation, [mbOK], 0);
        1: MessageDlg('No admissions wizard run results for the selected target year were found', mtInformation, [mbOK], 0);
        2: MessageDlg('No admissions wizard run results for the selected target year/term were found', mtInformation, [mbOK], 0);
        10..11: MessageDlg('No applicant matching your search criteria was found in the admission wizard run results. Please revise your search terms and try again', mtInformation, [mbOK], 0);
      end;

      cdDJinfo.Close;
    end;
  end;

end;

procedure Tstudentinformationmanagement.tvApplicationListingClick(
  Sender: TObject);
begin
if tvApplicationListing.Selected.Level = 2 then
  begin
    if tvApplicationListing.Selected.AbsoluteIndex <> application_term_index then
    begin
      application_list_option:=2;
      ListApplications(application_list_option);
    end;
  end;

  application_term_index:=tvApplicationListing.Selected.AbsoluteIndex;
end;

procedure Tstudentinformationmanagement.rbStudentsClassicViewClick(Sender: TObject);
begin
  pcStudentsView.ActivePageIndex:=0;

  if not cdstudents.Active then ListStudents(student_list_option);

end;

procedure Tstudentinformationmanagement.rbStudentsGalleryViewClick(Sender: TObject);
begin
  pcStudentsView.ActivePageIndex:=1;

  if not cdstudents.Active then ListStudents(student_list_option);
end;

procedure Tstudentinformationmanagement.rbStudentsParentsViewClick(Sender: TObject);
begin
  pcStudentsView.ActivePageIndex:=2;

  if not cdstudentsParents.Active then
  begin
    if cdstudents.Active then
    begin
      ListStudents(student_list_option);
    end;
  end;
end;


procedure Tstudentinformationmanagement.tvStudentsSelectClassClick(
  Sender: TObject);
begin
if not tvStudentsSelectClass.Selected.HasChildren then
  begin
    if tvStudentsSelectClass.Selected.AbsoluteIndex <> student_list_class_index then
    begin
      student_list_option:=tvStudentsSelectClass.Selected.Level + 10;
      lSchoolStudents.Caption:='School Students: '+tvStudentsSelectClass.Selected.Text;
      ListStudents(student_list_option);
    end;
  end;

  student_list_class_index:=tvStudentsSelectClass.Selected.AbsoluteIndex;
end;

procedure Tstudentinformationmanagement.tvStudentsSelectClassDblClick(
  Sender: TObject);
begin
//tv_node_parent:=tvStudentsSelectClass.Selected.HasChildren;

  student_list_option:=tvStudentsSelectClass.Selected.Level + 10;

  student_list_class_index:=tvStudentsSelectClass.Selected.AbsoluteIndex;

  lSchoolStudents.Caption:='School Students: '+tvStudentsSelectClass.Selected.Text;

  ListStudents(student_list_option);
end;

procedure Tstudentinformationmanagement.tvStudentsSelectDormClick(
  Sender: TObject);
begin
if tvStudentsSelectDorm.Selected.IsFirstNode then
  begin
    student_list_option:=21;
    ListStudents(student_list_option);
  end
  else
  begin
    if tvStudentsSelectDorm.Selected.AbsoluteIndex = 1 then
    begin
      student_list_option:=22;
      ListStudents(student_list_option);
    end
    else
    begin
      student_list_option:=23;
      ListStudents(student_list_option);
    end;
  end;
end;

procedure Tstudentinformationmanagement.ListStudents(student_list_option: Integer);
var
  i: Integer;
  all_cbs: Boolean;
  req_curriculum, req_level, req_class, req_class_stream: string;
begin
  //student_list_option: 10 - Class Tree, 20 - Dormitory, 30 - Search, 40 - Groups

  sdusereginfostd.Active:=False;
  sdusereginfostd.Active:=False;

  sdDJinfo.Active:=False;
  cdDJinfo.Active:=False;

  if student_list_option = -1 then Exit;

  //ShowMessage(IntToStr(student_list_option)+' : '+IntToStr(student_list_class_index));
  //Exit;

  //10 = All Students
  //11 = Ug or Cam
  //12 = Ug: Pre-Primary, Primary, O - Level, A - Level or Cam: Primary, Secondary 1, Secondary 2, Advanced
  //13 = Ug: Class and if Pre-Primary has no streams - class/stream OR Cam: Year/Class
  //14 = Ug. Stream
  //15 = Not yet in use...


  if student_list_option = 10 then   //All School Students
  begin
    //students
    sdusereginfostd.CommandText:='select concat(SG.SG_SURNAME, '' '', SG.SG_OTHERNAME) as SG_FULLNAME, SG.*, concat(T.T_SURNAME, '' '', T.T_OTHERNAMES) as T_FULLNAME '+
                                    'from students_general SG inner join staff_general T on SG.SG_CREATED_BY = T.T_ID ';

    if (rbActiveStudentsS.Checked) or (rbArchiveStudentsS.Checked) then sdusereginfostd.CommandText:=sdusereginfostd.CommandText+' where SG.SG_LEFT = :SG_LEFT order by concat(SG.SG_SURNAME, '' '', SG.SG_OTHERNAME)'
    else sdusereginfostd.CommandText:=sdusereginfostd.CommandText+' order by concat(SG.SG_SURNAME, '' '', SG.SG_OTHERNAME)';

    if rbActiveStudentsS.Checked then sdusereginfostd.Params[0].AsString:='N';
    if rbArchiveStudentsS.Checked then sdusereginfostd.Params[0].AsString:='Y';

    //parents
    sdstreamreginfostd.CommandText:='SELECT concat(sg.SG_SURNAME, '' '', sg.SG_OTHERNAME) SG_FULLNAME, sg.*, sp.* FROM students_general sg inner join students_parents sp on sg.SG_ID = sp.SP_SG_ID ';

    if (rbActiveStudentsS.Checked) or (rbArchiveStudentsS.Checked) then sdstreamreginfostd.CommandText:=sdstreamreginfostd.CommandText+' where sg.SG_LEFT = :SG_LEFT order by concat(sg.SG_SURNAME, '' '', sg.SG_OTHERNAME)'
    else sdstreamreginfostd.CommandText:=sdstreamreginfostd.CommandText+' order by concat(sg.SG_SURNAME, '' '', sg.SG_OTHERNAME)';

    if rbActiveStudentsS.Checked then sdstreamreginfostd.Params[0].AsString:='N';
    if rbArchiveStudentsS.Checked then sdstreamreginfostd.Params[0].AsString:='Y';
  end;


  if student_list_option = 11 then   //Ug or Cam
  begin
    if tvStudentsSelectClass.Selected.Text = 'Ugandan Curriculum' then req_curriculum:='UNEB'
    else if tvStudentsSelectClass.Selected.Text = 'Cambridge International' then req_curriculum:='CIE'
    else req_curriculum:='NA';


    sdusereginfostd.CommandText:='select concat(SG.SG_SURNAME, '' '', SG.SG_OTHERNAME) as SG_FULLNAME, SG.*, concat(T.T_SURNAME, '' '', T.T_OTHERNAMES) as T_FULLNAME '+
                                    'from students_general SG inner join staff_general T on SG.SG_CREATED_BY = T.T_ID ';

    if (rbActiveStudentsS.Checked) or (rbArchiveStudentsS.Checked) then
    begin
      sdusereginfostd.CommandText:=sdusereginfostd.CommandText+' where SG.SG_LEFT = :SG_LEFT and SG.SG_CLASS in (select distinct trim(concat(SC_CLASS, '' '', ifnull(SC_STREAM,''''))) CLASS_STREAM from school_classes where SC_CURRICULUM = :SC_CURRICULUM) order by concat(SG.SG_SURNAME, '' '', SG.SG_OTHERNAME) ';
    end
    else
    begin
      sdusereginfostd.CommandText:=sdusereginfostd.CommandText+' where SG.SG_CLASS in (select distinct trim(concat(SC_CLASS, '' '', ifnull(SC_STREAM,''''))) CLASS_STREAM from school_classes where SC_CURRICULUM = :SC_CURRICULUM) order by concat(SG.SG_SURNAME, '' '', SG.SG_OTHERNAME) ';
    end;

    if rbActiveStudentsS.Checked then sdusereginfostd.ParamByName('SG_LEFT').AsString:='N';
    if rbArchiveStudentsS.Checked then sdusereginfostd.ParamByName('SG_LEFT').AsString:='Y';

    sdusereginfostd.ParamByName('SC_CURRICULUM').AsString:=req_curriculum;

    //parents
    sdstreamreginfostd.CommandText:='SELECT concat(sg.SG_SURNAME, '' '', sg.SG_OTHERNAME) SG_FULLNAME, sg.*, sp.* FROM students_general sg inner join students_parents sp on sg.SG_ID = sp.SP_SG_ID ';

    if (rbActiveStudentsS.Checked) or (rbArchiveStudentsS.Checked) then sdstreamreginfostd.CommandText:=sdstreamreginfostd.CommandText+' where sg.SG_LEFT = :SG_LEFT and sg.SG_CLASS in (select distinct trim(concat(SC_CLASS, '' '', ifnull(SC_STREAM,''''))) CLASS_STREAM from school_classes where SC_CURRICULUM = :SC_CURRICULUM) order by concat(sg.SG_SURNAME, '' '', sg.SG_OTHERNAME) '
    else sdstreamreginfostd.CommandText:=sdstreamreginfostd.CommandText+' where sg.SG_CLASS in (select distinct trim(concat(SC_CLASS, '' '', ifnull(SC_STREAM,''''))) CLASS_STREAM from school_classes where SC_CURRICULUM = :SC_CURRICULUM) order by concat(sg.SG_SURNAME, '' '', sg.SG_OTHERNAME) ';

    if rbActiveStudentsS.Checked then sdstreamreginfostd.ParamByName('SG_LEFT').AsString:='N';
    if rbArchiveStudentsS.Checked then sdstreamreginfostd.ParamByName('SG_LEFT').AsString:='Y';

    sdstreamreginfostd.ParamByName('SC_CURRICULUM').AsString:=req_curriculum;
  end;



  if student_list_option = 12 then   //12 = Ug: Pre-Primary, Primary, O - Level, A - Level or Cam: Primary, Secondary 1, Secondary 2, Advanced
  begin
    if tvStudentsSelectClass.Selected.Parent.Text = 'Ugandan Curriculum' then req_curriculum:='UNEB'
    else if tvStudentsSelectClass.Selected.Parent.Text = 'Cambridge International' then req_curriculum:='CIE'
    else req_curriculum:='NA';

    req_level:=tvStudentsSelectClass.Selected.Text;
    if req_curriculum = 'CIE' then req_level:='Cambridge '+req_level;



    sdusereginfostd.CommandText:='select concat(SG.SG_SURNAME, '' '', SG.SG_OTHERNAME) as SG_FULLNAME, SG.*, concat(T.T_SURNAME, '' '', T.T_OTHERNAMES) as T_FULLNAME '+
                                    'from students_general SG inner join staff_general T on SG.SG_CREATED_BY = T.T_ID ';

    if (rbActiveStudentsS.Checked) or (rbArchiveStudentsS.Checked) then
    begin
      sdusereginfostd.CommandText:=sdusereginfostd.CommandText+' where SG.SG_LEFT = :SG_LEFT and SG.SG_CLASS in (select distinct trim(concat(SC_CLASS, '' '', ifnull(SC_STREAM,''''))) CLASS_STREAM from school_classes where SC_CURRICULUM = :SC_CURRICULUM and SC_LEVEL = :SC_LEVEL) '+
                                                                     'order by concat(SG.SG_SURNAME, '' '', SG.SG_OTHERNAME) ';
    end
    else
    begin
      sdusereginfostd.CommandText:=sdusereginfostd.CommandText+' where SG.SG_CLASS in (select distinct trim(concat(SC_CLASS, '' '', ifnull(SC_STREAM,''''))) CLASS_STREAM from school_classes where SC_CURRICULUM = :SC_CURRICULUM and SC_LEVEL = :SC_LEVEL) order by concat(SG.SG_SURNAME, '' '', SG.SG_OTHERNAME) ';
    end;

    if rbActiveStudentsS.Checked then sdusereginfostd.ParamByName('SG_LEFT').AsString:='N';
    if rbArchiveStudentsS.Checked then sdusereginfostd.ParamByName('SG_LEFT').AsString:='Y';

    sdusereginfostd.ParamByName('SC_CURRICULUM').AsString:=req_curriculum;
    sdusereginfostd.ParamByName('SC_LEVEL').AsString:=req_level;

    //parents
    sdstreamreginfostd.CommandText:='SELECT concat(sg.SG_SURNAME, '' '', sg.SG_OTHERNAME) SG_FULLNAME, sg.*, sp.* FROM students_general sg inner join students_parents sp on sg.SG_ID = sp.SP_SG_ID ';

    if (rbActiveStudentsS.Checked) or (rbArchiveStudentsS.Checked) then
    begin
      sdstreamreginfostd.CommandText:=sdstreamreginfostd.CommandText+' where sg.SG_LEFT = :SG_LEFT and sg.SG_CLASS in (select distinct trim(concat(SC_CLASS, '' '', ifnull(SC_STREAM,''''))) CLASS_STREAM from school_classes where SC_CURRICULUM = :SC_CURRICULUM and SC_LEVEL = :SC_LEVEL) '+
                                                                                   'order by concat(sg.SG_SURNAME, '' '', sg.SG_OTHERNAME) ';
    end
    else
    begin
      sdstreamreginfostd.CommandText:=sdstreamreginfostd.CommandText+' where sg.SG_CLASS in (select distinct trim(concat(SC_CLASS, '' '', ifnull(SC_STREAM,''''))) CLASS_STREAM from school_classes where SC_CURRICULUM = :SC_CURRICULUM and SC_LEVEL = :SC_LEVEL) order by concat(sg.SG_SURNAME, '' '', sg.SG_OTHERNAME) ';
    end;

    if rbActiveStudentsS.Checked then sdstreamreginfostd.ParamByName('SG_LEFT').AsString:='N';
    if rbArchiveStudentsS.Checked then sdstreamreginfostd.ParamByName('SG_LEFT').AsString:='Y';

    sdstreamreginfostd.ParamByName('SC_CURRICULUM').AsString:=req_curriculum;
    sdstreamreginfostd.ParamByName('SC_LEVEL').AsString:=req_level;
  end;


  if student_list_option = 13 then   ////13 = Ug: Class and if Pre-Primary has no streams - class/stream OR Cam: Year/Class
  begin
    if tvStudentsSelectClass.Selected.Parent.Parent.Text = 'Ugandan Curriculum' then req_curriculum:='UNEB'
    else if tvStudentsSelectClass.Selected.Parent.Parent.Text = 'Cambridge International' then req_curriculum:='CIE'
    else req_curriculum:='NA';

    req_class:=tvStudentsSelectClass.Selected.Text;

    sdusereginfostd.CommandText:='select concat(SG.SG_SURNAME, '' '', SG.SG_OTHERNAME) as SG_FULLNAME, SG.*, concat(T.T_SURNAME, '' '', T.T_OTHERNAMES) as T_FULLNAME '+
                                    'from students_general SG inner join staff_general T on SG.SG_CREATED_BY = T.T_ID ';

    if (rbActiveStudentsS.Checked) or (rbArchiveStudentsS.Checked) then
    begin
      sdusereginfostd.CommandText:=sdusereginfostd.CommandText+' where SG.SG_LEFT = :SG_LEFT and SG.SG_CLASS in (select distinct trim(concat(SC_CLASS, '' '', ifnull(SC_STREAM,''''))) CLASS_STREAM from school_classes where SC_CURRICULUM = :SC_CURRICULUM and SC_CLASS = :SC_CLASS) '+
                                                                     'order by concat(SG.SG_SURNAME, '' '', SG.SG_OTHERNAME) ';
    end
    else
    begin
      sdusereginfostd.CommandText:=sdusereginfostd.CommandText+' where SG.SG_CLASS in (select distinct trim(concat(SC_CLASS, '' '', ifnull(SC_STREAM,''''))) CLASS_STREAM from school_classes where SC_CURRICULUM = :SC_CURRICULUM and SC_CLASS = :SC_CLASS) order by concat(SG.SG_SURNAME, '' '', SG.SG_OTHERNAME) ';
    end;

    if rbActiveStudentsS.Checked then sdusereginfostd.ParamByName('SG_LEFT').AsString:='N';
    if rbArchiveStudentsS.Checked then sdusereginfostd.ParamByName('SG_LEFT').AsString:='Y';

    sdusereginfostd.ParamByName('SC_CURRICULUM').AsString:=req_curriculum;
    sdusereginfostd.ParamByName('SC_CLASS').AsString:=req_class;

    //parents
    sdstreamreginfostd.CommandText:='SELECT concat(sg.SG_SURNAME, '' '', sg.SG_OTHERNAME) SG_FULLNAME, sg.*, sp.* FROM students_general sg inner join students_parents sp on sg.SG_ID = sp.SP_SG_ID ';

    if (rbActiveStudentsS.Checked) or (rbArchiveStudentsS.Checked) then
    begin
      sdstreamreginfostd.CommandText:=sdstreamreginfostd.CommandText+' where sg.SG_LEFT = :SG_LEFT and sg.SG_CLASS in (select distinct trim(concat(SC_CLASS, '' '', ifnull(SC_STREAM,''''))) CLASS_STREAM from school_classes where SC_CURRICULUM = :SC_CURRICULUM and SC_CLASS = :SC_CLASS) '+
                                                                                   'order by concat(sg.SG_SURNAME, '' '', sg.SG_OTHERNAME) ';
    end
    else
    begin
      sdstreamreginfostd.CommandText:=sdstreamreginfostd.CommandText+' where sg.SG_CLASS in (select distinct trim(concat(SC_CLASS, '' '', ifnull(SC_STREAM,''''))) CLASS_STREAM from school_classes where SC_CURRICULUM = :SC_CURRICULUM and SC_CLASS = :SC_CLASS) order by concat(sg.SG_SURNAME, '' '', sg.SG_OTHERNAME) ';
    end;

    if rbActiveStudentsS.Checked then sdstreamreginfostd.ParamByName('SG_LEFT').AsString:='N';
    if rbArchiveStudentsS.Checked then sdstreamreginfostd.ParamByName('SG_LEFT').AsString:='Y';

    sdstreamreginfostd.ParamByName('SC_CURRICULUM').AsString:=req_curriculum;
    sdstreamreginfostd.ParamByName('SC_CLASS').AsString:=req_class;
  end;



  if student_list_option = 14 then   //14 = Ug. Stream
  begin
    if tvStudentsSelectClass.Selected.Parent.Parent.Parent.Text = 'Ugandan Curriculum' then req_curriculum:='UNEB'
    else if tvStudentsSelectClass.Selected.Parent.Parent.Parent.Text = 'Cambridge International' then req_curriculum:='CIE'
    else req_curriculum:='NA';

    req_class_stream:=Trim(tvStudentsSelectClass.Selected.Text);

    sdusereginfostd.CommandText:='select concat(SG.SG_SURNAME, '' '', SG.SG_OTHERNAME) as SG_FULLNAME, SG.*, concat(T.T_SURNAME, '' '', T.T_OTHERNAMES) as T_FULLNAME '+
                                    'from students_general SG inner join staff_general T on SG.SG_CREATED_BY = T.T_ID ';

    if (rbActiveStudentsS.Checked) or (rbArchiveStudentsS.Checked) then
    begin
      sdusereginfostd.CommandText:=sdusereginfostd.CommandText+' where SG.SG_LEFT = :SG_LEFT and SG.SG_CLASS = :SG_CLASS order by concat(SG.SG_SURNAME, '' '', SG.SG_OTHERNAME) ';
    end
    else
    begin
      sdusereginfostd.CommandText:=sdusereginfostd.CommandText+' where SG.SG_CLASS = :SG_CLASS order by concat(SG.SG_SURNAME, '' '', SG.SG_OTHERNAME) ';
    end;

    if rbActiveStudentsS.Checked then sdusereginfostd.ParamByName('SG_LEFT').AsString:='N';
    if rbArchiveStudentsS.Checked then sdusereginfostd.ParamByName('SG_LEFT').AsString:='Y';

    sdusereginfostd.ParamByName('SG_CLASS').AsString:=req_class_stream;

    //parents
    sdstreamreginfostd.CommandText:='SELECT concat(sg.SG_SURNAME, '' '', sg.SG_OTHERNAME) SG_FULLNAME, sg.*, sp.* FROM students_general sg inner join students_parents sp on sg.SG_ID = sp.SP_SG_ID ';

    if (rbActiveStudentsS.Checked) or (rbArchiveStudentsS.Checked) then
    begin
      sdstreamreginfostd.CommandText:=sdstreamreginfostd.CommandText+' where sg.SG_LEFT = :SG_LEFT and sg.SG_CLASS = :SG_CLASS order by concat(sg.SG_SURNAME, '' '', sg.SG_OTHERNAME) ';
    end
    else
    begin
      sdstreamreginfostd.CommandText:=sdstreamreginfostd.CommandText+' where sg.SG_CLASS = :SG_CLASS order by concat(sg.SG_SURNAME, '' '', sg.SG_OTHERNAME) ';
    end;

    if rbActiveStudentsS.Checked then sdstreamreginfostd.ParamByName('SG_LEFT').AsString:='N';
    if rbArchiveStudentsS.Checked then sdstreamreginfostd.ParamByName('SG_LEFT').AsString:='Y';

    sdstreamreginfostd.ParamByName('SG_CLASS').AsString:=req_class_stream;
  end;


  //15 = Not yet in use...



  //20 - Dormitory
  if student_list_option = 21 then   //21 - All Day Students
  begin
    lSchoolStudents.Caption:='School Students: Day Students';

    //students
    cdusereginfostd.Active:=False;
    sdusereginfostd.Active:=False;
    sdusereginfostd.CommandText:='select concat(SG.SG_SURNAME, '' '', SG.SG_OTHERNAME) as SG_FULLNAME, SG.*, concat(T.T_SURNAME, '' '', T.T_OTHERNAMES) as T_FULLNAME '+
                                    'from students_general SG inner join staff_general T on SG.SG_CREATED_BY = T.T_ID ';

    if (rbActiveStudentsS.Checked) or (rbArchiveStudentsS.Checked) then sdusereginfostd.CommandText:=sdusereginfostd.CommandText+' where SG.SG_LEFT = :SG_LEFT and SG.SG_IS_BOARDER = :SG_IS_BOARDER order by concat(SG.SG_SURNAME, '' '', SG.SG_OTHERNAME)  '
    else sdusereginfostd.CommandText:=sdusereginfostd.CommandText+' where SG.SG_IS_BOARDER = :SG_IS_BOARDER order by concat(SG.SG_SURNAME, '' '', SG.SG_OTHERNAME)  ';

    if rbActiveStudentsS.Checked then sdusereginfostd.ParamByName('SG_LEFT').AsString:='N';
    if rbArchiveStudentsS.Checked then sdusereginfostd.ParamByName('SG_LEFT').AsString:='Y';

    sdusereginfostd.ParamByName('SG_IS_BOARDER').AsString:='N';


    //parents
    cdusereginfostd.Active:=False;
    sdstreamreginfostd.Active:=False;
    sdstreamreginfostd.CommandText:='SELECT concat(sg.SG_SURNAME, '' '', sg.SG_OTHERNAME) SG_FULLNAME, sg.*, sp.* FROM students_general sg inner join students_parents sp on sg.SG_ID = sp.SP_SG_ID ';

    if (rbActiveStudentsS.Checked) or (rbArchiveStudentsS.Checked) then sdstreamreginfostd.CommandText:=sdstreamreginfostd.CommandText+' where sg.SG_LEFT = :SG_LEFT and sg.SG_IS_BOARDER = :SG_IS_BOARDER order by concat(sg.SG_SURNAME, '' '', sg.SG_OTHERNAME)  '
    else sdstreamreginfostd.CommandText:=sdstreamreginfostd.CommandText+' where sg.SG_IS_BOARDER = :SG_IS_BOARDER order by concat(sg.SG_SURNAME, '' '', sg.SG_OTHERNAME)  ';

    if rbActiveStudentsS.Checked then sdstreamreginfostd.ParamByName('SG_LEFT').AsString:='N';
    if rbArchiveStudentsS.Checked then sdstreamreginfostd.ParamByName('SG_LEFT').AsString:='Y';

    sdstreamreginfostd.ParamByName('SG_IS_BOARDER').AsString:='N';
  end;

  if student_list_option = 22 then   //22 - All Boarders
  begin
    lSchoolStudents.Caption:='School Students: Boarders';

    //students
    cdusereginfostd.Active:=False;
    sdusereginfostd.Active:=False;
    sdusereginfostd.CommandText:='select concat(SG.SG_SURNAME, '' '', SG.SG_OTHERNAME) as SG_FULLNAME, SG.*, concat(T.T_SURNAME, '' '', T.T_OTHERNAMES) as T_FULLNAME '+
                                    'from students_general SG inner join staff_general T on SG.SG_CREATED_BY = T.T_ID ';

    if (rbActiveStudentsS.Checked) or (rbArchiveStudentsS.Checked) then sdusereginfostd.CommandText:=sdusereginfostd.CommandText+' where SG.SG_LEFT = :SG_LEFT and SG.SG_IS_BOARDER = :SG_IS_BOARDER order by concat(SG.SG_SURNAME, '' '', SG.SG_OTHERNAME)  '
    else sdusereginfostd.CommandText:=sdusereginfostd.CommandText+' where SG.SG_IS_BOARDER = :SG_IS_BOARDER order by concat(SG.SG_SURNAME, '' '', SG.SG_OTHERNAME)  ';

    if rbActiveStudentsS.Checked then sdusereginfostd.ParamByName('SG_LEFT').AsString:='N';
    if rbArchiveStudentsS.Checked then sdusereginfostd.ParamByName('SG_LEFT').AsString:='Y';

    sdusereginfostd.ParamByName('SG_IS_BOARDER').AsString:='Y';


    //parents
    cdusereginfostd.Active:=False;
    sdstreamreginfostd.Active:=False;
    sdstreamreginfostd.CommandText:='SELECT concat(sg.SG_SURNAME, '' '', sg.SG_OTHERNAME) SG_FULLNAME, sg.*, sp.* FROM students_general sg inner join students_parents sp on sg.SG_ID = sp.SP_SG_ID ';

    if (rbActiveStudentsS.Checked) or (rbArchiveStudentsS.Checked) then sdstreamreginfostd.CommandText:=sdstreamreginfostd.CommandText+' where sg.SG_LEFT = :SG_LEFT and sg.SG_IS_BOARDER = :SG_IS_BOARDER order by concat(sg.SG_SURNAME, '' '', sg.SG_OTHERNAME)  '
    else sdstreamreginfostd.CommandText:=sdstreamreginfostd.CommandText+' where sg.SG_IS_BOARDER = :SG_IS_BOARDER order by concat(sg.SG_SURNAME, '' '', sg.SG_OTHERNAME)  ';

    if rbActiveStudentsS.Checked then sdstreamreginfostd.ParamByName('SG_LEFT').AsString:='N';
    if rbArchiveStudentsS.Checked then sdstreamreginfostd.ParamByName('SG_LEFT').AsString:='Y';

    sdstreamreginfostd.ParamByName('SG_IS_BOARDER').AsString:='Y';

  end;

  if student_list_option = 23 then   //23 - Particular dormitory
  begin
    lSchoolStudents.Caption:='School Students: '+tvStudentsSelectDorm.Selected.Text;

    //students
    cdusereginfostd.Active:=False;
    sdusereginfostd.Active:=False;
    sdusereginfostd.CommandText:='select concat(SG.SG_SURNAME, '' '', SG.SG_OTHERNAME) as SG_FULLNAME, SG.*, concat(T.T_SURNAME, '' '', T.T_OTHERNAMES) as T_FULLNAME '+
                                    'from students_general SG inner join staff_general T on SG.SG_CREATED_BY = T.T_ID ';

    if (rbActiveStudentsS.Checked) or (rbArchiveStudentsS.Checked) then sdusereginfostd.CommandText:=sdusereginfostd.CommandText+' where SG.SG_LEFT = :SG_LEFT and SG.SG_IS_BOARDER = :SG_IS_BOARDER and SG.SG_DORMITORY = :SG_DORMITORY order by concat(SG.SG_SURNAME, '' '', SG.SG_OTHERNAME) '
    else sdusereginfostd.CommandText:=sdusereginfostd.CommandText+' where SG.SG_IS_BOARDER = :SG_IS_BOARDER and SG.SG_DORMITORY = :SG_DORMITORY order by concat(SG.SG_SURNAME, '' '', SG.SG_OTHERNAME) ';

    if rbActiveStudentsS.Checked then sdusereginfostd.ParamByName('SG_LEFT').AsString:='N';
    if rbArchiveStudentsS.Checked then sdusereginfostd.ParamByName('SG_LEFT').AsString:='Y';

    sdusereginfostd.ParamByName('SG_IS_BOARDER').AsString:='Y';
    sdusereginfostd.ParamByName('SG_DORMITORY').AsString:=tvStudentsSelectDorm.Selected.Text;


    //parents
    cdusereginfostd.Active:=False;
    sdstreamreginfostd.Active:=False;
    sdstreamreginfostd.CommandText:='SELECT concat(sg.SG_SURNAME, '' '', sg.SG_OTHERNAME) SG_FULLNAME, sg.*, sp.* FROM students_general sg inner join students_parents sp on sg.SG_ID = sp.SP_SG_ID ';

    if (rbActiveStudentsS.Checked) or (rbArchiveStudentsS.Checked) then sdstreamreginfostd.CommandText:=sdstreamreginfostd.CommandText+' where sg.SG_LEFT = :SG_LEFT and sg.SG_IS_BOARDER = :SG_IS_BOARDER and sg.SG_DORMITORY = :SG_DORMITORY order by concat(sg.SG_SURNAME, '' '', sg.SG_OTHERNAME) '
    else sdstreamreginfostd.CommandText:=sdstreamreginfostd.CommandText+' where sg.SG_IS_BOARDER = :SG_IS_BOARDER and sg.SG_DORMITORY = :SG_DORMITORY order by concat(sg.SG_SURNAME, '' '', sg.SG_OTHERNAME) ';

    if rbActiveStudentsS.Checked then sdstreamreginfostd.ParamByName('SG_LEFT').AsString:='N';
    if rbArchiveStudentsS.Checked then sdstreamreginfostd.ParamByName('SG_LEFT').AsString:='Y';

    sdstreamreginfostd.ParamByName('SG_IS_BOARDER').AsString:='Y';
    sdstreamreginfostd.ParamByName('SG_DORMITORY').AsString:=tvStudentsSelectDorm.Selected.Text;
  end;




  //30 - Search
  if student_list_option = 31 then   //Search for Student
  begin
    lSchoolStudents.Caption:='School Students: Search for "'+Trim(tEditStudentSearch.Text)+'"';

    all_cbs:=False;

    for i := 0 to chkComboStudentSearch.Properties.Items.Count - 1 do    // Iterate
    begin
      if chkComboStudentSearch.GetItemState(i) = cbsChecked then
      begin
        all_cbs:=True;
        Break;
      end;
    end;    // for

    if all_cbs = False then
    begin
      MessageDlg('Please specify the search criteria!', mtInformation,[mbOk],0);
      chkComboStudentSearch.SetFocus;
      Exit;
    end;

    if Length(Trim(tEditStudentSearch.Text)) = 0 then
    begin
      MessageDlg('Please enter the search item!', mtInformation,[mbOk],0);
      tEditStudentSearch.SetFocus;
      Exit;
    end;

    //students
    sdusereginfostd.CommandText:='select concat(SG.SG_SURNAME, '' '', SG.SG_OTHERNAME) as SG_FULLNAME, SG.*, concat(T.T_SURNAME, '' '', T.T_OTHERNAMES) as T_FULLNAME '+
                                    'from students_general SG inner join staff_general T on SG.SG_CREATED_BY = T.T_ID where ';

    if not chkStudentSearchArchives.Checked then sdusereginfostd.CommandText:=sdusereginfostd.CommandText+' SG.SG_LEFT = :SG_LEFT and ( '
    else sdusereginfostd.CommandText:=sdusereginfostd.CommandText+' ( ';

    if chkComboStudentSearch.GetItemState(0) = cbsChecked then sdusereginfostd.CommandText:=sdusereginfostd.CommandText+' concat(SG.SG_SURNAME, '' '', SG.SG_OTHERNAME) like :STUDENT_SEARCH_ITEM or ';
    if chkComboStudentSearch.GetItemState(1) = cbsChecked then sdusereginfostd.CommandText:=sdusereginfostd.CommandText+' SG.SG_ID in (select distinct SP_SG_ID from students_parents where SP_NAME like :STUDENT_SEARCH_ITEM) or ';
    if chkComboStudentSearch.GetItemState(2) = cbsChecked then sdusereginfostd.CommandText:=sdusereginfostd.CommandText+' SG.SG_HOME_ADDRESS like :STUDENT_SEARCH_ITEM or ';
    if chkComboStudentSearch.GetItemState(3) = cbsChecked then sdusereginfostd.CommandText:=sdusereginfostd.CommandText+' SG.SG_ID in (select distinct SP_SG_ID from students_parents where SP_EMAIL like :STUDENT_SEARCH_ITEM) or SG_EMAIL like :STUDENT_SEARCH_ITEM or ';
    if chkComboStudentSearch.GetItemState(4) = cbsChecked then sdusereginfostd.CommandText:=sdusereginfostd.CommandText+' SG.SG_ID in (select distinct SP_SG_ID from students_parents where SP_TEL like :STUDENT_SEARCH_ITEM) or ';
    if chkComboStudentSearch.GetItemState(5) = cbsChecked then sdusereginfostd.CommandText:=sdusereginfostd.CommandText+' SG.SG_ADMISSION_NUMBER like :STUDENT_SEARCH_ITEM or ';

    sdusereginfostd.CommandText:=sdusereginfostd.CommandText+' SG.SG_ID < 0) ';

    if chkStudentSearchDateJoined.Checked then
    begin
      sdusereginfostd.CommandText:=sdusereginfostd.CommandText+' and (SG.SG_JOINED_DATE >= :SG_JOINED_START_DATE and SG.SG_JOINED_DATE <= :SG_JOINED_END_DATE) ';
    end;

    sdusereginfostd.CommandText:=sdusereginfostd.CommandText+' order by concat(SG.SG_SURNAME, '' '', SG.SG_OTHERNAME) ';

    if not chkStudentSearchArchives.Checked then sdusereginfostd.ParamByName('SG_LEFT').AsString:='N';

    if rbStudentSearchStarting.Checked then sdusereginfostd.ParamByName('STUDENT_SEARCH_ITEM').AsString:=AnsiReplaceStr(Trim(tEditStudentSearch.Text),' ','%')+'%'
    else sdusereginfostd.ParamByName('STUDENT_SEARCH_ITEM').AsString:='%'+AnsiReplaceStr(Trim(tEditStudentSearch.Text),' ','%')+'%';

    if chkStudentSearchDateJoined.Checked then
    begin
      sdusereginfostd.ParamByName('SG_JOINED_START_DATE').AsDate:=dEditStudentSearchStartDate.Date;
      sdusereginfostd.ParamByName('SG_JOINED_END_DATE').AsDate:=dEditStudentSearchEndDate.Date;
    end;


    //parents
    sdstreamreginfostd.CommandText:='SELECT concat(sg.SG_SURNAME, '' '', sg.SG_OTHERNAME) SG_FULLNAME, sg.*, sp.* FROM students_general sg inner join students_parents sp on sg.SG_ID = sp.SP_SG_ID ';

    if not chkStudentSearchArchives.Checked then sdstreamreginfostd.CommandText:=sdstreamreginfostd.CommandText+' sg.SG_LEFT = :SG_LEFT and ( '
    else sdstreamreginfostd.CommandText:=sdstreamreginfostd.CommandText+' ( ';

    if chkComboStudentSearch.GetItemState(0) = cbsChecked then sdstreamreginfostd.CommandText:=sdstreamreginfostd.CommandText+' concat(sg.SG_SURNAME, '' '', sg.SG_OTHERNAME) like :STUDENT_SEARCH_ITEM or ';
    if chkComboStudentSearch.GetItemState(1) = cbsChecked then sdstreamreginfostd.CommandText:=sdstreamreginfostd.CommandText+' sg.SG_ID in (select distinct SP_SG_ID from students_parents where SP_NAME like :STUDENT_SEARCH_ITEM) or ';
    if chkComboStudentSearch.GetItemState(2) = cbsChecked then sdstreamreginfostd.CommandText:=sdstreamreginfostd.CommandText+' sg.SG_HOME_ADDRESS like :STUDENT_SEARCH_ITEM or ';
    if chkComboStudentSearch.GetItemState(3) = cbsChecked then sdstreamreginfostd.CommandText:=sdstreamreginfostd.CommandText+' sg.SG_ID in (select distinct SP_SG_ID from students_parents where SP_EMAIL like :STUDENT_SEARCH_ITEM) or SG_EMAIL like :STUDENT_SEARCH_ITEM or ';
    if chkComboStudentSearch.GetItemState(4) = cbsChecked then sdstreamreginfostd.CommandText:=sdstreamreginfostd.CommandText+' sg.SG_ID in (select distinct SP_SG_ID from students_parents where SP_TEL like :STUDENT_SEARCH_ITEM) or ';
    if chkComboStudentSearch.GetItemState(5) = cbsChecked then sdstreamreginfostd.CommandText:=sdstreamreginfostd.CommandText+' sg.SG_ADMISSION_NUMBER like :STUDENT_SEARCH_ITEM or ';

    sdstreamreginfostd.CommandText:=sdstreamreginfostd.CommandText+' sg.SG_ID < 0) ';

    if chkStudentSearchDateJoined.Checked then
    begin
      sdstreamreginfostd.CommandText:=sdstreamreginfostd.CommandText+' and (sg.SG_JOINED_DATE >= :SG_JOINED_START_DATE and sg.SG_JOINED_DATE <= :SG_JOINED_END_DATE) ';
    end;

    sdstreamreginfostd.CommandText:=sdstreamreginfostd.CommandText+' order by concat(sg.SG_SURNAME, '' '', sg.SG_OTHERNAME) ';

    if not chkStudentSearchArchives.Checked then sdstreamreginfostd.ParamByName('SG_LEFT').AsString:='N';

    if rbStudentSearchStarting.Checked then sdstreamreginfostd.ParamByName('STUDENT_SEARCH_ITEM').AsString:=AnsiReplaceStr(Trim(tEditStudentSearch.Text),' ','%')+'%'
    else sdstreamreginfostd.ParamByName('STUDENT_SEARCH_ITEM').AsString:='%'+AnsiReplaceStr(Trim(tEditStudentSearch.Text),' ','%')+'%';

    if chkStudentSearchDateJoined.Checked then
    begin
      sdstreamreginfostd.ParamByName('SG_JOINED_START_DATE').AsDate:=dEditStudentSearchStartDate.Date;
      sdstreamreginfostd.ParamByName('SG_JOINED_END_DATE').AsDate:=dEditStudentSearchEndDate.Date;
    end;

  end;

  if student_list_option = 32 then   //Quick Search
  begin
    lSchoolStudents.Caption:='School Students: Quick Search for "'+Trim(bEditQuickSearchStudents.Text)+'"';

    if Length(Trim(bEditQuickSearchStudents.Text)) = 0 then
    begin
      MessageDlg('Please enter the search item!', mtInformation,[mbOk],0);
      bEditQuickSearchStudents.SetFocus;
      Exit;
    end;

    //students
    sdusereginfostd.CommandText:='select concat(SG.SG_SURNAME, '' '', SG.SG_OTHERNAME) as SG_FULLNAME, SG.*, concat(T.T_SURNAME, '' '', T.T_OTHERNAMES) as T_FULLNAME '+
                                    'from students_general SG inner join staff_general T on SG.SG_CREATED_BY = T.T_ID where ';

    if (rbActiveStudentsS.Checked) or (rbArchiveStudentsS.Checked) then sdusereginfostd.CommandText:=sdusereginfostd.CommandText+' SG.SG_LEFT = :SG_LEFT and (concat(SG.SG_SURNAME, '' '', SG.SG_OTHERNAME) like :STUDENT_SEARCH_ITEM or SG_ADMISSION_NUMBER like :STUDENT_SEARCH_ITEM ) order by concat(SG.SG_SURNAME, '' '', SG.SG_OTHERNAME) '
    else sdusereginfostd.CommandText:=sdusereginfostd.CommandText+' (concat(SG.SG_SURNAME, '' '', SG.SG_OTHERNAME) like :STUDENT_SEARCH_ITEM or SG_ADMISSION_NUMBER like :STUDENT_SEARCH_ITEM ) order by concat(SG.SG_SURNAME, '' '', SG.SG_OTHERNAME) ';

    sdusereginfostd.CommandText:=sdusereginfostd.CommandText+' ';

    if rbActiveStudentsS.Checked then sdusereginfostd.ParamByName('SG_LEFT').AsString:='N';
    if rbArchiveStudentsS.Checked then sdusereginfostd.ParamByName('SG_LEFT').AsString:='Y';

    sdusereginfostd.ParamByName('STUDENT_SEARCH_ITEM').AsString:='%'+AnsiReplaceStr(Trim(bEditQuickSearchStudents.Text),' ','%')+'%';


    //parents
    sdstreamreginfostd.CommandText:='SELECT concat(sg.SG_SURNAME, '' '', sg.SG_OTHERNAME) SG_FULLNAME, sg.*, sp.* FROM students_general sg inner join students_parents sp on sg.SG_ID = sp.SP_SG_ID where ';

    if (rbActiveStudentsS.Checked) or (rbArchiveStudentsS.Checked) then sdstreamreginfostd.CommandText:=sdstreamreginfostd.CommandText+' sg.SG_LEFT = :SG_LEFT and (concat(sg.SG_SURNAME, '' '', sg.SG_OTHERNAME) like :STUDENT_SEARCH_ITEM or SG_ADMISSION_NUMBER like :STUDENT_SEARCH_ITEM '+
                                                                                                                                                   'or sp.SP_NAME like :STUDENT_SEARCH_ITEM ) order by concat(sg.SG_SURNAME, '' '', sg.SG_OTHERNAME) '
    else sdstreamreginfostd.CommandText:=sdstreamreginfostd.CommandText+' (concat(sg.SG_SURNAME, '' '', sg.SG_OTHERNAME) like :STUDENT_SEARCH_ITEM or SG_ADMISSION_NUMBER like :STUDENT_SEARCH_ITEM or sp.SP_NAME like :STUDENT_SEARCH_ITEM ) order by concat(sg.SG_SURNAME, '' '', sg.SG_OTHERNAME) ';

    sdstreamreginfostd.CommandText:=sdstreamreginfostd.CommandText+' ';

    if rbActiveStudentsS.Checked then sdstreamreginfostd.ParamByName('SG_LEFT').AsString:='N';
    if rbArchiveStudentsS.Checked then sdstreamreginfostd.ParamByName('SG_LEFT').AsString:='Y';

    sdstreamreginfostd.ParamByName('STUDENT_SEARCH_ITEM').AsString:='%'+AnsiReplaceStr(Trim(bEditQuickSearchStudents.Text),' ','%')+'%';
  end;



  //40 - Groups
  if student_list_option = 41 then   //Special Group/Association
  begin
    lSchoolStudents.Caption:='School Students: '+cdcombinationreginfostd.FieldByName('SSG_NAME').AsString;

    //students
    sdusereginfostd.CommandText:='select distinct concat(SG.SG_SURNAME, '' '', SG.SG_OTHERNAME) as SG_FULLNAME, SG.*, concat(T.T_SURNAME, '' '', T.T_OTHERNAMES) as T_FULLNAME '+
                                    'from students_general SG inner join staff_general T on SG.SG_CREATED_BY = T.T_ID inner join students_group_membership M on SG.SG_ID = M.SGM_SG_ID '+
                                    'where M.SGM_SSG_ID = :SGM_SSG_ID ';

    if (rbActiveStudentsS.Checked) or (rbArchiveStudentsS.Checked) then sdusereginfostd.CommandText:=sdusereginfostd.CommandText+' and SG.SG_LEFT = :SG_LEFT order by concat(SG.SG_SURNAME, '' '', SG.SG_OTHERNAME)  '
    else sdusereginfostd.CommandText:=sdusereginfostd.CommandText+' order by concat(SG.SG_SURNAME, '' '', SG.SG_OTHERNAME)  ';

    sdusereginfostd.Params[0].AsInteger:=cdcombinationreginfostd.FieldByName('SSG_ID').AsInteger;

    if rbActiveStudentsS.Checked then sdusereginfostd.ParamByName('SG_LEFT').AsString:='N';
    if rbArchiveStudentsS.Checked then sdusereginfostd.ParamByName('SG_LEFT').AsString:='Y';


    //parents
    sdstreamreginfostd.CommandText:='SELECT concat(sg.SG_SURNAME, '' '', sg.SG_OTHERNAME) SG_FULLNAME, sg.*, sp.* FROM students_general sg inner join students_parents sp on sg.SG_ID = sp.SP_SG_ID '+
                                           'inner join students_group_membership M on sg.SG_ID = M.SGM_SG_ID where M.SGM_SSG_ID = :SGM_SSG_ID ';

    if (rbActiveStudentsS.Checked) or (rbArchiveStudentsS.Checked) then sdstreamreginfostd.CommandText:=sdstreamreginfostd.CommandText+' and sg.SG_LEFT = :SG_LEFT order by concat(sg.SG_SURNAME, '' '', sg.SG_OTHERNAME)  '
    else sdstreamreginfostd.CommandText:=sdstreamreginfostd.CommandText+' order by concat(sg.SG_SURNAME, '' '', SG.SG_OTHERNAME)  ';

    sdstreamreginfostd.Params[0].AsInteger:=cdcombinationreginfostd.FieldByName('SSG_ID').AsInteger;

    if rbActiveStudentsS.Checked then sdstreamreginfostd.ParamByName('SG_LEFT').AsString:='N';
    if rbArchiveStudentsS.Checked then sdstreamreginfostd.ParamByName('SG_LEFT').AsString:='Y';


  end;


  if pcStudentsView.ActivePageIndex < 2 then //oba put specific page indexes
  begin
    cdusereginfostd.Open;

    if (cdusereginfostd.Eof) and (cdusereginfostd.Bof) then
    begin
      lSchoolStudents.Caption:='School Students: Required Student(s) Not Found';

      if (rbArchiveStudentsS.Checked) then
      begin
        case student_list_option of
          10: MessageDlg('There are no archived students currently registered in the school', mtInformation, [mbOK], 0);
          11: MessageDlg('There are no archived students taking the specified curriculum', mtInformation, [mbOK], 0);
          12: MessageDlg('There are no archived students in the selected school level', mtInformation, [mbOK], 0);
          13: MessageDlg('There are no archived students in the selected class', mtInformation, [mbOK], 0);
          14: MessageDlg('There are no archived students in the selected stream', mtInformation, [mbOK], 0);

          21: MessageDlg('There are no archived day students', mtInformation, [mbOK], 0);
          22: MessageDlg('There are no archived  boarders', mtInformation, [mbOK], 0);
          23: MessageDlg('There are no archived students belonging to the selected dormitory or hostel ['+tvStudentsSelectDorm.Selected.Text+']', mtInformation, [mbOK], 0);

          31: MessageDlg('No archived student matching your search criteria was found. Please revise your search terms and try again', mtInformation, [mbOK], 0);
          32: MessageDlg('No archived student matching your quick search criteria was found. Please revise your search terms and try again', mtInformation, [mbOK], 0);

          41: MessageDlg('There are no archived students belonging to the selected special group or association', mtInformation, [mbOK], 0);
        end;
      end
      else
      begin
        case student_list_option of
          10: MessageDlg('There are no students currently registered in the school', mtInformation, [mbOK], 0);
          11: MessageDlg('There are no students taking the specified curriculum', mtInformation, [mbOK], 0);
          12: MessageDlg('There are no students in the selected school level', mtInformation, [mbOK], 0);
          13: MessageDlg('There are no students in the selected class', mtInformation, [mbOK], 0);
          14: MessageDlg('There are no students in the selected stream', mtInformation, [mbOK], 0);

          21: MessageDlg('There are currently no registered day students', mtInformation, [mbOK], 0);
          22: MessageDlg('There are currently no registered boarders', mtInformation, [mbOK], 0);
          23: MessageDlg('There are no students in the selected dormitory or hostel ['+tvStudentsSelectDorm.Selected.Text+']', mtInformation, [mbOK], 0);

          31: MessageDlg('No student matching your search criteria was found. Please revise your search terms and try again', mtInformation, [mbOK], 0);
          32: MessageDlg('No student matching your quick search criteria was found. Please revise your search terms and try again', mtInformation, [mbOK], 0);

          41: MessageDlg('There are currently no students belonging to the selected special group or association', mtInformation, [mbOK], 0);
        end;
      end;

      cdusereginfostd.Close;
      sdusereginfostd.Active:=False;
    end;
  end
  else
  begin
    cdstreamreginfostd.Open;

    if (cdstreamreginfostd.Eof) and (cdstreamreginfostd.Bof) then
    begin
      lSchoolStudents.Caption:='School Students: Required Parent(s) Not Found';

      if (rbArchiveStudentsS.Checked) then
      begin
        case student_list_option of
          10: MessageDlg('There are no archived students currently registered in the school', mtInformation, [mbOK], 0);
          11: MessageDlg('There are no archived students taking the specified curriculum', mtInformation, [mbOK], 0);
          12: MessageDlg('There are no archived students in the selected school level', mtInformation, [mbOK], 0);
          13: MessageDlg('There are no archived students in the selected class', mtInformation, [mbOK], 0);
          14: MessageDlg('There are no archived students in the selected stream', mtInformation, [mbOK], 0);

          21: MessageDlg('There are no archived day students', mtInformation, [mbOK], 0);
          22: MessageDlg('There are no archived  boarders', mtInformation, [mbOK], 0);
          23: MessageDlg('There are no archived students belonging to the selected dormitory or hostel ['+tvStudentsSelectDorm.Selected.Text+']', mtInformation, [mbOK], 0);

          31: MessageDlg('No archived student matching your search criteria was found. Please revise your search terms and try again', mtInformation, [mbOK], 0);
          32: MessageDlg('No archived student matching your quick search criteria was found. Please revise your search terms and try again', mtInformation, [mbOK], 0);

          41: MessageDlg('There are no archived students belonging to the selected special group or association', mtInformation, [mbOK], 0);
        end;
      end
      else
      begin
        case student_list_option of
         10: MessageDlg('There are no students currently registered in the school', mtInformation, [mbOK], 0);
          11: MessageDlg('There are no students taking the specified curriculum', mtInformation, [mbOK], 0);
          12: MessageDlg('There are no students in the selected school level', mtInformation, [mbOK], 0);
          13: MessageDlg('There are no students in the selected class', mtInformation, [mbOK], 0);
          14: MessageDlg('There are no students in the selected stream', mtInformation, [mbOK], 0);

          21: MessageDlg('There are currently no registered day students', mtInformation, [mbOK], 0);
          22: MessageDlg('There are currently no registered boarders', mtInformation, [mbOK], 0);
          23: MessageDlg('There are no students in the selected dormitory or hostel ['+tvStudentsSelectDorm.Selected.Text+']', mtInformation, [mbOK], 0);

          31: MessageDlg('No student matching your search criteria was found. Please revise your search terms and try again', mtInformation, [mbOK], 0);
          32: MessageDlg('No student matching your quick search criteria was found. Please revise your search terms and try again', mtInformation, [mbOK], 0);

          41: MessageDlg('There are currently no students belonging to the selected special group or association', mtInformation, [mbOK], 0);

        end;
      end;

      cdstreamreginfostd.Close;
      sdstreamreginfostd.Active:=False;
    end;
  end;


end;

end.
