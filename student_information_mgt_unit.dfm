object studentinformationmanagement: Tstudentinformationmanagement
  Left = 48
  Top = 28
  Caption = 'Student Information Management'
  ClientHeight = 660
  ClientWidth = 1339
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  WindowState = wsMinimized
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object label1l4: TLabel
    Left = 9
    Top = 4
    Width = 30
    Height = 13
    Caption = 'Select'
    Font.Charset = ANSI_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Segoe UI'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object label1l5: TLabel
    Left = 17
    Top = 12
    Width = 30
    Height = 13
    Caption = 'Select'
    Font.Charset = ANSI_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Segoe UI'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object s2: TcxSplitter
    Left = 1331
    Top = 25
    Width = 8
    Height = 635
    AlignSplitter = salRight
    ExplicitLeft = 1323
  end
  object pcStudents: TcxPageControl
    Left = 210
    Top = 25
    Width = 1121
    Height = 635
    ActivePage = tabViewStudentsCurrent
    Align = alClient
    HideTabs = True
    TabOrder = 1
    ExplicitLeft = 8
    ExplicitTop = 8
    ExplicitWidth = 1092
    ExplicitHeight = 476
    ClientRectBottom = 631
    ClientRectLeft = 4
    ClientRectRight = 1117
    ClientRectTop = 4
    object tabViewStudentsAdmissions: TcxTabSheet
      Caption = 'Student Admissions'
      ImageIndex = 0
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 468
      object l157: TLabel
        Left = 0
        Top = 0
        Width = 1113
        Height = 16
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        Caption = 'School Admissions View'
        Color = clBlue
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        ShowAccelChar = False
        Transparent = False
        Layout = tlCenter
        ExplicitLeft = 12
        ExplicitTop = -5
        ExplicitWidth = 1029
      end
      object gbApplicationListing: TcxGroupBox
        Left = 0
        Top = 16
        Align = alLeft
        Caption = 'Application Listing Options'
        ParentFont = False
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -13
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 0
        Height = 611
        Width = 195
        object pcApplicationListing: TcxPageControl
          Left = 2
          Top = 22
          Width = 191
          Height = 587
          ActivePage = tabSearchApplication
          Align = alClient
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          ExplicitTop = 21
          ExplicitWidth = 187
          ExplicitHeight = 517
          ClientRectBottom = 583
          ClientRectLeft = 4
          ClientRectRight = 187
          ClientRectTop = 24
          object tabAcademicYear: TcxTabSheet
            Caption = 'Year/Term'
            ImageIndex = 0
            object l144: TLabel
              Left = 0
              Top = 0
              Width = 183
              Height = 13
              Align = alTop
              Alignment = taCenter
              Caption = 'Select Target Year/Term'
              Font.Charset = ANSI_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Segoe UI'
              Font.Style = []
              ParentFont = False
              ExplicitWidth = 117
            end
            object tvApplicationListing: TcxTreeView
              Left = 0
              Top = 13
              Width = 183
              Height = 409
              Align = alClient
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -12
              Style.Font.Name = 'Segoe UI'
              Style.Font.Style = []
              Style.IsFontAssigned = True
              TabOrder = 0
              OnClick = tvApplicationListingClick
              HideSelection = False
              HotTrack = True
              ReadOnly = True
              RowSelect = True
            end
            object s14: TcxSplitter
              Left = 0
              Top = 422
              Width = 183
              Height = 8
              AlignSplitter = salBottom
              Control = gbFilterAdmissions
            end
            object gbFilterAdmissions: TcxGroupBox
              Left = 0
              Top = 430
              Align = alBottom
              Caption = 'Filter Applications'
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Segoe UI'
              Style.Font.Style = [fsBold]
              Style.IsFontAssigned = True
              TabOrder = 2
              Height = 129
              Width = 183
              object rbAllApplications: TcxRadioButton
                Left = 10
                Top = 23
                Width = 110
                Height = 17
                Caption = 'All Applications'
                Checked = True
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Segoe UI'
                Font.Style = []
                ParentFont = False
                TabOrder = 0
                TabStop = True
                OnClick = rbAllApplicationsClick
                Transparent = True
              end
              object rbAdmittedApplications: TcxRadioButton
                Left = 10
                Top = 47
                Width = 149
                Height = 17
                Caption = 'Successful Applications'
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Segoe UI'
                Font.Style = []
                ParentFont = False
                TabOrder = 1
                OnClick = rbAdmittedApplicationsClick
                Transparent = True
              end
              object rbRejectedApplications: TcxRadioButton
                Left = 10
                Top = 71
                Width = 160
                Height = 17
                Caption = 'Unsuccessful Applications'
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Segoe UI'
                Font.Style = []
                ParentFont = False
                TabOrder = 2
                OnClick = rbRejectedApplicationsClick
                Transparent = True
              end
              object rbUnprocessedApplications: TcxRadioButton
                Left = 10
                Top = 95
                Width = 162
                Height = 17
                Caption = 'Un-Processed Applications'
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Segoe UI'
                Font.Style = []
                ParentFont = False
                TabOrder = 3
                OnClick = rbUnprocessedApplicationsClick
                Transparent = True
              end
            end
          end
          object tabSearchApplication: TcxTabSheet
            Caption = 'Search'
            ImageIndex = 1
            DesignSize = (
              183
              559)
            object l145: TLabel
              Left = 0
              Top = 0
              Width = 183
              Height = 13
              Align = alTop
              Alignment = taCenter
              Caption = 'Specify Search Criteria'
              Font.Charset = ANSI_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Segoe UI'
              Font.Style = []
              ParentFont = False
              ExplicitWidth = 113
            end
            object l146: TLabel
              Left = 8
              Top = 27
              Width = 107
              Height = 13
              Caption = 'Search For Applicant'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Segoe UI'
              Font.Style = [fsBold]
              ParentFont = False
              Transparent = True
            end
            object tEditSearchApplicant: TcxTextEdit
              Left = 8
              Top = 45
              Anchors = [akLeft, akTop, akRight]
              BeepOnEnter = False
              Style.Color = clWindow
              TabOrder = 0
              Width = 170
            end
            object btnSearchApplicant: TcxButton
              Left = 103
              Top = 69
              Width = 75
              Height = 33
              Anchors = [akTop, akRight]
              Caption = 'Search'
              TabOrder = 1
              OnClick = btnSearchApplicantClick
            end
          end
        end
      end
      object gbApplications: TcxGroupBox
        Left = 203
        Top = 16
        Align = alClient
        Caption = 'Student Applications'
        ParentFont = False
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -13
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 1
        Height = 611
        Width = 910
        object pnlp25: TPanel
          Left = 2
          Top = 22
          Width = 906
          Height = 24
          Align = alTop
          BevelOuter = bvNone
          Color = clWindow
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentBackground = False
          ParentFont = False
          TabOrder = 0
          object l147: TLabel
            Left = 49
            Top = 4
            Width = 58
            Height = 13
            Caption = 'Select View'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Segoe UI'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object bvl31: TBevel
            Left = 505
            Top = 3
            Width = 6
            Height = 17
            Shape = bsLeftLine
          end
          object rbApplicationsFull: TcxRadioButton
            Left = 134
            Top = 3
            Width = 145
            Height = 17
            Caption = 'Application Entries'
            Checked = True
            TabOrder = 0
            TabStop = True
            OnClick = rbApplicationsFullClick
            Transparent = True
          end
          object rbApplicationsWizardSheet: TcxRadioButton
            Left = 296
            Top = 3
            Width = 190
            Height = 17
            Caption = 'Admission Wizard Results Sheet'
            Enabled = False
            TabOrder = 1
            OnClick = rbApplicationsWizardSheetClick
            Transparent = True
          end
          object bEditQuickSearchApplications: TcxButtonEdit
            Left = 533
            Top = 1
            BeepOnEnter = False
            Properties.Buttons = <
              item
                Default = True
                Glyph.Data = {
                  36030000424D3603000000000000360000002800000010000000100000000100
                  18000000000000030000120B0000120B00000000000000000000FFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAAAAAAAAAA
                  AAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFAAAAAAF8F8F8F9F9F9AAAAAAFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAAAAAAF8F8F8E3E3E3ECEC
                  ECAAAAAAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBA945DBA945DBA945DBA
                  945DAAAAAAF8F8F8E3E3E3ECECECAAAAAAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFBA945DFCEED8FCEED8FCEED8FCEED8BA945DE3E3E3ECECECAAAAAAFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBA945DF8E7D1F0C694F0C694F0C694F0
                  C694F8E7D1BA945DAAAAAAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBA945D
                  F7E4CFEDBF8FEDBF8FEDBF8FEDBF8FEDBF8FEDBF8FF7E4CFBA945DFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFBA945DF8E6D1EFC492EFC492EFC492EFC492EF
                  C492EFC492F8E6D1BA945DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBA945D
                  FAE9D3F3CC98F3CC98F3CC98F3CC98F3CC98F3CC98FAE9D3BA945DFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFBA945DFCEDD7F8D6A1F8D6A1F8D6A1F8D6A1F8
                  D6A1F8D6A1FCEDD7BA945DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  BA945DFEF1DAFCDFA8FCDFA8FCDFA8FCDFA8FEF1DABA945DFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBA945DFFF4DCFFF4DCFFF4DCFF
                  F4DCBA945DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFBA945DBA945DBA945DBA945DFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
                Kind = bkGlyph
              end>
            TabOrder = 2
            Text = '...Quick Search...'
            Width = 206
          end
        end
        object gApplications: TcxGrid
          Left = 2
          Top = 46
          Width = 906
          Height = 420
          Align = alClient
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          object gtvApplicationsFull: TcxGridDBTableView
            NavigatorButtons.ConfirmDelete = False
            OnFocusedRecordChanged = gtvApplicationsFullFocusedRecordChanged
            DataController.Summary.DefaultGroupSummaryItems = <
              item
                Format = '# Applicant(s)'
                Kind = skCount
              end>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = '#'
                Kind = skCount
              end>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.IncSearch = True
            OptionsSelection.MultiSelect = True
            OptionsView.NoDataToDisplayInfoText = 'No Applications Currently Listed'
            OptionsView.CellAutoHeight = True
            OptionsView.ColumnAutoWidth = True
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            Preview.Column = gcApplicationsFullSAG_REMARKS
            Preview.MaxLineCount = 5
            Preview.Visible = True
            object gcApplicationsFullSAG_APPLICATION_NO: TcxGridDBColumn
              DataBinding.FieldName = 'SAG_APPLICATION_NO'
              Visible = False
              Options.Editing = False
              Options.Filtering = False
              Options.Focusing = False
              Width = 21
              IsCaptionAssigned = True
            end
            object gcApplicationsFullSAG_TARGET_YEAR: TcxGridDBColumn
              Caption = 'Target Year'
              DataBinding.FieldName = 'SAG_TARGET_YEAR'
              Options.Editing = False
              Options.Filtering = False
              Options.Focusing = False
              Width = 37
            end
            object gcApplicationsFullSAG_TARGET_TERM: TcxGridDBColumn
              Caption = 'Term'
              DataBinding.FieldName = 'SAG_TARGET_TERM'
              Options.Editing = False
              Options.Filtering = False
              Options.Focusing = False
              Width = 23
            end
            object gcApplicationsFullSAG_TARGET_CLASS: TcxGridDBColumn
              Caption = 'Class'
              DataBinding.FieldName = 'SAG_TARGET_CLASS'
              Options.Editing = False
              Options.Filtering = False
              Options.Focusing = False
              Width = 31
            end
            object gcApplicationsFullSAG_SURNAME: TcxGridDBColumn
              Caption = 'Surname'
              DataBinding.FieldName = 'SAG_SURNAME'
              Options.Editing = False
              Options.Filtering = False
              Options.Focusing = False
              Width = 59
            end
            object gcApplicationsFullSAG_OTHERNAMES: TcxGridDBColumn
              Caption = 'Other Names'
              DataBinding.FieldName = 'SAG_OTHERNAMES'
              Options.Editing = False
              Options.Filtering = False
              Options.Focusing = False
              Width = 72
            end
            object gcApplicationsFullSAG_REG_NO: TcxGridDBColumn
              Caption = 'Reg. No.'
              DataBinding.FieldName = 'SAG_REG_NO'
              Options.Editing = False
              Options.Filtering = False
              Options.Focusing = False
              Width = 62
            end
            object gcApplicationsFullSAG_DOB: TcxGridDBColumn
              Caption = 'Date of Birth'
              DataBinding.FieldName = 'SAG_DOB'
              Options.Editing = False
              Options.Filtering = False
              Options.Focusing = False
              Width = 72
            end
            object gcApplicationsFullSAG_GENDER: TcxGridDBColumn
              Caption = 'Gender'
              DataBinding.FieldName = 'SAG_GENDER'
              Options.Editing = False
              Options.Filtering = False
              Options.Focusing = False
              Width = 27
            end
            object gcApplicationsFullSAG_CITIZENSHIP: TcxGridDBColumn
              Caption = 'Citizenship'
              DataBinding.FieldName = 'SAG_CITIZENSHIP'
              Options.Editing = False
              Options.Filtering = False
              Options.Focusing = False
              Width = 64
            end
            object gcApplicationsFullSAG_HOME_DISTRICT: TcxGridDBColumn
              Caption = 'Home District'
              DataBinding.FieldName = 'SAG_HOME_DISTRICT'
              Options.Editing = False
              Options.Filtering = False
              Options.Focusing = False
              Width = 78
            end
            object gcApplicationsFullSAG_ADDRESS: TcxGridDBColumn
              Caption = 'Address'
              DataBinding.FieldName = 'SAG_ADDRESS'
              Options.Editing = False
              Options.Filtering = False
              Options.Focusing = False
              Width = 135
            end
            object gcApplicationsFullSAG_TELEPHONE: TcxGridDBColumn
              Caption = 'Telephone'
              DataBinding.FieldName = 'SAG_TELEPHONE'
              Options.Editing = False
              Options.Filtering = False
              Options.Focusing = False
              Width = 78
            end
            object gcApplicationsFullSAG_EMAIL_ADDRESS: TcxGridDBColumn
              Caption = 'E-Mail Address'
              DataBinding.FieldName = 'SAG_EMAIL_ADDRESS'
              Options.Editing = False
              Options.Filtering = False
              Options.Focusing = False
              Width = 107
            end
            object gcApplicationsFullSAG_APPLICATION_FEE_MODE: TcxGridDBColumn
              Caption = 'Mode of Payment'
              DataBinding.FieldName = 'SAG_APPLICATION_FEE_MODE'
              Visible = False
              Options.Editing = False
              Options.Filtering = False
              Options.Focusing = False
            end
            object gcApplicationsFullSAG_APPLICATION_FEE_DETAILS: TcxGridDBColumn
              Caption = 'Fee Details'
              DataBinding.FieldName = 'SAG_APPLICATION_FEE_DETAILS'
              Visible = False
              Options.Editing = False
              Options.Filtering = False
              Options.Focusing = False
            end
            object gcApplicationsFullSAG_APPLICATION_FEE_AMOUNT: TcxGridDBColumn
              Caption = 'Amount Paid'
              DataBinding.FieldName = 'SAG_APPLICATION_FEE_AMOUNT'
              Visible = False
              Options.Editing = False
              Options.Filtering = False
              Options.Focusing = False
            end
            object gcApplicationsFullSAG_POSTED_TIME: TcxGridDBColumn
              Caption = 'Time Posted'
              DataBinding.FieldName = 'SAG_POSTED_TIME'
              Visible = False
              Options.Editing = False
              Options.Filtering = False
              Options.Focusing = False
            end
            object gcApplicationsFullSAG_REMARKS: TcxGridDBColumn
              Caption = 'Remarks'
              DataBinding.FieldName = 'SAG_REMARKS'
              Options.Editing = False
              Options.Filtering = False
              Options.Focusing = False
              Width = 34
            end
            object gcApplicationsFullSAG_SUFFICIENT_QUALIFICATIONS: TcxGridDBColumn
              Caption = 'Sufficient Qualifications'
              DataBinding.FieldName = 'SAG_SUFFICIENT_QUALIFICATIONS'
              Visible = False
              Options.Editing = False
              Options.Filtering = False
              Options.Focusing = False
            end
            object gcApplicationsFullSAG_INTERVIEW: TcxGridDBColumn
              Caption = 'Interviewed?'
              DataBinding.FieldName = 'SAG_INTERVIEW'
              Options.Editing = False
              Options.Filtering = False
              Options.Focusing = False
              Width = 24
            end
            object gcApplicationsFullSAG_ADMITTED: TcxGridDBColumn
              Caption = 'Admitted'
              DataBinding.FieldName = 'SAG_ADMITTED'
              Visible = False
              Options.Editing = False
              Options.Filtering = False
              Options.Focusing = False
            end
            object gcApplicationsFullSAG_ADMITTED_DATE: TcxGridDBColumn
              Caption = 'Date Admitted'
              DataBinding.FieldName = 'SAG_ADMITTED_DATE'
              Visible = False
              Options.Editing = False
              Options.Filtering = False
              Options.Focusing = False
            end
            object gcApplicationsFullSAG_ADMITTED_METHOD: TcxGridDBColumn
              Caption = 'Admission Method'
              DataBinding.FieldName = 'SAG_ADMITTED_METHOD'
              Visible = False
              Options.Editing = False
              Options.Filtering = False
              Options.Focusing = False
            end
            object gcApplicationsFullSAG_ADMITTED_BY: TcxGridDBColumn
              Caption = 'Admitted By'
              DataBinding.FieldName = 'SAG_ADMITTED_BY'
              Visible = False
              Options.Editing = False
              Options.Filtering = False
              Options.Focusing = False
            end
            object gcApplicationsFullSAG_DATE_FORM_RETURNED: TcxGridDBColumn
              Caption = 'Application Date'
              DataBinding.FieldName = 'SAG_DATE_FORM_RETURNED'
              Options.Editing = False
              Options.Filtering = False
              Options.Focusing = False
              Width = 65
            end
          end
          object gbtvApplicationsWizard: TcxGridDBBandedTableView
            NavigatorButtons.ConfirmDelete = False
            DataController.Summary.DefaultGroupSummaryItems = <
              item
                Format = '# Applicant(s)'
                Kind = skCount
              end>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = '# Applicant(s)'
                Kind = skCount
              end>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.IncSearch = True
            OptionsView.NoDataToDisplayInfoText = 'No Admission Wizard Results Displayed'
            OptionsView.CellAutoHeight = True
            OptionsView.Footer = True
            OptionsView.HeaderHeight = 150
            OptionsView.FixedBandSeparatorColor = clBlack
            Bands = <
              item
              end
              item
                Caption = 'O-LEVEL'
              end
              item
                Caption = 'A-LEVEL'
              end
              item
                Caption = 'ADMISSION'
              end>
          end
          object glApplicationsFull: TcxGridLevel
            GridView = gtvApplicationsFull
          end
          object glApplicationsWizard: TcxGridLevel
            GridView = gbtvApplicationsWizard
          end
        end
        object pcApplicationAdditionalDetails: TcxPageControl
          Left = 2
          Top = 474
          Width = 906
          Height = 135
          ActivePage = tabApplicantParents
          Align = alBottom
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          TabPosition = tpBottom
          ExplicitTop = 374
          ExplicitWidth = 797
          ClientRectBottom = 111
          ClientRectLeft = 4
          ClientRectRight = 902
          ClientRectTop = 4
          object tabApplicantParents: TcxTabSheet
            Caption = 'Applicant'#39's Parents/Next-of-Kin'
            ImageIndex = 0
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object gApplicantParents: TcxGrid
              Left = 0
              Top = 0
              Width = 898
              Height = 107
              Align = alClient
              TabOrder = 0
              object gtvApplicantParents: TcxGridDBTableView
                NavigatorButtons.ConfirmDelete = False
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                OptionsView.NoDataToDisplayInfoText = 'No Programme Choices Posted for Selected Applicant'
                OptionsView.CellAutoHeight = True
                OptionsView.ColumnAutoWidth = True
                OptionsView.GroupByBox = False
                object gcApplicantParentsSAP_TYPE: TcxGridDBColumn
                  Caption = 'Type'
                  DataBinding.FieldName = 'SAP_TYPE'
                end
                object gcApplicantParentsSAP_NAME: TcxGridDBColumn
                  Caption = 'Name'
                  DataBinding.FieldName = 'SAP_NAME'
                end
                object gcApplicantParentsSAP_TEL: TcxGridDBColumn
                  Caption = 'Telephone'
                  DataBinding.FieldName = 'SAP_TEL'
                end
                object gcApplicantParentsSAP_EMAIL: TcxGridDBColumn
                  Caption = 'E-Mail'
                  DataBinding.FieldName = 'SAP_EMAIL'
                end
                object gcApplicantParentsSAP_OCCUPATION: TcxGridDBColumn
                  Caption = 'Occupation'
                  DataBinding.FieldName = 'SAP_OCCUPATION'
                end
              end
              object glApplicantParents: TcxGridLevel
                GridView = gtvApplicantParents
              end
            end
          end
          object tabApplicantFormerSchools: TcxTabSheet
            Caption = 'Former Schools/Results'
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object gApplicantFormerSchools: TcxGrid
              Left = 0
              Top = 0
              Width = 604
              Height = 107
              Align = alLeft
              TabOrder = 0
              object gtvApplicantFormerSchools: TcxGridDBTableView
                NavigatorButtons.ConfirmDelete = False
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                OptionsView.NoDataToDisplayInfoText = 'No O-Level or A-Level Results Posted for Selected Applicant'
                OptionsView.CellAutoHeight = True
                OptionsView.ColumnAutoWidth = True
                OptionsView.GroupByBox = False
                object gcApplicantFormerSchoolsSAPS_SCHOOL: TcxGridDBColumn
                  Caption = 'School'
                  DataBinding.FieldName = 'SAPS_SCHOOL'
                  Options.Editing = False
                  Options.Filtering = False
                end
                object gcApplicantFormerSchoolsSAPS_ATTENDED_FROM: TcxGridDBColumn
                  Caption = 'Attended From'
                  DataBinding.FieldName = 'SAPS_ATTENDED_FROM'
                  Options.Editing = False
                  Options.Filtering = False
                end
                object gcApplicantFormerSchoolsSAPS_ATTENDED_TO: TcxGridDBColumn
                  Caption = 'To'
                  DataBinding.FieldName = 'SAPS_ATTENDED_TO'
                  Options.Editing = False
                  Options.Filtering = False
                end
                object gcApplicantFormerSchoolsSAPS_INDEX_NO: TcxGridDBColumn
                  Caption = 'Index No.'
                  DataBinding.FieldName = 'SAPS_INDEX_NO'
                  Options.Editing = False
                  Options.Filtering = False
                end
                object gcApplicantFormerSchoolsSAPS_VERIFIED: TcxGridDBColumn
                  Caption = 'Verified'
                  DataBinding.FieldName = 'SAPS_VERIFIED'
                  PropertiesClassName = 'TcxCheckBoxProperties'
                  Properties.ValueChecked = 'Y'
                  Properties.ValueUnchecked = 'N'
                  Options.Editing = False
                  Options.Filtering = False
                end
              end
              object glApplicantFormerSchools: TcxGridLevel
                GridView = gtvApplicantFormerSchools
              end
            end
            object gApplicantFormerResults: TcxGrid
              Left = 612
              Top = 0
              Width = 286
              Height = 107
              Align = alClient
              TabOrder = 1
              object gtvApplicantFormerResults: TcxGridDBTableView
                NavigatorButtons.ConfirmDelete = False
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                OptionsView.NoDataToDisplayInfoText = 'No Additional Qualifications Posted for Selected Applicant'
                OptionsView.CellAutoHeight = True
                OptionsView.ColumnAutoWidth = True
                OptionsView.GroupByBox = False
                object gcApplicantFormerResultsSAERD_WHICH_RESULT: TcxGridDBColumn
                  Caption = 'Result Type/Level'
                  DataBinding.FieldName = 'SAERD_WHICH_RESULT'
                  Options.Editing = False
                  Options.Filtering = False
                  Options.Focusing = False
                  Width = 166
                end
                object gcApplicantFormerResultsSAERD_SUBJECT: TcxGridDBColumn
                  Caption = 'Subject'
                  DataBinding.FieldName = 'SAERD_SUBJECT'
                  Options.Editing = False
                  Options.Filtering = False
                  Options.Focusing = False
                  Width = 222
                end
                object gcApplicantFormerResultsSAERD_GRADE: TcxGridDBColumn
                  Caption = 'Grade'
                  DataBinding.FieldName = 'SAERD_GRADE'
                  Options.Editing = False
                  Options.Filtering = False
                  Options.Focusing = False
                  Width = 57
                end
              end
              object glApplicantFormerResults: TcxGridLevel
                GridView = gtvApplicantFormerResults
              end
            end
            object s9: TcxSplitter
              Left = 604
              Top = 0
              Width = 8
              Height = 107
              Control = gApplicantFormerSchools
            end
          end
        end
        object sApplicationAdditionalDetails: TcxSplitter
          Left = 2
          Top = 466
          Width = 906
          Height = 8
          AlignSplitter = salBottom
          Control = pcApplicationAdditionalDetails
        end
      end
      object sApplications: TcxSplitter
        Left = 195
        Top = 16
        Width = 8
        Height = 611
        Control = gbApplicationListing
      end
    end
    object tabViewStudentsCurrent: TcxTabSheet
      Caption = 'Current Students'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object lSchoolStudents: TLabel
        Left = 0
        Top = 0
        Width = 1113
        Height = 16
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        Caption = 'School Students'
        Color = 4227072
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        ShowAccelChar = False
        Transparent = False
        Layout = tlCenter
        ExplicitLeft = 12
        ExplicitTop = -5
        ExplicitWidth = 1029
      end
      object s1: TcxSplitter
        Left = 0
        Top = 16
        Width = 8
        Height = 611
      end
      object pnlStudentsView: TPanel
        Left = 8
        Top = 16
        Width = 1105
        Height = 611
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitLeft = 505
        ExplicitTop = 168
        ExplicitWidth = 185
        ExplicitHeight = 41
        object bEditQuickSearchStudents: TcxButtonEdit
          Left = 674
          Top = 6
          BeepOnEnter = False
          ParentFont = False
          Properties.Buttons = <
            item
              Default = True
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                18000000000000030000120B0000120B00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAAAAAAAAAA
                AAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFAAAAAAF8F8F8F9F9F9AAAAAAFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAAAAAAF8F8F8E3E3E3ECEC
                ECAAAAAAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBA945DBA945DBA945DBA
                945DAAAAAAF8F8F8E3E3E3ECECECAAAAAAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFBA945DFCEED8FCEED8FCEED8FCEED8BA945DE3E3E3ECECECAAAAAAFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBA945DF8E7D1F0C694F0C694F0C694F0
                C694F8E7D1BA945DAAAAAAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBA945D
                F7E4CFEDBF8FEDBF8FEDBF8FEDBF8FEDBF8FEDBF8FF7E4CFBA945DFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFBA945DF8E6D1EFC492EFC492EFC492EFC492EF
                C492EFC492F8E6D1BA945DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBA945D
                FAE9D3F3CC98F3CC98F3CC98F3CC98F3CC98F3CC98FAE9D3BA945DFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFBA945DFCEDD7F8D6A1F8D6A1F8D6A1F8D6A1F8
                D6A1F8D6A1FCEDD7BA945DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                BA945DFEF1DAFCDFA8FCDFA8FCDFA8FCDFA8FEF1DABA945DFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBA945DFFF4DCFFF4DCFFF4DCFF
                F4DCBA945DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFBA945DBA945DBA945DBA945DFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
              Kind = bkGlyph
            end>
          Style.Color = clWindow
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -9
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          TabOrder = 0
          Text = '...Quick Search...'
          Width = 188
        end
        object pcStudentsView: TcxPageControl
          Left = 6
          Top = 36
          Width = 1124
          Height = 609
          ActivePage = cxTabSheet1
          HideTabs = True
          TabOrder = 1
          ClientRectBottom = 605
          ClientRectLeft = 4
          ClientRectRight = 1120
          ClientRectTop = 4
          object cxTabSheet1: TcxTabSheet
            Caption = 'cxTabSheet1'
            ImageIndex = 0
            ExplicitLeft = 0
            ExplicitTop = 24
            ExplicitWidth = 855
            ExplicitHeight = 581
            object gbSelectedStudentDetails: TcxGroupBox
              Left = 556
              Top = 0
              Align = alRight
              Alignment = alTopCenter
              Caption = 'Selected Student Details'
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -13
              Style.Font.Name = 'Segoe UI'
              Style.Font.Style = []
              Style.IsFontAssigned = True
              TabOrder = 0
              ExplicitLeft = 322
              Height = 601
              Width = 560
              object gbHomeFamilyInformation: TcxGroupBox
                Left = 2
                Top = 174
                Align = alTop
                Caption = 'Home, Family && Background Information'
                PanelStyle.Active = True
                ParentFont = False
                Style.Font.Charset = ANSI_CHARSET
                Style.Font.Color = clWindowText
                Style.Font.Height = -11
                Style.Font.Name = 'Segoe UI'
                Style.Font.Style = [fsBold]
                Style.IsFontAssigned = True
                TabOrder = 0
                Height = 129
                Width = 556
                object l8: TLabel
                  Left = 25
                  Top = 26
                  Width = 69
                  Height = 13
                  Alignment = taRightJustify
                  Caption = 'Home District'
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Segoe UI'
                  Font.Style = []
                  ParentFont = False
                end
                object l9: TLabel
                  Left = 218
                  Top = 26
                  Width = 74
                  Height = 13
                  Alignment = taRightJustify
                  Caption = 'Home Address'
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Segoe UI'
                  Font.Style = []
                  ParentFont = False
                end
                object l10: TLabel
                  Left = 5
                  Top = 51
                  Width = 105
                  Height = 13
                  Caption = 'Parents/Next-of-Kin'
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Segoe UI'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object cxdbm1: TcxDBMemo
                  Left = 295
                  Top = 23
                  DataBinding.DataField = 'SG_HOME_ADDRESS'
                  ParentFont = False
                  Properties.ScrollBars = ssVertical
                  Style.Color = 13433087
                  Style.Font.Charset = ANSI_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -11
                  Style.Font.Name = 'Segoe UI'
                  Style.Font.Style = []
                  Style.IsFontAssigned = True
                  TabOrder = 2
                  Height = 21
                  Width = 255
                end
                object dbTEdit4: TcxDBTextEdit
                  Left = 97
                  Top = 23
                  DataBinding.DataField = 'SG_DISTRICT'
                  ParentFont = False
                  Properties.ReadOnly = True
                  Style.Color = 13433087
                  Style.Font.Charset = ANSI_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -11
                  Style.Font.Name = 'Segoe UI'
                  Style.Font.Style = []
                  Style.IsFontAssigned = True
                  TabOrder = 0
                  Width = 116
                end
                object gStudentParents: TcxGrid
                  Left = 2
                  Top = 66
                  Width = 552
                  Height = 61
                  Align = alBottom
                  Anchors = [akLeft, akTop, akRight, akBottom]
                  BevelInner = bvNone
                  BorderStyle = cxcbsNone
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Segoe UI'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 1
                  object gtv3: TcxGridDBTableView
                    NavigatorButtons.ConfirmDelete = False
                    DataController.Summary.DefaultGroupSummaryItems = <>
                    DataController.Summary.FooterSummaryItems = <
                      item
                        Format = '# Offense(s)'
                        Kind = skCount
                        Column = gc12
                      end>
                    DataController.Summary.SummaryGroups = <>
                    OptionsView.NoDataToDisplayInfoText = 'No Disciplinary Offenses logged'
                    OptionsView.CellAutoHeight = True
                    OptionsView.ColumnAutoWidth = True
                    OptionsView.Footer = True
                    OptionsView.GroupByBox = False
                    OptionsView.RowSeparatorColor = clGreen
                    OptionsView.RowSeparatorWidth = 1
                    Preview.Column = gc11
                    Preview.Place = ppTop
                    Preview.Visible = True
                    object gc11: TcxGridDBColumn
                      Caption = 'Offense Details'
                      DataBinding.FieldName = 'DISC_ISSUE'
                      PropertiesClassName = 'TcxMemoProperties'
                      Options.Editing = False
                      Options.Filtering = False
                      Options.Focusing = False
                      Options.Grouping = False
                      Options.Moving = False
                    end
                    object gc12: TcxGridDBColumn
                      Caption = 'Date of Offense'
                      DataBinding.FieldName = 'DISC_DATE'
                      Options.Editing = False
                      Options.Filtering = False
                      Options.Focusing = False
                      Options.Grouping = False
                      Options.Moving = False
                      Width = 103
                    end
                    object gc13: TcxGridDBColumn
                      Caption = 'Class'
                      DataBinding.FieldName = 'DISC_CLASS'
                      Options.Editing = False
                      Options.Filtering = False
                      Options.Focusing = False
                      Options.Grouping = False
                      Options.Moving = False
                      Width = 79
                    end
                    object gc14: TcxGridDBColumn
                      Caption = 'Action Taken'
                      DataBinding.FieldName = 'DISC_ACTION'
                      PropertiesClassName = 'TcxMemoProperties'
                      Options.Editing = False
                      Options.Filtering = False
                      Options.Focusing = False
                      Options.Grouping = False
                      Options.Moving = False
                      Width = 291
                    end
                    object gc15: TcxGridDBColumn
                      Caption = 'Recorded By'
                      DataBinding.FieldName = 'U_FULLNAME'
                      Options.Editing = False
                      Options.Filtering = False
                      Options.Focusing = False
                      Options.Grouping = False
                      Width = 108
                    end
                  end
                  object gtvStudentParents: TcxGridDBTableView
                    NavigatorButtons.ConfirmDelete = False
                    DataController.Summary.DefaultGroupSummaryItems = <>
                    DataController.Summary.FooterSummaryItems = <>
                    DataController.Summary.SummaryGroups = <>
                    OptionsView.NoDataToDisplayInfoText = 'No Parents or Next-of-Kin Registered for this Student'
                    OptionsView.CellAutoHeight = True
                    OptionsView.ColumnAutoWidth = True
                    OptionsView.GridLines = glHorizontal
                    OptionsView.GroupByBox = False
                    object gcSP_TYPE: TcxGridDBColumn
                      DataBinding.FieldName = 'SP_TYPE'
                      Options.Editing = False
                      Options.Filtering = False
                      Width = 49
                      IsCaptionAssigned = True
                    end
                    object gcSP_NAME: TcxGridDBColumn
                      Caption = 'Name'
                      DataBinding.FieldName = 'SP_NAME'
                      Options.Editing = False
                      Options.Filtering = False
                      Width = 133
                    end
                    object gcSP_OCCUPATION: TcxGridDBColumn
                      Caption = 'Occupation'
                      DataBinding.FieldName = 'SP_OCCUPATION'
                      Options.Editing = False
                      Options.Filtering = False
                      Width = 80
                    end
                    object gcSP_TEL: TcxGridDBColumn
                      Caption = 'Telephone'
                      DataBinding.FieldName = 'SP_TEL'
                      Options.Editing = False
                      Options.Filtering = False
                      Width = 83
                    end
                    object gcSP_EMAIL: TcxGridDBColumn
                      Caption = 'E-Mail'
                      DataBinding.FieldName = 'SP_EMAIL'
                      Options.Editing = False
                      Options.Filtering = False
                      Width = 174
                    end
                    object gcSP_SMO_SUBSCRIBE: TcxGridDBColumn
                      AlternateCaption = 'PSMo?'
                      Caption = 'SMo?'
                      DataBinding.FieldName = 'SP_PSMO_SUBSCRIBE'
                      PropertiesClassName = 'TcxCheckBoxProperties'
                      Properties.ValueChecked = 'Y'
                      Properties.ValueUnchecked = 'N'
                      Options.Editing = False
                      Options.Filtering = False
                      Width = 35
                    end
                  end
                  object glStudentParents: TcxGridLevel
                    GridView = gtvStudentParents
                  end
                end
              end
              object s3: TcxSplitter
                Left = 2
                Top = 303
                Width = 556
                Height = 8
                AlignSplitter = salTop
                Control = gbHomeFamilyInformation
              end
              object pnl8: TPanel
                Left = 2
                Top = 22
                Width = 556
                Height = 152
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 2
                ExplicitTop = 21
                object pnl9: TPanel
                  Left = 0
                  Top = 0
                  Width = 142
                  Height = 152
                  Align = alLeft
                  BevelOuter = bvNone
                  TabOrder = 0
                  ExplicitLeft = 414
                  object pcStudentSnap: TcxPageControl
                    Left = 0
                    Top = 0
                    Width = 142
                    Height = 152
                    ActivePage = tabEntryID
                    Align = alClient
                    Font.Charset = ANSI_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'Trebuchet MS'
                    Font.Style = []
                    HideTabs = True
                    ParentFont = False
                    TabOrder = 0
                    TabPosition = tpRight
                    ExplicitLeft = -12
                    ExplicitWidth = 169
                    ExplicitHeight = 163
                    ClientRectBottom = 148
                    ClientRectLeft = 4
                    ClientRectRight = 138
                    ClientRectTop = 4
                    object tabEntryID: TcxTabSheet
                      Caption = 'Entry I.D'
                      ImageIndex = 0
                      object dbImgEntry: TcxDBImage
                        Left = 0
                        Top = 0
                        Align = alClient
                        DataBinding.DataField = 'SG_PHOTO_1'
                        Properties.GraphicClassName = 'TJPEGImage'
                        Properties.PopupMenuLayout.MenuItems = [pmiCopy, pmiSave]
                        Properties.PopupMenuLayout.CustomMenuItemCaption = 'Update Photo'
                        Properties.ReadOnly = True
                        Properties.Stretch = True
                        TabOrder = 0
                        Height = 144
                        Width = 134
                      end
                    end
                    object tabExitID: TcxTabSheet
                      Caption = 'Exit I.D'
                      ImageIndex = 1
                      object dbImgExitImage: TcxDBImage
                        Left = 0
                        Top = 0
                        Align = alClient
                        DataBinding.DataField = 'S_PHOTO_2'
                        Properties.GraphicClassName = 'TJPEGImage'
                        Properties.PopupMenuLayout.MenuItems = [pmiCopy, pmiSave]
                        Properties.ReadOnly = True
                        TabOrder = 0
                        Height = 144
                        Width = 134
                      end
                    end
                  end
                end
                object pnl10: TPanel
                  Left = 142
                  Top = 0
                  Width = 414
                  Height = 152
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 1
                  object gbPersonalSchoolInformation: TcxGroupBox
                    Left = 0
                    Top = 24
                    Align = alClient
                    Caption = 'Personal && School Information'
                    PanelStyle.Active = True
                    ParentFont = False
                    Style.Font.Charset = ANSI_CHARSET
                    Style.Font.Color = clWindowText
                    Style.Font.Height = -11
                    Style.Font.Name = 'Segoe UI'
                    Style.Font.Style = [fsBold]
                    Style.IsFontAssigned = True
                    TabOrder = 0
                    Height = 128
                    Width = 414
                    object l11: TLabel
                      Left = 27
                      Top = 25
                      Width = 66
                      Height = 13
                      Alignment = taRightJustify
                      Caption = 'Date of Birth'
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'Segoe UI'
                      Font.Style = []
                      ParentFont = False
                    end
                    object l12: TLabel
                      Left = 37
                      Top = 50
                      Width = 56
                      Height = 13
                      Alignment = taRightJustify
                      Caption = 'Nationality'
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'Segoe UI'
                      Font.Style = []
                      ParentFont = False
                    end
                    object l13: TLabel
                      Left = 25
                      Top = 81
                      Width = 68
                      Height = 13
                      Alignment = taRightJustify
                      Caption = 'Current Class'
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'Segoe UI'
                      Font.Style = []
                      ParentFont = False
                    end
                    object l14: TLabel
                      Left = 219
                      Top = 25
                      Width = 20
                      Height = 13
                      Alignment = taRightJustify
                      Caption = 'Age'
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'Segoe UI'
                      Font.Style = []
                      ParentFont = False
                    end
                    object l15: TLabel
                      Left = 331
                      Top = 25
                      Width = 38
                      Height = 13
                      Alignment = taRightJustify
                      Caption = 'Gender'
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'Segoe UI'
                      Font.Style = []
                      ParentFont = False
                    end
                    object l16: TLabel
                      Left = 228
                      Top = 50
                      Width = 43
                      Height = 13
                      Alignment = taRightJustify
                      Caption = 'Religion'
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'Segoe UI'
                      Font.Style = []
                      ParentFont = False
                    end
                    object l17: TLabel
                      Left = 216
                      Top = 81
                      Width = 51
                      Height = 13
                      Alignment = taRightJustify
                      Caption = 'Dormitory'
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'Segoe UI'
                      Font.Style = []
                      ParentFont = False
                    end
                    object l18: TLabel
                      Left = 216
                      Top = 106
                      Width = 79
                      Height = 13
                      Alignment = taRightJustify
                      Caption = 'Progress Status'
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'Segoe UI'
                      Font.Style = []
                      ParentFont = False
                    end
                    object Bevel1: TBevel
                      Left = 18
                      Top = 67
                      Width = 390
                      Height = 6
                      Shape = bsBottomLine
                    end
                    object l19: TLabel
                      Left = 20
                      Top = 106
                      Width = 74
                      Height = 13
                      Alignment = taRightJustify
                      Caption = 'Admission No.'
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'Segoe UI'
                      Font.Style = []
                      ParentFont = False
                    end
                    object dbTEdit6: TcxDBTextEdit
                      Left = 96
                      Top = 21
                      DataBinding.DataField = 'SG_DOB'
                      ParentFont = False
                      Properties.ReadOnly = True
                      Style.Color = 13433087
                      Style.Font.Charset = ANSI_CHARSET
                      Style.Font.Color = clWindowText
                      Style.Font.Height = -11
                      Style.Font.Name = 'Segoe UI'
                      Style.Font.Style = []
                      Style.IsFontAssigned = True
                      TabOrder = 0
                      Width = 111
                    end
                    object tEditStudentAge: TcxTextEdit
                      Left = 243
                      Top = 22
                      ParentFont = False
                      Properties.ReadOnly = True
                      Style.Color = 13433087
                      Style.Font.Charset = ANSI_CHARSET
                      Style.Font.Color = clWindowText
                      Style.Font.Height = -11
                      Style.Font.Name = 'Segoe UI'
                      Style.Font.Style = []
                      Style.IsFontAssigned = True
                      TabOrder = 1
                      Text = 'Student Age'
                      Width = 75
                    end
                    object dbTEdit7: TcxDBTextEdit
                      Left = 375
                      Top = 22
                      DataBinding.DataField = 'SG_SEX'
                      ParentFont = False
                      Properties.ReadOnly = True
                      Style.Color = 13433087
                      Style.Font.Charset = ANSI_CHARSET
                      Style.Font.Color = clWindowText
                      Style.Font.Height = -11
                      Style.Font.Name = 'Segoe UI'
                      Style.Font.Style = []
                      Style.IsFontAssigned = True
                      TabOrder = 2
                      Width = 33
                    end
                    object dbTEdit8: TcxDBTextEdit
                      Left = 96
                      Top = 47
                      DataBinding.DataField = 'SG_NATIONALITY'
                      ParentFont = False
                      Properties.ReadOnly = True
                      Style.Color = 13433087
                      Style.Font.Charset = ANSI_CHARSET
                      Style.Font.Color = clWindowText
                      Style.Font.Height = -11
                      Style.Font.Name = 'Segoe UI'
                      Style.Font.Style = []
                      Style.IsFontAssigned = True
                      TabOrder = 3
                      Width = 111
                    end
                    object dbTEdit9: TcxDBTextEdit
                      Left = 273
                      Top = 47
                      DataBinding.DataField = 'SG_RELIGION'
                      ParentFont = False
                      Properties.ReadOnly = True
                      Style.Color = 13433087
                      Style.Font.Charset = ANSI_CHARSET
                      Style.Font.Color = clWindowText
                      Style.Font.Height = -11
                      Style.Font.Name = 'Segoe UI'
                      Style.Font.Style = []
                      Style.IsFontAssigned = True
                      TabOrder = 4
                      Width = 135
                    end
                    object dbTEdit10: TcxDBTextEdit
                      Left = 96
                      Top = 78
                      DataBinding.DataField = 'SG_CLASS'
                      ParentFont = False
                      Properties.ReadOnly = True
                      Style.Color = 13433087
                      Style.Font.Charset = ANSI_CHARSET
                      Style.Font.Color = clWindowText
                      Style.Font.Height = -11
                      Style.Font.Name = 'Segoe UI'
                      Style.Font.Style = []
                      Style.IsFontAssigned = True
                      TabOrder = 5
                      Width = 111
                    end
                    object dbTEdit11: TcxDBTextEdit
                      Left = 273
                      Top = 78
                      DataBinding.DataField = 'SG_DORMITORY'
                      ParentFont = False
                      Properties.ReadOnly = True
                      Style.Color = 13433087
                      Style.Font.Charset = ANSI_CHARSET
                      Style.Font.Color = clWindowText
                      Style.Font.Height = -11
                      Style.Font.Name = 'Segoe UI'
                      Style.Font.Style = []
                      Style.IsFontAssigned = True
                      TabOrder = 6
                      Width = 135
                    end
                    object bDBEditBEdit1: TcxDBButtonEdit
                      Left = 301
                      Top = 103
                      DataBinding.DataField = 'SG_PROGRESS_STATUS'
                      ParentFont = False
                      Properties.Buttons = <
                        item
                          Default = True
                          Kind = bkEllipsis
                        end>
                      Properties.ReadOnly = True
                      Style.Color = 13433087
                      Style.Font.Charset = ANSI_CHARSET
                      Style.Font.Color = clWindowText
                      Style.Font.Height = -11
                      Style.Font.Name = 'Segoe UI'
                      Style.Font.Style = []
                      Style.IsFontAssigned = True
                      TabOrder = 7
                      Width = 107
                    end
                    object dbTEdit12: TcxDBTextEdit
                      Left = 97
                      Top = 103
                      DataBinding.DataField = 'SG_ADMISSION_NUMBER'
                      ParentFont = False
                      Properties.ReadOnly = True
                      Style.Color = 13433087
                      Style.Font.Charset = ANSI_CHARSET
                      Style.Font.Color = clWindowText
                      Style.Font.Height = -11
                      Style.Font.Name = 'Segoe UI'
                      Style.Font.Style = []
                      Style.IsFontAssigned = True
                      TabOrder = 8
                      Width = 111
                    end
                  end
                  object pnl11: TPanel
                    Left = 0
                    Top = 0
                    Width = 414
                    Height = 24
                    Align = alTop
                    BevelOuter = bvNone
                    ParentColor = True
                    TabOrder = 1
                    object cxDBLabel1: TcxDBLabel
                      Left = 0
                      Top = 0
                      Align = alClient
                      DataBinding.DataField = 'SG_FULLNAME'
                      ParentColor = False
                      ParentFont = False
                      Properties.Alignment.Horz = taLeftJustify
                      Properties.Alignment.Vert = taVCenter
                      Style.Color = clWindow
                      Style.Font.Charset = ANSI_CHARSET
                      Style.Font.Color = clMaroon
                      Style.Font.Height = -16
                      Style.Font.Name = 'Segoe UI'
                      Style.Font.Style = [fsBold]
                      Style.LookAndFeel.NativeStyle = True
                      Style.TextColor = clNavy
                      Style.IsFontAssigned = True
                      StyleDisabled.LookAndFeel.NativeStyle = True
                      StyleFocused.LookAndFeel.NativeStyle = True
                      StyleHot.LookAndFeel.NativeStyle = True
                      Transparent = True
                      Height = 24
                      Width = 414
                      AnchorY = 12
                    end
                  end
                end
              end
              object pnl3: TPanel
                Left = 2
                Top = 311
                Width = 556
                Height = 156
                Align = alClient
                BevelOuter = bvNone
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Segoe UI'
                Font.Style = []
                ParentFont = False
                TabOrder = 3
                ExplicitLeft = 11
                ExplicitTop = 349
                ExplicitWidth = 517
                ExplicitHeight = 130
                object l20: TLabel
                  Left = 0
                  Top = 0
                  Width = 556
                  Height = 16
                  Align = alTop
                  AutoSize = False
                  Caption = ' Student Track Records && File Information'
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Segoe UI'
                  Font.Style = [fsBold]
                  ParentFont = False
                  Transparent = True
                end
                object pcStudentTrackRecords: TcxPageControl
                  Left = 0
                  Top = 16
                  Width = 556
                  Height = 140
                  ActivePage = tabDiscipline
                  Align = alClient
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Segoe UI'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 0
                  ExplicitLeft = 1
                  ExplicitTop = 21
                  ExplicitWidth = 554
                  ExplicitHeight = 176
                  ClientRectBottom = 136
                  ClientRectLeft = 4
                  ClientRectRight = 552
                  ClientRectTop = 24
                  object tabDiscipline: TcxTabSheet
                    Caption = 'Discipline'
                    ImageIndex = 0
                    object pnlp3: TPanel
                      Left = 0
                      Top = 77
                      Width = 548
                      Height = 35
                      Align = alBottom
                      BevelOuter = bvNone
                      TabOrder = 0
                      object btnNewDisc: TcxButton
                        Left = 213
                        Top = 6
                        Width = 100
                        Height = 25
                        Caption = 'New Offence'
                        TabOrder = 0
                      end
                      object btnPrintDisc: TcxButton
                        Left = 325
                        Top = 6
                        Width = 100
                        Height = 25
                        Caption = 'Print'
                        TabOrder = 1
                      end
                      object btnRefreshDisc: TcxButton
                        Left = 437
                        Top = 6
                        Width = 100
                        Height = 25
                        Caption = 'Refresh'
                        TabOrder = 2
                      end
                    end
                    object gDisc: TcxGrid
                      Left = 0
                      Top = 0
                      Width = 548
                      Height = 77
                      Align = alClient
                      BevelInner = bvNone
                      BorderStyle = cxcbsNone
                      TabOrder = 1
                      object gtvDisc: TcxGridDBTableView
                        NavigatorButtons.ConfirmDelete = False
                        DataController.Summary.DefaultGroupSummaryItems = <>
                        DataController.Summary.FooterSummaryItems = <
                          item
                            Format = '0 Offence(s)'
                            Kind = skCount
                            Column = gcDiscDBTableView1DISC_ISSUE
                          end>
                        DataController.Summary.SummaryGroups = <>
                        OptionsView.NoDataToDisplayInfoText = 'No Disciplinary Offenses'
                        OptionsView.CellAutoHeight = True
                        OptionsView.ColumnAutoWidth = True
                        OptionsView.Footer = True
                        OptionsView.GridLines = glHorizontal
                        OptionsView.GroupByBox = False
                        object gcDiscDBTableView1DISC_CLASS: TcxGridDBColumn
                          Caption = 'Class'
                          DataBinding.FieldName = 'STD_CLASS'
                          Options.Editing = False
                          Options.Filtering = False
                          Options.Focusing = False
                          Options.CellMerging = True
                          Options.Grouping = False
                          Width = 90
                        end
                        object gcDiscDBTableView1DISC_DATE: TcxGridDBColumn
                          Caption = 'Date'
                          DataBinding.FieldName = 'STD_DATE'
                          Options.Editing = False
                          Options.Filtering = False
                          Options.Focusing = False
                          Options.Grouping = False
                          Width = 81
                        end
                        object gcDiscDBTableView1DISC_ISSUE: TcxGridDBColumn
                          Caption = 'Offence Details'
                          DataBinding.FieldName = 'STD_ISSUE'
                          Options.Editing = False
                          Options.Filtering = False
                          Options.Focusing = False
                          Options.Grouping = False
                          Width = 178
                        end
                        object gcDiscDBTableView1DISC_ACTION: TcxGridDBColumn
                          Caption = 'Action Taken'
                          DataBinding.FieldName = 'STD_ACTION'
                          Options.Editing = False
                          Options.Filtering = False
                          Options.Focusing = False
                          Options.Grouping = False
                          Width = 178
                        end
                        object gcDiscDBTableView1T_FULLNAME: TcxGridDBColumn
                          Caption = 'Logged By'
                          DataBinding.FieldName = 'T_FULLNAME'
                          Options.Editing = False
                          Options.Filtering = False
                          Options.Focusing = False
                          Options.Grouping = False
                          Width = 157
                        end
                      end
                      object glDisc: TcxGridLevel
                        GridView = gtvDisc
                      end
                    end
                  end
                  object tabHealth: TcxTabSheet
                    Caption = 'Health'
                    ImageIndex = 1
                    ExplicitLeft = 0
                    ExplicitTop = 0
                    ExplicitWidth = 0
                    ExplicitHeight = 0
                    object pnlp4: TPanel
                      Left = 0
                      Top = 77
                      Width = 548
                      Height = 35
                      Align = alBottom
                      BevelOuter = bvNone
                      TabOrder = 0
                      object btnNewHealth: TcxButton
                        Left = 214
                        Top = 6
                        Width = 100
                        Height = 25
                        Caption = 'New Incident'
                        TabOrder = 0
                      end
                      object btn1: TcxButton
                        Left = 326
                        Top = 6
                        Width = 100
                        Height = 25
                        Caption = 'Print'
                        TabOrder = 1
                      end
                      object btnRefreshHealth: TcxButton
                        Left = 438
                        Top = 6
                        Width = 100
                        Height = 25
                        Caption = 'Refresh'
                        TabOrder = 2
                      end
                    end
                    object gHealth: TcxGrid
                      Left = 0
                      Top = 0
                      Width = 548
                      Height = 77
                      Align = alClient
                      BevelInner = bvNone
                      BorderStyle = cxcbsNone
                      TabOrder = 1
                      object gtvgdbtv1: TcxGridDBTableView
                        NavigatorButtons.ConfirmDelete = False
                        DataController.Summary.DefaultGroupSummaryItems = <>
                        DataController.Summary.FooterSummaryItems = <
                          item
                            Format = '# Incident(s)'
                            Kind = skCount
                            Column = gc2
                          end>
                        DataController.Summary.SummaryGroups = <>
                        OptionsView.CellAutoHeight = True
                        OptionsView.ColumnAutoWidth = True
                        OptionsView.Footer = True
                        OptionsView.GroupByBox = False
                        OptionsView.RowSeparatorColor = clGreen
                        OptionsView.RowSeparatorWidth = 1
                        Preview.Column = gc1
                        Preview.Place = ppTop
                        Preview.Visible = True
                        object gc1: TcxGridDBColumn
                          Caption = 'Incident Details'
                          DataBinding.FieldName = 'HEALTH_ISSUE'
                          PropertiesClassName = 'TcxMemoProperties'
                          Options.Editing = False
                          Options.Filtering = False
                          Options.Focusing = False
                          Options.Grouping = False
                          Options.Moving = False
                        end
                        object gc2: TcxGridDBColumn
                          Caption = 'Date of Incident'
                          DataBinding.FieldName = 'HEALTH_DATE'
                          Options.Editing = False
                          Options.Filtering = False
                          Options.Focusing = False
                          Options.Grouping = False
                          Options.Moving = False
                          Width = 103
                        end
                        object gc3: TcxGridDBColumn
                          Caption = 'Class'
                          DataBinding.FieldName = 'HEALTH_CLASS'
                          Options.Editing = False
                          Options.Filtering = False
                          Options.Focusing = False
                          Options.Grouping = False
                          Options.Moving = False
                          Width = 79
                        end
                        object gc4: TcxGridDBColumn
                          Caption = 'Action Taken'
                          DataBinding.FieldName = 'HEALTH_ACTION'
                          PropertiesClassName = 'TcxMemoProperties'
                          Options.Editing = False
                          Options.Filtering = False
                          Options.Focusing = False
                          Options.Grouping = False
                          Options.Moving = False
                          Width = 291
                        end
                        object gc5: TcxGridDBColumn
                          Caption = 'Recorded By'
                          DataBinding.FieldName = 'U_FULLNAME'
                          Options.Editing = False
                          Options.Filtering = False
                          Options.Focusing = False
                          Options.Grouping = False
                          Width = 108
                        end
                      end
                      object gtvHealth: TcxGridDBTableView
                        NavigatorButtons.ConfirmDelete = False
                        DataController.Summary.DefaultGroupSummaryItems = <>
                        DataController.Summary.FooterSummaryItems = <
                          item
                            Format = '0 Incident(s)'
                            Kind = skCount
                            Column = gcHealthDBTableView1HEALTH_ISSUE
                          end>
                        DataController.Summary.SummaryGroups = <>
                        OptionsView.NoDataToDisplayInfoText = 'No Health Incidents'
                        OptionsView.CellAutoHeight = True
                        OptionsView.ColumnAutoWidth = True
                        OptionsView.Footer = True
                        OptionsView.GridLines = glHorizontal
                        OptionsView.GroupByBox = False
                        object gcHealthDBTableView1HEALTH_CLASS: TcxGridDBColumn
                          Caption = 'Class'
                          DataBinding.FieldName = 'STH_CLASS'
                          Options.Editing = False
                          Options.Filtering = False
                          Options.Focusing = False
                          Options.CellMerging = True
                          Options.Grouping = False
                          Width = 65
                        end
                        object gcHealthDBTableView1HEALTH_DATE: TcxGridDBColumn
                          Caption = 'Date'
                          DataBinding.FieldName = 'STH_DATE'
                          Options.Editing = False
                          Options.Filtering = False
                          Options.Focusing = False
                          Options.Grouping = False
                          Width = 89
                        end
                        object gcHealthDBTableView1HEALTH_ISSUE: TcxGridDBColumn
                          Caption = 'Incident Details'
                          DataBinding.FieldName = 'STH_ISSUE'
                          Options.Editing = False
                          Options.Filtering = False
                          Options.Focusing = False
                          Options.Grouping = False
                          Width = 184
                        end
                        object gcHealthDBTableView1HEALTH_ACTION: TcxGridDBColumn
                          Caption = 'Action Taken'
                          DataBinding.FieldName = 'STH_ACTION'
                          Options.Editing = False
                          Options.Filtering = False
                          Options.Focusing = False
                          Options.Grouping = False
                          Width = 156
                        end
                        object gcHealthDBTableView1HEALTH_COST: TcxGridDBColumn
                          Caption = 'Cost'
                          DataBinding.FieldName = 'STH_COST'
                          PropertiesClassName = 'TcxCurrencyEditProperties'
                          Properties.DisplayFormat = ',0'
                          Options.Editing = False
                          Options.Filtering = False
                          Options.Focusing = False
                          Options.Grouping = False
                          Width = 77
                        end
                        object gcHealthDBTableView1T_FULLNAME: TcxGridDBColumn
                          Caption = 'Logged By'
                          DataBinding.FieldName = 'T_FULLNAME'
                          Options.Editing = False
                          Options.Filtering = False
                          Options.Focusing = False
                          Options.Grouping = False
                          Width = 113
                        end
                      end
                      object glHealth: TcxGridLevel
                        GridView = gtvHealth
                      end
                    end
                  end
                  object tabExtraCurricular: TcxTabSheet
                    Caption = 'Co-Curricular Activity'
                    ImageIndex = 2
                    ExplicitLeft = 0
                    ExplicitTop = 0
                    ExplicitWidth = 0
                    ExplicitHeight = 0
                    object pnlp5: TPanel
                      Left = 0
                      Top = 77
                      Width = 548
                      Height = 35
                      Align = alBottom
                      BevelOuter = bvNone
                      TabOrder = 0
                      object btnNewActivity: TcxButton
                        Left = 214
                        Top = 6
                        Width = 100
                        Height = 25
                        Caption = 'New Activity'
                        TabOrder = 0
                      end
                      object btn2: TcxButton
                        Left = 326
                        Top = 6
                        Width = 100
                        Height = 25
                        Caption = 'Print'
                        TabOrder = 1
                      end
                      object btnRefreshCoCur: TcxButton
                        Left = 438
                        Top = 6
                        Width = 100
                        Height = 25
                        Caption = 'Refresh'
                        TabOrder = 2
                      end
                    end
                    object gCoCur: TcxGrid
                      Left = 0
                      Top = 0
                      Width = 548
                      Height = 77
                      Align = alClient
                      BevelInner = bvNone
                      BorderStyle = cxcbsNone
                      TabOrder = 1
                      object gtvgdbtv2: TcxGridDBTableView
                        NavigatorButtons.ConfirmDelete = False
                        DataController.Summary.DefaultGroupSummaryItems = <>
                        DataController.Summary.FooterSummaryItems = <
                          item
                            Format = '# Activit(ies)'
                            Kind = skCount
                            Column = gc7
                          end>
                        DataController.Summary.SummaryGroups = <>
                        OptionsView.CellAutoHeight = True
                        OptionsView.ColumnAutoWidth = True
                        OptionsView.Footer = True
                        OptionsView.GroupByBox = False
                        OptionsView.RowSeparatorColor = clGreen
                        OptionsView.RowSeparatorWidth = 1
                        Preview.Column = gc6
                        Preview.Place = ppTop
                        Preview.Visible = True
                        object gc6: TcxGridDBColumn
                          Caption = 'Activity Details'
                          DataBinding.FieldName = 'COCUR_ISSUE'
                          PropertiesClassName = 'TcxMemoProperties'
                          Options.Editing = False
                          Options.Filtering = False
                          Options.Focusing = False
                          Options.Grouping = False
                          Options.Moving = False
                        end
                        object gc7: TcxGridDBColumn
                          Caption = 'Date of Activity'
                          DataBinding.FieldName = 'COCUR_DATE'
                          Options.Editing = False
                          Options.Filtering = False
                          Options.Focusing = False
                          Options.Grouping = False
                          Options.Moving = False
                          Width = 103
                        end
                        object gc8: TcxGridDBColumn
                          Caption = 'Class'
                          DataBinding.FieldName = 'COCUR_CLASS'
                          Options.Editing = False
                          Options.Filtering = False
                          Options.Focusing = False
                          Options.Grouping = False
                          Options.Moving = False
                          Width = 79
                        end
                        object gc9: TcxGridDBColumn
                          Caption = 'Results'
                          DataBinding.FieldName = 'COCUR_ACTION'
                          PropertiesClassName = 'TcxMemoProperties'
                          Options.Editing = False
                          Options.Filtering = False
                          Options.Focusing = False
                          Options.Grouping = False
                          Options.Moving = False
                          Width = 291
                        end
                        object gc10: TcxGridDBColumn
                          Caption = 'Recorded By'
                          DataBinding.FieldName = 'U_FULLNAME'
                          Options.Editing = False
                          Options.Filtering = False
                          Options.Focusing = False
                          Options.Grouping = False
                          Width = 108
                        end
                      end
                      object gtvCoCur: TcxGridDBTableView
                        NavigatorButtons.ConfirmDelete = False
                        DataController.Summary.DefaultGroupSummaryItems = <>
                        DataController.Summary.FooterSummaryItems = <
                          item
                            Format = '0 Activit(ies)'
                            Kind = skCount
                            Column = gcCoCurDBTableView1COCUR_ISSUE
                          end>
                        DataController.Summary.SummaryGroups = <>
                        OptionsView.NoDataToDisplayInfoText = 'No Co\Extra-Curricular Activities'
                        OptionsView.CellAutoHeight = True
                        OptionsView.ColumnAutoWidth = True
                        OptionsView.Footer = True
                        OptionsView.GridLines = glHorizontal
                        OptionsView.GroupByBox = False
                        object gcCoCurDBTableView1COCUR_CLASS: TcxGridDBColumn
                          Caption = 'Class'
                          DataBinding.FieldName = 'STCA_CLASS'
                          Options.Editing = False
                          Options.Filtering = False
                          Options.CellMerging = True
                          Options.Grouping = False
                          Width = 70
                        end
                        object gcCoCurDBTableView1COCUR_DATE: TcxGridDBColumn
                          Caption = 'Date'
                          DataBinding.FieldName = 'STCA_DATE'
                          Options.Editing = False
                          Options.Filtering = False
                          Options.Grouping = False
                          Width = 77
                        end
                        object gcCoCurDBTableView1COCUR_ISSUE: TcxGridDBColumn
                          Caption = 'Activity Details'
                          DataBinding.FieldName = 'STCA_ISSUE'
                          Options.Editing = False
                          Options.Filtering = False
                          Options.Grouping = False
                          Width = 209
                        end
                        object gcCoCurDBTableView1COCUR_ACTION: TcxGridDBColumn
                          Caption = 'Results/Remarks'
                          DataBinding.FieldName = 'STCA_ACTION'
                          Options.Editing = False
                          Options.Filtering = False
                          Options.Grouping = False
                          Width = 183
                        end
                        object gcCoCurDBTableView1T_FULLNAME: TcxGridDBColumn
                          Caption = 'Logged By'
                          DataBinding.FieldName = 'T_FULLNAME'
                          Options.Editing = False
                          Options.Filtering = False
                          Options.Grouping = False
                          Width = 145
                        end
                      end
                      object glCoCur: TcxGridLevel
                        GridView = gtvCoCur
                      end
                    end
                  end
                  object tabLibraryActivity: TcxTabSheet
                    Caption = 'Library Activity'
                    ImageIndex = 3
                    ExplicitLeft = 0
                    ExplicitTop = 0
                    ExplicitWidth = 0
                    ExplicitHeight = 0
                  end
                end
              end
              object gbStudentFile: TcxGroupBox
                Left = 2
                Top = 475
                Align = alBottom
                PanelStyle.Active = True
                PanelStyle.CaptionIndent = 4
                ParentFont = False
                Style.Font.Charset = ANSI_CHARSET
                Style.Font.Color = clWindowText
                Style.Font.Height = -11
                Style.Font.Name = 'Segoe UI'
                Style.Font.Style = [fsBold]
                Style.IsFontAssigned = True
                TabOrder = 4
                ExplicitLeft = 3
                ExplicitTop = 488
                Height = 124
                Width = 556
                object pnl100: TPanel
                  Left = 2
                  Top = 2
                  Width = 552
                  Height = 61
                  Align = alClient
                  BevelOuter = bvNone
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Segoe UI'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 0
                  ExplicitWidth = 550
                  ExplicitHeight = 134
                  object l24: TLabel
                    Left = 0
                    Top = 0
                    Width = 552
                    Height = 15
                    Align = alTop
                    AutoSize = False
                    Caption = ' Student Additional Important Information'
                    Font.Charset = ANSI_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'Segoe UI'
                    Font.Style = [fsBold]
                    ParentFont = False
                    Layout = tlBottom
                    ExplicitWidth = 550
                  end
                  object pnl23: TPanel
                    Left = 0
                    Top = 15
                    Width = 552
                    Height = 46
                    Align = alClient
                    BevelOuter = bvNone
                    TabOrder = 0
                    ExplicitLeft = -1
                    ExplicitTop = 35
                    ExplicitWidth = 550
                    ExplicitHeight = 34
                    object dbMemo1: TcxDBMemo
                      Left = 0
                      Top = 0
                      Align = alLeft
                      DataBinding.DataField = 'SG_OTHER_INFO'
                      ParentFont = False
                      Properties.ReadOnly = True
                      Properties.ScrollBars = ssVertical
                      Style.Color = 13433087
                      Style.Font.Charset = ANSI_CHARSET
                      Style.Font.Color = clWindowText
                      Style.Font.Height = -11
                      Style.Font.Name = 'Segoe UI'
                      Style.Font.Style = []
                      Style.IsFontAssigned = True
                      TabOrder = 0
                      Height = 46
                      Width = 442
                    end
                    object btnViewNotes: TcxButton
                      Left = 450
                      Top = 2
                      Width = 92
                      Height = 25
                      Caption = 'View Notes'
                      TabOrder = 1
                      WordWrap = True
                      OnClick = btnViewNotesClick
                    end
                  end
                end
                object pnl22: TPanel
                  Left = 2
                  Top = 63
                  Width = 552
                  Height = 59
                  Align = alBottom
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Segoe UI'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 1
                  object l25: TLabel
                    Left = 28
                    Top = 10
                    Width = 61
                    Height = 13
                    Alignment = taRightJustify
                    Caption = 'Date Joined'
                    Font.Charset = ANSI_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'Segoe UI'
                    Font.Style = []
                    ParentFont = False
                  end
                  object l26: TLabel
                    Left = 240
                    Top = 10
                    Width = 63
                    Height = 13
                    Alignment = taRightJustify
                    Caption = 'Class Joined'
                    Font.Charset = ANSI_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'Segoe UI'
                    Font.Style = []
                    ParentFont = False
                  end
                  object l27: TLabel
                    Left = 43
                    Top = 34
                    Width = 46
                    Height = 13
                    Alignment = taRightJustify
                    Caption = 'Date Left'
                    Font.Charset = ANSI_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'Segoe UI'
                    Font.Style = []
                    ParentFont = False
                  end
                  object l28: TLabel
                    Left = 255
                    Top = 34
                    Width = 48
                    Height = 13
                    Alignment = taRightJustify
                    Caption = 'Class Left'
                    Font.Charset = ANSI_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'Segoe UI'
                    Font.Style = []
                    ParentFont = False
                  end
                  object btnStudentFileInformation: TcxButton
                    Left = 450
                    Top = 6
                    Width = 92
                    Height = 45
                    Caption = 'Student File Details'
                    TabOrder = 0
                    WordWrap = True
                    OnClick = btnStudentFileInformationClick
                  end
                  object cxdbtxtdtTEdit13: TcxDBTextEdit
                    Left = 94
                    Top = 6
                    DataBinding.DataField = 'SG_JOINED_DATE'
                    ParentFont = False
                    Properties.ReadOnly = True
                    Style.Color = 13433087
                    Style.Font.Charset = ANSI_CHARSET
                    Style.Font.Color = clWindowText
                    Style.Font.Height = -11
                    Style.Font.Name = 'Segoe UI'
                    Style.Font.Style = []
                    Style.IsFontAssigned = True
                    TabOrder = 1
                    Width = 134
                  end
                  object cxdbtxtdtTEdit14: TcxDBTextEdit
                    Left = 308
                    Top = 6
                    DataBinding.DataField = 'SG_CLASS_JOINED'
                    ParentFont = False
                    Properties.ReadOnly = True
                    Style.Color = 13433087
                    Style.Font.Charset = ANSI_CHARSET
                    Style.Font.Color = clWindowText
                    Style.Font.Height = -11
                    Style.Font.Name = 'Segoe UI'
                    Style.Font.Style = []
                    Style.IsFontAssigned = True
                    TabOrder = 2
                    Width = 134
                  end
                  object cxdbtxtdtTEdit15: TcxDBTextEdit
                    Left = 94
                    Top = 30
                    DataBinding.DataField = 'SG_LEFT_DATE'
                    ParentFont = False
                    Properties.ReadOnly = True
                    Style.Color = 13433087
                    Style.Font.Charset = ANSI_CHARSET
                    Style.Font.Color = clWindowText
                    Style.Font.Height = -11
                    Style.Font.Name = 'Segoe UI'
                    Style.Font.Style = []
                    Style.IsFontAssigned = True
                    TabOrder = 3
                    Width = 134
                  end
                  object cxdbtxtdtTEdit16: TcxDBTextEdit
                    Left = 308
                    Top = 30
                    DataBinding.DataField = 'SG_CLASS_LEFT'
                    ParentFont = False
                    Properties.ReadOnly = True
                    Style.Color = 13433087
                    Style.Font.Charset = ANSI_CHARSET
                    Style.Font.Color = clWindowText
                    Style.Font.Height = -11
                    Style.Font.Name = 'Segoe UI'
                    Style.Font.Style = []
                    Style.IsFontAssigned = True
                    TabOrder = 4
                    Width = 134
                  end
                end
              end
              object s4: TcxSplitter
                Left = 2
                Top = 467
                Width = 556
                Height = 8
                AlignSplitter = salBottom
                Control = gbStudentFile
              end
            end
            object gbSelectedStudentList: TcxGroupBox
              Left = 0
              Top = 0
              Align = alClient
              Alignment = alTopCenter
              Caption = 'Student List'
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -13
              Style.Font.Name = 'Segoe UI'
              Style.Font.Style = []
              Style.IsFontAssigned = True
              TabOrder = 1
              Height = 601
              Width = 556
              object gStudentList: TcxGrid
                Left = 2
                Top = 22
                Width = 552
                Height = 577
                Align = alClient
                BorderStyle = cxcbsNone
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Segoe UI'
                Font.Style = []
                ParentFont = False
                TabOrder = 0
                object gtvStudentList: TcxGridDBTableView
                  NavigatorButtons.ConfirmDelete = False
                  OnCellDblClick = gtvStudentListCellDblClick
                  DataController.Summary.DefaultGroupSummaryItems = <
                    item
                      Format = '0 Student(s)'
                      Kind = skCount
                      Column = gcStudentListSG_FULLNAME
                    end>
                  DataController.Summary.FooterSummaryItems = <
                    item
                      Format = '0 Student(s)'
                      Kind = skCount
                      Column = gcStudentListSG_FULLNAME
                    end>
                  DataController.Summary.SummaryGroups = <>
                  OptionsBehavior.IncSearch = True
                  OptionsBehavior.IncSearchItem = gcStudentListSG_FULLNAME
                  OptionsData.Deleting = False
                  OptionsData.DeletingConfirmation = False
                  OptionsData.Editing = False
                  OptionsData.Inserting = False
                  OptionsSelection.CellSelect = False
                  OptionsSelection.HideFocusRectOnExit = False
                  OptionsSelection.HideSelection = True
                  OptionsSelection.MultiSelect = True
                  OptionsSelection.UnselectFocusedRecordOnExit = False
                  OptionsView.NoDataToDisplayInfoText = 'No Students Currently Listed'
                  OptionsView.CellAutoHeight = True
                  OptionsView.ColumnAutoWidth = True
                  OptionsView.Footer = True
                  OptionsView.GroupByBox = False
                  OptionsView.Indicator = True
                  object gcStudentListSG_ID: TcxGridDBColumn
                    Caption = 'SG ID'
                    DataBinding.FieldName = 'SG_ID'
                    Visible = False
                    Options.Editing = False
                    Options.Filtering = False
                  end
                  object gcStudentListSG_FULLNAME: TcxGridDBColumn
                    Caption = 'Student'
                    DataBinding.FieldName = 'SG_FULLNAME'
                    HeaderAlignmentHorz = taCenter
                    HeaderAlignmentVert = vaCenter
                    Options.Editing = False
                    Width = 184
                  end
                  object gcStudentListSG_IS_BOARDER: TcxGridDBColumn
                    Caption = 'Boarder?'
                    DataBinding.FieldName = 'SG_IS_BOARDER'
                    PropertiesClassName = 'TcxCheckBoxProperties'
                    Properties.ValueChecked = 'Y'
                    Properties.ValueUnchecked = 'N'
                    Visible = False
                    Options.Editing = False
                    Options.Focusing = False
                    Width = 55
                  end
                  object gcStudentListSG_DORMITORY: TcxGridDBColumn
                    Caption = 'Dormitory'
                    DataBinding.FieldName = 'SG_DORMITORY'
                    HeaderAlignmentHorz = taCenter
                    HeaderAlignmentVert = vaCenter
                    Options.Editing = False
                  end
                  object gcStudentListSG_CLASS: TcxGridDBColumn
                    Caption = 'Class'
                    DataBinding.FieldName = 'SG_CLASS'
                    Visible = False
                    HeaderAlignmentHorz = taCenter
                    HeaderAlignmentVert = vaCenter
                    Options.Editing = False
                    Options.Focusing = False
                  end
                  object gcStudentListSG_SEX: TcxGridDBColumn
                    Caption = 'Sex'
                    DataBinding.FieldName = 'SG_SEX'
                    Visible = False
                    HeaderAlignmentHorz = taCenter
                    HeaderAlignmentVert = vaCenter
                    Options.Editing = False
                    Options.Focusing = False
                  end
                  object gcStudentListSG_RELIGION: TcxGridDBColumn
                    Caption = 'Religion'
                    DataBinding.FieldName = 'SG_RELIGION'
                    Visible = False
                    HeaderAlignmentHorz = taCenter
                    HeaderAlignmentVert = vaCenter
                    Options.Editing = False
                    Options.Focusing = False
                  end
                  object gcStudentListSG_NATIONALITY: TcxGridDBColumn
                    Caption = 'Nationality'
                    DataBinding.FieldName = 'SG_NATIONALITY'
                    Visible = False
                    HeaderAlignmentHorz = taCenter
                    HeaderAlignmentVert = vaCenter
                    Options.Editing = False
                    Options.Focusing = False
                  end
                  object gcStudentListSG_JOINED_DATE: TcxGridDBColumn
                    Caption = 'Date Joined'
                    DataBinding.FieldName = 'SG_JOINED_DATE'
                    Visible = False
                    HeaderAlignmentHorz = taCenter
                    HeaderAlignmentVert = vaCenter
                    Options.Editing = False
                    Options.Focusing = False
                  end
                  object gcStudentListSG_CLASS_JOINED: TcxGridDBColumn
                    Caption = 'Class Joined'
                    DataBinding.FieldName = 'SG_CLASS_JOINED'
                    Visible = False
                    HeaderAlignmentHorz = taCenter
                    HeaderAlignmentVert = vaCenter
                    Options.Editing = False
                    Options.Focusing = False
                  end
                  object gcStudentListSG_LEFT: TcxGridDBColumn
                    Caption = 'Left?'
                    DataBinding.FieldName = 'SG_LEFT'
                    Visible = False
                    HeaderAlignmentHorz = taCenter
                    HeaderAlignmentVert = vaCenter
                    Options.Editing = False
                    Options.Focusing = False
                  end
                  object gcStudentListSG_LEFT_DATE: TcxGridDBColumn
                    Caption = 'Date Left'
                    DataBinding.FieldName = 'SG_LEFT_DATE'
                    Visible = False
                    HeaderAlignmentHorz = taCenter
                    HeaderAlignmentVert = vaCenter
                    Options.Editing = False
                    Options.Focusing = False
                  end
                  object gcStudentListSG_CLASS_LEFT: TcxGridDBColumn
                    Caption = 'Class Left'
                    DataBinding.FieldName = 'SG_CLASS_LEFT'
                    Visible = False
                    HeaderAlignmentHorz = taCenter
                    HeaderAlignmentVert = vaCenter
                    Options.Editing = False
                    Options.Focusing = False
                  end
                  object gcStudentListSG_FEES_STATUS: TcxGridDBColumn
                    Caption = 'Fees Status'
                    Visible = False
                    HeaderAlignmentHorz = taCenter
                    HeaderAlignmentVert = vaCenter
                    Options.Editing = False
                    Options.Focusing = False
                  end
                  object gcStudentListSG_OOS: TcxGridDBColumn
                    Caption = 'OoS?'
                    DataBinding.FieldName = 'SG_OOS'
                    PropertiesClassName = 'TcxCheckBoxProperties'
                    Properties.ValueChecked = 'Y'
                    Properties.ValueGrayed = 'L'
                    Properties.ValueUnchecked = 'N'
                    Visible = False
                    Options.Editing = False
                    Options.Focusing = False
                  end
                  object gcStudentListSG_EMAIL: TcxGridDBColumn
                    Caption = 'E-Mail'
                    DataBinding.FieldName = 'SG_EMAIL'
                    Options.Editing = False
                    Width = 121
                  end
                end
                object glStudentList: TcxGridLevel
                  GridView = gtvStudentList
                end
              end
            end
          end
          object cxTabSheet2: TcxTabSheet
            Caption = 'cxTabSheet2'
            ImageIndex = 1
            object gb1: TcxGroupBox
              Left = 0
              Top = 0
              Align = alClient
              Alignment = alTopCenter
              Caption = 'Student ID View'
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -13
              Style.Font.Name = 'Segoe UI'
              Style.Font.Style = []
              Style.IsFontAssigned = True
              TabOrder = 0
              Height = 601
              Width = 1116
              object gIDView: TcxGrid
                Left = 2
                Top = 22
                Width = 1112
                Height = 577
                Align = alClient
                BorderStyle = cxcbsNone
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Segoe UI'
                Font.Style = []
                ParentFont = False
                TabOrder = 0
                object gcvIDView: TcxGridDBCardView
                  NavigatorButtons.ConfirmDelete = False
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  LayoutDirection = ldVertical
                  OptionsBehavior.IncSearch = True
                  OptionsBehavior.IncSearchItem = cvrIDViewSG_FULLNAME
                  OptionsData.Deleting = False
                  OptionsData.DeletingConfirmation = False
                  OptionsData.Editing = False
                  OptionsData.Inserting = False
                  OptionsSelection.CellSelect = False
                  OptionsSelection.UnselectFocusedRecordOnExit = False
                  OptionsView.NoDataToDisplayInfoText = 'No Students Currently Listed'
                  OptionsView.CardWidth = 158
                  OptionsView.CellAutoHeight = True
                  RowLayout = rlVertical
                  object cvrIDViewSG_FULLNAME: TcxGridDBCardViewRow
                    Caption = 'Student'
                    DataBinding.FieldName = 'SG_FULLNAME'
                    PropertiesClassName = 'TcxLabelProperties'
                    Properties.Alignment.Horz = taCenter
                    Properties.Alignment.Vert = taVCenter
                    Properties.WordWrap = True
                    CaptionAlignmentHorz = taCenter
                    CaptionAlignmentVert = vaCenter
                    Options.Editing = False
                    Options.ShowCaption = False
                    Position.BeginsLayer = True
                  end
                  object cvrIDViewSG_DOB: TcxGridDBCardViewRow
                    Caption = 'Date of Birth'
                    DataBinding.FieldName = 'SG_DOB'
                    Visible = False
                    Options.Editing = False
                    Options.Filtering = False
                    Options.ShowCaption = False
                    Position.BeginsLayer = False
                  end
                  object cvrIDViewSG_SEX: TcxGridDBCardViewRow
                    Caption = 'Sex'
                    DataBinding.FieldName = 'SG_SEX'
                    Visible = False
                    Options.Editing = False
                    Options.ShowCaption = False
                    Position.BeginsLayer = False
                  end
                  object cvrIDViewSG_CLASS: TcxGridDBCardViewRow
                    Caption = 'Class'
                    DataBinding.FieldName = 'SG_CLASS'
                    PropertiesClassName = 'TcxLabelProperties'
                    Properties.Alignment.Horz = taCenter
                    Options.Editing = False
                    Options.ShowCaption = False
                    Position.BeginsLayer = False
                  end
                  object cvrIDViewSG_IS_BOARDER: TcxGridDBCardViewRow
                    Caption = 'Is Boarder?'
                    DataBinding.FieldName = 'SG_IS_BOARDER'
                    Visible = False
                    Options.Editing = False
                    Options.ShowCaption = False
                    Position.BeginsLayer = False
                  end
                  object cvrIDViewSG_DORMITORY: TcxGridDBCardViewRow
                    Caption = 'Dormitory'
                    DataBinding.FieldName = 'SG_DORMITORY'
                    Visible = False
                    Options.Editing = False
                    Options.ShowCaption = False
                    Position.BeginsLayer = False
                  end
                  object cvrIDViewSG_RELIGION: TcxGridDBCardViewRow
                    Caption = 'Religion'
                    DataBinding.FieldName = 'SG_RELIGION'
                    Visible = False
                    Options.Editing = False
                    Options.ShowCaption = False
                    Position.BeginsLayer = False
                  end
                  object cvrIDViewSG_DISTRICT: TcxGridDBCardViewRow
                    Caption = 'District'
                    DataBinding.FieldName = 'SG_DISTRICT'
                    Visible = False
                    Options.Editing = False
                    Options.ShowCaption = False
                    Position.BeginsLayer = False
                  end
                  object cvrIDViewSG_NATIONALITY: TcxGridDBCardViewRow
                    Caption = 'Nationality'
                    DataBinding.FieldName = 'SG_NATIONALITY'
                    Visible = False
                    Options.Editing = False
                    Options.ShowCaption = False
                    Position.BeginsLayer = False
                  end
                  object cvrIDViewSG_PHOTO_1: TcxGridDBCardViewRow
                    Caption = 'ID'
                    DataBinding.FieldName = 'SG_PHOTO_1'
                    PropertiesClassName = 'TcxImageProperties'
                    Properties.Caption = '(No ID)'
                    Properties.GraphicClassName = 'TJPEGImage'
                    Options.Editing = False
                    Options.Filtering = False
                    Options.ShowCaption = False
                    Position.BeginsLayer = False
                  end
                  object cvrIDViewSG_LEFT: TcxGridDBCardViewRow
                    Caption = 'Left?'
                    DataBinding.FieldName = 'SG_LEFT'
                    Visible = False
                    Options.Editing = False
                    Options.ShowCaption = False
                    Position.BeginsLayer = False
                  end
                  object cvrIDViewSG_OOS: TcxGridDBCardViewRow
                    Caption = 'OoS'
                    DataBinding.FieldName = 'SG_OOS'
                    Visible = False
                    Options.Editing = False
                    Options.ShowCaption = False
                    Position.BeginsLayer = False
                  end
                end
                object glIDView: TcxGridLevel
                  GridView = gcvIDView
                end
              end
            end
          end
          object cxTabSheet3: TcxTabSheet
            Caption = 'cxTabSheet3'
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object gStudentsParents: TcxGrid
              Left = 0
              Top = 0
              Width = 1116
              Height = 601
              Align = alClient
              TabOrder = 0
              object gbtvStudentsParents: TcxGridDBBandedTableView
                NavigatorButtons.ConfirmDelete = False
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <
                  item
                    Format = '0'
                    Kind = skCount
                    Column = gbcStudentsParentsSP_NAME
                  end>
                DataController.Summary.SummaryGroups = <>
                OptionsView.NoDataToDisplayInfoText = 'No Students Selected'
                OptionsView.ColumnAutoWidth = True
                OptionsView.Footer = True
                Bands = <
                  item
                    Caption = 'Student'
                    Options.HoldOwnColumnsOnly = True
                  end
                  item
                    Caption = 'Parents, Guardians & Other Next-of-Kin'
                    Options.HoldOwnColumnsOnly = True
                  end>
                object gbcStudentsParentsSG_PHOTO_1: TcxGridDBBandedColumn
                  Caption = 'ID'
                  DataBinding.FieldName = 'SG_PHOTO_1'
                  PropertiesClassName = 'TcxBlobEditProperties'
                  Properties.BlobEditKind = bekPict
                  Properties.PictureGraphicClassName = 'TJPEGImage'
                  Properties.ReadOnly = True
                  Position.BandIndex = 0
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object gbcStudentsParentsSG_FULLNAME: TcxGridDBBandedColumn
                  Caption = 'Student'
                  DataBinding.FieldName = 'SG_FULLNAME'
                  Options.Editing = False
                  Position.BandIndex = 0
                  Position.ColIndex = 1
                  Position.RowIndex = 0
                end
                object gbcStudentsParentsSG_DOB: TcxGridDBBandedColumn
                  Caption = 'DoB'
                  DataBinding.FieldName = 'SG_DOB'
                  Visible = False
                  Options.Editing = False
                  Position.BandIndex = 0
                  Position.ColIndex = 2
                  Position.RowIndex = 0
                end
                object gbcStudentsParentsSG_SEX: TcxGridDBBandedColumn
                  Caption = 'Gender'
                  DataBinding.FieldName = 'SG_SEX'
                  Options.Editing = False
                  Position.BandIndex = 0
                  Position.ColIndex = 3
                  Position.RowIndex = 0
                end
                object gbcStudentsParentsSG_CLASS: TcxGridDBBandedColumn
                  Caption = 'Class'
                  DataBinding.FieldName = 'SG_CLASS'
                  Options.Editing = False
                  Position.BandIndex = 0
                  Position.ColIndex = 4
                  Position.RowIndex = 0
                end
                object gbcStudentsParentsSG_IS_BOARDER: TcxGridDBBandedColumn
                  Caption = 'Boarder?'
                  DataBinding.FieldName = 'SG_IS_BOARDER'
                  Visible = False
                  Options.Editing = False
                  Position.BandIndex = 0
                  Position.ColIndex = 5
                  Position.RowIndex = 0
                end
                object gbcStudentsParentsSG_DORMITORY: TcxGridDBBandedColumn
                  Caption = 'Dormitory'
                  DataBinding.FieldName = 'SG_DORMITORY'
                  Options.Editing = False
                  Position.BandIndex = 0
                  Position.ColIndex = 6
                  Position.RowIndex = 0
                end
                object gbcStudentsParentsSG_RELIGION: TcxGridDBBandedColumn
                  Caption = 'Religion'
                  DataBinding.FieldName = 'SG_RELIGION'
                  Options.Editing = False
                  Position.BandIndex = 0
                  Position.ColIndex = 7
                  Position.RowIndex = 0
                end
                object gbcStudentsParentsSG_DISTRICT: TcxGridDBBandedColumn
                  Caption = 'District'
                  DataBinding.FieldName = 'SG_DISTRICT'
                  Options.Editing = False
                  Position.BandIndex = 0
                  Position.ColIndex = 8
                  Position.RowIndex = 0
                end
                object gbcStudentsParentsSG_NATIONALITY: TcxGridDBBandedColumn
                  Caption = 'Nationality'
                  DataBinding.FieldName = 'SG_NATIONALITY'
                  Visible = False
                  Options.Editing = False
                  Position.BandIndex = 0
                  Position.ColIndex = 9
                  Position.RowIndex = 0
                end
                object gbcStudentsParentsSG_HOME_ADDRESS: TcxGridDBBandedColumn
                  Caption = 'Home Address'
                  DataBinding.FieldName = 'SG_HOME_ADDRESS'
                  Visible = False
                  Options.Editing = False
                  Position.BandIndex = 0
                  Position.ColIndex = 10
                  Position.RowIndex = 0
                end
                object gbcStudentsParentsSG_DISC_KOUNT: TcxGridDBBandedColumn
                  Caption = 'Disciplinary Incidents'
                  DataBinding.FieldName = 'SG_DISC_KOUNT'
                  Visible = False
                  Options.Editing = False
                  Position.BandIndex = 0
                  Position.ColIndex = 11
                  Position.RowIndex = 0
                end
                object gbcStudentsParentsSG_HEALTH_KOUNT: TcxGridDBBandedColumn
                  Caption = 'Health Incidents'
                  DataBinding.FieldName = 'SG_HEALTH_KOUNT'
                  Visible = False
                  Options.Editing = False
                  Position.BandIndex = 0
                  Position.ColIndex = 12
                  Position.RowIndex = 0
                end
                object gbcStudentsParentsSG_RESP_KOUNT: TcxGridDBBandedColumn
                  Caption = 'Responsibilities'
                  DataBinding.FieldName = 'SG_RESP_KOUNT'
                  Visible = False
                  Options.Editing = False
                  Position.BandIndex = 0
                  Position.ColIndex = 13
                  Position.RowIndex = 0
                end
                object gbcStudentsParentsSG_COCURR_KOUNT: TcxGridDBBandedColumn
                  Caption = 'Co-Curricular Participation'
                  DataBinding.FieldName = 'SG_COCURR_KOUNT'
                  Visible = False
                  Options.Editing = False
                  Position.BandIndex = 0
                  Position.ColIndex = 14
                  Position.RowIndex = 0
                end
                object gbcStudentsParentsSG_LIBRARY_KOUNT: TcxGridDBBandedColumn
                  Caption = 'Library Utilization'
                  DataBinding.FieldName = 'SG_LIBRARY_KOUNT'
                  Visible = False
                  Options.Editing = False
                  Position.BandIndex = 0
                  Position.ColIndex = 15
                  Position.RowIndex = 0
                end
                object gbcStudentsParentsSG_LIBRARY_STATUS: TcxGridDBBandedColumn
                  Caption = 'Library Status'
                  DataBinding.FieldName = 'SG_LIBRARY_STATUS'
                  Visible = False
                  Options.Editing = False
                  Position.BandIndex = 0
                  Position.ColIndex = 16
                  Position.RowIndex = 0
                end
                object gbcStudentsParentsSG_LEFT: TcxGridDBBandedColumn
                  Caption = 'Left?'
                  DataBinding.FieldName = 'SG_LEFT'
                  Visible = False
                  Options.Editing = False
                  Position.BandIndex = 0
                  Position.ColIndex = 17
                  Position.RowIndex = 0
                end
                object gbcStudentsParentsSG_COMBINATION: TcxGridDBBandedColumn
                  Caption = 'Combination'
                  DataBinding.FieldName = 'SG_COMBINATION'
                  Visible = False
                  Options.Editing = False
                  Position.BandIndex = 0
                  Position.ColIndex = 18
                  Position.RowIndex = 0
                end
                object gbcStudentsParentsSG_EMAIL: TcxGridDBBandedColumn
                  Caption = 'E-Mail'
                  DataBinding.FieldName = 'SG_EMAIL'
                  Visible = False
                  Options.Editing = False
                  Position.BandIndex = 0
                  Position.ColIndex = 19
                  Position.RowIndex = 0
                end
                object gbcStudentsParentsSG_IS_OBOG: TcxGridDBBandedColumn
                  Caption = 'OB/OG?'
                  DataBinding.FieldName = 'SG_IS_OBOG'
                  Visible = False
                  Options.Editing = False
                  Position.BandIndex = 0
                  Position.ColIndex = 20
                  Position.RowIndex = 0
                end
                object gbcStudentsParentsSG_ADMISSION_NUMBER: TcxGridDBBandedColumn
                  Caption = 'Admission Number'
                  DataBinding.FieldName = 'SG_ADMISSION_NUMBER'
                  Options.Editing = False
                  Position.BandIndex = 0
                  Position.ColIndex = 21
                  Position.RowIndex = 0
                end
                object gbcStudentsParentsSG_OOS: TcxGridDBBandedColumn
                  Caption = 'OoS?'
                  DataBinding.FieldName = 'SG_OOS'
                  Visible = False
                  Options.Editing = False
                  Position.BandIndex = 0
                  Position.ColIndex = 22
                  Position.RowIndex = 0
                end
                object gbcStudentsParentsSG_DISABLED: TcxGridDBBandedColumn
                  Caption = 'Disabled?'
                  DataBinding.FieldName = 'SG_DISABLED'
                  Visible = False
                  Options.Editing = False
                  Position.BandIndex = 0
                  Position.ColIndex = 23
                  Position.RowIndex = 0
                end
                object gbcStudentsParentsSP_TYPE: TcxGridDBBandedColumn
                  Caption = 'Kind'
                  DataBinding.FieldName = 'SP_TYPE'
                  Options.Editing = False
                  Position.BandIndex = 1
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object gbcStudentsParentsSP_NAME: TcxGridDBBandedColumn
                  Caption = 'Name'
                  DataBinding.FieldName = 'SP_NAME'
                  Options.Editing = False
                  Position.BandIndex = 1
                  Position.ColIndex = 1
                  Position.RowIndex = 0
                end
                object gbcStudentsParentsSP_TEL: TcxGridDBBandedColumn
                  Caption = 'Tel.'
                  DataBinding.FieldName = 'SP_TEL'
                  Options.Editing = False
                  Position.BandIndex = 1
                  Position.ColIndex = 2
                  Position.RowIndex = 0
                end
                object gbcStudentsParentsSP_EMAIL: TcxGridDBBandedColumn
                  Caption = 'E-Mail'
                  DataBinding.FieldName = 'SP_EMAIL'
                  Options.Editing = False
                  Position.BandIndex = 1
                  Position.ColIndex = 3
                  Position.RowIndex = 0
                end
                object gbcStudentsParentsSP_OCCUPATION: TcxGridDBBandedColumn
                  Caption = 'Occupation'
                  DataBinding.FieldName = 'SP_OCCUPATION'
                  Options.Editing = False
                  Position.BandIndex = 1
                  Position.ColIndex = 4
                  Position.RowIndex = 0
                end
                object gbcStudentsParentsSP_REMARKS: TcxGridDBBandedColumn
                  Caption = 'Remarks'
                  DataBinding.FieldName = 'SP_REMARKS'
                  Options.Editing = False
                  Position.BandIndex = 1
                  Position.ColIndex = 5
                  Position.RowIndex = 0
                end
                object gbcStudentsParentsSP_SMO_SUBSCRIBE: TcxGridDBBandedColumn
                  Caption = 'PSMO?'
                  DataBinding.FieldName = 'SP_SMO_SUBSCRIBE'
                  Options.Editing = False
                  Position.BandIndex = 1
                  Position.ColIndex = 6
                  Position.RowIndex = 0
                end
              end
              object glStudentsParents: TcxGridLevel
                GridView = gbtvStudentsParents
              end
            end
          end
        end
      end
    end
  end
  object rbStudentsClassicView: TcxRadioButton
    Left = 358
    Top = 51
    Width = 140
    Height = 17
    Caption = 'Students Classic View'
    Checked = True
    TabOrder = 2
    TabStop = True
    OnClick = rbStudentsClassicViewClick
    Transparent = True
  end
  object rbStudentsGalleryView: TcxRadioButton
    Left = 555
    Top = 51
    Width = 140
    Height = 17
    Caption = 'Students Gallery View'
    TabOrder = 3
    OnClick = rbStudentsGalleryViewClick
    Transparent = True
  end
  object rbStudentsParentsView: TcxRadioButton
    Left = 714
    Top = 51
    Width = 161
    Height = 17
    Caption = 'Parents/Guardians View'
    TabOrder = 4
    OnClick = rbStudentsParentsViewClick
    Transparent = True
  end
  object gbStudentListing: TcxGroupBox
    Left = 0
    Top = 25
    Align = alLeft
    Alignment = alTopCenter
    Caption = 'Student Listing Options'
    ParentFont = False
    Style.Font.Charset = ANSI_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -13
    Style.Font.Name = 'Segoe UI'
    Style.Font.Style = []
    Style.IsFontAssigned = True
    TabOrder = 9
    ExplicitTop = 16
    Height = 635
    Width = 210
    object pcStudentListing: TcxPageControl
      Left = 2
      Top = 22
      Width = 206
      Height = 611
      ActivePage = tabSelectClassDorm
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      ExplicitLeft = 3
      ExplicitTop = 19
      ExplicitWidth = 204
      ExplicitHeight = 439
      ClientRectBottom = 607
      ClientRectLeft = 4
      ClientRectRight = 202
      ClientRectTop = 24
      object tabSelectClassDorm: TcxTabSheet
        Caption = 'Class'
        ImageIndex = 0
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object pnlp2: TPanel
          Left = 0
          Top = 0
          Width = 198
          Height = 57
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object bvl1: TBevel
            Left = 0
            Top = 55
            Width = 198
            Height = 2
            Align = alBottom
            Shape = bsBottomLine
            ExplicitTop = 5
            ExplicitWidth = 185
          end
          object rbStudentSelectClass: TcxRadioButton
            Left = 12
            Top = 7
            Width = 158
            Height = 17
            Caption = 'Select Class and/or Stream'
            Checked = True
            Color = clBtnFace
            ParentColor = False
            TabOrder = 0
            TabStop = True
            OnClick = rbStudentSelectClassClick
            Transparent = True
          end
          object rbStudentSelectDorm: TcxRadioButton
            Left = 12
            Top = 31
            Width = 160
            Height = 17
            Caption = 'Select Dormitory or Hostel'
            Color = clBtnFace
            ParentColor = False
            TabOrder = 1
            OnClick = rbStudentSelectDormClick
            Transparent = True
          end
        end
        object pcStudentSelect: TcxPageControl
          Left = 0
          Top = 57
          Width = 198
          Height = 407
          ActivePage = tabStudentSelectClass
          Align = alClient
          HideTabs = True
          TabOrder = 1
          ExplicitWidth = 183
          ExplicitHeight = 208
          ClientRectBottom = 403
          ClientRectLeft = 4
          ClientRectRight = 194
          ClientRectTop = 4
          object tabStudentSelectClass: TcxTabSheet
            Caption = 'Class'
            ImageIndex = 0
            object tvStudentsSelectClass: TcxTreeView
              Left = 0
              Top = 0
              Width = 190
              Height = 399
              Align = alClient
              ParentFont = False
              Style.BorderStyle = cbsNone
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -12
              Style.Font.Name = 'Segoe UI'
              Style.Font.Style = []
              Style.HotTrack = False
              Style.IsFontAssigned = True
              TabOrder = 0
              OnClick = tvStudentsSelectClassClick
              OnDblClick = tvStudentsSelectClassDblClick
              HideSelection = False
              ReadOnly = True
              RowSelect = True
            end
          end
          object tabStudentSelectDorm: TcxTabSheet
            Caption = 'Dorm'
            ImageIndex = 1
            object tvStudentsSelectDorm: TcxTreeView
              Left = 0
              Top = 0
              Width = 190
              Height = 399
              Align = alClient
              ParentFont = False
              Style.BorderStyle = cbsNone
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -12
              Style.Font.Name = 'Segoe UI'
              Style.Font.Style = []
              Style.HotTrack = False
              Style.IsFontAssigned = True
              TabOrder = 0
              OnClick = tvStudentsSelectDormClick
              HideSelection = False
              ReadOnly = True
              RowSelect = True
            end
          end
        end
        object gbStudentListOptions: TcxGroupBox
          Left = 0
          Top = 472
          Align = alBottom
          Alignment = alTopCenter
          Caption = 'Student Status Filter Options'
          PanelStyle.Active = True
          PanelStyle.CaptionIndent = 4
          ParentFont = False
          Style.Font.Charset = ANSI_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = [fsBold]
          Style.IsFontAssigned = True
          TabOrder = 2
          Height = 111
          Width = 198
          object rbActiveStudentsS: TcxRadioButton
            Left = 8
            Top = 29
            Width = 181
            Height = 17
            Caption = 'Active (Current) Students Only'
            Checked = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Segoe UI'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            TabStop = True
            OnClick = rbActiveStudentsSClick
            Transparent = True
          end
          object rbArchiveStudentsS: TcxRadioButton
            Left = 8
            Top = 56
            Width = 176
            Height = 17
            Caption = 'Archived (Past) Students Only'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Segoe UI'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            OnClick = rbArchiveStudentsSClick
            Transparent = True
          end
          object rbBothActiveArchiveStudentsS: TcxRadioButton
            Left = 8
            Top = 83
            Width = 186
            Height = 17
            Caption = 'All Students (Active && Archived)'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Segoe UI'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            OnClick = rbBothActiveArchiveStudentsSClick
            Transparent = True
          end
        end
        object s11: TcxSplitter
          Left = 0
          Top = 464
          Width = 198
          Height = 8
          AlignSplitter = salBottom
          Control = gbStudentListOptions
        end
      end
      object tabSearchStudent: TcxTabSheet
        Caption = 'Search'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object label1lbl8: TLabel
          Left = 0
          Top = 0
          Width = 113
          Height = 13
          Align = alTop
          Alignment = taCenter
          Caption = 'Specify Search Criteria'
          Font.Charset = ANSI_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object label1l2: TLabel
          Left = 8
          Top = 27
          Width = 54
          Height = 13
          Alignment = taRightJustify
          Caption = 'Search For'
          Transparent = True
        end
        object bvl2: TBevel
          Left = 8
          Top = 111
          Width = 174
          Height = 5
          Shape = bsBottomLine
        end
        object label1StudentJoined: TLabel
          Left = 9
          Top = 181
          Width = 78
          Height = 13
          Caption = 'Student Joined'
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          Transparent = True
          Visible = False
        end
        object label1StartDate: TLabel
          Left = 11
          Top = 204
          Width = 45
          Height = 13
          Caption = 'between'
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          Transparent = True
          Visible = False
        end
        object label1EndDate: TLabel
          Left = 36
          Top = 231
          Width = 20
          Height = 13
          Caption = 'and'
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          Transparent = True
          Visible = False
        end
        object rbStudentSearchContaining: TcxRadioButton
          Left = 102
          Top = 56
          Width = 81
          Height = 17
          Caption = 'Containing'
          TabOrder = 0
          Transparent = True
        end
        object rbStudentSearchStarting: TcxRadioButton
          Left = 8
          Top = 56
          Width = 89
          Height = 17
          Caption = 'Starting With'
          Checked = True
          TabOrder = 1
          TabStop = True
          Transparent = True
        end
        object tEditStudentSearch: TcxTextEdit
          Left = 8
          Top = 80
          BeepOnEnter = False
          Style.Color = clWindow
          TabOrder = 2
          Width = 174
        end
        object btnSearchStudents: TcxButton
          Left = 107
          Top = 181
          Width = 75
          Height = 33
          Caption = 'Search'
          TabOrder = 3
          OnClick = btnSearchStudentsClick
        end
        object chkComboStudentSearch: TcxCheckComboBox
          Left = 69
          Top = 24
          Properties.Items = <
            item
              Description = 'Student'
            end
            item
              Description = 'Parent\Guardian'
            end
            item
              Description = 'Home Address'
            end
            item
              Description = 'E-Mail Address'
            end
            item
              Description = 'Telephone'
            end
            item
              Description = 'Admission No.'
            end>
          Style.Color = clWindow
          TabOrder = 4
          Width = 113
        end
        object chkStudentSearchArchives: TcxCheckBox
          Left = 8
          Top = 129
          Caption = 'Include Archives in Search'
          ParentFont = False
          Style.Font.Charset = ANSI_CHARSET
          Style.Font.Color = clMaroon
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          TabOrder = 5
          Transparent = True
          Width = 153
        end
        object chkStudentSearchDateJoined: TcxCheckBox
          Left = 8
          Top = 153
          Caption = 'Filter by Admission Date'
          ParentFont = False
          Style.Font.Charset = ANSI_CHARSET
          Style.Font.Color = clMaroon
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          TabOrder = 6
          Transparent = True
          OnClick = chkStudentSearchDateJoinedClick
          Width = 148
        end
        object dEditStudentSearchStartDate: TcxDateEdit
          Left = 61
          Top = 201
          EditValue = 0d
          Enabled = False
          Properties.SaveTime = False
          Properties.ShowTime = False
          Properties.WeekNumbers = True
          Style.Color = clWindow
          TabOrder = 7
          Visible = False
          Width = 121
        end
        object dEditStudentSearchEndDate: TcxDateEdit
          Left = 61
          Top = 228
          EditValue = 0d
          Enabled = False
          Properties.SaveTime = False
          Properties.ShowTime = False
          Properties.WeekNumbers = True
          Style.Color = clWindow
          TabOrder = 8
          Visible = False
          Width = 121
        end
      end
      object tabSpecialGroups: TcxTabSheet
        Caption = 'Groups'
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object label1lbl9: TLabel
          Left = 0
          Top = 0
          Width = 198
          Height = 18
          Align = alTop
          Alignment = taCenter
          AutoSize = False
          Caption = 'Student Clubs && Associations'
          Font.Charset = ANSI_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 185
        end
        object label1lbl10: TLabel
          Left = 0
          Top = 570
          Width = 145
          Height = 13
          Align = alBottom
          Alignment = taCenter
          Caption = 'Double-click to list members'
          Font.Charset = ANSI_CHARSET
          Font.Color = clMaroon
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          Transparent = True
          Layout = tlCenter
          WordWrap = True
        end
        object gStudentGroups: TcxGrid
          Left = 0
          Top = 18
          Width = 198
          Height = 552
          Align = alClient
          BevelInner = bvNone
          BevelOuter = bvNone
          BorderStyle = cxcbsNone
          TabOrder = 0
          LookAndFeel.NativeStyle = True
          object gtvStudentGroups: TcxGridDBTableView
            NavigatorButtons.ConfirmDelete = False
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = '0 Group(s)/Association(s)'
                Kind = skCount
                Column = gcStudentGroupsDBTableView1G_NAME
              end>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.IncSearch = True
            OptionsBehavior.IncSearchItem = gcStudentGroupsDBTableView1G_NAME
            OptionsSelection.CellSelect = False
            OptionsSelection.HideFocusRectOnExit = False
            OptionsSelection.UnselectFocusedRecordOnExit = False
            OptionsView.CellEndEllipsis = True
            OptionsView.NoDataToDisplayInfoText = 'No Special Groups Found'
            OptionsView.CellAutoHeight = True
            OptionsView.ColumnAutoWidth = True
            OptionsView.Footer = True
            OptionsView.GridLines = glNone
            OptionsView.GroupByBox = False
            OptionsView.Header = False
            OptionsView.RowSeparatorWidth = 1
            object gcStudentGroupsDBTableView1G_NAME: TcxGridDBColumn
              Caption = 'Group/Association'
              DataBinding.FieldName = 'SSG_NAME'
              Options.Editing = False
              Options.Filtering = False
              Options.FilteringFilteredItemsList = False
              Options.Focusing = False
            end
          end
          object glStudentGroups: TcxGridLevel
            GridView = gtvStudentGroups
          end
        end
      end
      object tabStudentLists: TcxTabSheet
        Caption = 'Lists'
        Enabled = False
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
      end
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 452
    Top = 131
    DockControlHeights = (
      0
      0
      25
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = 'stdinfomgttoolbar'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 16
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 169
      FloatTop = 156
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem8'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarButton1: TdxBarButton
      Caption = 'Register Student'
      Category = 0
      Hint = 'Register Student'
      Visible = ivAlways
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = 'Register Student'
      Category = 0
      Visible = ivAlways
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarButton3'
        end>
    end
    object dxBarSubItem2: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarSubItem3: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarSubItem4: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarSubItem5: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarSubItem6: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarButton2: TdxBarButton
      Caption = 'New/Current Student'
      Category = 0
      Hint = 'New/Current Student'
      Visible = ivAlways
      OnClick = dxBarButton2Click
    end
    object dxBarButton3: TdxBarButton
      Caption = 'Old Student'
      Category = 0
      Hint = 'Old Student'
      Visible = ivAlways
      OnClick = dxBarButton3Click
    end
    object dxBarButton4: TdxBarButton
      Caption = 'Update Student'
      Category = 0
      Hint = 'Update Student'
      Visible = ivAlways
    end
    object dxBarSubItem7: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarButton5: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarSubItem8: TdxBarSubItem
      Caption = 'Update Student'
      Category = 0
      Visible = ivAlways
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton6'
        end>
    end
    object dxBarButton6: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
  end
  object dlgSaveExport: TSaveDialog
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Left = 666
    Top = 158
  end
  object aeSM: TApplicationEvents
    Left = 494
    Top = 273
  end
  object pmRefreshTreeAcademics: TPopupMenu
    Left = 63
    Top = 248
    object mniRefreshSelection1: TMenuItem
      Caption = 'Refresh Selection'
    end
  end
  object pmStaffStatistics: TPopupMenu
    Left = 127
    Top = 245
    object SourceData2: TMenuItem
      Caption = 'Source Data'
      object AllCells2: TMenuItem
        Caption = 'All Cells'
        Checked = True
      end
      object SelectedCellsOnly2: TMenuItem
        Tag = 1
        Caption = 'Selected Cells Only'
      end
    end
    object BasisforCategories2: TMenuItem
      Caption = 'Basis for Categories'
      object Rows2: TMenuItem
        Caption = 'Rows'
        Checked = True
      end
      object Columns2: TMenuItem
        Tag = 1
        Caption = 'Columns'
      end
    end
    object ChangeChartType2: TMenuItem
      Caption = 'Change Chart Type'
      Enabled = False
      Visible = False
    end
    object N38: TMenuItem
      Caption = '-'
    end
    object Export8: TMenuItem
      Caption = 'Export'
      object MSExcel7: TMenuItem
        Caption = 'MS Excel'
      end
      object HTML7: TMenuItem
        Caption = 'HTML'
      end
      object XML8: TMenuItem
        Caption = 'XML'
      end
      object PlainText7: TMenuItem
        Caption = 'Plain Text'
      end
    end
    object MenuItem15: TMenuItem
      Caption = 'Print'
      Enabled = False
    end
    object MenuItem16: TMenuItem
      Caption = '-'
    end
    object Refresh10: TMenuItem
      Caption = 'Refresh'
    end
  end
  object pmStudentStatistics: TPopupMenu
    Left = 91
    Top = 249
    object SourceData1: TMenuItem
      Caption = 'Source Data'
      object AllCells1: TMenuItem
        Caption = 'All Cells'
        Checked = True
      end
      object SelectedCellsOnly1: TMenuItem
        Tag = 1
        Caption = 'Selected Cells Only'
      end
    end
    object BasisforCategories1: TMenuItem
      Caption = 'Basis for Categories'
      object Rows1: TMenuItem
        Caption = 'Rows'
        Checked = True
      end
      object Columns1: TMenuItem
        Tag = 1
        Caption = 'Columns'
      end
    end
    object ChangeChartType1: TMenuItem
      Caption = 'Change Chart Type'
      Enabled = False
      Visible = False
    end
    object N36: TMenuItem
      Caption = '-'
    end
    object Export7: TMenuItem
      Caption = 'Export'
      object MSExcel6: TMenuItem
        Caption = 'MS Excel'
      end
      object H1: TMenuItem
        Caption = 'HTML'
      end
      object XML7: TMenuItem
        Caption = 'XML'
      end
      object PlainText6: TMenuItem
        Caption = 'Plain Text'
      end
    end
    object Print2: TMenuItem
      Caption = 'Print'
      Enabled = False
    end
    object N37: TMenuItem
      Caption = '-'
    end
    object Refresh9: TMenuItem
      Caption = 'Refresh'
    end
  end
  object pmFinanceSummaryMaster: TPopupMenu
    Left = 24
    Top = 587
    object Listing1: TMenuItem
      Caption = 'Listing'
      object Expand1: TMenuItem
        Caption = 'Expand'
      end
      object Collapse1: TMenuItem
        Caption = 'Collapse'
      end
      object Grouping1: TMenuItem
        Caption = 'Grouping'
      end
    end
    object N23: TMenuItem
      Caption = '-'
    end
    object Export4: TMenuItem
      Caption = 'Export'
      object Excel1: TMenuItem
        Caption = 'MS Excel'
      end
      object HTML4: TMenuItem
        Caption = 'HTML'
      end
      object XML4: TMenuItem
        Caption = 'XML'
      end
      object PlainText3: TMenuItem
        Caption = 'Plain Text'
      end
    end
    object N28: TMenuItem
      Caption = '-'
    end
    object Refresh8: TMenuItem
      Caption = 'Refresh'
    end
  end
  object pmFinanceDetailed: TPopupMenu
    Left = 58
    Top = 587
    object EditPayment1: TMenuItem
      Caption = 'Edit Payment'
    end
    object DeletePayment1: TMenuItem
      Caption = 'Delete Payment'
    end
    object N24: TMenuItem
      Caption = '-'
    end
    object RePrintReceipt1: TMenuItem
      Caption = 'Re-Print Receipt'
    end
    object N27: TMenuItem
      Caption = '-'
    end
    object Listing2: TMenuItem
      Caption = 'Listing'
      object Expand2: TMenuItem
        Caption = 'Expand'
      end
      object Collapse2: TMenuItem
        Caption = 'Collapse'
      end
      object Grouping2: TMenuItem
        Caption = 'Grouping'
      end
    end
    object N29: TMenuItem
      Caption = '-'
    end
    object Export5: TMenuItem
      Caption = 'Export'
      object MSExcel4: TMenuItem
        Caption = 'MS Excel'
      end
      object HTML5: TMenuItem
        Caption = 'HTML'
      end
      object XML5: TMenuItem
        Caption = 'XML'
      end
      object PlainText4: TMenuItem
        Caption = 'Plain Text'
      end
    end
    object N30: TMenuItem
      Caption = '-'
    end
    object Refresh7: TMenuItem
      Caption = 'Refresh'
    end
  end
  object pmFinanceDue: TPopupMenu
    Left = 91
    Top = 587
    object RecordPayment1: TMenuItem
      Caption = 'Record Payment'
    end
    object N25: TMenuItem
      Caption = '-'
    end
    object EditDuesPayable1: TMenuItem
      Caption = 'Edit Dues Payable'
    end
    object SetAdditionalDuesPayable1: TMenuItem
      Caption = 'Set Additional Dues Payable'
      object SelectedStudent1: TMenuItem
        Caption = 'Selected Student'
      end
      object EntireClass1: TMenuItem
        Caption = 'Entire Class'
      end
    end
    object N26: TMenuItem
      Caption = '-'
    end
    object Listing3: TMenuItem
      Caption = 'Listing'
      object Expand3: TMenuItem
        Caption = 'Expand'
      end
      object Collapse3: TMenuItem
        Caption = 'Collapse'
      end
      object Grouping3: TMenuItem
        Caption = 'Grouping'
      end
    end
    object N31: TMenuItem
      Caption = '-'
    end
    object Export6: TMenuItem
      Caption = 'Export'
      object MSExcel5: TMenuItem
        Caption = 'MS Excel'
      end
      object HTML6: TMenuItem
        Caption = 'HTML'
      end
      object XML6: TMenuItem
        Caption = 'XML'
      end
      object PlainText5: TMenuItem
        Caption = 'Plain Text'
      end
    end
    object N32: TMenuItem
      Caption = '-'
    end
    object Refresh6: TMenuItem
      Caption = 'Refresh'
    end
  end
  object bpmFinanceAdditional: TdxBarPopupMenu
    BarManager = dxBarManager1
    ItemLinks = <
      item
        Visible = True
      end
      item
        BeginGroup = True
        Visible = True
      end
      item
        Visible = True
      end>
    UseOwnFont = False
    Left = 291
    Top = 294
  end
  object bpmStaffManagement: TdxBarPopupMenu
    BarManager = dxBarManager1
    ItemLinks = <
      item
        Visible = True
      end
      item
        Visible = True
      end>
    UseOwnFont = False
    Left = 318
    Top = 294
  end
  object bpmStaffEditStaff: TdxBarPopupMenu
    BarManager = dxBarManager1
    ItemLinks = <
      item
        Visible = True
      end
      item
        BeginGroup = True
        Visible = True
      end
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        BeginGroup = True
        Visible = True
      end
      item
        BeginGroup = True
        Visible = True
      end>
    UseOwnFont = False
    Left = 279
    Top = 359
  end
  object bpmStaffAttendanceRecords: TdxBarPopupMenu
    BarManager = dxBarManager1
    ItemLinks = <
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        BeginGroup = True
        Visible = True
      end>
    UseOwnFont = False
    Left = 251
    Top = 400
  end
  object bpmAdmissionTasks: TdxBarPopupMenu
    BarManager = dxBarManager1
    ItemLinks = <
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        BeginGroup = True
        Visible = True
      end
      item
        BeginGroup = True
        Visible = True
      end
      item
        Visible = True
      end
      item
        BeginGroup = True
        Visible = True
      end
      item
        BeginGroup = True
        Visible = True
      end>
    UseOwnFont = False
    Left = 275
    Top = 399
  end
  object bpmSchoolInformation: TdxBarPopupMenu
    BarManager = dxBarManager1
    ItemLinks = <
      item
        Visible = True
      end
      item
        BeginGroup = True
        Visible = True
      end
      item
        Visible = True
      end>
    UseOwnFont = False
    Left = 316
    Top = 399
  end
  object bpmRegisterStudents: TdxBarPopupMenu
    BarManager = dxBarManager1
    ItemLinks = <
      item
        Visible = True
      end
      item
        Visible = True
      end>
    UseOwnFont = False
    Left = 346
    Top = 401
  end
  object bpmUpdateStudent: TdxBarPopupMenu
    BarManager = dxBarManager1
    ItemLinks = <
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        BeginGroup = True
        Visible = True
      end
      item
        Visible = True
      end
      item
        Visible = True
      end>
    UseOwnFont = False
    Left = 385
    Top = 402
  end
  object bpmManageStudents: TdxBarPopupMenu
    BarManager = dxBarManager1
    ItemLinks = <
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        BeginGroup = True
        Visible = True
      end
      item
        Visible = True
      end
      item
        BeginGroup = True
        Visible = True
      end
      item
        BeginGroup = True
        Visible = True
      end
      item
        BeginGroup = True
        Visible = True
      end>
    UseOwnFont = False
    Left = 422
    Top = 405
  end
  object bpmAcademicsEnterMarks: TdxBarPopupMenu
    BarManager = dxBarManager1
    ItemLinks = <
      item
        Visible = True
      end
      item
        BeginGroup = True
        Visible = True
      end
      item
        BeginGroup = True
        Visible = True
      end>
    UseOwnFont = False
    Left = 227
    Top = 442
  end
  object bpmPrintAllStudents: TdxBarPopupMenu
    BarManager = dxBarManager1
    ItemLinks = <>
    UseOwnFont = False
    Left = 267
    Top = 439
  end
  object bpmAcademicsView: TdxBarPopupMenu
    BarManager = dxBarManager1
    ItemLinks = <
      item
        Visible = True
      end
      item
        BeginGroup = True
        Visible = True
      end>
    UseOwnFont = False
    Left = 237
    Top = 485
  end
  object bpmStaffTasks: TdxBarPopupMenu
    BarManager = dxBarManager1
    ItemLinks = <
      item
        Visible = True
      end
      item
        BeginGroup = True
        Visible = True
      end>
    UseOwnFont = False
    Left = 270
    Top = 488
  end
  object bpmAcademicsUpdateMarks: TdxBarPopupMenu
    BarManager = dxBarManager1
    ItemLinks = <
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        BeginGroup = True
        Visible = True
      end>
    UseOwnFont = False
    Left = 302
    Top = 485
  end
  object bpmAcademicsSubjectAllocation: TdxBarPopupMenu
    BarManager = dxBarManager1
    ItemLinks = <
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        BeginGroup = True
        Visible = True
      end>
    UseOwnFont = False
    Left = 339
    Top = 484
  end
  object bpmComputeResults: TdxBarPopupMenu
    BarManager = dxBarManager1
    ItemLinks = <
      item
        Visible = True
      end
      item
        Visible = True
      end>
    UseOwnFont = False
    Left = 380
    Top = 481
  end
  object bpmPrintStudentStandardForms: TdxBarPopupMenu
    BarManager = dxBarManager1
    ItemLinks = <>
    UseOwnFont = False
    Left = 417
    Top = 478
  end
  object bpmPrintSelectedStudent: TdxBarPopupMenu
    BarManager = dxBarManager1
    ItemLinks = <>
    UseOwnFont = False
    Left = 449
    Top = 477
  end
  object pmAcademicsDetailedMarks: TPopupMenu
    Left = 488
    Top = 476
    object EditStudentsMarks1: TMenuItem
      Caption = 'Edit Student'#39's Marks'
    end
    object ResultsComputationInclusion1: TMenuItem
      Caption = 'Results Computation Inclusion'
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object MarksEntry1: TMenuItem
      Caption = 'Marks Entry'
      object MarksEntryWizard1: TMenuItem
        Caption = 'Marks Entry Wizard'
      end
      object ImportMarks1: TMenuItem
        Caption = 'Import Marks'
      end
    end
    object AcademicTasks1: TMenuItem
      Caption = 'Academic Tasks'
      object ComputeResults1: TMenuItem
        Caption = 'Compute Results'
      end
      object CommitMarks1: TMenuItem
        Caption = 'Commit Marks'
      end
      object EnterComments1: TMenuItem
        Caption = 'Enter Comments'
      end
      object AssignSubjects1: TMenuItem
        Caption = 'Assign Subjects'
      end
      object DeAssignSubjects1: TMenuItem
        Caption = 'De-Assign Subjects'
      end
      object AuditMarksEntry1: TMenuItem
        Caption = 'Audit Marks Entry'
      end
    end
    object PrintOptions1: TMenuItem
      Caption = 'Print Options'
      object PrintCurrentView1: TMenuItem
        Caption = 'Print Current View'
      end
      object AcademicReports1: TMenuItem
        Caption = 'Academic Reports'
        object Intermediate1: TMenuItem
          Caption = 'Intermediate'
        end
        object ermly1: TMenuItem
          Caption = 'Termly'
        end
        object ranscripts1: TMenuItem
          Caption = 'Transcripts'
        end
      end
      object ResultSheets1: TMenuItem
        Caption = 'Result Sheets'
      end
    end
    object Export1: TMenuItem
      Caption = 'Export'
      object MSExcel1: TMenuItem
        Caption = 'MS Excel'
      end
      object HTML1: TMenuItem
        Caption = 'HTML'
      end
      object XML1: TMenuItem
        Caption = 'XML'
      end
      object PlainText1: TMenuItem
        Caption = 'Plain Text'
      end
    end
    object ViewOptions1: TMenuItem
      Caption = 'View Options'
      object ActivateFilters1: TMenuItem
        Caption = 'Activate Filters'
      end
      object ShowGroupingBar1: TMenuItem
        Caption = 'Show Grouping Bar'
      end
      object ExpandAll1: TMenuItem
        Caption = 'Expand All'
      end
      object CollapseAll1: TMenuItem
        Caption = 'Collapse All'
      end
      object N4: TMenuItem
        Caption = '-'
      end
      object SwitchView1: TMenuItem
        Caption = 'Switch View'
        object SummaryView1: TMenuItem
          Caption = 'Summary View'
        end
        object N5: TMenuItem
          Caption = '-'
        end
        object ArchivedResults1: TMenuItem
          Caption = 'Archived Results'
        end
        object UNEBResults1: TMenuItem
          Caption = 'UNEB Results'
        end
        object rendsStatistics1: TMenuItem
          Caption = 'Trends/Statistics'
        end
      end
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Refresh2: TMenuItem
      Caption = 'Refresh'
    end
  end
  object pmAcademicsUNEB: TPopupMenu
    Left = 534
    Top = 475
    object mniExport3: TMenuItem
      Caption = 'Export '
      object mniMSExcel3: TMenuItem
        Caption = 'MS Excel'
      end
      object mniHTML3: TMenuItem
        Caption = 'HTML'
      end
      object mniXML3: TMenuItem
        Caption = 'XML'
      end
      object mniPlainText2: TMenuItem
        Caption = 'Plain Text'
      end
    end
    object mniN16: TMenuItem
      Caption = '-'
    end
    object mniComputePositions1: TMenuItem
      Caption = 'Compute Positions'
    end
    object mniR: TMenuItem
      Caption = '-'
    end
    object mniRefresh6: TMenuItem
      Caption = 'Refresh'
    end
  end
  object pmResultSheets: TPopupMenu
    Left = 569
    Top = 473
    object mniViewOptions2: TMenuItem
      Caption = 'View Options'
      object mniColumnsAutoSize1: TMenuItem
        Caption = 'Columns Auto-Size'
        Checked = True
      end
      object mniActivateFilterBar1: TMenuItem
        Caption = 'Activate Filter Bar'
      end
      object mniActivateGroupingBar1: TMenuItem
        Caption = 'Activate Grouping Bar'
      end
      object mniN41: TMenuItem
        Caption = '-'
      end
      object mniExpandAll2: TMenuItem
        Caption = 'Expand All'
        Enabled = False
      end
      object mniCollapseAll2: TMenuItem
        Caption = 'Collapse All'
        Enabled = False
      end
      object mniN42: TMenuItem
        Caption = '-'
      end
      object mniSwitchView2: TMenuItem
        Caption = 'Switch View'
        Enabled = False
      end
    end
    object N39: TMenuItem
      Caption = '-'
    end
    object Export9: TMenuItem
      Caption = 'Export'
      object MSExcel8: TMenuItem
        Tag = 1
        Caption = 'MS Excel'
      end
      object HTML8: TMenuItem
        Tag = 2
        Caption = 'HTML'
      end
      object XML9: TMenuItem
        Tag = 3
        Caption = 'XML'
      end
      object PlainText8: TMenuItem
        Tag = 4
        Caption = 'Plain Text'
      end
    end
    object Print3: TMenuItem
      Caption = 'Print'
      Enabled = False
    end
    object N40: TMenuItem
      Caption = '-'
    end
    object Refresh12: TMenuItem
      Caption = 'Refresh'
    end
  end
  object pmMarksSummaries: TPopupMenu
    Left = 605
    Top = 472
    object ViewOptions2: TMenuItem
      Caption = 'View Options'
      object ColumnsAutoSize1: TMenuItem
        Caption = 'Columns Auto-Size'
        Checked = True
      end
      object ActivateFilterBar1: TMenuItem
        Caption = 'Activate Filter Bar'
      end
      object ActivateGroupingBar1: TMenuItem
        Caption = 'Activate Grouping Bar'
      end
      object N41: TMenuItem
        Caption = '-'
      end
      object ExpandAll2: TMenuItem
        Caption = 'Expand All'
      end
      object CollapseAll2: TMenuItem
        Caption = 'Collapse All'
      end
    end
    object N42: TMenuItem
      Caption = '-'
    end
    object Export10: TMenuItem
      Caption = 'Export'
      object MSExcel9: TMenuItem
        Tag = 1
        Caption = 'MS Excel'
      end
      object HTML9: TMenuItem
        Tag = 2
        Caption = 'HTML'
      end
      object XML10: TMenuItem
        Tag = 3
        Caption = 'XML'
      end
      object PlainText9: TMenuItem
        Tag = 4
        Caption = 'Plain Text'
      end
    end
    object Print5: TMenuItem
      Caption = 'Print'
      Enabled = False
    end
    object N43: TMenuItem
      Caption = '-'
    end
    object Refresh13: TMenuItem
      Caption = 'Refresh'
    end
  end
  object pmSpecialGroups: TPopupMenu
    Left = 669
    Top = 473
    object ViewMembers1: TMenuItem
      Caption = 'View Members'
    end
    object ViewGroupDetails1: TMenuItem
      Caption = 'View Group Details'
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Refresh1: TMenuItem
      Caption = 'Refresh'
    end
  end
  object pmSchoolInformation: TPopupMenu
    Left = 704
    Top = 472
    object Print1: TMenuItem
      Caption = 'Print'
    end
    object Export2: TMenuItem
      Caption = 'Export'
      object MSExcel2: TMenuItem
        Caption = 'MS Excel'
      end
      object HTML2: TMenuItem
        Caption = 'HTML'
      end
      object XML2: TMenuItem
        Caption = 'XML'
      end
    end
    object N19: TMenuItem
      Caption = '-'
    end
    object Refresh5: TMenuItem
      Caption = 'Refresh'
    end
  end
  object bpmAcademicsAdditionalTasks: TdxBarPopupMenu
    BarManager = dxBarManager1
    ItemLinks = <
      item
        Visible = True
      end
      item
        Visible = True
      end>
    UseOwnFont = False
    Left = 750
    Top = 471
  end
  object bpmFinanceView: TdxBarPopupMenu
    BarManager = dxBarManager1
    ItemLinks = <
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        BeginGroup = True
        Visible = True
      end
      item
        BeginGroup = True
        Visible = True
      end>
    UseOwnFont = False
    Left = 241
    Top = 535
  end
  object bpmFinanceSetDuesPayable: TdxBarPopupMenu
    BarManager = dxBarManager1
    ItemLinks = <
      item
        Visible = True
      end
      item
        Visible = True
      end>
    UseOwnFont = False
    Left = 275
    Top = 534
  end
  object bpmLibraryView: TdxBarPopupMenu
    BarManager = dxBarManager1
    ItemLinks = <
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        Visible = True
      end>
    UseOwnFont = False
    Left = 329
    Top = 536
  end
  object bpmLibraryAddItems: TdxBarPopupMenu
    BarManager = dxBarManager1
    ItemLinks = <
      item
        Visible = True
      end
      item
        Visible = True
      end>
    UseOwnFont = False
    Left = 362
    Top = 536
  end
  object bpmUpdateLibraryItem: TdxBarPopupMenu
    BarManager = dxBarManager1
    ItemLinks = <
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        BeginGroup = True
        Visible = True
      end
      item
        BeginGroup = True
        Visible = True
      end
      item
        Visible = True
      end>
    UseOwnFont = False
    Left = 396
    Top = 536
  end
  object bpmLibraryLend: TdxBarPopupMenu
    BarManager = dxBarManager1
    ItemLinks = <
      item
        Visible = True
      end
      item
        Visible = True
      end>
    UseOwnFont = False
    Left = 430
    Top = 536
  end
  object bpmLibraryReceive: TdxBarPopupMenu
    BarManager = dxBarManager1
    ItemLinks = <
      item
        Visible = True
      end
      item
        Visible = True
      end>
    UseOwnFont = False
    Left = 463
    Top = 536
  end
  object bpmLibraryReserve: TdxBarPopupMenu
    BarManager = dxBarManager1
    ItemLinks = <
      item
        Visible = True
      end
      item
        Visible = True
      end>
    UseOwnFont = False
    Left = 497
    Top = 536
  end
  object bpmPrintLibraryDefaulters: TdxBarPopupMenu
    BarManager = dxBarManager1
    ItemLinks = <>
    UseOwnFont = False
    Left = 530
    Top = 535
  end
  object pmApplicationsFull: TPopupMenu
    Left = 615
    Top = 523
    object mniAdmitApplicant1: TMenuItem
      Caption = 'Admit Applicant'
    end
    object RejectApplication1: TMenuItem
      Caption = 'Reject Application'
    end
    object mniRevokeAdmission1: TMenuItem
      Caption = 'Revoke Admission'
    end
    object EditApplication1: TMenuItem
      Caption = 'Edit Application'
    end
    object N20: TMenuItem
      Caption = '-'
    end
    object PostInterviewResults1: TMenuItem
      Caption = 'Post Interview Results'
    end
    object N21: TMenuItem
      Caption = '-'
    end
    object mniRegisterApplicantAsStudent1: TMenuItem
      Caption = 'Register Applicant As Student'
    end
    object N35: TMenuItem
      Caption = '-'
    end
    object CommunicateNotify1: TMenuItem
      Caption = 'Communicate/Notify'
    end
    object mniN2: TMenuItem
      Caption = '-'
    end
    object mniExportApplicationData1: TMenuItem
      Caption = 'Export Application Data'
      object mniMSExcelSpreadsheet1: TMenuItem
        Caption = 'MS Excel Spreadsheet'
      end
      object mniHTMLDocument1: TMenuItem
        Caption = 'HTML Document'
      end
    end
    object mniN5: TMenuItem
      Caption = '-'
    end
    object mniRefresh1: TMenuItem
      Caption = 'Refresh'
    end
  end
  object pmStudentsList: TPopupMenu
    Left = 654
    Top = 522
    object MultipleStudentOptions1: TMenuItem
      Caption = 'Multiple Student Options'
      Visible = False
      object ChangeClass2: TMenuItem
        Caption = 'Change Class'
      end
    end
    object ViewStudentProfile1: TMenuItem
      Caption = 'View Complete Student Profile'
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object UpdateStudentDetails1: TMenuItem
      Caption = 'Update Student Details'
      object EditGeneralInformation1: TMenuItem
        Caption = 'Edit General Information'
      end
      object mniDescribeDisabilities1: TMenuItem
        Caption = 'Describe Disabilities'
        Visible = False
      end
      object ChangeClass1: TMenuItem
        Caption = 'Change Class'
      end
      object agAsDueToRepeat1: TMenuItem
        Caption = 'Tag As Due To Repeat'
      end
      object MarkAsLeftSchool1: TMenuItem
        Caption = 'Mark As Left School'
      end
      object mniDiscoverUpdateSiblings1: TMenuItem
        Caption = 'Discover/Update Siblings'
      end
      object mniN39: TMenuItem
        Caption = '-'
      end
      object mniAssignCombination1: TMenuItem
        Caption = 'Assign Combination'
        object TMenuItem
        end
      end
    end
    object TrackRecords1: TMenuItem
      Caption = 'Track Records'
      object mniNewDisciplinaryIncident1: TMenuItem
        Caption = 'New Disciplinary Incident'
      end
      object mniNewHealthIncident1: TMenuItem
        Caption = 'New Health Incident'
      end
      object mniNewCoCurricularActivity1: TMenuItem
        Caption = 'New Co-Curricular Activity'
      end
    end
    object ManageStudent1: TMenuItem
      Caption = 'Manage Student'
      object N8: TMenuItem
        Caption = 'Add To Student Group'
      end
      object OutofSchoolPermission1: TMenuItem
        Caption = 'Out-of-School Permission'
      end
      object MarkAsAbsent2: TMenuItem
        Caption = 'Mark As Absent'
      end
      object N9: TMenuItem
        Caption = '-'
      end
      object AssignRoleResponsibility1: TMenuItem
        Caption = 'Assign Role/Responsibility'
      end
      object ViewAssignedRoles1: TMenuItem
        Caption = 'View Assigned Roles'
      end
      object N10: TMenuItem
        Caption = '-'
      end
      object ermlyReportingRegistration1: TMenuItem
        Caption = 'Termly Reporting Registration'
      end
      object BoTClearance1: TMenuItem
        Caption = 'BoT Clearance'
      end
      object EoTClearance1: TMenuItem
        Caption = 'EoT Clearance'
      end
      object ClearanceStatus1: TMenuItem
        Caption = 'Clearance Status'
      end
    end
    object StudentNotes1: TMenuItem
      Caption = 'Student Notes'
      object PostNote1: TMenuItem
        Caption = 'Post Note'
      end
      object mniViewNotes1: TMenuItem
        Caption = 'View Notes'
      end
    end
    object N11: TMenuItem
      Caption = '-'
    end
    object CommunicateWithParents1: TMenuItem
      Caption = 'Communicate With Parent(s)'
      object GeneralCommunication2: TMenuItem
        Caption = 'General Communication'
      end
      object SendPSMOAccountDetails3: TMenuItem
        Caption = 'Send PSMO Account Details'
      end
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object StudentRollCalls1: TMenuItem
      Caption = 'Student Roll-Calls'
      object UploadNewRollcallData1: TMenuItem
        Caption = 'Upload New Roll-Call Data'
      end
      object ViewRollCallRecords1: TMenuItem
        Caption = 'View Roll-Call Records'
      end
    end
    object N13: TMenuItem
      Caption = '-'
    end
    object RegisterNewStudent1: TMenuItem
      Caption = 'Register New Student'
    end
    object N14: TMenuItem
      Caption = '-'
    end
    object Refresh3: TMenuItem
      Caption = 'Refresh'
    end
  end
  object bpmLibraryAdditionalTasks: TdxBarPopupMenu
    BarManager = dxBarManager1
    ItemLinks = <
      item
        Visible = True
      end
      item
        Visible = True
      end>
    UseOwnFont = False
    Left = 757
    Top = 509
  end
  object bpmPrintFinanceS: TdxBarPopupMenu
    BarManager = dxBarManager1
    ItemLinks = <>
    UseOwnFont = False
    Left = 250
    Top = 565
  end
  object bpmPrintFinanceM: TdxBarPopupMenu
    BarManager = dxBarManager1
    ItemLinks = <>
    UseOwnFont = False
    Left = 280
    Top = 564
  end
  object pmBookCategoryTree: TPopupMenu
    Left = 393
    Top = 577
  end
  object pmLibraryBooks: TPopupMenu
    Left = 431
    Top = 577
    object mniLendItemOut1: TMenuItem
      Caption = 'Lend Item Out'
    end
    object mniReceiveItemBack1: TMenuItem
      Caption = 'Receive Item Back'
    end
    object mniN161: TMenuItem
      Caption = '-'
    end
    object mniRegisterNewLibraryItem1: TMenuItem
      Caption = 'Register New Library Item'
    end
    object mniRegisterAdditionalItemCopies1: TMenuItem
      Caption = 'Register Additional Item Copies'
    end
    object mniN17: TMenuItem
      Caption = '-'
    end
    object mniEditItemDetails1: TMenuItem
      Caption = 'Edit Item Details'
    end
    object mniUpdateImportedItem1: TMenuItem
      Caption = 'Update Imported Item'
    end
    object mniDeleteItem1: TMenuItem
      Caption = 'Delete Item'
    end
    object mniN18: TMenuItem
      Caption = '-'
    end
    object History2: TMenuItem
      Caption = 'History'
    end
    object N16: TMenuItem
      Caption = '-'
    end
    object mni1: TMenuItem
      Caption = 'Refresh'
    end
  end
  object pmLibraryBookCopies: TPopupMenu
    Left = 467
    Top = 577
    object mniLendItemCopyOut1: TMenuItem
      Caption = 'Lend Item Copy Out'
    end
    object mniReceiveItemCopyBack1: TMenuItem
      Caption = 'Receive Item Copy Back'
    end
    object mniN19: TMenuItem
      Caption = '-'
    end
    object mniEditItemCopyDetails1: TMenuItem
      Caption = 'Edit Item Copy Details'
    end
    object mniRemoveItemCopy1: TMenuItem
      Caption = 'Remove Item Copy'
    end
    object mniN20: TMenuItem
      Caption = '-'
    end
    object History1: TMenuItem
      Caption = 'History'
    end
    object N15: TMenuItem
      Caption = '-'
    end
    object mniRefresh51: TMenuItem
      Caption = 'Refresh'
    end
  end
  object pmLibraryBorrowers: TPopupMenu
    Left = 500
    Top = 577
    object mniLendItemtoBorrower1: TMenuItem
      Caption = 'Lend Item to Borrower'
    end
    object mniReceiveItemfromBorrower1: TMenuItem
      Caption = 'Receive Item from Borrower'
    end
    object mniN21: TMenuItem
      Caption = '-'
    end
    object mniPrintDefaulterLetter1: TMenuItem
      Caption = 'Print Defaulter Letter'
      Enabled = False
    end
    object mniPrintBorrowerHistory1: TMenuItem
      Caption = 'Print Borrower History'
    end
    object mniN22: TMenuItem
      Caption = '-'
    end
    object mniRefresh61: TMenuItem
      Caption = 'Refresh'
    end
  end
  object pmLibraryBorrowerHistory: TPopupMenu
    Left = 536
    Top = 577
    object mniReceiveItemBack2: TMenuItem
      Caption = 'Receive Item Back'
    end
    object mniExtendBorrowingPeriod1: TMenuItem
      Caption = 'Extend Borrowing Period'
    end
    object mniMarkasNotReturnableLostStolen1: TMenuItem
      Caption = 'Mark as Not Returnable (Lost/Stolen)'
    end
    object mniN23: TMenuItem
      Caption = '-'
    end
    object mniRefresh7: TMenuItem
      Caption = 'Refresh'
    end
  end
  object pmParents1: TPopupMenu
    Left = 639
    Top = 572
    object SubscribetoPSMO1: TMenuItem
      Caption = 'Subscribe to myChild'
    end
    object mniRegister1: TMenuItem
      Caption = 'Register'
      object mniFather1: TMenuItem
        Tag = 1
        Caption = 'Father'
      end
      object mniMother1: TMenuItem
        Tag = 2
        Caption = 'Mother'
      end
      object mniGuardian1: TMenuItem
        Tag = 3
        Caption = 'Guardian'
      end
    end
    object EditDetails1: TMenuItem
      Caption = 'Edit Details'
    end
    object N18: TMenuItem
      Caption = '-'
    end
    object Communicate3: TMenuItem
      Caption = 'Communicate'
      object General1: TMenuItem
        Caption = 'General Communication'
      end
      object SendPSMOAccountDetails1: TMenuItem
        Caption = 'Send myChild Account Details'
      end
      object mniN40: TMenuItem
        Caption = '-'
      end
      object mniViewmyChildAccount1: TMenuItem
        Caption = 'View myChild Account'
      end
    end
    object N12: TMenuItem
      Caption = '-'
    end
    object Refresh4: TMenuItem
      Caption = 'Refresh'
    end
  end
  object pmParents2: TPopupMenu
    Left = 682
    Top = 572
    object mniSubscribetoPSMO2: TMenuItem
      Caption = 'Subscribe to PSMO'
    end
    object mniEditDetails2: TMenuItem
      Caption = 'Edit Details'
    end
    object N17: TMenuItem
      Caption = '-'
    end
    object Communicate1: TMenuItem
      Caption = 'Communicate'
      object GeneralCommunication1: TMenuItem
        Caption = 'General Communication'
      end
      object SendPSMOAccountDetails2: TMenuItem
        Caption = 'Send PSMO Account Details'
      end
    end
    object MenuItem3: TMenuItem
      Caption = '-'
    end
    object mniExport2: TMenuItem
      Caption = 'Export'
      object mniMSExcel2: TMenuItem
        Caption = 'MS Excel'
      end
      object mniHTML2: TMenuItem
        Caption = 'HTML'
      end
      object mniXML2: TMenuItem
        Caption = 'XML'
      end
    end
    object mniN15: TMenuItem
      Caption = '-'
    end
    object mniRefresh5: TMenuItem
      Caption = 'Refresh'
    end
  end
  object pmStaffList: TPopupMenu
    Left = 720
    Top = 572
    object UpdateStaffMember1: TMenuItem
      Caption = 'Update Staff Member'
      object GeneralInformation1: TMenuItem
        Caption = 'General Information'
      end
      object N45: TMenuItem
        Caption = '-'
      end
      object ProfessionalInformation1: TMenuItem
        Caption = 'Professional Information'
      end
      object DutiesResponsibilities1: TMenuItem
        Caption = 'Duties && Responsibilities'
      end
      object ClassesSubjectsTaught1: TMenuItem
        Caption = 'Classes/Subjects Taught'
      end
      object N46: TMenuItem
        Caption = '-'
      end
      object MarkAsAbsent1: TMenuItem
        Caption = 'Mark As Absent'
      end
      object N47: TMenuItem
        Caption = '-'
      end
      object MarkAsLeft1: TMenuItem
        Caption = 'Mark As Left'
      end
    end
    object N48: TMenuItem
      Caption = '-'
    end
    object Communicate2: TMenuItem
      Caption = 'Communicate'
      object SendEMail2: TMenuItem
        Caption = 'Send E-Mail'
      end
      object SendSMS2: TMenuItem
        Caption = 'Send SMS'
      end
      object N49: TMenuItem
        Caption = '-'
      end
      object SendBothEMailSMS2: TMenuItem
        Caption = 'Send Both E-Mail && SMS'
      end
    end
    object N50: TMenuItem
      Caption = '-'
    end
    object ExportStaffListMSExcel1: TMenuItem
      Caption = 'Export Staff List [MS Excel]'
    end
    object Print4: TMenuItem
      Caption = 'Print'
    end
    object N53: TMenuItem
      Caption = '-'
    end
    object UploadNewDocument1: TMenuItem
      Caption = 'Upload New Document'
    end
    object N51: TMenuItem
      Caption = '-'
    end
    object ViewOptions4: TMenuItem
      Caption = 'View Options'
    end
    object N52: TMenuItem
      Caption = '-'
    end
    object Refresh11: TMenuItem
      Caption = 'Refresh'
    end
  end
  object pmAnnouncements: TPopupMenu
    Left = 628
    Top = 350
    object NewAnnouncement1: TMenuItem
      Caption = 'New Notice/Announcement'
    end
    object EditNoticeAnnoucement1: TMenuItem
      Caption = 'Edit Notice/Announcement'
    end
    object N34: TMenuItem
      Caption = '-'
    end
    object ShowCurrentAnnouncements1: TMenuItem
      Caption = 'Show Current Notices/Announcements'
    end
    object ShowActiveAnnouncements1: TMenuItem
      Caption = 'Show Active Notices/Announcements'
    end
    object PrintSelectedNotice1: TMenuItem
      Caption = 'Print Selected Notice'
    end
    object N33: TMenuItem
      Caption = '-'
    end
    object SendReminders1: TMenuItem
      Caption = 'Send Reminders'
    end
    object N22: TMenuItem
      Caption = '-'
    end
    object Export3: TMenuItem
      Caption = 'Export'
      object MSExcel3: TMenuItem
        Caption = 'MS Excel'
      end
      object HTML3: TMenuItem
        Caption = 'HTML'
      end
      object XML3: TMenuItem
        Caption = 'XML'
      end
      object PlainText2: TMenuItem
        Caption = 'Plain Text'
      end
    end
  end
  object pgccStudentStatistics: TcxPivotGridChartConnection
    Left = 733
    Top = 288
  end
  object pgccStaffStatistics: TcxPivotGridChartConnection
    Left = 766
    Top = 289
  end
  object pgccAnalysisClassPivot: TcxPivotGridChartConnection
    Left = 801
    Top = 290
  end
  object bpmMenuExport: TdxBarPopupMenu
    BarManager = dxBarManager1
    ItemLinks = <
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        Visible = True
      end
      item
        Visible = True
      end>
    UseOwnFont = False
    Left = 642
    Top = 257
  end
end
